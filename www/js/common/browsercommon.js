/******************************************************************************************/
/************************** BROWSERCOMMON.JS VERSION 2.0 14/02/2013 ******************************/
/******************************************************************************************/
/******************************************************************************************/
/********************************* General  - Start ***********************************/
/******************************************************************************************/
_TOAST_LONG = 1;
_TOAST_SHORT = 2;
navigator = {};
navigator.connection = {};
navigator.connection.type = "";
navigator.splashscreen = {};
navigator.splashscreen.show = function () {
navigator.splashscreen.show = function () {
};
Connection = {};
Connection.NONE = "____";
var _HRTEMP_IDX = 1;
var _HRTemp = [98, 104, 97, 97, 98, 97, 106, 106, 115, 116, 116, 116, 115, 115, 114, 115, 115, 115, 115, 116, 116, 115, 115, 115, 115, 116, 117, 117, 118, 124, 123, 122, 122, 121, 121, 121, 122, 122, 122, 121, 121, 121, 121, 121, 121, 122, 121, 121, 121, 122, 123, 124, 129, 129, 129, 128, 128, 128, 127, 128, 127, 127, 127, 127, 127, 126, 126, 125, 124, 125, 125, 126, 127, 126, 127, 129, 133, 133, 132, 131, 131, 131, 130, 130, 131, 132, 133, 133, 134, 134, 133, 133, 134, 134, 133, 133, 133, 134, 134, 135, 136, 137, 138, 138, 137, 137, 138, 139, 139, 140, 140, 140, 140, 139, 138, 137, 137, 136, 137, 138, 139, 140, 141, 141, 142, 141, 142, 141, 141, 141, 141, 141, 141, 142, 143, 144, 144, 144, 145, 145, 144, 143, 143, 142, 143, 143, 142, 143, 144, 144, 145, 146, 145, 145, 146, 146, 147, 147, 148, 149, 150, 151, 151, 151, 151, 150, 148, 148, 149, 149, 149, 149, 149, 151, 151, 153, 154, 154, 154, 154, 155, 156, 157, 157, 158, 158, 159, 159, 159, 159, 160, 160, 160, 160, 159, 158, 158, 158, 159, 159, 159, 159, 158, 156, 155, 151, 150, 149, 149, 147, 146, 144, 142, 140, 139, 138, 137, 136, 135, 134, 133, 132, 130];
/*
 Random HeartRate ARRAY
 */
var HEARTRATE = [{
    service: '180d',
    measurement: '60'/*Temp:'2a37'*/
}, {
    service: '180d',
    measurement: '62'/*Temp:'2a37'*/
}, {
    service: '180d',
    measurement: '65'
}, {
    service: '180d',
    measurement: '68'
}, {
    service: '180d',
    measurement: '74'
}, {
    service: '180d',
    measurement: '76'
}, {
    service: '180d',
    measurement: '58'
}];

var GLOBAL_REPLCATE_BLE_NOTIFY_TIMIER = null;
var GLOBAL_REPLCATE_MATRIX_NOTIFY_TIMIER = null;
/*
 * 
 */
window.onload = function () {
    onDeviceReady();
};
/*
 $(document).ready(function(){
 onDeviceReady();        
 });*/
/**/
function onPlatformReady() {
    _DEVICE_ID = "";
}

function HideSplashScreen() {

}

function ExitApplication() {

}
/******************************************************************************************/
/********************************* General  - Start ***********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* File Utils  - Start ***********************************/
/******************************************************************************************/
function CreateApplicationDirectories() {
    logStatus("Excuting CreateApplicationDirectories", LOG_COMMON_FUNCTIONS);
    try {
    } catch (e) {
        ShowExceptionMessage("CreateApplicationDirectories", e);
    }
}

/******************************************************************************************/
/********************************* File Utils  - End ***********************************/
/******************************************************************************************/
/******************************************************************************************/
/********************************* Notification Objects  - Start *********************************/
/******************************************************************************************/
function NotificationAlert(message) {
    alert(message);
}
function ShowSplashScreen() {

}
function NotificationActivityStart(title, msg) {
    logStatus("Calling NotificationActivityStart: Title: " + title + " Message :" + msg, LOG_COMMON_FUNCTIONS);
    var HTML = '<div class="loader-mask" style="position:absolute;top:0;left:0;right:0;bottom:0; background-color:#898989;opacity:0.5;z-index:888"></div>' +
        '<div class="loader-message" style="position:absolute; top:200px;left:20%;right:20%;width:auto;padding:10px;border:1px solid #CFCFCF;background-color:#898989;z-index:9999;color:#fff">' +
        '<h3>' + title + '</h3><br/><hr/><br/>' +
        '<div style="padding:3px;float:left"><img src="images/ajax-loader.gif"/></div>' +
        '<div>' +
        '<p>' + msg + '</p>' +
        '</div>' +
        '</div>';
    $('body').append(HTML);
}
function NotificationActivityStop() {
    logStatus("Progress Stop", LOG_COMMON_FUNCTIONS);
    $(".loader-mask,.loader-message").remove();
}

function NotificationConfirmMessage(strMsg, callBackFunction, buttons) {
    try {
        logStatus(buttons, LOG_DEBUG);
        var _confirm = confirm(strMsg);
        if (callBackFunction) {
            var status = 2;
            if (_confirm) {
                status = 1;
            }
            callBackFunction(status);
        }
    } catch (e) {
        ShowExceptionMessage("NotificationConfirmMessage", e);
    }
}
/******************************************************************************************/
/********************************* Notification Objects  - End *********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Other Native Stuff  - Start ****************************/
/******************************************************************************************/
function ShowToastMessage(msg, toastlen) {
    logStatus("Calling ShowToastMessage", LOG_COMMON_FUNCTIONS);
    logStatus(toastlen, LOG_DEBUG);
    try {
        var $toastbox = $('.toastbox');
        $toastbox.html('');
        var HTML = '<p>' + msg + '</p>';
        $toastbox.append(HTML);
        $toastbox.show().fadeOut(6000);
        $(".tstouter").show().fadeOut(6000);
    } catch (e) {
        ShowExceptionMessage("ShowToastMessage", e);
    }
}
/*function ShowToastMessage(msg, toastlen){
 try {
 logStatus("Calling ShowToastMessage", LOG_COMMON_FUNCTIONS); 
 var HTML	=	'<div class="popupinner_panel clsmessagebox">'+
 '<div class="popupinner_panel_Cnt">'+
 '<p>'+msg+'</p>'+
 '</div>'+
 '</div>';
 $('.mainbox').append(HTML);
 setTimeout(function(){ 
 $(".popupinner_panel").fadeOut(1000);
 }, 2000);
 } catch (e) {
 ShowExceptionMessage("ShowToastMessage", e);
 }
 }*/

function ShowKeyBoard() {
    try {
        logStatus("Calling ShowKeyBoard", LOG_COMMON_FUNCTIONS);
    } catch (e) {
        ShowExceptionMessage("ShowKeyBoard", e);
    }
}

function HideKeyBoard() {
    try {
        logStatus("Calling HideKeyBoard", LOG_COMMON_FUNCTIONS);
    } catch (e) {
        ShowExceptionMessage("HideKeyBoard", e);
    }
}

/******************************************************************************************/
/********************************* Other Native Stuff  - End ******************************/
/******************************************************************************************/
/******************************************************************************************/
/********************************* Barcode Scanning  - Start *********************************/
/******************************************************************************************/
function StartBarcodeScan(SuccessHandler, FailureHandler) {
    var result = {};
    result["text"] = '432154';
    result["format"] = 'test';
    result["cancelled"] = false;
    if (SuccessHandler) {
        SuccessHandler(result);
    }
    if (FailureHandler) {
        FailureHandler(result);
    }
}
/******************************************************************************************/
/********************************* Barcode Scanning  - End *********************************/
/******************************************************************************************/
/******************************************************************************************/
/********************************* Geo Location  - Start *********************************/
/******************************************************************************************/
function getCurrentGeoPosition(SuccessHandler, FailureHandler, options) {
    var position = {};
    var coords = {};
    logStatus(options, LOG_DEBUG);
    coords["latitude"] = '1.122';
    coords["longitude"] = '2.122';
    coords["altitude"] = '3.122';
    coords["accuracy"] = '4';
    coords["altitudeAccuracy"] = '5';
    coords["heading"] = '6';
    coords["speed"] = '7';
    position["coords"] = coords;
    position["timestamp"] = new Date().getTime();
    if (SuccessHandler) {
        SuccessHandler(position);
    }
    if (FailureHandler) {
        FailureHandler(position);
    }
}

/**
 * @return {number}
 */
function WatchGeoPosition(SuccessHandler, FailureHandler, options) {
    var position = {};
    var coords = {};
    logStatus(options, LOG_DEBUG);
    coords["latitude"] = '1.122';
    coords["longitude"] = '2.122';
    coords["altitude"] = '3.122';
    coords["accuracy"] = '4';
    coords["altitudeAccuracy"] = '5';
    coords["heading"] = '6';
    coords["speed"] = '7';
    position["coords"] = coords;
    position["timestamp"] = '20140425121212';
    return setInterval(function () {
        if (SuccessHandler) {
            SuccessHandler(position);
        }
        if (FailureHandler) {
            FailureHandler(position);
        }
    }, 3000);
}
function getCurrentCapturedImage(SuccessHandler, FailureHandler, options) {
    logStatus(options, LOG_DEBUG);
	console.log(options);
    var imageData = "";
    var message = "";
    if (!SuccessHandler) {
        SuccessHandler(imageData);
    }
    if (!FailureHandler) {
        FailureHandler(message);
    }

}
function ClearWatchGeoPosition(watchgeoid) {
    clearInterval(watchgeoid);
}
function AddDownloadFile() {

}
function StartDownload() {

}
/*
 function getFullpath(type,file){
 logStatus(type, LOG_DEBUG);
 return BASE_URL + "upload/equip/"+file;
 }
 */
function UploadJpgImageToServer(fileURI, hosturl, successfunction, failfunction, getdata, postdata) {
    try {
		alert(fileURI);
		console.log("browser === ");
        logStatus(fileURI, LOG_DEBUG);
        var formData = new FormData();
        if (postdata) {
            $.each(postdata, function (k, v) {
                formData.append(k, v);
            });
        }
        do_service_upload(getdata, formData, function (response) {
            if (successfunction) {
                successfunction(response);
            }
            if (failfunction) {
                failfunction(response);
            }
        }, 'json', hosturl);
    } catch (e) {
        ShowExceptionMessage("UploadJpgImageToServer", e);
    }
}
/******************************************************************************************/
/********************************* Geo Location  - End *********************************/
/******************************************************************************************/
/*MK Added for Browser uploaded image Preview*/
function getSelectedFilePath(elem, onReadFileSuccess) {
    try {
        logStatus("Calling getSelectedFilePath", LOG_COMMON_FUNCTIONS);
        if (elem.files && elem.files[0]) {
            var flreader = new FileReader();
            if (onReadFileSuccess) {
                flreader.onload = onReadFileSuccess;
            }
            flreader.readAsDataURL(elem.files[0]);
        }
    } catch (e) {
        ShowExceptionMessage("getSelectedFilePath", e);
    }

}

/******************************************************************************************/
/********************************* HR BLE Bluetooth  - Start *********************************/
/******************************************************************************************/
var heartRate = {
    service: '180d',
    measurement: '2a37'
};

function ScanHR_BLEDevice() {
    logStatus("Excuting ScanHR_BLEDevice", LOG_COMMON_FUNCTIONS);
    ON_SCAN_BLEDevice();
}

function isHR_Connected(deviceid, successhandler, failurehandler) {
    logStatus("Excuting isHR_Connected", LOG_COMMON_FUNCTIONS);
    logStatus(deviceid, LOG_DEBUG);
    if (successhandler) {
        successhandler();
    }
    if (failurehandler) {
        failurehandler();
    }
}

function ON_SCAN_BLEDevice(peripheral) {
    logStatus("Excuting ON_SCAN_BLEDevice", LOG_COMMON_FUNCTIONS);
    //This will have the list of Devices found

    // this is demo code, assume there is only one heart rate monitor
    peripheral = [{"name": "Heart Rate Sensor", "id": "TEST:123:32:32", "advertising": null, "rssi": -67}];
    if (OnHearRateDeviceScanSuccess) {
        OnHearRateDeviceScanSuccess(peripheral);
    }
    //XX BLEDeviceConnect(peripheral);
}

function BLEDeviceConnect(peripheralid) {
    logStatus("Excuting BLEDeviceConnect", LOG_COMMON_FUNCTIONS);
    logStatus(peripheralid, LOG_DEBUG);
    var peripheral = {"name": "Heart Rate Sensor", "id": "TEST:123:32:32", "advertising": null, "rssi": -67};
    BLEDeviceOnConnect(peripheral);
}

function BLEDeviceDisConnect(peripheralid) {
    logStatus("Excuting BLEDeviceConnect", LOG_COMMON_FUNCTIONS);
    logStatus(peripheralid, LOG_DEBUG);
    BLEDeviceOnDisConnect();
}


function BLEDeviceOnConnect(peripheral) {
    logStatus("Excuting BLEDeviceOnConnect", LOG_COMMON_FUNCTIONS);
    if (OnHeartRateConnectedCallBack) {
        OnHeartRateConnectedCallBack(peripheral);
    }
}

function BLEDeviceOnDisConnect(reason) {
    logStatus("Excuting BLEDeviceOnDisConnect", LOG_COMMON_FUNCTIONS);
    if (OnHeartRateDisconnectCallBack) {
        OnHeartRateDisconnectCallBack(reason);
    }
}

function StartBLENotificationProcess(peripheralid, successhandler, errorhandler) {
    logStatus("Excuting StartBLENotificationProcess", LOG_COMMON_FUNCTIONS);
    if (!successhandler) {
        successhandler = BLEDeviceOnData;
    }
    if (!errorhandler) {
        errorhandler = BLEDeviceOnError;
    }
    StopBLENotificationProcess(peripheralid);
    var timerinc = 0;
    GLOBAL_REPLCATE_BLE_NOTIFY_TIMIER = setInterval(function () {
        /*
         var min = 50;
         var max = 70;
         var hrdata = Math.floor(Math.random()*(max-min+1)+min);
         */
        if (_HRTemp[0]) {
            var hrdata = _HRTemp[0];
            //_HRTemp	=	
            successhandler([0, hrdata]);
            if (_HRTEMP_IDX >= 5) {
                _HRTEMP_IDX = 0;
                _HRTemp.splice(0, 1);
            }

            if (CallBackFromCardioMachineMatrixdata) {
                CallBackFromCardioMachineMatrixdata({
                    elapsedtime: timerinc,
                    calories: '%d',
                    caloriesPerHour: '%d',
                    mets: '%.1f',
                    watts: 10,
                    averageWatts: '%d',
                    distanceInmiles: '%.2f',
                    bpm: '%d',
                    peakBpm: '%d',
                    secondsInHeartRateZone: '%d',
                    speedInMil: '%.1f',
                    avgspeedInmiles: '%.1f',
                    paceInSec: '%d:%02d',
                    avgPaceInSec: '%d:%02d',
                    currentStridesPerMinute: '%d',
                    averageStridesPerMinute: 160,
                    currentTargetSpm: 160,
                    currentCrankRPM: 80,
                    averageCrankRPM: 80
                });

            }

            _HRTEMP_IDX++;
            timerinc++;
        }
    }, 1000);
}

function StopBLENotificationProcess(peripheralid, successhandler, errorhandler) {
    logStatus("Excuting StopBLENotificationProcess", LOG_COMMON_FUNCTIONS);
    logStatus(peripheralid, LOG_DEBUG);
    if (GLOBAL_REPLCATE_BLE_NOTIFY_TIMIER) {
        clearInterval(GLOBAL_REPLCATE_BLE_NOTIFY_TIMIER);
    }
    if (successhandler) {
        successhandler();
    }
    if (errorhandler) {
        errorhandler();
    }
}

/*
 function BLEDeviceOnConnect(peripheral) {
 setTimeout(function(){
 BLEDeviceOnData([0, 50])
 }, 5000);
 }
 */
/*
 function BLEDeviceOnDisConnect(reason) {
 logStatus("Excuting BLEDeviceOnDisConnect", LOG_COMMON_FUNCTIONS);
 logStatus(reason, LOG_DEBUG);
 StopBLENotificationProcess();
 }   
 */
function BLEDeviceOnData(buffer) {
    logStatus("Excuting BLEDeviceOnData", LOG_COMMON_FUNCTIONS);
    // assuming heart rate measurement is Uint8 format, real code should check the flags
    // See the characteristic specs http://goo.gl/N7S5ZS
    var data = new Uint8Array(buffer);
    var curdatetime = new Date();
    if (_CurrTime == 0) {
        _CurrTime = curdatetime;
    }
    var hrtime = Math.round((curdatetime - _CurrTime) / 1000);
    if (THRChart.ChartHRData_FULL[THRChart.ChartHRData_FULL.length - 1] != hrtime) {
        var graphdata = {
            x: hrtime / 60,
            y: data[1]
        };
        if (hrtime <= _WARM_UP_SECOND) {
            THRChart.ChartHRData_WARMUP.push(graphdata);
        }
        else if (hrtime > 80 /*XX For Testing Cool Down */) {
            //Just to get the continuation in the graph add the last same to  _TRAIN as well
            if (THRChart.ChartHRData_COOLDWN.length == 0) {
                THRChart.ChartHRData_COOLDWN.push(THRChart.ChartHRData_TEST[THRChart.ChartHRData_TEST.length - 1]);
            }
            THRChart.ChartHRData_COOLDWN.push(graphdata)
        } else {
            //Just to get the continuation in the graph add the last same to  _TRAIN as well
            if (THRChart.ChartHRData_TEST.length == 0) {
                THRChart.ChartHRData_TEST.push(THRChart.ChartHRData_WARMUP[THRChart.ChartHRData_WARMUP.length - 1]);
            }
            THRChart.ChartHRData_TEST.push(graphdata)
        }
        THRChart.ChartHRData_FULL.push(graphdata);

        //Need to refresh the Chart Here
    }
}

function BLEDeviceOnError(reason) {
    alert("There was an error " + reason);
}
/******************************************************************************************/
/********************************* HR BLE Bluetooth  - End *********************************/
/******************************************************************************************/
function getHeartRate(_getCurrentHeartRate) {
    logStatus("Excuting getHeartRate", LOG_COMMON_FUNCTIONS);
    try {
        var randHeartRate = HEARTRATE[Math.floor(Math.random() * HEARTRATE.length)];
        if (_getCurrentHeartRate) {
            _getCurrentHeartRate(randHeartRate)
        }
    } catch (e) {
        ShowExceptionMessage("getHeartRate", e);
    }
}
function getRandomHeartRate() {
    logStatus("Excuting getRandomHeartRate", LOG_COMMON_FUNCTIONS);

    try {
    } catch (e) {
        ShowExceptionMessage("getHeartRate", e);
    }
}
function MATRIXDeviceOnData() {
    logStatus("Excuting MATRIXDeviceOnData", LOG_COMMON_FUNCTIONS);
}
function MATRIXDeviceOnError() {
    logStatus("Excuting MATRIXDeviceOnError", LOG_COMMON_FUNCTIONS);
}
function StartMATRIXNotificationProcess(deviceid, successhandler, errorhandler) {
    logStatus("Excuting StartMATRIXNotificationProcess", LOG_COMMON_FUNCTIONS);
    logStatus(deviceid, LOG_DEBUG);
    if (!successhandler) {
        successhandler = MATRIXDeviceOnData;
    }
    if (!errorhandler) {
        errorhandler = MATRIXDeviceOnError;
    }
    StopMATRIXNotificationProcess();
    GLOBAL_REPLCATE_MATRIX_NOTIFY_TIMIER = setInterval(function () {
        var min = 50;
        var max = 70;
        var rpm = Math.floor(Math.random() * (max - min + 1) + min);
        var _CurrentWATT = 0;
        var crpm = 0;
        var _CurrentRPM = 0;
        logStatus(rpm, LOG_DEBUG);
        updateMatrixdataCallback();
        //getCurrentTargetRPM(function(crpm){
        successhandler({
            elapsedtime: '%@',
            calories: '%d',
            caloriesPerHour: '%d',
            mets: '%.1f',
            watts: _CurrentWATT,
            averageWatts: '%d',
            distanceInmiles: '%.2f',
            bpm: '%d',
            peakBpm: '%d',
            secondsInHeartRateZone: '%d',
            speedInMil: '%.1f',
            avgspeedInmiles: '%.1f',
            paceInSec: '%d:%02d',
            avgPaceInSec: '%d:%02d',
            currentStridesPerMinute: '%d',
            averageStridesPerMinute: crpm,
            currentTargetSpm: crpm,
            currentCrankRPM: _CurrentRPM,
            averageCrankRPM: _CurrentRPM
        });
        //})
    }, 1000);
}
//CallBackFromCardioMachineMatrixdata