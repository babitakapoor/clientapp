/******************************************************************************************/
/************************** COMMON.JS VERSION 2.1 23/02/2013 ******************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Global Events  - Start *********************************/
/******************************************************************************************/

/* Setting up the Device ready event */
document.addEventListener("deviceready", onDeviceReady, false);
document.addEventListener("pause", onPause, false);
document.addEventListener("resume", onResume, false);
document.addEventListener("online", onOnline, false);
document.addEventListener("offline", onOffline, false);
window.addEventListener("batterycritical", onBatteryCritical, false);
window.addEventListener("batterylow", onBatteryLow, false);
window.addEventListener("batterystatus", onBatteryStatus, false);
document.addEventListener("menubutton", onMenuKeyDown, false);
//document.addEventListener("searchbutton", onSearchKeyDown, false);
function onDeviceReady() {
getToken();
    //XX ShowSplashScreen();
    logStatus("Calling OnDeviceReady", LOG_EVENTS);
    try {
        CreateApplicationDirectories();
        _IS_DEVICE_READY_PROGRESS = 1;
        onPlatformReady();
				 navigator.splashscreen.show();
        InitializeDBObjects();
        //document.addEventListener("backbutton", onBackKeyDown, false);
        ApplicationReady(); //Call to Application Level Function
		// pagetitelanimation();
document.addEventListener("backbutton", onBackKeyDown, false);   
   } catch (e) {
        ShowExceptionMessage("OndeviceReady", e);
    }
}

function onPause() {
    logStatus("Excuting onPause", LOG_EVENTS);
    try {
        ExecutePause();
    } catch (e) {
        ShowExceptionMessage("onPause", e);
    }
    // Handle the pause event
}

function onResume() {
    logStatus("Excuting onResume", LOG_EVENTS);
    try {
        ExecuteResume();
    } catch (e) {
        ShowExceptionMessage("onResume", e);
    }
    // Handle the resume event

}

function onOnline() {
    logStatus("Excuting onOnline", LOG_EVENTS);
    try {
        ExecuteOnline();
    } catch (e) {
        ShowExceptionMessage("onOnline", e);
    }
    // Handle the online event

}

function onOffline() {
    logStatus("Excuting onOffline", LOG_EVENTS);
    try {
        ExecuteOffline();
    } catch (e) {
        ShowExceptionMessage("onOffline", e);
    }
    // Handle the offline event

}

//Only on Andriod
function onBackKeyDown() {
    logStatus("Excuting onBatteryCritical", LOG_EVENTS);
    try {
        ExecuteBackKeyDown();
    } catch (e) {
        ShowExceptionMessage("onBackKeyDown", e);
    }
    // Handle the back button    


}

function onBatteryCritical(info) {
    logStatus("Excuting onBatteryCritical", LOG_EVENTS);
    logStatus(info, LOG_DEBUG);
    try {
        ExecuteBatteryCritical();
    } catch (e) {
        ShowExceptionMessage("onBatteryCritical", e);
    }
    // Handle the battery critical event
    //alert("Battery Level Critical " + info.level + "%\nRecharge Soon!"); 

}

function onBatteryLow(info) {
    logStatus("Excuting onBatteryLow", LOG_EVENTS);
    logStatus(info, LOG_DEBUG);
    try {
        ExecuteBatteryLow();
    } catch (e) {
        ShowExceptionMessage("onBatteryLow", e);
    }
    // Handle the battery low event
    //alert("Battery Level Low " + info.level + "%"); 

}

function onBatteryStatus(info) {
    logStatus("Excuting onBatteryStatus", LOG_EVENTS);
    try {
        ExecuteBatteryStatus();
    } catch (e) {
        ShowExceptionMessage("onBatteryStatus", e);
    }
    // Handle the online event
    /**
     * @param info.level This is info level
     * @param info.isPlugged This is isPlugged
     */
    console.log("Level: " + info.level + " isPlugged: " + info.isPlugged);
}

function onMenuKeyDown() {
    logStatus("Excuting onMenuKeyDown", LOG_EVENTS);
    try {
        ExecuteMenuKeyDown();
    } catch (e) {
        ShowExceptionMessage("onMenuKeyDown", e);
    }
    // Handle the back button
}

function onSearchKeyDown() {
    logStatus("Excuting onSearchKeyDown", LOG_EVENTS);
    try {
        ExecuteSearchKeyDown();
    } catch (e) {
        ShowExceptionMessage("onSearchKeyDown", e);
    }
    // Handle the search button
}
/******************************************************************************************/
/********************************* Global Events  - End *********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Functions  - Start ************************************/
/******************************************************************************************/


/******************************************************************************************/
/********************************* Translations - Start ***********************************/
/******************************************************************************************/
function getSettingsLangidfromlandcode() {
    return 1;
}
function getTraslatedString(nodestr) {
    logStatus("Excuting getTraslatedString", LOG_COMMON_FUNCTIONS);
    try {
        if (!localStorage.curLangCode) {
            localStorage.curLangCode = "en";
            localStorage.curLangId = getSettingsLangidfromlandcode(localStorage.curLangCode);
        }

        //  var labelCurval;
        if (!__LangXmlData) {
            if ((!localStorage.translations) || (localStorage.translations == null)) {
                localStorage.translations = __LangXmlDatastr_Default;
            }
            var parser = new DOMParser();
            __LangXmlData = parser.parseFromString(localStorage.translations, "text/xml"); //$.parseXML(localStorage.translations);
        }
        var labelCurval = '';
        $(__LangXmlData).find(localStorage.curLangCode).find("trans").each(function () {
            if ($(this).attr("id") == nodestr) {
                labelCurval = $(this).text();
            }
        });
        return labelCurval;
    } catch (e) {
        ShowExceptionMessage("getTraslatedString", e);
    }
}

function getTraslatedText(nodestr, destinationId) {
    logStatus("Excuting getTraslatedText", LOG_COMMON_FUNCTIONS);
    try {

        var labelCurval = getTraslatedString(nodestr);

        $("#" + destinationId).html(labelCurval)
    } catch (e) {
        ShowExceptionMessage("getTraslatedText", e);
    }
}
/******************************************************************************************/
/********************************* Translations  - End ************************************/
/******************************************************************************************/


/******************************************************************************************/
/********************************* Alert Message  - Start *********************************/
/******************************************************************************************/
var _ISIN_PROGRESS = 0;
function startprogress(title, msg) {
    logStatus("Excuting startprogress", LOG_COMMON_FUNCTIONS);
    try {
        var durationOptions = {
            minDuration: 2
        };
        logStatus(durationOptions, LOG_DEBUG);
        if (_ISIN_PROGRESS == 1) {
            stopprogress();
            setTimeout(function () {
                _ISIN_PROGRESS = 1;
                NotificationActivityStart(title, msg);
            }, 700);
            //XX },1500)
        } else {
            _ISIN_PROGRESS = 1;
            NotificationActivityStart(title, msg);
        }

    } catch (e) {
        ShowExceptionMessage("startprogress", e);
    }
}

function stopprogress() {
    logStatus("Excuting stopprogress", LOG_COMMON_FUNCTIONS);
    try {
        window.setTimeout(function () {
            NotificationActivityStop();
            _ISIN_PROGRESS = 0;
        }, 200);
        //XX }, 1000);    
        //NotificationActivityStop(); 
    } catch (e) {
        ShowExceptionMessage("stopprogress", e);
    }
}

function showAlert(message) {
    logStatus("Excuting showAlert", LOG_COMMON_FUNCTIONS);
    try {
        NotificationAlert(message);
    } catch (e) {
        ShowExceptionMessage("showAlert", e);
    }
}

function alertDismissed() {
    logStatus("Excuting alertDismissed", LOG_COMMON_FUNCTIONS);
}

function ShowAlertMessage(strMsg) {
    logStatus("Excuting ShowAlertMessage", LOG_COMMON_FUNCTIONS);
    try {
        setTimeout(function () {
            showAlert(strMsg);
        }, 0);
    } catch (e) {
        ShowExceptionMessage("ShowAlertMessage", e);
    }
}

function ShowAlertConfirmMessage(strMsg, callBackFunction, buttons) {
    logStatus("Excuting ShowAlertConfirmMessage", LOG_COMMON_FUNCTIONS);
    try {
        setTimeout(function () {
            NotificationConfirmMessage(
                strMsg,  // message
                callBackFunction,         // callback
                buttons                 // buttonName
            );
        }, 0);
    } catch (e) {
        ShowExceptionMessage("ShowAlertConfirmMessage", e);
    }
}

function ShowTransAlertMessage(strMsg) {
    logStatus("Excuting ShowTransAlertMessage", LOG_COMMON_FUNCTIONS);
    try {
        var messageTrans = getTraslatedString(strMsg);
        ShowAlertMessage(messageTrans);
    } catch (e) {
        ShowExceptionMessage("ShowTransAlertMessage", e);
    }
}

/**
 * @return {string}
 */
function FormatExceptionMessage(Module, exception) {
    return "Exception: On " + Module + " " + exception;
}

function ShowExceptionMessage(Module, exception) {
    logStatus("Excuting ShowExceptionMessage", LOG_COMMON_FUNCTIONS);
    try {
        var Message = FormatExceptionMessage(Module, exception);
        if (SHOW_EXCEPTION_ALERT == 1) {
            ShowAlertMessage(Message);
        } else {
            LogMessage(Message);
        }
    } catch (e) {
        ShowExceptionMessage("ShowExceptionMessage", e);
    }
}

/******************************************************************************************/
/********************************* Alert Message  - End   *********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Log Status  - Start ************************************/
/******************************************************************************************/
function LogMessage(strMsglog) {
    //if(!IS_WEBAPP ) {
    console.log(strMsglog);
    //}
}

function logStatus(strMsglog, loglevel) {

    try {
        if (!loglevel) {
            loglevel = LOG_DEBUG;
        }
        if (loglevel <= APPLICATION_LOG_LEVEL) {

            LogMessage(strMsglog);
        }
    } catch (e) {
        ShowExceptionMessage("logStatus", e);
    }
}

/******************************************************************************************/
/********************************* Log Status  - End **************************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Date and Time - Start **********************************/
/******************************************************************************************/
/* Updates Current system date and time to the Global Variable */
function updateDateAndTime() {
    logStatus("Excuting updateDateAndTime", LOG_COMMON_FUNCTIONS);
    try {
        _todaydate = getValidDateAndTime();
    } catch (e) {
        ShowExceptionMessage("updateDateAndTime", e);
    }
}

function getfourdigitsYear(number) {
    logStatus("Excuting updateDateAndTime", LOG_COMMON_FUNCTIONS);
    try {
        return (number < 1000) ? number + 1900 : number;
    } catch (e) {
        ShowExceptionMessage("getfourdigitsYear", e);
    }
}

function getValidDateAndTime() {
    logStatus("Excuting getValidDateAndTime", LOG_COMMON_FUNCTIONS);
    try {
        var months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        var now = new Date();
        var hours = now.getHours();
        var minutes = now.getMinutes();
        var seconds = now.getSeconds();
        var timeStr = "" + hours;
        timeStr += ((minutes < 10) ? ":0" : ":") + minutes;
        timeStr += ((seconds < 10) ? ":0" : ":") + seconds;

        var today = (getfourdigitsYear(now.getYear())) + "-" + months[now.getMonth()] + "-" + now.getDate();
        return today + " " + timeStr;
    } catch (e) {
        ShowExceptionMessage("getValidDateAndTime", e);
    }
}

function getPlainDateandTime() {
    logStatus("Excuting getPlainDateandTime", LOG_COMMON_FUNCTIONS);
    try {
        var d = new Date();

        var month = d.getMonth() + 1;
        var day = d.getDate();
        var hour = d.getHours();
        var minute = d.getMinutes();
        var second = d.getSeconds();
        return d.getFullYear() + '' +
            (('' + month).length < 2 ? '0' : '') + month + '' +
            (('' + day).length < 2 ? '0' : '') + day + '' +
            (('' + hour).length < 2 ? '0' : '') + hour + '' +
            (('' + minute).length < 2 ? '0' : '') + minute + '' +
            (('' + second).length < 2 ? '0' : '') + second;
    } catch (e) {
        ShowExceptionMessage("getPlainDateandTime", e);
    }
}

function getPlainDate() {
    logStatus("Excuting getPlainDate", LOG_COMMON_FUNCTIONS);
    try {
        var d = new Date();

        var month = d.getMonth() + 1;
        var day = d.getDate();
        return d.getFullYear() + '' +
            (('' + month).length < 2 ? '0' : '') + month + '' +
            (('' + day).length < 2 ? '0' : '') + day;
    } catch (e) {
        ShowExceptionMessage("getPlainDate", e);
    }
}

function hour2mins(timestr) {
    logStatus("Excuting hour2mins", LOG_COMMON_FUNCTIONS);
    try {
        var spltd = timestr.split(":");
        if (spltd.length > 2) {
            return parseInt(parseInt(spltd[0] * 60) + parseInt(spltd[1]));
        } else {
            return false;
        }
    } catch (e) {
        ShowExceptionMessage("hour2mins", e);
    }
}

function mins2hour(instr) {
    logStatus("Excuting mins2hour", LOG_COMMON_FUNCTIONS);
    try {
        var hourstr = parseInt(instr / 60);
        if (hourstr.toString().length == 1) {
            hourstr = "0" + (hourstr + '');
        }
        var minstr = parseInt(instr % 60);
        if (minstr.toString().length == 1) {
            minstr = "0" + (minstr + '');
        }
        return hourstr + ':' + minstr;
    } catch (e) {
        ShowExceptionMessage("mins2hour", e);
    }
}

function timediff(t1, t2) {
    logStatus("Excuting timediff", LOG_COMMON_FUNCTIONS);
    try {
        var time1 = hour2mins(t1);
        var time2 = hour2mins(t2);
        var ret = mins2hour(parseInt(time2 - time1));
        if (time2 < time1) {
            ret = mins2hour(parseInt(parseInt(time2 + 1440) - time1));
        }
        return ret;
    } catch (e) {
        ShowExceptionMessage("timediff", e);
    }
}

function getTwodigittime(timestr) {
    logStatus("Excuting getTwodigittime", LOG_COMMON_FUNCTIONS);
    try {
        var spltd = timestr.split(":");
        var timeplain = '';
        for (var i = 0; i < spltd.length; i++) {
            var timeinc = spltd[i];
            timeplain = timeplain + (('' + timeinc).length < 2 ? '0' : '') + timeinc + '';
        }
        return timeplain;
    } catch (e) {
        ShowExceptionMessage("getTwodigittime", e);
    }
}

function getOnlyHoursAndMinutes(time) {
    var timepart = time.split(":");
    if (timepart.length > 1) {
        return timepart[0] + ":" + timepart[1];
    }
    return "";
}

function getTimeFromDateAndTime(datetime) {
    var datepart = datetime.split(" ");
    if (datepart.length > 1) {
        return datepart[1];
    }
    return "";
}

function getDateFromDBDateAndTime(datetime) {
    var datepart = datetime.split(" ");
    if (datepart.length > 1) {
        var datepartarray = datepart[0].split("-");
        return datepartarray[2] + "/" + datepartarray[1] + "/" + datepartarray[0];
    }
    return "";
}

function convert24TimeTo12Time(time) {
    var hms = time.split(':');
    if (hms.length > 0) {
        var h = +hms[0],
            suffix = (h < 12) ? 'am' : 'pm';
        hms[0] = h % 12 || 12;
        return hms.join(':') + suffix
    }
}

/******************************************************************************************/
/********************************* Date and Time - End **********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* String Functions-Start *********************************/
/******************************************************************************************/
function addSlash(path) {
    var lastChar = path.substr(-1); // Selects the last character
    if (lastChar != '/') {         // If the last character is not a slash
        path = path + '/';            // Append a slash to it.
    }
    return path;
}

function getFileNameFromPath(fullPath) {
    return fullPath.replace(/^.*[\\\/]/, '');
}
/******************************************************************************************/
/********************************* String Functions -End **********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Number Functions -End **********************************/
/******************************************************************************************/
function parseValidFloat(val) {
    var value = parseFloat(val);
    if (isNaN(value)) {
        return 0;
    }
    return value;
}

/******************************************************************************************/
/********************************* Number Functions -End **********************************/
/******************************************************************************************/


/******************************************************************************************/
/********************************* Work Around Func - Start **********************************/
/******************************************************************************************/
/**
 * @return {boolean}
 */
function CanIExecute() {
    var current = new Date().getTime();
    var delta = current - _LASTCLICKTIME;
    window._LASTCLICKTIME = current;
    if (delta < 500) {
        console.log("false event");
        //ShowAlertMessage("false " + delta);
        return false;
    } else {
        console.log("true event");
        //ShowAlertMessage("true " + delta);
        return true;
    }
}

/******************************************************************************************/
/********************************* Work Around - End **********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Validation Func - Start **********************************/
/******************************************************************************************/
/**
 * @return {boolean}
 */
function IsEmailValid(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

/**
 * @return {boolean}
 */
function IsPhoneValid(phone) {
    var regex = /^[0-9-+()]+$/;
    return regex.test(phone);
}

function ReplaceAllString(givenstr, searchstr, replacestr) {

    var regexp = new RegExp(searchstr, "g");
    return givenstr.replace(regexp, replacestr);
}

function replaceQuoats(str) {
    str = StripNullValue(str);
    var replacedstr = str.replace(/\"/g, '""');
    replacedstr = replacedstr.replace(/\'/g, "\\'");
    return replacedstr;
}
/*
 function utf8_decode(str) {
 var output = "";
 var i = 0;var c = 0;var c1 = 0;var c2 = 0;var c3 = 0;
 logStatus(c1, LOG_DEBUG);
 while ( i < str.length ) {
 c = str.charCodeAt(i);
 if (c < 128) {
 output += String.fromCharCode(c);
 i++;
 }
 else if((c > 191) && (c < 224)) {
 c2 = str.charCodeAt(i+1);
 output += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
 i += 2;
 }
 else {
 c2 = str.charCodeAt(i+1);
 c3 = str.charCodeAt(i+2);
 output += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
 i += 3;
 }
 }
 return output;
 }*/
function validateSteps(parentSelector, errordisplay) {
    logStatus("Calling validateSteps", LOG_FUNCTIONS);
    logStatus(errordisplay, LOG_FUNCTIONS);
    try {
        var _nonempty = jQuery(parentSelector + " .req-nonempty");
        var _none_zero = jQuery(parentSelector + " .req-nonzero");
        var _email = jQuery(parentSelector + " .req-email");
        var _emptyselect = jQuery(parentSelector + " .req-emptyselect");
        //var _ukpostcode	=	jQuery(parentSelector + " .req-ukpostcode");
        var _novalequal = jQuery(parentSelector + " .req-novalequal");
        var noError = true;
        _nonempty.each(function () {
            var $this = jQuery(this);
            var $reportelem = $this;
            if ($this.attr("err_reporton") && $this.attr("err_reporton") != '') {
                $reportelem = $("#" + $this.attr("err_reporton"));
            }
            if (jQuery.trim($this.val()) == ''|| jQuery.trim($this.val()) == -1) {
                noError = false;
                $reportelem.css({"border": "1px solid red"});
                $this.keyup(function () {
                    $reportelem.css("border", "");
                });
                $this.change(function () {
                    $reportelem.css('border', "");
                });
            } else {
                $reportelem.css("border", "");
            }
        });
        _email.each(function () {
            var $this = jQuery(this);
            var $reportelem = $this;
            if ($this.attr("err_reporton") && $this.attr("err_reporton") != '') {
                $reportelem = $("#" + $this.attr("err_reporton"));
            }
            if (!IsEmailValid($this.val())) {
                noError = false;
                $reportelem.css({"border": "1px solid red"});
                $this.keyup(function () {
                    $reportelem.css("border", "");
                });
            } else {
                $reportelem.css("border", "");
            }
        });
        _emptyselect.each(function () {
            var $this = jQuery(this);
            var $reportelem = $this;
            if ($this.attr("err_reporton") && $this.attr("err_reporton") != '') {
                $reportelem = $("#" + $this.attr("err_reporton"));
            }
            if ((!$this.find("option:selected").text()) || ($this.find("option:selected").text() == "")) {
                noError = false;
                $reportelem.css({"border": "1px solid red"});
                $this.change(function () {
                    $reportelem.css('border', "");
                });
            } else {
                $reportelem.css("border", "");
            }
        });
        _none_zero.each(function () {
            var $this = jQuery(this);
            var $reportelem = $this;
            if ($this.attr("err_reporton") && $this.attr("err_reporton") != '') {
                $reportelem = $("#" + $this.attr("err_reporton"));
            }
            if ($this.val() == '0') {
                noError = false;
                $reportelem.css({"border": "1px solid red"});
                $this.change(function () {
                    $reportelem.css('border', "");
                });
            } else {
                $reportelem.css('border', "");
            }
        });
        _novalequal.each(function () {
            var $this = jQuery(this);
            var $reportelem = $this;
            if ($this.attr("err_reporton") && $this.attr("err_reporton") != '') {
                $reportelem = $("#" + $this.attr("err_reporton"));
            }
            var ne_value = $this.attr("novalequal") ? $this.attr("novalequal") : -1;
            if ($this.val() == ne_value) {
                noError = false;
                $reportelem.css({"border": "1px solid red"});
                $this.change(function () {
                    $reportelem.css('border', "");
                });
            } else {
                $reportelem.css('border', "");
            }
        });
        /* - Re-Enable Later If needed 
         if(_ukpostcode) {
         _ukpostcode.each(function(){
         var $this=	jQuery(this);
         var $reportelem	=	$this;
         if($this.attr("err_reporton") && $this.attr("err_reporton")!=''){
         $reportelem	=	$("#"+$this.attr("err_reporton"));
         }
         if(!validateUKPostCode($this.val())){
         noError	=	false;
         $reportelem.css({"border":"1px solid red"});
         } else{
         $reportelem.css("border","");
         }
         });
         }*/
        return noError;
    } catch (e) {
        ShowExceptionMessage("validateSteps", e);
    }
}
/******************************************************************************************/
/********************************* Validation Func - End **********************************/
/******************************************************************************************/

/*
 function DisableProcessBtn(id, enable, text,isHtmlType){
 logStatus("Calling DisableProcessBtn",LOG_FUNCTIONS);
 try {
 if (!text){
 text = "Please wait...";
 }
 if (enable){	 
 $(id).removeAttr("disabled");
 }else{
 FIELDTYPE = '';
 LAST_DIS_ID = id;
 LAST_DIS_TXT = $(id).val();		
 $(id).attr("disabled", "disabled");
 }
 if(isHtmlType){
 FIELDTYPE = 'html';
 $(id).html(text);
 return;
 }
 $(id).val(text);
 } catch (e) {
 ShowExceptionMessage("DisableProcessBtn", e);
 }
 }
 */
/*Increment And Decrement the Input*/
function incInput(MaxVal, step, selector) {
    logStatus("incInput", LOG_FUNCTIONS);
    try {
        var $selector = (selector) ? $(selector) : $("#rangerinput");
        // var $selector	=	(selector) ? $("#"+selector) : $("#rangerinput");
        var incval = $.trim($selector.html());
        var nevalue = (parseInt(incval) + parseInt(step));
        if (nevalue <= MaxVal) {
            $selector.html(nevalue);
        }
    } catch (e) {
        ShowExceptionMessage("incInput", e);
    }
}
function decInput(MinVal, step, selector) {
    logStatus("decInput", LOG_FUNCTIONS);
    try {
        // var $selector	=	(selector) ? $("#"+selector) : $("#rangerinput");
        var $selector = (selector) ? $(selector) : $("#rangerinput");
        var decval = $.trim($selector.html());
        var newvalue = parseInt(decval) - step;
        if (newvalue >= MinVal) {
            $selector.html(newvalue);
        }
    } catch (e) {
        ShowExceptionMessage("decInput", e);
    }
}
function AddProcessingLoader(obj) {
    logStatus("AddProcessingLoader", LOG_FUNCTIONS);
    try {
        LAST_LOADER_ID = obj;
        if (LAST_LOADER_ID) {
            $(LAST_LOADER_ID).addClass('clsprocessing');
            $(LAST_LOADER_ID).attr("disabled", "disabled");
        }
    } catch (e) {
        ShowExceptionMessage("AddProcessingLoader", e);
    }
}
function RemoveProcessingLoader(obj) {
    logStatus("RemoveProcessingLoader", LOG_FUNCTIONS);
    try {
        var removeobj = LAST_LOADER_ID;
        if (!obj) {
            removeobj = obj;
        }
        if (removeobj) {
            $(removeobj).removeClass('clsprocessing');
            $(removeobj).removeAttr("disabled");
        }
    } catch (e) {
        ShowExceptionMessage("RemoveProcessingLoader", e);
    }

}
function EnableLastProcessBtn() {
    if (LAST_DIS_ID && LAST_DIS_ID != '') {
        if ($(LAST_DIS_ID)) {
            $(LAST_DIS_ID).removeAttr("disabled");
            if (FIELDTYPE == 'html') {
                $(LAST_DIS_ID).html(LAST_DIS_TXT);
            } else {
                $(LAST_DIS_ID).val(LAST_DIS_TXT);
            }
        }
    }
}
function isNumber(evt) {
    logStatus("isNumber", LOG_FUNCTIONS);
    try {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        return !(charCode > 31 && (charCode < 48 || charCode > 57));

    } catch (e) {
        ShowExceptionMessage("isNumber", e);
    }
}
function isNumberDecimal(evt) {
    logStatus("isNumber", LOG_FUNCTIONS);
    try {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        return !(charCode > 31 && (charCode < 46 || charCode > 57));

    } catch (e) {
        ShowExceptionMessage("isNumber", e);
    }
}
/*MK Added Compare Date*/
/**
 * @return {boolean}
 */
function ComapreDate(date, date_alt, compare_query) {
    logStatus("ComapreDate", LOG_FUNCTIONS);
    try {
        var d1 = date.getDate();
        var m1 = date.getMonth();
        var y1 = date.getFullYear();
        var d2 = date_alt.getDate();
        var m2 = date_alt.getMonth();
        var y2 = date_alt.getFullYear();
        var result = false;
        if (compare_query == 'less_than') {
            if (d1 < d2 && m1 < m2 && y1 < y2) {
                result = true;
            }
        } else if (compare_query == 'less_than_or_equal') {
            if (d1 <= d2 && m1 <= m2 && y1 <= y2) {
                result = true;
            }
        } else if (compare_query == 'equal') {
            if (d1 == d2 && m1 == m2 && y1 == y2) {
                result = true;
            }
        }
        return result;
    } catch (e) {
        ShowExceptionMessage("ComapreDate", e);
    }
}
function getDateTimeByFormat(format, byDate) {

    var _Date = (byDate) ? byDate : new Date();
    var day = _Date.getDate();

    if (day.toString().split('').length == 1) {
        day = '0' + day;
    }
    format = format.replace('d', day);

    var month = _Date.getMonth();
    var mm = (month + 1);
    if (mm.toString().split('').length == 1) {
        mm = '0' + mm;
    }
    format = format.replace('m', mm);

    var fullyear = _Date.getFullYear();
    format = format.replace('Y', fullyear);

    var hours = _Date.getHours();
    if (hours.toString().split('').length == 1) {
        hours = '0' + hours;
    }
    format = format.replace('H', hours);
    var minutes = _Date.getMinutes();
    if (minutes.toString().split('').length == 1) {
        minutes = '0' + minutes;
    }
    format = format.replace('i', minutes);
    var seconds = _Date.getSeconds();
    if (seconds.toString().split('').length == 1) {
        seconds = '0' + seconds;
    }
    format = format.replace('s', seconds);
    return format;
}
/**
 * @return {number}
 */
function DateDifferece(date2, date1) {
    logStatus("Calling DateDifferece", LOG_FUNCTIONS);
    try {
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        return Math.ceil(timeDiff / (1000 * 3600 * 24));
    } catch (e) {
        ShowExceptionMessage("DateDifferece", e);
    }
}
function ShowToastMessageTrans(messagetranscode, type) {
    var transmsg = GetLanguageText(messagetranscode);
    ShowToastMessage(transmsg, type);
}
/**/

function getPathHashData() {
    logStatus("getValidHashPath", LOG_FUNCTIONS);
    try {
        var _hashdata = {};
        var _hash = $(location).attr('hash');
        var _path = (_hash.split("#/")[1]) ? _hash.split("#/")[1] : '';
        _path = _path.split("?");
        _hashdata.path = _path[0];
        var astr = _path[1] ? _path[1] : '';
        _hashdata.args = {};
        if (astr != '') {
            var _astr1 = astr.split("&");
            for (var kval in _astr1) {
                if (_astr1.hasOwnProperty(kval)) {
                    var _kval = _astr1[kval].split("=");
                    _hashdata.args[_kval[0]] = _kval[1];
                }
            }
        }
        return _hashdata;
    } catch (e) {
        ShowExceptionMessage("getValidHashPath", e);
    }
}

function rtrim(_char, _string) {
    logStatus("Calling rtrim", LOG_FUNCTIONS);
    try {
        if (_string.charAt(_string.length - _char.length) == _char) {
            _string = _string.substr(0, _string.length - _char.length);
        }
        return _string;
    } catch (e) {
        ShowExceptionMessage("rtrim", e);
    }
}
function base64_encode(data) {
    logStatus("Calling base64_encode", LOG_FUNCTIONS);
    try {
        var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        var i = 0, ac = 0, tmp_arr = [];
        if (!data) {
            return data;
        }
        data = unescape(encodeURIComponent(data));
        do {
            // pack three octets into four hexets
            var o1 = data.charCodeAt(i++);
            var o2 = data.charCodeAt(i++);
            var o3 = data.charCodeAt(i++);
            var bits = o1 << 16 | o2 << 8 | o3;
            var h1 = bits >> 18 & 0x3f;
            var h2 = bits >> 12 & 0x3f;
            var h3 = bits >> 6 & 0x3f;
            var h4 = bits & 0x3f;

            // use hexets to index into b64, and append result to encoded string
            tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
        } while (i < data.length);
        var enc = tmp_arr.join('');
        var r = data.length % 3;
        return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
    } catch (e) {
        ShowExceptionMessage("base64_encode", e);
    }
}

function base64_decode(data) {
    logStatus("Calling base64_decode", LOG_FUNCTIONS);
    try {
        var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        var i = 0, ac = 0, tmp_arr = [];
        if (!data) {
            return data;
        }
        data += '';
        do {
            // unpack four hexets into three octets using index points in b64
            var h1 = b64.indexOf(data.charAt(i++));
            var h2 = b64.indexOf(data.charAt(i++));
            var h3 = b64.indexOf(data.charAt(i++));
            var h4 = b64.indexOf(data.charAt(i++));

            var bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
            var o1 = bits >> 16 & 0xff;
            var o2 = bits >> 8 & 0xff;
            var o3 = bits & 0xff;
            if (h3 == 64) {
                tmp_arr[ac++] = String.fromCharCode(o1);
            } else if (h4 == 64) {
                tmp_arr[ac++] = String.fromCharCode(o1, o2);
            } else {
                tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
            }
        } while (i < data.length);
        var dec = tmp_arr.join('');
        return decodeURIComponent(escape(dec.replace(/\0+$/, '')));
    } catch (e) {
        ShowExceptionMessage("base64_decode", e);
    }
}
function _movePageTo(page, args) {
    logStatus("_movepageto", LOG_FUNCTIONS);
    try {
        var templateurl = __GetValidPageTemplate(page, args);
        var curpath = GetCurrentPath();
        var index = __PathHistory.indexOf(templateurl);
        if (index >= 0) {
            __PathHistory.splice(index, 1);
        }
        __PathHistory.push('#/' + curpath);
        window.location.href = templateurl;//'#/'+_page+_argstr;		
    } catch (e) {
        ShowExceptionMessage("_movePageTo", e);
    }
}
/*PK Added 20160325-Go back Page*/
function goBack() {
    /*MK Changed*/
    var movepage = __getHistoryPagePath();//__PathHistory[__PathHistory.length-1];
    if (movepage != '') {
        window.location.href = movepage + ((movepage.toString().split("?").length > 1) ? '' : '?') + URL_SUFIX_FOR_BACK;
    }
}
function _setCacheArray(key, array) {
    logStatus("Calling _setCacheArray", LOG_FUNCTIONS);
    try {
        localStorage.setItem(key, JSON.stringify(array));
    } catch (e) {
        ShowExceptionMessage("_setCacheArray", e);
    }
}
function _getCacheArray(key) {
    logStatus("Calling _getCacheArray", LOG_FUNCTIONS);
    try {
        var data = localStorage.getItem(key);
        return (data && data != '' && data != 'undefined' && typeof(data) != 'undefined') ? JSON.parse(data) : {};
    } catch (e) {
        ShowExceptionMessage("_getCacheArray", e);
    }
}
function _setCacheValue(key, value) {
    logStatus("Calling _setCacheValue", LOG_FUNCTIONS);
    try {
        localStorage.setItem(key, value);
    } catch (e) {
        ShowExceptionMessage("_setCacheValue", e);
    }
}
function _getCacheValue(key) {
    logStatus("Calling _getCacheValue", LOG_FUNCTIONS);
    try {
        return localStorage.getItem(key);
    } catch (e) {
        ShowExceptionMessage("_getCacheValue", e);
    }
}
/*
 function is_page(page){
 logStatus("Calling is_page",LOG_FUNCTIONS);
 try{
 var pageinfo	=	getPathHashData();
 return (page==pageinfo.path);
 }catch (e) {
 ShowExceptionMessage("is_page", e);
 }		
 }
 */
function _close_popup() {
    logStatus("Calling _close_popup", LOG_FUNCTIONS);
    try {
        $(".comm_popup_inner ").remove();
        $(".popupmask").remove();
    } catch (e) {
        ShowExceptionMessage("_close_popup", e);
    }
}
function _popup(message, obj, success,page) {
    logStatus("Calling _popup", LOG_FUNCTIONS);
    try {
        var _obj = (obj) ? obj : {};
        var customClassMask = "";
        var closeButton = "";
        /**
         * @param _obj.closeOnOuterClick This is closeon outer click
         */
        if (_obj.closeOnOuterClick) {
            customClassMask = 'close_onouterclick';
        }
        if (_obj.close) {
            closeButton = '<button class="close selectbtn" onclick="_close_popup()">close</button>';
        }
		var $class ='';
		if(page=='forget'){
			$class ='comm_popup_inner';
		}
        var HTML = '<div class="popupmask ' + customClassMask + '"  onclick=""></div><div class="'+$class+' ">' + closeButton;
        HTML += message;
        HTML += '</div>';
        $('.popupmask').remove();
        $('.comm_popup_inner ').remove();
        $('body').append(HTML);
        if (success) {
            success();
        }
    } catch (e) {
        ShowExceptionMessage("_popup", e);
    }
}
/*PK Added FB Login*/
function fb_login() {
    
    openFB.login(
        function (response) {
            if (response.status === 'connected') {
                //alert('Facebook login succeeded, got access token: ' + response.authResponse.accessToken);
                fb_getInfo(response.authResponse.accessToken);
                //setTimeout(function(){fb_getInfo();}, 1000);
            } else {
                ShowAlertMessage('Facebook login failed: ' + response.error);
            }
        }, {scope: 'email,public_profile,user_birthday'});
}

function fb_getInfo(accessToken) {
    logStatus(accessToken, LOG_DEBUG);
    ShowLoader();
    openFB.api({
        path: '/me',
        params: { "access_token": accessToken, "fields":"name,email,gender,user_birthday,locale,bio,photos" },
        success: function (data) {
            /*Registeration Methods*/
            var clientinfo = {};
            var photolink=getFbProfilePhoto(data.id);
            var fullname=data.name.split(" ",2);
            clientinfo.first_name = fullname[0];
            clientinfo.name 		= fullname[1];
            clientinfo.email = data.email;//(data.email) ?data.email:'';
            clientinfo.gender = (data.gender == 'female') ? 1 : 0;
            clientinfo.requestPassword = data.id;
            clientinfo.regdob=data.birthday;
            clientinfo.logintype = LOGINTYPE_SOCIALMEDIA;
            clientinfo.loginreffieldid = data.id;
            clientinfo.image_url=photolink;
            if (data.email == '' || data.email == 'undefined' || typeof(data.email) == 'undefined') {
                HideLoader();
                var fbcheck=checkFbUserRegistered(data.id);
                if(fbcheck.status==1){
                    clientinfo.image_url=base64_encode(clientinfo.image_url);
                    _movePageTo("register",clientinfo);
                    return;    
                }else{
                    var logindata={email:fbcheck.email,password:fbcheck.password};
                     doUserLoginFacebook(logindata);
                     return;
                }
                //ShowAlertMessage(SHOW_EXCEPTION_SOCIALMEDIA_NO_EMAIL);
            }
			/***************/
			  else if (data.user_birthday == '' || data.user_birthday == 'undefined' || typeof(data.user_birthday) == 'undefined') {
				   HideLoader();
				   var check_dob  = checkFbdob(data.email);
				   if(check_dob.status==1){
                    var logindata={email:check_dob.email,password:check_dob.password};
                    					   doUserLoginFacebook(logindata);
                    					   return;
				   }
				   else{

                   					    clientinfo.image_url=base64_encode(clientinfo.image_url);
                                       					   _movePageTo("register",clientinfo);
                                       					   return;
				   }

			}
			   /*************/
			else(data.email != '')
			{
				var fbcheck=checkFbUserRegistered(data.id);
				var logindata={email:fbcheck.email,password:fbcheck.password};
                doUserLoginFacebook(logindata);
			}
            var userid = '';
            _registerMember(clientinfo, function (response) {
                //HideLoader();
                if (response.status == 1) {
                    if (response.data) {
                        var logindata = response.data;
                        
                        doUserLoginFacebook(logindata);
                        
                    } else {
                            HideLoader();
                            ShowAlertMessage("Please try again");
                            HideLoader();
                            //ShowAlertMessage(SHOW_EXCEPTION_SOCIALMEDIA_EMAIL_EXISTS);
                            return;
                      }

                } else {
                    HideLoader();
                    ShowToastMessage("Some issue occured", _TOAST_LONG);
                }
            });
        },
        error: function (err) {
            HideLoader();
            alert(JSON.stringify(err));
        }
    });
}
function checkFbUserRegistered(userid){
    var response={};
    $.ajax({
        url:BASE_URL+"reporting/index.php/voucherlist/check_loginreffield",
        type:"post",
        data:{loginreffield:userid},
        async:false,
        success:function(data){
            var getdata=JSON.parse(data);
            response.status=(getdata.status);
            response.email=((getdata.email)?getdata.email:'');
            response.password=((getdata.password)?getdata.password:'');
        }
    });
    return response;
}
function checkFbdob(email){
    //alert("enter");
    var response={};
    $.ajax({
        url:BASE_URL+"reporting/index.php/voucherlist/check_DOB",
        type:"post",
        data:{email:email},
        async:false,
        success:function(data){
            var getdata=JSON.parse(data);
            response.status=(getdata.status);
            response.dob=((getdata.dob)?getdata.dob:'');
            response.email=((getdata.email)?getdata.email:'');
            response.password=((getdata.password)?getdata.password:'');
        }
    });
    return response;
}
function getFbProfilePhoto(userid){
    console.log("Calling getFbProfilePhoto ");
    var link;
    $.ajax({
        type:"get",
        async: false,
        url:"http://graph.facebook.com/"+userid+"/picture?redirect=false&type=large",
        success:function(data){
            var object=data.data.url;
            link=object;
        }
    });
    return link;
}
$(document).delegate("#facebook", "click", function (e) {
     e.preventDefault();
    var isTermsAccepted	=	$("#termscond").find('input').is(":checked");
    if(isTermsAccepted){
        fb_login();
    }else{
        ShowToastMessageTrans(MSG_PLZ_CONFIRM_TERMS,_TOAST_LONG);
        return;
    }
   
});
function ShowLoader(Message) {
    var msg = (Message) ? Message : '';
    var maskHTML = '<div class="loadermask" style="position:absolute;z-index:100;top:0;bottom:0;left:0;right:0; background-color:rgba(0,0,0,0.6);text-align:center"><span style="left:30%;color:#fff;margin: 55% 47%;display: block;" ><img src="images/loading.gif"/>' + msg + '</span></div>';
    $(".loadermask").remove();
    $('body').append(maskHTML);
}
function HideLoader() {
    $(".loadermask").remove();
}

function addslashes(str) {
    return (str + '')
        .replace(/[\\"']/g, '\\$&')
        .replace(/\u0000/g, '\\0');
}
function getValidJsonObjectByString(jsonString) {
    logStatus("getValidJsonObjectByString", LOG_FUNCTIONS);
    try {
        var jsonobj = {};
        if ((jsonString) && jsonString != "") {
            var jsonStringnew = jsonString.replace(/""/g, '\\""');
            jsonStringnew = jsonStringnew.replace(/\\\\"/g, '\\"');
            jsonobj = JSON.parse(jsonStringnew);
        }
    } catch (e) {
        ShowExceptionMessage("getValidJsonObjectByString", e);
    }
    return jsonobj;
}
function _preventSourceElement($event, isDisable, handleOnSuccess) {
    logStatus("_preventSourceElement " + isDisable, LOG_FUNCTIONS);
    try {
        var _isDisable = (isDisable) ? isDisable : false;
        var elem = $event.currentTarget || $event.srcElement;
        var $elem = jQuery(elem);
        if (_isDisable) {
            $elem.attr("disabled", true);
        } else {
            $elem.removeAttr("disabled");
        }
        if (handleOnSuccess) {
            handleOnSuccess();
        }
    } catch (e) {
        ShowExceptionMessage("_preventSourceElement", e);
    }
}
function utf8_encode(argString) {
    if (argString === null || typeof argString === 'undefined') {
        return '';
    }
    var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
    var utftext = '', start, end;
    start = end = 0;
    var stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;
        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode((c1 >> 6) | 192, (c1 & 63) | 128);
        } else if ((c1 & 0xF800) != 0xD800) {
            enc = String.fromCharCode((c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128);
        } else { // surrogate pairs
            if ((c1 & 0xFC00) != 0xD800) {
                throw new RangeError('Unmatched trail surrogate at ' + n);
            }
            var c2 = string.charCodeAt(++n);
            if ((c2 & 0xFC00) != 0xDC00) {
                throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
            }
            c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
            enc = String.fromCharCode((c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128);
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n + 1;
        }
    }
    if (end > start) {
        utftext += string.slice(start, stringl);
    }
    return utftext;
}
function utf8_decode(str_data) {
    var tmp_arr = [], i = 0, ac = 0, c1 = 0, c2 = 0, c3 = 0, c4 = 0;

    str_data += '';
    while (i < str_data.length) {
        c1 = str_data.charCodeAt(i);
        if (c1 <= 191) {
            tmp_arr[ac++] = String.fromCharCode(c1);
            i++;
        } else if (c1 <= 223) {
            c2 = str_data.charCodeAt(i + 1);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
            i += 2;
        } else if (c1 <= 239) {
            c2 = str_data.charCodeAt(i + 1);
            c3 = str_data.charCodeAt(i + 2);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        } else {
            c2 = str_data.charCodeAt(i + 1);
            c3 = str_data.charCodeAt(i + 2);
            c4 = str_data.charCodeAt(i + 3);
            c1 = ((c1 & 7) << 18) | ((c2 & 63) << 12) | ((c3 & 63) << 6) | (c4 & 63);
            c1 -= 0x10000;
            tmp_arr[ac++] = String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF));
            tmp_arr[ac++] = String.fromCharCode(0xDC00 | (c1 & 0x3FF));
            i += 4;
        }
    }
    return tmp_arr.join('');
}
function searchInArray(objectarray, key, LIKE) {
    var results = [];
    $.each(objectarray, function (i, rowdata) {
        /*
         if(rowdata[key].split('').indexOf(LIKE)!=-1){
         results.push(rowdata);
         }*/
        if (rowdata[key].search(LIKE) > -1) {
            results.push(rowdata);
        }
    });
    return results;
}
function setValidUTF8_encode(_string) {
    return _string.match('!!u') ? _string : utf8_encode(_string);
}
function setValidUTF8_decode(_string) {
    //return _string.match('!!u')? utf8_decode(_string):_string;
    return _string.match('!!u') ? decodeURIComponent(escape(_string)) : _string;
}
function setArrayIn_cache(key, arrayVal) {
    localStorage.setItem(key, JSON.stringify(arrayVal))
}
function getArrayFrom_cache(key) {
    return JSON.parse(localStorage.getItem(key));
}
function setValIn_cache(key, val) {
    localStorage.setItem(key, val);
}
function getValFrom_cache(key) {
    return localStorage.getItem(key);
}
function getItemInArrayByKeyValue(key, value, searchArray, mode) {
    logStatus("getIndexInArrayByKeyVal", LOG_FUNCTIONS);
    try {
        var _customResult = [];
        for (var index in searchArray) {
            if (searchArray.hasOwnProperty(index)) {
                if (searchArray[index]) {
                    if (searchArray[index][key] && searchArray[index][key] == value) {
                        if (mode && mode == true) {
                            _customResult.push(searchArray[index]);
                        } else {
                            return searchArray[index];
                        }
                    }
                }
            }
        }
        return _customResult;
    } catch (e) {
        ShowExceptionMessage("getIndexInArrayByKeyVal", e);
    }
}
function getKeyItemInArrayByKeyValue(getKey, whereKey, value, searchArray) {
    logStatus("getKeyItemArrayByKeyValue", LOG_FUNCTIONS);
    try {
        for (var index in searchArray) {
            if (searchArray.hasOwnProperty(index)) {
                if (searchArray[index][whereKey] == value) {
                    return (searchArray[index][getKey]) ? searchArray[index][getKey] : null;
                }
            }

        }
    } catch (e) {
        ShowExceptionMessage("getKeyItemArrayByKeyValue", e);
    }

}
function removeItemFromArrayByKeyValue(key, value, searchArray) {
    logStatus("removeItemFromArrayByKeyValue", LOG_FUNCTIONS);
    try {
        var _TempArray = [];
        for (var index in searchArray) {
            if (searchArray.hasOwnProperty(index)) {
                if (searchArray[index][key] != value) {
                    _TempArray.push(searchArray[index]);
                }
            }

        }
        return _TempArray;
    } catch (e) {
        ShowExceptionMessage("removeItemFromArrayByKeyValue", e);
    }
}

function rTrim(text, token) {
    token = !token ? ' \\s\u00A0' : (token + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\\$1');
    var re = new RegExp('[' + token + ']+$', 'g');
    return (text + '').replace(re, '');
}
function lTrim(text, token) {
    token = !token ? ' \\s\u00A0' : (token + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
    var re = new RegExp('^[' + token + ']+', 'g');
    return (text + '').replace(re, '');
}
function numPad(number, pad) {
    var N = Math.pow(10, pad);
    return number < N ? String(N + number).slice(-pad) : number;
}
function getFullVersionDetail() {
    return APP_VERSION + "." + _DBVERSION;
}
function _goBackOnSave(pages, onBackSuccess) {
    logStatus("_goBackOnSave", LOG_FUNCTIONS);
    try {
        window.history.go(-(pages));
        if (onBackSuccess) {
            onBackSuccess()
        }
    } catch (e) {
        alert(JSON.stringify(e));
    }

    //window.history.back(-(pages));
    //XX window.history.go(-(pages));
    /*setTimeout(function(){
     window.history.go(-(pages));
     }, 50);*/
}
function _forceToChangePage(href) {
    logStatus("_forceToChangePage > to #" + href, LOG_FUNCTIONS);
    try {
        window.location.href = "#" + href;
    } catch (e) {
        ShowExceptionMessage("_forceToChangePage : " + href, e);
    }
}
/*
 function base64_encode(data) {
 var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
 var i = 0, ac = 0, enc = '', tmp_arr = [];
 if (!data) {
 return data;
 }
 data = unescape(encodeURIComponent(data));
 do {
 // pack three octets into four hexets
 var o1 = data.charCodeAt(i++);
 var o2 = data.charCodeAt(i++);
 var o3 = data.charCodeAt(i++);
 var bits = o1 << 16 | o2 << 8 | o3;
 var h1 = bits >> 18 & 0x3f;
 var h2 = bits >> 12 & 0x3f;
 var h3 = bits >> 6 & 0x3f;
 var h4 = bits & 0x3f;

 // use hexets to index into b64, and append result to encoded string
 tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
 } while (i < data.length);
 enc = tmp_arr.join('');
 var r = data.length % 3;
 return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
 }
 function base64_decode(data) {
 var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
 var i = 0, ac = 0, tmp_arr = [];
 if (!data) {
 return data;
 }
 data += '';
 do {
 // unpack four hexets into three octets using index points in b64
 var h1 = b64.indexOf(data.charAt(i++));
 var h2 = b64.indexOf(data.charAt(i++));
 var h3 = b64.indexOf(data.charAt(i++));
 var h4 = b64.indexOf(data.charAt(i++));

 var bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
 var o1 = bits >> 16 & 0xff;
 var o2 = bits >> 8 & 0xff;
 var o3 = bits & 0xff;
 if (h3 == 64) {
 tmp_arr[ac++] = String.fromCharCode(o1);
 } else if (h4 == 64) {
 tmp_arr[ac++] = String.fromCharCode(o1, o2);
 } else {
 tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
 }
 } while (i < data.length);
 var dec = tmp_arr.join('');
 return decodeURIComponent(escape(dec.replace(/\0+$/, '')));
 }*/
/**
 * @return {string}
 */
function FormatedDate(date, type) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
    var dd = date.getDate().toString();
    var h = date.getHours().toString();
    var i = date.getMinutes().toString();
    var s = date.getSeconds().toString();
    var formatedDate = '';
    switch (type) {
        case FORMAT_ONLY_DATE:
            formatedDate = ( yyyy + "-" + ( mm[1] ? mm : "0" + mm[0] ) + "-" + ( dd[1] ? dd : "0" + dd[0] ) );
            break;
        case FORMAT_ONLY_TIME:
            formatedDate = (( h[1] ? h : "0" + h[0] ) + ":" + ( i[1] ? i : '0' + i[0] ) + ":" + ( s[1] ? s : '0' + s[0]));
            break;
        case FORMAT_DATE_TIME:
            formatedDate = (yyyy + "-" + ( mm[1] ? mm : "0" + mm[0] ) + "-" + ( dd[1] ? dd : "0" + dd[0] ) + " " + ( h[1] ? h : "0" + h[0] ) + ":" + ( i[1] ? i : '0' + i[0] ) + ":" + ( s[1] ? s : '0' + s[0]));
            break;
        case FORMAT_SLASH_DATE_DMY_HIS:
            formatedDate = (( dd[1] ? dd : "0" + dd[0] ) + "/" + ( mm[1] ? mm : "0" + mm[0] ) + "/" + yyyy + " " + ( h[1] ? h : "0" + h[0] ) + ":" + ( i[1] ? i : '0' + i[0] ) + ":" + ( s[1] ? s : '0' + s[0]));
            break;
        default:
            formatedDate = (yyyy + "-" + ( mm[1] ? mm : "0" + mm[0] ) + "-" + ( dd[1] ? dd : "0" + dd[0] ) + " " + ( h[1] ? h : "0" + h[0] ) + ":" + ( i[1] ? i : '0' + i[0] ) + ":" + ( s[1] ? s : '0' + s[0]));
    }
    return formatedDate;
}
function onlyNos(e, t) {
    try {
        logStatus(t, LOG_DEBUG);
        var charCode = '';
        if (window.event) {
            charCode = window.event.keyCode;
        } else if (e) {
            charCode = e.which;
        } else {
            return true;
        }
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46));

    } catch (e) {
        ShowExceptionMessage("onlyNos", e);
    }
}
function isInArray(key, value, array) {
    for (var index in array) {
        if (array.hasOwnProperty(index)) {
            var row = array[index];
            for (var k in row) {
                if (row.hasOwnProperty(k)) {
                    if (k == key && row[k] == value) {
                        return true;
                    }
                }
            }
        }

    }
    return false;
}
function is_array(mixed_var) {
    var ini,
        _getFuncName = function (fn) {
            var name = (/\W*function\s+([\w\$]+)\s*\(/).exec(fn);
            if (!name) {
                return '(Anonymous)';
            }
            return name[1];
        };
    var _isArray = function (mixed_var) {
        // return Object.prototype.toString.call(mixed_var) === '[object Array]';
        // The above works, but let's do the even more stringent approach: (since Object.prototype.toString could be overridden)
        // Null, Not an object, no length property so couldn't be an Array (or String)
        if (!mixed_var || typeof mixed_var !== 'object' || typeof mixed_var.length !== 'number') {
            return false;
        }
        var len = mixed_var.length;
        mixed_var[mixed_var.length] = 'bogus';
        // The only way I can think of to get around this (or where there would be trouble) would be to have an object defined
        // with a custom "length" getter which changed behavior on each call (or a setter to mess up the following below) or a custom
        // setter for numeric properties, but even that would need to listen for specific indexes; but there should be no false negatives
        // and such a false positive would need to rely on later JavaScript innovations like __defineSetter__
        if (len !== mixed_var.length) { // We know it's an array since length auto-changed with the addition of a
            // numeric property at its length end, so safely get rid of our bogus element
            mixed_var.length -= 1;
            return true;
        }
        // Get rid of the property we added onto a non-array object; only possible
        // side-effect is if the user adds back the property later, it will iterate
        // this property in the older order placement in IE (an order which should not
        // be depended on anyways)
        delete mixed_var[mixed_var.length];
        return false;
    };
    if (!mixed_var || typeof mixed_var !== 'object') {
        return false;
    }
    // BEGIN REDUNDANT
    this.php_js = this.php_js || {};
    this.php_js.ini = this.php_js.ini || {};
    // END REDUNDANT
    ini = this.php_js.ini['phpjs.objectsAsArrays'];
    return _isArray(mixed_var) ||
        // Allow returning true unless user has called
        // ini_set('phpjs.objectsAsArrays', 0) to disallow objects as arrays
        /**
         * @param ini.local_value
         */
        ((!ini || ( // if it's not set to 0 and it's not 'off', check for objects as arrays
            (parseInt(ini.local_value, 10) !== 0 && (!ini.local_value.toLowerCase
            || ini.local_value.toLowerCase() !== 'off')))) && (Object.prototype.toString.call(mixed_var) === '[object Object]'
            && _getFuncName(mixed_var.constructor) === 'Object' // Most likely a literal and intended as assoc. array
        ));
}
function getObject(data) {
    return data;
}
function stripslashes(_string) {
    return (_string + '').replace(/\\(.?)/g, function (s, n1) {
        switch (n1) {
            case '\\':
                return '\\';
            case '0':
                return '\u0000';
            case '':
                return '';
            default:
                return n1;
        }
    });
}
/*
 function _autoPostDataOnTimer(){
 if (_initTimerAfterPost){
 clearTimeout(_initTimerAfterPost);
 }
 _initTimerAfterPost =	setTimeout(function(){
 if(_IsNotCancelledAutoTimer){
 _isNeedForceDataUpdate=false;
 postDataForSynch();
 } else {
 clearInterval(_autoposttimer);
 } 
 },AUTO_POST_TIMEER_DELAY * 1000 + 1800);
 }
 */
function _showAutoPostTimer(onTimerCompletion) {
    var _x = 15;
    _autoposttimer = setInterval(function () {
        $("#timer").html(_x);
        if (_x == 0) {
            clearInterval(_autoposttimer);
            if (onTimerCompletion) {
                onTimerCompletion();
            }
        }
        _x--;
    }, 1000);
}
/**
 * @return {string}
 */
function GetCurrentOnlyDate() {
    return FormatedDate(new Date(), FORMAT_ONLY_DATE);
}
function getTotalFromObjectValues(_Object) {
    var _total = 0;
    for (var elem in _Object) {
        if (_Object.hasOwnProperty(elem)) {
            _total += ((_Object[elem]) && _Object[elem] != "" ) ? parseFloat(_Object[elem]) : 0;
        }
    }
    return _total;
}
function CheckAndBlockScreenNotSyncronized() {

}

function getWholeWord(givestring, MaxLimit) {
    var subtextslen = givestring.length;
    if (subtextslen > MaxLimit) {
        subtextslen = givestring.substr(0, MaxLimit).lastIndexOf("\n");
        if (subtextslen <= 0) {
            subtextslen = givestring.substr(0, MaxLimit).lastIndexOf(" ");
        }
        if (subtextslen <= 0) {
            subtextslen = MaxLimit;
        }
    }
    return givestring.substr(0, subtextslen);
}

function getSplittedTextsBasedonLimits(teststring, Maxlimit) {
    var charlength = teststring.length;
    //var currlen = 0;
    var balancetxt = teststring;
    var retrunvalue = "";
    //alert("teststring: " + teststring);
    while (balancetxt != "") {
        var subtexts = getWholeWord(balancetxt, Maxlimit);
        //alert("subtexts: " + subtexts);
        balancetxt = trim(balancetxt.substr(subtexts.length, charlength));
        if (retrunvalue != "") {
            var subtextslen = subtexts.indexOf("\n");
            if (subtextslen <= 0) {
                retrunvalue = retrunvalue + "\n" + subtexts;
            } else {
                retrunvalue = retrunvalue + subtexts;
            }
        } else {
            retrunvalue = subtexts;
        }
        //alert("retrunvalue " + retrunvalue);
        //alert("balancetxt: "+ balancetxt);
    }
    return retrunvalue;
}
function trim(txt) {
    return txt.replace(/^\s+|\s+$/g, "");
}

function pagetitelanimation() {
    setTimeout(function(){
        $(".comm_banner_img_box h4").animate({
           "bottom": 0
        });
    }, 500);
}

function footermeun_act() {
	
	$(".ftr_bx_two .menuspan").on("click" ,function(){
		$(".menuspan").removeClass("act");
		$(this).addClass("act");
	});
}

var pushNotification;

document.addEventListener("deviceready", function(){
    pushNotification = window.plugins.pushNotification;
});

/******************************************************************************************/
/********************************* Functions  - End ***************************************/
/******************************************************************************************/