/********************************Added**********************************************************/
/************************** WEBSERVICELAYER.JS VERSION 2.1 23/02/2013 *********************/
/******************************************************************************************/
/*
 var SERVICE_CLIENT_SYNC_DOCUMENT = "http://115.254.39.12:8080/AnsellMedicationTabletServices/service/Documents?VersionId=";//Version details will be taken from DB
 */
/*
 function ServiceCallFailure(request, status, error) {
 logStatus("Excuting ServiceCallFailure", LOG_COMMON_FUNCTIONS);
 /*if (request.status === 0) {
 alert('Not connect.\n Verify Network.');
 }*/
/*if (jqXHR.status === 0) {
 alert('Not connect.\n Verify Network.');
 } else if (jqXHR.status == 404) {
 alert('Requested page not found. [404]');
 } else if (jqXHR.status == 500) {
 alert('Internal Server Error [500].');
 } else if (exception === 'parsererror') {
 alert('Requested JSON parse failed.');
 } else if (exception === 'timeout') {
 alert('Time out error.');
 } else if (exception === 'abort') {
 alert('Ajax request aborted.');
 } else {
 alert('Uncaught Error.\n' + jqXHR.responseText);  
 }* /
 try{
 stopprogress();  
 HideLoader();
 //EnableWait();
 for (var i=0; i < 2; i++)
 {
 if (status === 0) {
 ShowToastMessageTrans(EXCEPTION_DATACONNECTION_LOST, _TOAST_LONG);                
 }else if (status > 0){                
 ShowToastMessageTrans(EXCEPTION_UNEXPECTED_PROCESSING_ERROR, _TOAST_LONG);                
 }
 }        
 console.log("Error status :" + status); 
 console.log("Error type :" + error);
 console.log("Error message :" + request.responseXML);
 console.log("Error request status text: " + request.statusText);
 console.log("Error request status: " + request.status);
 console.log("Error request response text: " + request.responseText);
 console.log("Error response header: " + request.getAllResponseHeaders());
 // HandleServiceUnavailable(); //** Need to handle this webservicelayer_app.js
 } catch (e) { 
 stopprogress();
 ShowExceptionMessage("ServiceCallFailure", e);
 }

 }
 */
/*
 function ServiceServerCallFailure(xhr, textStatus, errorThrown) {
 logStatus("Excuting ServiceServerCallFailure", LOG_COMMON_FUNCTIONS);
 try{
 stopprogress();    
 console.log("Error status :" + textStatus);
 console.log("Error type :" + errorThrown);
 console.log("Error message :" + xhr.responseXML);
 } catch (e) {
 stopprogress();
 ShowExceptionMessage("ServiceServerCallFailure", e);
 }
 }

 function executeWebserviceService(serviceName, successFuntion) {
 logStatus("Excuting executeWebserviceService", LOG_COMMON_FUNCTIONS);
 try{
 $.ajax({
 type: "GET",
 url: serviceName,
 dataType: "xml",
 async: true,
 success: successFuntion,
 error: ServiceCallFailure
 });
 } catch (e) {
 ShowExceptionMessage("executeWebserviceService", e);
 }
 }

 function executePHPWebserviceService(serviceName, successFuntion) {
 logStatus("Excuting executePHPWebserviceService", LOG_COMMON_FUNCTIONS);
 logStatus(serviceName, LOG_DEBUG);
 try{
 $.support.cors = true;  
 $.ajax({
 type: "GET",
 url: serviceName, 
 dataType: "json",
 async: true,
 success: successFuntion,
 error: ServiceCallFailure
 });
 } catch (e) {
 ShowExceptionMessage("executePHPWebserviceService", e);
 }
 }

 function executePHPWebservicePostService(serviceName, successFuntion, dataobj)
 {
 logStatus("Excuting executePHPWebservicePostService", LOG_COMMON_FUNCTIONS);	
 logStatus(serviceName, LOG_DEBUG);
 try{
 //ShowLoader();
 $.support.cors = true;
 $.ajax({
 type: "POST",
 url: serviceName,
 /*dataType: "json",* /
 async: true,
 data: {postdataobj: dataobj},
 success: successFuntion,
 error: ServiceCallFailure 
 }); 
 logStatus("service called..waiting..", LOG_DEBUG);		
 } catch (e) {
 ShowExceptionMessage("executePHPWebservicePostService", e);
 }
 }
 */

function do_service(get, postdata, responseSuccess, dataType, _customBase) {
    logStatus("Calling do_service", LOG_FUNCTIONS);
    try {
        var url = (_customBase) ? _customBase : URL_SERVICE;
        var urldata = "";
        console.log("Get Data" + JSON.stringify(get));
        if (!$.isEmptyObject(get)) {
            $.each(get, function (key, value) {
                urldata += key + '=' + encodeURI(value) + '&';
            });
            urldata = rtrim('&', urldata);
        }
        if (urldata != '') {
            url += "?_tk=" + url_encrypt(urldata);
        }
		console.log("POST URL" + url);
        var ajaxAction = {};
        ajaxAction.url = url;
        ajaxAction.type = 'post';
        ajaxAction.async = true;
        ajaxAction.data = postdata;
        ajaxAction.success = function(data){
		
            var JSONObj = {};
            try {
                JSONObj = data;
                if (typeof data !== object){
                    JSONObj = JSON.parse(data);
                }
            }catch(e) {

            }
            var decryptData = '';
            if (dataType == 'json'){
                decryptData = {};
            }
            if (JSONObj.data){
                decryptData = JSON.parse(CryptoJS.AES.decrypt(base64_decode(JSONObj.data.substring(1)), ENCKEY,
                    {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));

            }
            if (dataType == 'json'){
                try {
                    decryptData = JSON.parse(decryptData);
                }catch(e) {

                }
            }
           // console.log("RESPONSE DATA === "+JSON.stringify(decryptData));
            responseSuccess(decryptData);
        };
        //console.log("Requested URL" + url);
        //console.log("Posted Data" + JSON.stringify(postdata));
        ajaxAction.error = function (request, status, error) {
            console.log("Service Call Error");
            console.log("Requested URL" + url);
            console.log("Error status :" + status);
            console.log("Error type :" + error);
            console.log("Error message :" + request.responseXML);
            console.log("Error request status text: " + request.statusText);
            console.log("Error request status: " + request.status);
            console.log("Error request response text: " + request.responseText);
            console.log("Error response header: " + request.getAllResponseHeaders());
            console.log("Common network error. Please try again later");
            console.log("Posted Data" + JSON.stringify(postdata));
            EnableLastProcessBtn();
            RemoveProcessingLoader();
            HideLoader();
        };
        if (dataType && dataType != '') {
            ajaxAction.dataType = dataType;
        }
		console.log("HEADERS TOKEN=="+GEN_TOKEN);
		console.log("HEADERS DEVICE ID=="+_DEVICE_ID);
		console.log("HEADERS KEY=="+TOKEN_KEY);
		
		ajaxAction.headers = {
        'Token':GEN_TOKEN,
        'Deviceid':_DEVICE_ID,
        'Secretkey':TOKEN_KEY
		};
        $.ajax(ajaxAction);
    } catch (e) {
        ShowExceptionMessage("do_service", e);
    }
}
function do_service_upload(get, postdata, responseSuccess, dataType, _customBase) {
    logStatus("do_srvice_upload", LOG_FUNCTIONS);
    try {
        var url = (_customBase) ? _customBase : URL_SERVICE;
        var urldata = "";
        if (!$.isEmptyObject(get)) {
            $.each(get, function (key, value) {
                urldata += key + '=' + value + '&';
            });
            urldata = rtrim('&', urldata);
        }
        if (urldata != '') {
            url += "?_tk=" + url_encrypt(urldata);
        }
        var ajaxAction = {};
        ajaxAction.url = url;
        ajaxAction.type = 'post';
        ajaxAction.data = postdata;
        ajaxAction.async = true;
        ajaxAction.cache = false;
        ajaxAction.contentType = false;
        ajaxAction.processData = false;
        ajaxAction.success = responseSuccess;
        ajaxAction.error = function (request, status, error) {
            console.log("Service Call Error");
            console.log("Requested URL" + url);
            console.log("Error status :" + status);
            console.log("Error type :" + error);
            console.log("Error message :" + request.responseXML);
            console.log("Error request status text: " + request.statusText);
            console.log("Error request status: " + request.status);
            console.log("Error request response text: " + request.responseText);
            console.log("Error response header: " + request.getAllResponseHeaders());
            console.log("Common network error. Please try again later");
            console.log("Posted Data" + JSON.stringify(postdata));
            EnableLastProcessBtn();
            RemoveProcessingLoader();
        };
        if (dataType && dataType != '') {
            ajaxAction.dataType = dataType;
        }
		console.log("HEADERS TOKEN=="+GEN_TOKEN);
		console.log("HEADERS DEVICE ID=="+_DEVICE_ID);
		console.log("HEADERS KEY=="+TOKEN_KEY);
		
		ajaxAction.headers = {
        'Token':GEN_TOKEN,
        'Deviceid':_DEVICE_ID,
        'Secretkey':TOKEN_KEY
		};
        $.ajax(ajaxAction);
    } catch (e) {
        ShowExceptionMessage("do_service_upload", e);
    }
}

function AppendURLWithGetData(url, get) {
    var urldata = "";
    if (!$.isEmptyObject(get)) {
        $.each(get, function (key, value) {
            urldata += key + '=' + value + '&';
        });
        urldata = rtrim('&', urldata);
    }
    if (urldata != '') {
        url += "?_tk=" + url_encrypt(urldata);
    }
    return url;
}

function url_encrypt(urldata) {
    logStatus("Calling url_encrypt", LOG_FUNCTIONS);
    try {
        return base64_encode(urldata);
    } catch (e) {
        ShowExceptionMessage("url_encrypt", e);
    }
}


/* Start - Server to Client Sync */

/* Start Document */
/* Sample Code */
/*
 function getServiceDocumentDataXML() {
 executeWebserviceService(ServiceWithVersion, getServiceDocumentDataXMLSucess);
 }

 function getServiceDocumentDataXMLSucess(xml) {

 }
 */
/* End - Server to Client Sync */

/******************************
       Notification
**********************************/

function do_notification(get, postdata, responseSuccess, dataType, _customBase) {
    logStatus("Calling do_notification", LOG_FUNCTIONS);
    try {
        var url = (_customBase) ? _customBase : URL_SERVICE_NOTI;
        var urldata = "";
        console.log("Get Data" + JSON.stringify(get));
        if (!$.isEmptyObject(get)) {
            $.each(get, function (key, value) {
                urldata += key + '=' + encodeURI(value) + '&';
            });
            urldata = rtrim('&', urldata);
        }
        if (urldata != '') {
            url += "?_tk=" + url_encrypt(urldata);
        }
  console.log("POST URL" + url);
        var ajaxAction = {};
        ajaxAction.url = url;
        ajaxAction.type = 'post';
        ajaxAction.async = true;
        ajaxAction.data = postdata;
        ajaxAction.success = function(data){
  
            var JSONObj = {};
            try {
                JSONObj = data;
                if (typeof data !== object){
                    JSONObj = JSON.parse(data);
                }
            }catch(e) {

            }
            var decryptData = '';
            if (dataType == 'json'){
                decryptData = {};
            }
            if (JSONObj.data){
                decryptData = JSON.parse(CryptoJS.AES.decrypt(base64_decode(JSONObj.data.substring(1)), ENCKEY,
                    {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));

            }
            if (dataType == 'json'){
                try {
                    decryptData = JSON.parse(decryptData);
                }catch(e) {

                }
            }
           // console.log("RESPONSE DATA === "+JSON.stringify(decryptData));
            responseSuccess(decryptData);
        };
        console.log("Requested URL" + url);
        console.log("Posted Data" + JSON.stringify(postdata));
        ajaxAction.error = function (request, status, error) {
            console.log("Service Call Error");
            console.log("Requested URL" + url);
            console.log("Error status :" + status);
            console.log("Error type :" + error);
            console.log("Error message :" + request.responseXML);
            console.log("Error request status text: " + request.statusText);
            console.log("Error request status: " + request.status);
            console.log("Error request response text: " + request.responseText);
            console.log("Error response header: " + request.getAllResponseHeaders());
            console.log("Common network error. Please try again later");
            console.log("Posted Data" + JSON.stringify(postdata));
            EnableLastProcessBtn();
            RemoveProcessingLoader();
            HideLoader();
        };
        if (dataType && dataType != '') {
            ajaxAction.dataType = dataType;
        }
  console.log("HEADERS TOKEN=="+GEN_TOKEN);
  console.log("HEADERS DEVICE ID=="+_DEVICE_ID);
  console.log("HEADERS KEY=="+TOKEN_KEY);
  
  ajaxAction.headers = {
        'Token':GEN_TOKEN,
        'Deviceid':_DEVICE_ID,
        'Secretkey':TOKEN_KEY
  };    
  
       
        $.ajax(ajaxAction);
    } catch (e) {
        ShowExceptionMessage("do_notification", e);
    }
}