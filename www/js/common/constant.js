/******************************************************************************************/
/************************** CONSTANT.JS VERSION 2.0 14/02/2013 ******************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* FILE UTILS  - Start ************************************/
/******************************************************************************************/
var isTouch = ('ontouchstart' in window);
var START_EVENT = isTouch ? 'touchstart' : 'mousedown';
var MOVE_EVENT = isTouch ? 'touchmove' : 'mousemove';
var END_EVENT = isTouch ? 'touchend' : 'mouseup';

var APPLICATION_DIR = "movesmartclient/";//APPLICATION_NAME;//addSlash(APPLICATION_NAME);
var DIR_GENERAL = "General/";

/******************************************************************************************/
/********************************* FILE UTILS  - End   ************************************/
/******************************************************************************************/
var DIR_GENERAL_INDEX = 1;
/******************************************************************************************/
/********************************* APPLICATOIN LOGS  - Start ******************************/
/******************************************************************************************/

var APPLICATION_LOG_LEVEL = 4;
var LOG_EVENTS = 1;
var LOG_FUNCTIONS = 2;
var LOG_DEBUG = 3;
var LOG_COMMON_FUNCTIONS = 4;

var SHOW_EXCEPTION_ALERT = 1; //Set 1 to receive alert message when there is an exception or 0 to Just Log the exception (Invisible to the user)
/******************************************************************************************/
/********************************* APPLICATOIN LOGS  - END   ******************************/
/******************************************************************************************/


/******************************************************************************************/
/********************************* Translations  - Start **********************************/
/******************************************************************************************/
var __LangXmlData;
var __LangXmlDatastr_Default = '';//'<translations> <en> <trans id="countryLabel">Country</trans> <trans id="amtTitle">AMT <![CDATA[<sup>TM</sup>]]>Antimikrobielle Technologie</trans> <trans id="msg_sync_progress_doc_xml">Generating documents XML..</trans> </en> <de> <trans id="countryLabel">Land</trans> <trans id="amtTitle">AMT <![CDATA[<sup>TM</sup>]]>Antimikrobielle Technologie</trans> <trans id="msg_sync_progress_doc_xml">Generating documents XML..</trans> </de> <fr> <trans id="countryLabel">Pays</trans> <trans id="amtTitle">AMT <![CDATA[<sup>TM</sup>]]>Technologie Antimicrobienne</trans> <trans id="msg_sync_progress_doc_xml">Generating documents XML..</trans> </fr> </translations>';

/******************************************************************************************/
/********************************* Translations  - End **********************************/
/******************************************************************************************/


/******************************************************************************************/
/*********************************   - Start ******************************/
/******************************************************************************************/


var CLIENT_SYNC_DET = '1';
var SERVER_SYNC_DET = '2';
var SYNC_STATUS_PENDING = -1;
var SYNC_STATUS_SUCCESS = 1;
var SYNC_STATUS_ERROR = 2;
