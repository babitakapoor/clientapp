var pushNotification = null;
function RegisterPushNotification(){
    try 
    { 
        if (!window.plugins.pushNotification){
            window.plugins.pushNotification = new PushNotification();
        }            
        pushNotification = window.plugins.pushNotification;
        pushNotification.register(successPushHandler, errorPushHandler, {"senderID":"267534559020","ecb":"onNotificationGCM"});
        //ShowAlertMessage("initiating register..");
    }
    catch(e) 
    { 
        ShowExceptionMessage("RegisterPushNotification", e); 
    }     
}

function onNotificationGCM(e) {
    //ShowAlertMessage("e.event " + e.event);
    //$("#app-status-ul").append('<li>EVENT -> RECEIVED:' + e.event + '</li>');    
    switch( e.event )
    {
        case 'registered':
        if ( e.regid.length > 0 )
        {
            _PUSH_TOKEN = e.regid;
            //ShowAlertMessage("info:" + _PUSH_TOKEN);
            //$("#app-status-ul").append('<li>REGISTERED -> REGID:' + e.regid + "</li>");
            console.log("regID = " + e.regID);
            // Your GCM push server needs to know the regID before it can push to this device
            // here is where you might want to send it the regID for later use.
        }
        break;
        
        case 'message':
            setTimeout(function(){
                ShowAlertNWaitMessage(e.payload.message, function(){
                    var messagetype = 0;
                    var actionpage = "";
                    var actionpageid = 0;
                    if (e.payload.notificationtype){
                        messagetype = e.payload.notificationtype;
                    }    
                    if (e.payload.actionoptpage){
                        actionpage = e.payload.actionoptpage;
                    }    
                    if (e.payload.actionextra){
                        actionpageid = e.payload.actionextra;
                    }    
                    /*
                    if (messagetype == NOTIFICATION_TYPE_REFERRAL_POINT_RECEIVED){
                        if (e.payload.totalrewards){
                            UpdateCustomerRewardPoints(e.payload.totalrewards);
                        }
                    }   
                    if (messagetype == NOTIFICATION_TYPE_RESTRO){
                            isvaliddata = 0;
                            if(HOTEL_OBJ){
                                if (HOTEL_OBJ.HotelMenuCat){
                                    if (HOTEL_OBJ.HotelMenuCat.length > 0){
                                        isvaliddata = 1;
                                    }                            
                                }
                            }
                            setTimeout(function(){
                                if (isvaliddata == 1){
                                    processNotifications(actionpage, actionpageid);                                        
                                }else{
                                    SetActivePageLoadHandler(function(){
                                        processNotifications(actionpage, actionpageid);    
                                        updateSplashScreen();                                
                                    });                                 
                                }
                            }, 250);                                                
                    } 
                    */                       
                    // if this flag is set, this notification happened while we were in the foreground.
                    // you might want to play a sound to get the user's attention, throw up a dialog, etc.
                    if (e.foreground)
                    {
                        ShowAlertMessage(e.msg);
                        //$("#app-status-ul").append('<li>--INLINE NOTIFICATION--' + '</li>');
                        
                        // if the notification contains a soundname, play it.
                        /*
                        var my_media = new Media("/android_asset/www/"+e.soundname);
                        my_media.play();
                        */
                        
                        
                        
                        
                        /*
                        if (messagetype == NOTIFICATION_TYPE_STATUS_UPDATE){
                            CheckCartProgressStatusFromServer();                    
                        }
                        */
                        /*
                        alert(e.payload.message);
                        alert(e.payload.msgcnt);
                        */                    
                    }
                    else 
                        if (e.coldstart){
                            
                        }
                        else{
                            
                        }
                        // otherwise we were launched because the user touched a notification in the notification tray.
                        //$("#app-status-ul").append('<li>--BACKGROUND NOTIFICATION--' + '</li>');
                        /*
                    $("#app-status-ul").append('<li>MESSAGE -> MSG: ' + e.payload.message + '</li>');
                    $("#app-status-ul").append('<li>MESSAGE -> MSGCNT: ' + e.payload.msgcnt + '</li>');
                    */                    
                });                
            }, 500);
        break;
        
        case 'error':
            ShowAlertMessage("error: "+e.msg);
            //    alert(e.msg);
            //$("#app-status-ul").append('<li>ERROR -> MSG:' + e.msg + '</li>');
        break;
        
        default:
            ShowAlertMessage('Unknown, an event was received and we do not know what it is');
            //$("#app-status-ul").append('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
        break;
    }
}

function successPushHandler (result) {
    //ShowAlertMessage("result: " + result);
    console.log("result: " + result);
}

function errorPushHandler (error) {
    //ShowAlertMessage("error: "+error);
    console.log(error);
}
