/******************************************************************************************/
/********************************* COMMON Layer - Application Common - Start ******************/
/******************************************************************************************/
var IS_APP_PAUSED = 0;
/*Application Common variables*/
var _Questierdatainfo;
var _Generalquestions;
var _PageContentDetails;
var _QuestionSliderObj = {};
var _QuestionSliderObjIndex = 0;
var _DashboardSliderObj = {};
//var dashboardpage={};
//var _TimerShowIndexValue;

var _IsLastQuestionAnswered = false;
var _LoginInfo = {};
var _LoggedUser = {};
var LANGUAGE = {};
var LAST_LOADER_ID;
var LAST_DIS_ID;
var FIELDTYPE;
var LAST_DIS_ID;
var APP_VERSION;
var _DBVERSION;
var SEARCH_CLUBS = {};
//var APP_LINK = "movesmartclientapp://?username="+base64_encode('aneethashankar@gmail.com')+"&pwd="+base64_encode('123');
var LAMP_POPUP_CNT = '<div class="lamp_popup" >' +
    '<div class="lamp_popup_header" >' +
    '<span class="light_icon"></span>' +
    '<span class="lamp_popup_close" onclick="" >>></span>' +
    '</div><div class="lamp_popup_ctn">' +
    '<p class="comm_para" >The best medicine</p>' +
    '<p class="comm_para" >Being active is perhaps the cheapest and most accessible ' +
    'medicine, but eating healthy is the best medicine. ' +
    'That\'s why it\'s great you\'ve started monitoring the way ' +
    'you eat and drink. </p>' +
    '<p class="comm_para" >Just like a car, the things you eat and drink are your fuel. ' +
    'Using the wrong fuel leads to a malfunctioning engine, ' +
    'just like unhealthy eating leads to malfunctioning of your ' +
    'body. But your body is more special than a car, the food' +
    'you have is not only your fuel but your body is also made ' +
    'from food!</p>' +
    '<p class="comm_para" >"You are your food"</p>' +
    '<div class="hints_img">' +
    '<img src="images/hints_img1.png" alt="hints_img1" />' +
    '</div></div>' +
    '<button class="white_btn close_btn"  onclick="">Close</button></div>';
/*var COACH_DETAIL_PARA = '<p class="comm_para">'+
 '<p class="comm_para">Your MOVESMART.coach will contact you as soon as possible.</p>'+
 '<p class="comm_para">In the mean time, don\'t forget your goal.Enter your steps in the 10.000 stepgame.'+
 MARKER_QUERY_PAGEBREAK+'<p class="comm_para">'+
 'Try to stand up more and move around, don\'tkeep seated the whole time.Don\'t forget to drink water and eat a small'+
 'healthy snack.Your sleep is a crusial part of your life, makesure your sleep enough.</p>';*/
var lastTimeBackPress=0;
var timePeriodToExit=2000;
function ApplicationReady() {
    logStatus("ApplicationReady", LOG_COMMON_FUNCTIONS);
    try {
        setTimeout(function () {
            HideSplashScreen();
        }, 3000);
        IS_APP_PAUSED = 0;
        _IS_SYNC_PROGRESS = 1;
        HandleQuestion_ActivePage();
        pageHeight();
    } catch (e) {
        ShowExceptionMessage("ApplicationReady", e);
    }
}

/* Events */
function ExecutePause() {
    logStatus("ExecutePause", LOG_COMMON_FUNCTIONS);
    try {
        //xx IS_APP_PAUSED = 1;
    } catch (e) {
        ShowExceptionMessage("onResume", e);
    }
    // Handle the pause event
}

function ExecuteResume() {
    logStatus("ExecuteResume", LOG_COMMON_FUNCTIONS);
    try {
        IS_APP_PAUSED = 0;
        //XX getServerdata();
    } catch (e) {
        stopprogress();
        ShowExceptionMessage("ExecuteResume", e);
    }
    // Handle the resume event
}

function ExecuteOnline() {
    logStatus("ExecuteOnline", LOG_COMMON_FUNCTIONS);
    try {
    } catch (e) {
        ShowExceptionMessage("ExecuteOnline", e);
    }
    // Handle the online event
}

function ExecuteOffline() {
    logStatus("ExecuteOffline", LOG_COMMON_FUNCTIONS);
    try {
    } catch (e) {
        ShowExceptionMessage("ExecuteOffline", e);
    }
    // Handle the offline event
}

//Only on Andriod
function ExecuteBackKeyDown() {
    logStatus("ExecuteBackKeyDown", LOG_COMMON_FUNCTIONS);
    try {
        /**
         * @param navigator.app This is navigator app
         * @param navigator.app.backHistory() This is navigator app back history function
         */
        if (GLOBAL_DIALOG) {
            if (GLOBAL_DIALOG.close) {
                GLOBAL_DIALOG.close();
            }
        }
        //navigator.app.backHistory();
		if(new Date().getTime() - lastTimeBackPress < timePeriodToExit){
                navigator.app.exitApp();
        }else{
          ShowToastMessage(APP_EXIT,_TOAST_SHORT);
        lastTimeBackPress=new Date().getTime();
              }
        /*
         ShowAlertConfirmMessage("Are you sure you want to EXIT the app?", function(selbtn){
         if (selbtn == 2){
         if (_ISIN_PROGRESS == 1){
         stopprogress();
         setTimeout(function(){
         IS_APP_PAUSED = 1;
         navigator.app.exitApp();
         }, 2000);
         }else{
         IS_APP_PAUSED = 1;
         navigator.app.exitApp();                    
         }                
         }  
         }, 'Cancel,OK');
         */
    } catch (e) {
        ShowExceptionMessage("ExecuteBackKeyDown", e);
    }
    // Handle the back button    
    console.log("back");
}

function ExecuteBatteryCritical(info) {
    logStatus("ExecuteBatteryCritical", LOG_COMMON_FUNCTIONS);
    logStatus(info, LOG_DEBUG);
    try {
    } catch (e) {
        ShowExceptionMessage("ExecuteBatteryCritical", e);
    }
    // Handle the battery critical event
    //alert("Battery Level Critical " + info.level + "%\nRecharge Soon!"); 
}

//function ExecuteBatteryLow(info) {
function ExecuteBatteryLow() {
    logStatus("ExecuteBatteryLow", LOG_COMMON_FUNCTIONS);
    try {
    } catch (e) {
        ShowExceptionMessage("ExecuteBatteryLow", e);
    }
    // Handle the battery low event
    //alert("Battery Level Low " + info.level + "%"); 
}

function ExecuteBatteryStatus(info) {
    logStatus("ExecuteBatteryStatus", LOG_COMMON_FUNCTIONS);
    try {
    } catch (e) {
        ShowExceptionMessage("ExecuteBatteryStatus", e);
    }
    // Handle the online event
    console.log("Level: " + info.level + " isPlugged: " + info.isPlugged);
}

function ExecuteMenuKeyDown() {
    logStatus("ExecuteMenuKeyDown", LOG_COMMON_FUNCTIONS);
    try {
    } catch (e) {
        ShowExceptionMessage("ExecuteMenuKeyDown", e);
    }
    // Handle the back button
}

function ExecuteSearchKeyDown() {
    logStatus("ExecuteSearchKeyDown", LOG_COMMON_FUNCTIONS);
    try {
    } catch (e) {
        ShowExceptionMessage("ExecuteSearchKeyDown", e);
    }
    // Handle the search button
}

function getValidStringFromHTMLCode(value) {
    if (value) {
        return value.replace(/amp;/g, "").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&#039;/g, "'").replace(/<br>/g, "\n");
    }
    return "";
}
function escapeSqlString(strInputString) {
    return strInputString.replace(/'/g, "\\'");
}
function getSQLSafeString(value) {
    if (value) {
        var escaped1 = getValidStringFromHTMLCode(value);
        return escaped1.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
    } else {
        return "";
    }
}
/**/

$(document).ready(function () {
                   $body = $("body");
                  $(document)
                  .on('focus', 'input', function() {
                      $body.addClass('fixfixed');
                      //alert('asfaf')
                      })
                  .on('blur', 'input', function() {
                      $body.removeClass('fixfixed');
                      //alert('xxxx')
                      });
    logStatus("Calling Document.Event.Ready", LOG_FUNCTIONS);
    try {
        //_Questierdatainfo
        //_PageContentDetails
        //Splash Screen Init Loading..
		/*
        LoadApplicationData(function () {
            //FB Login
            openFB.init({appId: '946205342143418', tokenStore: window.localStorage});
            //Remove - Splash Screen Init loading completed
            var RedeemCredits = GetCredits();
            //XX var selectedGroupID	=	getlastActiveGroupId();
            //XX var RedeemCredits	=	GetCredits(selectedGroupID);
            updateLoginUserOnRefresh(function () {
                //Need to be used for user data update later.
            });
            if (RedeemCredits.redeemable != 0) {
                //Need to workout
                _movePageTo('phase-credits');
            }
            initPageTemplate();
            $(window).bind('hashchange', function () {
                initPageTemplate();
            });
        });
		*/
        //handleOpenURL(APP_LINK);
        //MK Need to Change later
        $(".ftr_bx_one").hide();
        $(".ftr_bx_two").show();
        //MK Need to change later - end
    } catch (e) {
        ShowExceptionMessage("Document.Event.Ready", e);
    }
});
$(document).delegate('.formfocus_ipentry input ,.questions_list .input_blue', "blur", function () {
    var $this = $(this);
    var isfilled = false;
    var checkemptyonval = 'checkemptyonval';
    var selectchrbox = '.selectchrbox';
    var ipfilled = 'ipfilled';
    if ($this.is('select')) {
        var checkval = ($this.attr(checkemptyonval)) ? $this.attr(checkemptyonval) : 0;
        if ($this.val() != checkval) {
            isfilled = true
        }
    } else {
        if ($this.val() != '') {
            isfilled = true
        }
    }
    if (isfilled) {
        $this.siblings(selectchrbox).addClass(ipfilled);
        $this.siblings('label').addClass(ipfilled);
        $this.addClass(ipfilled);
    } else {
        $this.siblings(selectchrbox).removeClass(ipfilled);
        $this.siblings('label').removeClass(ipfilled);
        $this.removeClass(ipfilled);
    }
});
$(document).delegate('.formfocus_ipentry select', "change", function () {
    var $this = $(this);
    var isfilled = false;
    var checkemptyonval = 'checkemptyonval';
    var selectchrbox = '.selectchrbox';
    var ipfilled = 'ipfilled';
    if ($this.is('select')) {
        var checkval = ($this.attr(checkemptyonval)) ? $this.attr(checkemptyonval) : 0;
        if ($this.val() != checkval) {
            isfilled = true
        }
    } else {
        if ($this.val() != '') {
            isfilled = true
        }
    }
    if (isfilled) {
        $this.siblings(selectchrbox).addClass(ipfilled);
        $this.siblings('label').addClass(ipfilled);
        $this.addClass(ipfilled);
    } else {
        $this.siblings(selectchrbox).removeClass(ipfilled);
        $this.siblings('label').removeClass(ipfilled);
        $this.removeClass(ipfilled);
    }
});
$(document).delegate('.applyucfirst', 'keyup', function () {
    var $this = $(this);
    var value = $this.val();
    $this.val(value.toString().to_ucfirst());
});
$(document).delegate(".selectdropdown", "change", function () {
    var selecttext = $(this).find("option:selected").text();
    $(this).siblings('.selectchrbox').html(selecttext);
});
$(document).delegate(".checkouter", "click", function () {
    if ($(this).find("input").is(':checked')) {
        $(this).find("input").removeAttr('checked');
        $(this).removeClass("checkbtn");
    } else {
        $(this).find("input").attr('checked', 'checked');
        $(this).addClass("checkbtn");
    }
});

$(document).delegate('.radiobox', "click", function () {
    var $this = $(this);
    var $assoc = $this.find('input[type="radio"]');
    var _class = $assoc.attr("class");
    var $class = $('.' + _class);
    $class.parent().removeClass("radiobtn");
    if ($assoc.is(":checked")) {
        $assoc.removeAttr("checked");
        $this.removeClass("radiobtn");
    } else {
        $assoc.attr("checked", "true");
        $this.addClass("radiobtn");
    }
    if (_class && _class != "") {
        $class.trigger("change");
    }
});
/*Edison */
$(document).delegate(".home_menulist_box li, .menu_mask_bg,.popupmask,.lamp_popup_close, .close_btn ", "click", function () {
    $(".menu_side_box").hide().animate({right: '-200px'});
    $(".menu_mask_bg").hide();
    $(".popupmask").hide();
    $(".popup").hide();
    $(".lamp_popup").hide();
    $(".comm_popup_inner ").hide();
    $(".Newcountinfo").hide();
});
$(document).delegate(".menu_icon", "click", function () {
    if (!is_login()) {
        var delay = 100;
        var $menu_side_box = $('.menu_side_box');
        var $menu_mask_bg = $('.menu_mask_bg');
        if ($menu_side_box.is(':visible')) {
            $menu_side_box.delay(delay).animate({right: '-200px'});
            setTimeout(function () {
                $menu_mask_bg.hide();
                $menu_side_box.hide();
            }, 500);
        } else {
            $menu_side_box.delay(delay).show().animate({right: '0'});
            setTimeout(function () {
                $menu_mask_bg.show();
            }, 500);
        }
    }
});
$(document).delegate(".home_menulist_box li, .menu_mask_bg", "click", function () {
    $(".menu_side_box").hide();
    $(".menu_mask_bg").hide();
});

/*SN added profile pic 20160321*/
$(document).delegate(".profileimages ", "click", function () {
    if (!IsWebLoad()) {
        $(".choosephoto_popup").show();
    } else {
        uploadProfileImage();
    }
});

$(document).delegate(".choosephoto_popup .close_btn", "click", function () {
    $(".choosephoto_popup.clspopupscreen").hide();
});
/*ED Popup For Time Reminder*/
$(document).delegate(".daily_reminder", "click", function () {
    $(".timeboxpopup_outer").show();
});

$(document).delegate(".stepokbtn", "click", function () {
    $(".timeboxpopup_outer").hide();
});
/*Time reminder popup ends */
/*Profile Picture - Upload*/
$(document).delegate(".capturephotoopt", "touchstart", function () {
    logStatus("Calling capturephotoopt touchstart event", LOG_COMMON_FUNCTIONS);
    try {
        var selectedphotoopt = parseInt($(this).attr("selectopt"));
        if (!selectedphotoopt) {
            selectedphotoopt = 0;
        }
        var $choosephoto_popup = $('.choosephoto_popup');
        /**
         * @param Camera This is Camera event
         * @param Camera.EncodingType This is Camera event Encoding Type
         * @param Camera.EncodingType.JPEG This is Camera event Encoding Type JPEG
         */
        getCurrentCapturedImage(function (fileurl) {
                $choosephoto_popup.hide();
                UploadProfilePic(fileurl, URL_SERVICE);
            }, function () {
                $choosephoto_popup.hide();
                ShowAlertMessage("Capture Failed");
                //Need to handle the failure
            }, {
                quality: 25, allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 200,
                targetHeight: 200, sourceType: selectedphotoopt
            }
        );
    } catch (e) {
        ShowExceptionMessage("capturephotoopt touchstart", e);
    }
});
$(document).delegate("#profilepic", "change", function () {
    logStatus("Calling Event#profilepic.Change", LOG_FUNCTIONS);
    try {
        UploadProfilePic('', null);
    } catch (e) {
        ShowExceptionMessage("Event#profilepic.Change", e);
    }
});
/*SN Color Change On Select Answer*/
$(document).delegate(".imgcls_clk", "click", function () {
    $(this).parent().find('.imgcls_clk').removeClass("checked");
    /*$(this).parent().find('.imgcls_clk').each(function(i,j){
     alert($(j).attr("class"));
     })*/
    $(this).addClass("checked");
});
/*SN onclick Tips - Popup */
$(document).delegate(".lamp_icon", "click", function () {
    _popup(LAMP_POPUP_CNT, {close: false});
    $("#openeatfresh_label").find(".Newcountinfo").show();
});
/*Added TEmporary contents popup*/
$(document).delegate(".showresulttipef", "click", function () {
    var prefgroup = GetPreferredGroup();
    if (prefgroup == GROUP_EATFRESH) {
        /*Do New content*/
    } else {

    }
    _popup('<div class="lamp_popup" >' +
        '<div class="lamp_popup_header" >' +
        '<span class="light_icon"></span>' +
        '<span class="lamp_popup_close" onclick="" >>></span>' +
        '</div>' +
        '<p class="comm_para">Eating healthy is the best medicine. Not sure if it\'s something you should change?</p><br><p class="comm_para">EATFRESH, a healthy choice?</p><p class="comm_para">The easiest way to find out whether you should change the way you eat and drink is to start the EATFRESH diary!</p>' +
        '</p>' +
        '<p class="comm_para">What advice should you follow and when are guidelines revised again?</p>' +
        '<div class="hints_img">' +
        '<img src="images/hints_img2.png" alt="hints_img3" />' +
        '</div><button class="white_btn close_btn"  onclick="">Close</button></div>', {close: false});
});
$(document).delegate(".showresulttipms", "click", function () {
    var prefgroup = GetPreferredGroup();
    if (prefgroup == GROUP_MINDSWITCH) {
        /*Do New content*/
    } else {

    }
    _popup('<div class="lamp_popup" >' +
        '<div class="lamp_popup_header" >' +
        '<span class="light_icon"></span>' +
        '<span class="lamp_popup_close" onclick="" >>></span>' +
        '</div>' +
        '<p class="comm_para"><p class="comm_para">Having energy is a must when dealing with daily hassles. If you are not sure what effects your state of energy we advise you to do the MIND diary. </p><p class="comm_para">MINDSWITCH? Should I change?</p><p class="comm_para">The easiest way to find out whether you should change the way your energy level is to start the MINDSWITCH diary!</p>' +
        '</p>' +
        '<div class="hints_img">' +
        '<img src="images/hints_img4.png" alt="hints_img2" />' +
        '</div><button class="white_btn close_btn"  onclick="">Close</button></div></div>', {close: true});
});
/*Document Element Events - Ends*/
function LoadAllQuestionerdata(params, handleOnsuccess) {
    logStatus("Calling LoadAllQuestionerdata", LOG_FUNCTIONS);
    try {
        /**
         * @param response.data.grouplist This is group list data
         */
        _loadClientQuestionaries(params, function (response) {
            if (response.data && response.data.grouplist) {
                UpdateQuestionariesObjectWithGeneralQuestions(response.data);  
            }
            if (handleOnsuccess) {
                handleOnsuccess()
            }
        });
    } catch (e) {
        ShowExceptionMessage("LoadAllQuestionerdata", e);
    }
}
function GetAllPageContentData(onLoadPageContentSuccess) {
    logStatus("Calling GetAllPageContentData", LOG_FUNCTIONS);
    try {
        _loadAllPageContents({}, function (response) {
            if (response) {
                _PageContentDetails = response;
            }
            if (onLoadPageContentSuccess) {
                onLoadPageContentSuccess();
            }
        });
    } catch (e) {
        ShowExceptionMessage("GetAllPageContentData", e);
    }
}
function UpdateDynamicPageForId(selector, pageid, onSucessHandler) {
    logStatus("Calling UpdateDynamicPageForId", LOG_FUNCTIONS);
    try {
        var pagecontent = _PageContentDetails[USER_LANG];
        var HTML = '';
        var CNT = '';
        var pagetitle = '';
        /*MK - It will not come as mulptiple - so need to fix later*/
        if (!$.isEmptyObject(pagecontent)) {
			//console.log(pagecontent);
            var epage = pagecontent[pageid];
            if (!$.isEmptyObject(epage)) {
                $.each(epage, function (i, row) {
                    HTML += '<div class="home_slider_panel swiper-slide">' + row.content + '</div>';
                    CNT += row.content;
                    pagetitle = row.t_title;
                   // $(".comm_banner_img_box h4").text(pagetitle);
                });
                $(selector).html(HTML);
            }else{
                $(selector).html("");
            }
        } else {
            $(selector).html("");
            //ShowToastMessage("The intro content is un-available in you selected language")
        }
        if (onSucessHandler) {
            onSucessHandler(CNT,pagetitle);
        }
    } catch (e) {
        ShowExceptionMessage("UpdateDynamicPageForId", e);
    }
}
function updateUserInfoLocal(data, onSuccessUpdateLocal) {
    logStatus("Calling updateUserInfoLocal", LOG_FUNCTIONS);
    try {
        var UserLoginInfo = getCurrentLoginInfo();
        var User = getCurrentUser();
        if ((!$.isEmptyObject(User))) {
            $.each(data, function (key, value) {
                if (User.hasOwnProperty(key)) {
                    User[key] = value;
                }
            });
            UserLoginInfo.user = User;
            
        }
        setCurrentUserLoginInfo(UserLoginInfo);
        if (onSuccessUpdateLocal) {
            onSuccessUpdateLocal();
        }
    } catch (e) {
        ShowExceptionMessage("updateUserInfoLocal", e);
    }
}
function setCurrentUserLoginInfo(login_info) {
    logStatus("Calling setCurrentUserLoginInfo", LOG_FUNCTIONS);
    try {
        _LoginInfo = login_info;
        _LoggedUser = login_info.user;
        var cookieData = _getCacheArray('login_info');
        if (!$.isEmptyObject(cookieData)) {
            _setCacheArray('login_info', _LoginInfo);
            _setCacheArray('user', _LoggedUser);
        }
    } catch (e) {
        ShowExceptionMessage("setCurrentUserLoginInfo", e);
    }
}
/**
 * @return {boolean}
 */
function IsCompletedProfile() {
    logStatus("IsCompletedProfile", LOG_FUNCTIONS);
    try {
        var cuserinfo = getCurrentUser();
        return !!((cuserinfo.dob) && (cuserinfo.dob != '0000-00-00') && (cuserinfo.dob != ''));
        /* We will implement the below code later * /
         var st		=	GetUserCompletedSteps();
         var tsteps	=	TOTAL_CLIENT_REGSTEPS_wo_medical;
         if(cuserinfo.isclubmedicalinfo && cuserinfo.isclubmedicalinfo==1){
         tsteps=	TOTAL_CLIENT_REGSTEPS;
         }
         if (tsteps <= st) {
         } else {
         app_tempvars('compregstep', st);
         return false;
         }
         return true;
         */
    } catch (e) {
        ShowExceptionMessage("IsCompletedProfile", e);
    }
}
/**
 * @return {number}
 */
/*
 function GetUserCompletedSteps(){
 logStatus("Calling GetUserCompletedSteps", LOG_FUNCTIONS);
 try {
 var st	=	0;
 var cuserinfo	= getCurrentUser();
 if(cuserinfo.personal_track && cuserinfo.personal_track!=''){
 var completedstep	=	JSON.parse(cuserinfo.personal_track);
 st	=	(completedstep && completedstep.step) ? completedstep.step:0;
 }
 return st;
 } catch(e){
 ShowExceptionMessage("GetUserCompletedSteps", e);
 }
 }
 */
//function profileIncomplete(redirectpage){
function profileIncomplete() {
    logStatus("Calling profileIncomplete", LOG_FUNCTIONS);
    try {
        /*if(IsCompletedProfile()){
         alert(1);*/
        HandleQuestion_ActivePage();
        /*} else {
         alert(2);
         app_tempvars('compregstep',GetUserCompletedSteps());
         }*/
    } catch (e) {
        ShowExceptionMessage("profileIncomplete", e);
    }
}

function getCurrentUser() {
    logStatus("Calling getCurrentUser", LOG_FUNCTIONS);
    try {
        if (!$.isEmptyObject(_LoggedUser)) {
            return _LoggedUser;
        }
        _LoginInfo = _getCacheArray('login_info');
        _LoggedUser = _getCacheArray('user');
        return _LoggedUser;
    } catch (e) {
        ShowExceptionMessage("getCurrentUser", e);
    }
}
function getCurrentLoginInfo() {
    logStatus("Calling getCurrentLoginInfo", LOG_FUNCTIONS);
    try {
        if (!$.isEmptyObject(_LoginInfo)) {
            return _LoginInfo;
        }
        _LoginInfo = _getCacheArray('login_info');
        _LoggedUser = _getCacheArray('user');
        return _LoginInfo;
    } catch (e) {
        ShowExceptionMessage("getCurrentLoginInfo", e);
    }
}
function TranslateText(transobj, fieldtype, transkey) {
    var $transobj = $(transobj);

    if (fieldtype == HTMLTRANS) {
        $transobj.html(GetLanguageText(transkey));
    } else if (fieldtype == VALTRANS) {
		 $transobj.val(GetLanguageText(transkey));
    } else if (fieldtype == TEXTTRANS) {
        $transobj.text(GetLanguageText(transkey));
    } else {
        $transobj.attr('placeholder', GetLanguageText(transkey));
    }
}
   function TranslateTextLocal(transobj, fieldtype, transkey) {
   var $transobj = $(transobj);
   if (fieldtype == HTMLTRANS) {
   $transobj.html(GetLanguageTextLocal(transkey));
   } else if (fieldtype == VALTRANS) {
   $transobj.val(GetLanguageTextLocal(transkey));
   } else if (fieldtype == TEXTTRANS) {
   $transobj.text(GetLanguageTextLocal(transkey));
   } else {
   $transobj.attr('placeholder', GetLanguageTextLocal(transkey));
   }
   }
   
function GetLanguageTextLocal(Key){
var Lang = USER_LANG;

return LOCAL_LANG_TRANS[Lang][Key];
}
function GetLanguageText(Key) {
    var Lang = USER_LANG;
	//console.log("Langg === "+JSON.stringify(LANGUAGE));
    if ((LANGUAGE[Lang]) && LANGUAGE[Lang][Key]) {
        return LANGUAGE[Lang][Key];
    }
    return LANGUAGE[APP_LANG_ID][Key];
}
/* Create by Sankar to handle the active question page based on the phase */
/**
 * @return {boolean}
 */
function HandleQuestion_ActivePage() {
//function HandleQuestion_ActivePage(groupid){
    logStatus("HandleQuestion_ActivePage", LOG_FUNCTIONS);
    try {
        return MoveActiveUserPage();
    } catch (e) {
        ShowExceptionMessage("HandleQuestion_ActivePage", e);
    }
}
/*
 function CheckAndRedirectPage(){
 LoadAllQuestionerdata({},function(){
 var groupobj = getLastGroupObject();
 var RedeemCredits = '';
 if (IsCheckFaseActive(groupobj)){
 if (IsPhaseQuestionStarted(groupobj)){
 if (IsPhaseQuestionComplete(PHASE_Check)){
 CheckDashboardAndMovePageTo('dashboard', false);
 }else{
 if (IsAnyGroupCheckFaseComplete()){
 RedeemCredits	=	GetCredits();
 if(RedeemCredits.redeemable!=0){
 _movePageTo('phase-credits');
 }else{
 CheckDashboardAndMovePageTo('dashboard', false);
 }
 } else {
 //_movePageTo('phase-check'); MAM denoted once
 _movePageTo('welcome');
 }
 }	
 }else{
 if (IsAnyGroupCheckFaseComplete()){
 RedeemCredits	=	GetCredits();
 if(RedeemCredits.redeemable!=0){
 _movePageTo('phase-credits');
 }else{
 CheckDashboardAndMovePageTo('dashboard', false);
 }
 }else{
 _movePageTo('welcome');
 }
 }
 } else {
 CheckDashboardAndMovePageTo('dashboard', false);
 }
 return false;
 });	
 }
 */
/*Does not load the LocalObject Questionairy 
 And moves the page (Which Currently Active to the User) 
 MoveActiveUserPage */
/**
 * @return {boolean}
 */
function MoveActiveUserPage() {
    logStatus("Calling MoveActiveUserPage", LOG_FUNCTIONS);
    try {
        var groupobj = getLastGroupObject();
        if ((!groupobj) || ($.isEmptyObject(groupobj))) {
            LoadAllQuestionerdata({}, function () {
                var template = GetUserActiveTemplate();
                HideLoader();
                _movePageTo(template);
            });
        } else {
            var template = GetUserActiveTemplate();
            
            HideLoader();
            _movePageTo(template);
            $(".menuspan").removeClass("act");
            $(".frt").addClass("act");
        }
        return false;
    } catch (e) {
        ShowExceptionMessage("MoveActiveUserPage", e);
    }
}
/**
 * @return {string}
 */
function GetUserActiveTemplate() {
    var template = _DefultHomePage;
    var groupobj = getLastGroupObject();
	
    var RedeemCredits = '';
    if (IsCheckFaseActive(groupobj)) {
        if (IsPhaseQuestionStarted(groupobj)) {
            if (IsPhaseQuestionComplete(PHASE_Check)) {
                RedeemCredits = GetCredits();
                if (RedeemCredits.redeemable != 0) {
                    template = 'phase-credits';
                } else {
                    //template	=	'dashboard';
                    template = IsCheckSummaryViewed() ? 'dashboard' : 'checkphase-summary';

                }
            } else {
                if (IsAnyGroupCheckFaseComplete()) {
                    RedeemCredits = GetCredits();
                    if (RedeemCredits.redeemable != 0) {
                        template = 'phase-credits';
                    } else {
                        template = 'dashboard';
                    }
                } else {
                    //_movePageTo('phase-check'); MAM denoted once
                    template = 'welcome';
                }
            }
        } else {
            if (IsAnyGroupCheckFaseComplete()) {
                RedeemCredits = GetCredits();
                if (RedeemCredits.redeemable != 0) {
                    template = 'phase-credits';
                } else {
                    template = 'dashboard';
                }
            } else {
                template = 'welcome';
            }
        }
    } else {
        RedeemCredits = GetCredits();
        if (RedeemCredits.redeemable != 0) {
            template = 'phase-credits';
        } else {
            template = 'dashboard';
        }
    }
    template = CheckDashboardAndMovePageTo(template, false, true);

    return CheckAndShowIntro(template);
}

/**
 * @return {string}
 */
function CheckAndShowIntro(redirect) {
    var intro = _getCacheValue('isintro');
    if (!intro) {
        _setCacheValue('isintro', '1');
        return 'intro';
    }
    return redirect;
}

/**
 * @return {boolean}
 */
function IsCheckFaseActive(groupobjs) {
    var ischeckactive = false;
    $.each(groupobjs, function (i, groupobj) {
        if ((groupobj._phase) && (groupobj._phase.phase_id == PHASE_Check)) {
            ischeckactive = true;
            return false;
        }
    });
    return ischeckactive;
}

/**
 * @return {boolean}
 */
function IsAnyGroupCheckFaseComplete(groupobjs) {
    if (!groupobjs) {
        groupobjs = getLastGroupObject();
    }
    var ischeckinactive = false;
    $.each(groupobjs, function (i, groupobj) {
        if ((groupobj._phase) && (groupobj._phase.phase_id != PHASE_Check)) {
            ischeckinactive = true;
            return false;
        }
    });
    return ischeckinactive;
    //XX return (!IsCheckFaseActive(groupobjs));
}

/**
 * @return {boolean}
 */
function IsTrainingActive() {
    var testobj = GetAnalysedTest();
    /**
     * @param testobj.r_test_id This is test id
     */
    return (!(($.isEmptyObject(testobj)) || (!testobj.r_test_id)));
}


function getLastQuestionObjectFromSession() {
    return _getCacheArray('lastquestionobj');
}

function getLastGroupObject() {
    return getLastQuestionObjectFromSession();
}

function getQuestionTransObjForGroupIdAndQuestId(groupid, questid) {
    logStatus("getQuestionTransObjForGroupIdAndQuestId", LOG_FUNCTIONS);
    try {
        var questiontransobj = {};
        var iscontloop = true;
        var groupobj = getLastGroupObject();
        /**
         * @param questionobj.langcount This is language count
         */
        if (groupobj['group' + groupid]) {
            var phaseobj = groupobj['group' + groupid]._phase;
            var _questions = phaseobj._questionscount == 1 ? [phaseobj._questions] : phaseobj._questions;
            $.each(_questions, function (i, questionobj) {
                if (questionobj.id == questid) {
                    if (questionobj.langcount > 0) {
                        questiontransobj = questionobj['lang']['lang_' + USER_LANG];
                        iscontloop = false;
                        return false;
                    }
                }
            })
        }
        return questiontransobj;
    } catch (e) {
        ShowExceptionMessage("getQuestionTransObjForGroupIdAndQuestId", e);
    }
}
/*
 function getQuestionObjForGroupIdAndQuestId(groupid, questid){
 logStatus("getQuestionObjForGroupIdAndQuestId",LOG_FUNCTIONS);
 try {
 var questionreurnobj = {};
 var iscontloop = true;
 var groupobj = getLastGroupObject();
 if (groupobj['group'+groupid]){
 var phaseobj = groupobj['group'+groupid]._phase;
 var _questions = phaseobj._questionscount == 1?[phaseobj._questions]:phaseobj._questions;
 $.each(_questions, function(i, questionobj){
 if (parseInt(questionobj.id) == questid){
 questionreurnobj = questionobj;
 iscontloop = false;
 return false;
 }
 })
 }
 return questionreurnobj;
 } catch(e){
 ShowExceptionMessage("getQuestionObjForGroupIdAndQuestId", e);
 }	
 }
 */
function updateSavedQuestionsInLocal(answerarrobj) {
    _setCacheArray('savedanswerobj', answerarrobj);
}
function getSavedQuestionsInLocal() {
    return _getCacheArray('savedanswerobj');
}
/*
 function updateQuestionSessionObj(questionobj){
 _setCacheArray('lastquestionobj', questionobj);
 }*//*
 function updateGroupClubSessionObj(questionobj){
 _setCacheArray('memberclubs', questionobj);
 }
 function updateGroupCoachSessionObj(questionobj){
 _setCacheArray('membercoaches', questionobj);
 }
 */
/**
 * @return {boolean}
 */
function IsPhaseQuestionComplete(faseid) {
    var groupobjs = getLastGroupObject();
    var isincomp = !$.isEmptyObject(groupobjs);
    $.each(groupobjs, function (i, groupobj) {
        if ((groupobj._phase) && (groupobj._phase.phase_id == faseid) && (groupobj._phase._questions)) {
            var _questions = groupobj._phase._questionscount == 1 ? [groupobj._phase._questions] : groupobj._phase._questions;
            $.each(_questions, function (i, questionobj) {
                if ((!questionobj._answercount) || (questionobj._answercount == 0)) {
                    isincomp = false;
                    //alert('one'+isincomp);
                    return isincomp;
                }
            })
        }
        //alert('two'+isincomp);
        return isincomp;
    });
    //alert('three'+isincomp);
    return isincomp;
}

/**
 * @return {boolean}
 */
function IsPhaseQuestionStarted(groupobjs) {
    if (!groupobjs) {
        groupobjs = getLastGroupObject();
    }
    var isstarted = false;
    $.each(groupobjs, function (i, groupobj) {
        if ((groupobj._phase) && (groupobj._phase._questions)) {
            var _questions = groupobj._phase._questionscount == 1 ? [groupobj._phase._questions] : groupobj._phase._questions;
            $.each(_questions, function (i, questionobj) {
                if ((questionobj._answercount) && (questionobj._answercount > 0)) {
                    isstarted = true;
                    return false;
                }
            })
        }
        return isstarted;
    });
    return isstarted;
}

function getCurrentFaseForGroupId(groupid) {
    var groupobjs = getLastGroupObject();
    var groupobj = groupobjs['group' + groupid];
    if (groupobj) {
        if (groupobj._phase) {
            return groupobj._phase.phase_id;
        }
    }
}


function getCurrentGroupwiseIndexPercentage() {
    logStatus("getCurrentGroupwiseIndexPercentage", LOG_FUNCTIONS);
    try {
        var currentindexobj = {};
        var groupobjs = getLastGroupObject();
        var totalindex = 0;
        /**
         * @param  _creditinfo.index_weightage This is credit info index weightage
         * @param  _creditinfo.max_weightage This is credit info max weightage
         */
        $.each(groupobjs, function (i, groupobj) {
            var totalpoint = 0;
            var maxcreditindex = 0;
            var maxcreditlimit = 0;
            var creditindex = 0;
            if (groupobj._phase) {
                if (groupobj._phase._creditinfocount > 0) {
                    maxcreditindex = parseFloat(groupobj._phase._creditinfo.index_weightage);
                    maxcreditlimit = parseFloat(groupobj._phase._creditinfo.max_weightage);
                    var _questions = groupobj._phase._questionscount == 1 ? [groupobj._phase._questions] : groupobj._phase._questions;
                    $.each(_questions, function (j, questionobj) {
                        //console.log(j);
                        if ((questionobj._answercount) && (questionobj._answercount > 0)) {
                            var answers = questionobj._answercount == 1 ? [questionobj._answers] : questionobj._answers;
                            $.each(answers, function (idx, answer) {
                                if(idx == 0)
                                {   
                                    if ((answer) && (answer.value)) {
                                        totalpoint += parseFloat(answer.value);
                                    }
                                }
                            });
                        }
                    });
                    
                } else {
                    if (parseInt(groupobj._phase._activitiescount) > 0) {
                        var activityobjs = groupobj._phase._activities;
                        if (parseInt(groupobj._phase._activitiescount) == 1) {
                            activityobjs = [activityobjs];
                        }
                        //maxcreditlimit //Need to find out how
                        //maxcreditindex //Need to find out from the days they started this activity
                        $.each(activityobjs, function (j, activityobj) {
                            if ((activityobj._answerscount) && (activityobj._answerscount > 0)) {
                                var answers = activityobj._answerscount == 1 ? [activityobj._answers] : activityobj._answers;
                                var points = (activityobj._pointscount == 1) ? activityobj._points : activityobj._points[0];
                                $.each(answers, function (idx, answer) {
                                    if(idx == 0)
                                    { 
                                        if ((answer) && (answer.value)) {
                                            totalpoint += parseFloat(answer.value);
                                        }
                                    }  
                                });
                                //maxcreditlimit	=	GetAcitvityDashBoardData(answers,points,activityobj.activitysteps);
                                maxcreditlimit = GetAcitvityDashBoardData(answers, points);

                                maxcreditindex = maxcreditlimit
                                
                            }
                        })
                    }
                }
            }
            if ((maxcreditlimit > 0) && (totalpoint > maxcreditlimit)) {
                totalpoint = maxcreditlimit;
            }
            if (maxcreditindex > 0) {
                creditindex = Math.round((totalpoint / maxcreditindex) * 100);
                if (creditindex > 100) {
                    creditindex = 100;
                }
            }
            currentindexobj[groupobj['group_id']] = {
                groupname: groupobj['group_name'],
                'totalpoint': totalpoint,
                'maxcreditlimit': maxcreditlimit,
                'creditindex': creditindex
            };
            totalindex = totalindex + creditindex;

            //return isstarted;		
        });
        currentindexobj['total'] = totalindex;
        
        return currentindexobj;
    } catch (e) {
        ShowExceptionMessage("getCurrentGroupwiseIndexPercentage", e);
    }
}
/*
 function calculateStepsForActivity(activityobj,answerobj){
 logStatus("calculateStepsForActivity",LOG_FUNCTIONS);
 try {
 var currentstep = 0;
 var activitystep = parseInt(activityobj.activitysteps);
 if (activitystep > 0){
 if(!$.isEmptyObject(answerobj)) { 
 var currentcredit = answerobj.value;
 if (activityobj._points){
 var dailylimit = parseInt(activityobj._points.weekcredits) / parseInt(activityobj._points.perweek);
 currentstep = (currentcredit/dailylimit) * activitystep;
 }
 }
 }
 return currentstep;
 } catch(e){
 ShowExceptionMessage("calculateStepsForActivity", e);
 }
 }
 function getCurrentStepsForActivity(activityobj){
 var currentstep = 0;
 if (activityobj){
 if (activityobj._answerscount > 0){
 var answers = activityobj._answerscount == 1?[activityobj._answers]:activityobj._answers;
 if (answers[0]){
 var activitystep = parseInt(activityobj.activitysteps);
 if (activitystep > 0){
 var currentcredit = answers[0].value;
 if (activityobj._points){
 var dailylimit = parseInt(activityobj._points.weekcredits) / parseInt(activityobj._points.perweek);
 currentstep = (currentcredit/dailylimit) * activitystep;
 }					
 }
 }
 }
 }
 return currentstep;
 }

 function getCurrentIndexRangeObject(groupid){
 var rangeobject = {};
 var creditindexobject =  getCurrentGroupwiseIndexPercentage();
 var creditobj = creditindexobject[groupid+''];
 if (creditobj){
 var creditindexpercentage = parseFloat(creditobj.creditindex);
 /**
 * @param range.min_range This is min range
 * @param range.max_range This is max range
 * /
 var groupobjs = getLastGroupObject();
 var groupobj = groupobjs['group'+groupid];
 if (groupobj){
 $.each(groupobj._phase._phaserange, function(i, range){
 if ((parseFloat(range.min_range) <= creditindexpercentage) && (parseFloat(range.max_range) >= creditindexpercentage)){
 rangeobject = range;
 return false;
 }
 });
 }
 return rangeobject;
 }
 }

 function getRangeTransTextBasedOnType(range, txttype){
 var typetext = '';
 /**
 * @param range._language This is language detail list
 * @param rangelang.text_type This is text type
 * @param rangelang.languageid This is language id
 * /
 $.each(range._language, function(i, rangelang){
 if ((rangelang.text_type == txttype) && (rangelang.languageid == USER_LANG)){
 typetext = rangelang.text;
 return false;
 }
 })
 return typetext;
 }
 */
function getlastActiveGroupId() {
    var selectedgrpid = _getCacheValue('lastactivetab');
    if (!selectedgrpid) {
        selectedgrpid = GetPreferredGroup();
    }
    return selectedgrpid;
}

function getGroupDesc(groupid) {
    var groupobjs = getLastGroupObject();
    var groupobj = groupobjs['group' + groupid];
    if (groupobj) {
        return groupobj.group_name;
    }
}

function RefreshCurrectPageGroupDet() {
    var selectedgrpid = getlastActiveGroupId();
    var grpname = getGroupDesc(selectedgrpid);
    if (!grpname) {
        grpname = '';
    }
    $(".clscurrentgrouptext").html(grpname.toUpperCase());
    //var grouptxthtml = '';
    UpdateDynamicPageForId('.clsprefgroup.move', MOVESMART_FASE, function (cnt,pagetitle) {
        //$('.curgrpbtn').html('Start <br/> step <br/> counter');
         $(".comm_banner_img_box h4").text(pagetitle);
        var $idfasecompletedbutton_move = $("#idfasecompletedbutton_move");
        var ActiveFaseId = getCurrentFaseForGroupId(GROUP_MOVESMART);
        if (ActiveFaseId != PHASE_Check) {//Task No.5086
            $idfasecompletedbutton_move.html(GetLanguageText(TXT_TO_STEPCOUNTER));//"To Stepcounter"
        } else {
            $idfasecompletedbutton_move.html(GetLanguageText(TXT_START_STEPCOUNTER))
        }
    });
    UpdateDynamicPageForId('.clsprefgroup.eat', EATFRESH_FASE, function (cnt,pagetitle) {
         $(".comm_banner_img_box h4").text(pagetitle);
        var ActiveFaseId = getCurrentFaseForGroupId(GROUP_EATFRESH);
        if (ActiveFaseId != PHASE_Check) {//Task No.5086
            $("#idfasecompletedbutton_eat").html(GetLanguageText(TXT_TO_EAT_DIARY));
        } else {
            $("#idfasecompletedbutton_eat").html(GetLanguageText(TXT_START_DIARY));
        }
        //$('.curgrpbtn').html('Start <br/> EAT <br/> diary.');
    });
    UpdateDynamicPageForId('.clsprefgroup.mind', MINDSWITCH_FASE, function (cnt,pagetitle) {
         $(".comm_banner_img_box h4").text(pagetitle);
        //$('.curgrpbtn').html('Start <br/> MIND <br/> activity.');
        var ActiveFaseId = getCurrentFaseForGroupId(GROUP_MINDSWITCH);
        if (ActiveFaseId != PHASE_Check) {//Task No.5086
            $("#idfasecompletedbutton_mind").html(GetLanguageText(TXT_TO_MIND_ACTIVITY));
        } else {
            $("#idfasecompletedbutton_mind").html(GetLanguageText(TXT_START_MIND_ACTIVITY));
        }
    });
    //var agc	=	GetCredits(selectedgrpid);
    //$("#total_credits").val(parseFloat(agc.inprocess).toFixed(0));
}

function getNextPhaseIdForGroupId(groupid, isappointment) {
    var currentfaseid = getCurrentFaseForGroupId(groupid);
    if (isappointment) {
        return PHASE_phase2;
    }
    if (!currentfaseid) {
        return PHASE_Check;
    } else if (currentfaseid == PHASE_Check) {
        return PHASE_phase1;
    } else if (currentfaseid == PHASE_phase1) {
        return PHASE_phase2;
    } else if (currentfaseid == PHASE_phase2) {
        return PHASE_phase3;
    }
}

function updateQuestionSessionObj(questionobj) {
    _setCacheArray('lastquestionobj', questionobj);
}

function UpdateGeneralQuestionsObject(questionobj) {
    _Generalquestions = questionobj;
    _setCacheArray('generalquestions', questionobj);
}

function GetGeneralQuestions() {
    return _getCacheArray('generalquestions');
}

function UpdateQuestionariesObjectWithGeneralQuestions(data) {
	
    /**
     * @param data.activetraining This is active training
     * @param data.toptopics This is active top topics
     * @param data.userpreference This is user preference
     * @param data.usertest This is user test
     * @param data.education This is education detail
     * @param data.credits_byphase This is credits phase detail
     * @param data.topic_maxpoints This is topic max points
     * @param data.topic_result This is topic result
     * @param data.generalquestions This is general questions
     */
    
	var grouplist = data.grouplist;
    var credits = data.credits;
    var activetraining = data.activetraining;
    var toptopics = data.toptopics;
    var selectedgrpid = getlastActiveGroupId();
    var ActiveFaseId = getCurrentFaseForGroupId(selectedgrpid);
    var user = data.userpreference;
    var usertest = data.usertest;
    var education = data.education;
    var creditphase = data.credits_byphase;
    var topic_maxpoints = data.topic_maxpoints;
    var topic_result = data.topic_result;
    ApplyPrefferedGroup(data);
    updateUserInfoLocal({
        r_club_id: user.r_club_id
    });
    UpdateCreditSessionObj(credits);
    UpdateTopthreeTopics(toptopics);
    UpdateTrainingSessionObj(activetraining);
    UpdateEducationSessionObj(education);
    UpdatePhaseCreditSessionObj(creditphase);
    UpdateTopicMaxPointsSessionObj(topic_maxpoints);
    UpdateTopicResultSessionObj(topic_result);
    if ((!ActiveFaseId) || (ActiveFaseId == PHASE_Check)) {
        var genquestions = (data && data.generalquestions ) ? data.generalquestions : null;
        if (genquestions && genquestions._questionscount > 0) {
            grouplist['group' + GROUP_GENERAL] = getDummyGroupObject(GROUP_GENERAL, genquestions, 'General');
        }
    }
    
    UpdateAnalysedTest(usertest);
    UpdateQuestionariesObject(grouplist);
}

function UpdateAnalysedTest(usertestobj) {
    _setCacheArray('usertestobj', usertestobj);
}

function UpdateCreditSessionObj(creditobj) {
    _setCacheArray('lastcreditobj', creditobj);
}
function UpdatePhaseCreditSessionObj(creditobj) {
    _setCacheArray('lastphasecreditobj', creditobj);
}
function UpdateTopicMaxPointsSessionObj(topicmaxpointsobj) {
    _setCacheArray('topicmaxpointsobj', topicmaxpointsobj);
}
function UpdateTopicResultSessionObj(topicresultobj) {
    _setCacheArray('topicresultobj', topicresultobj);
}
function UpdateTrainingSessionObj(trainingobj) {
    _setCacheArray('lasttrainingobj', trainingobj);
}
function UpdateEducationSessionObj(educationobj) {
    _setCacheArray('lasteducationobj', educationobj);
}

function UpdateTrainingActivityBasedDetailObj(acvitiyobj) {
    _setCacheArray('lasttrainingactivityobj', acvitiyobj);
}

function UpdateLastSelectedActivityIdSession(activityid) {
    _setCacheValue('lastselectedtrainingactivity', activityid);
}

function UpdateLastSelectedActivityIdHrReqSession(hrreq) {
    _setCacheValue('lastselectedtrainingactivityhrreq', hrreq);
}

function UpdateLastSelectedCoach(coachobj) {
    _setCacheArray('selectedcoachobj', coachobj);
}
/*
 function UpdateQuestionBackInfo(qobj){
 _setCacheArray('selectedquesobj',qobj);
 }
 */
function UpdateTopthreeTopics(toptopics) {
    _setCacheArray('toptopics', toptopics);
}
function GetTopthreeTopics(toptopics) {
    return _getCacheArray('toptopics');
}
function GetLastSelectedActivityIdSession() {
    return _getCacheValue('lastselectedtrainingactivity');
}
function GetLastSelectedActivityIdHrReqSession() {
    return _getCacheValue('lastselectedtrainingactivityhrreq');
}
function GetSelectedCoachObj() {
    return _getCacheArray('selectedcoachobj');
}
function GetSelectedQuestionBackInfoObj() {
    return _getCacheArray('selectedquesobj');
}
function GetLastCreditObject() {
    return _getCacheArray('lastcreditobj');
}
function GetLastPhaseCreditObject() {
    return _getCacheArray('lastphasecreditobj');
}
/*
 function GetLastTopicMaxPointsObject(){
 return _getCacheArray('topicmaxpointsobj');
 }
 */
function GetLastTopicResultObject() {
    return _getCacheArray('topicresultobj');
}
function GetEducationObject() {
    return _getCacheArray('lasteducationobj');
}
function GetLastTrainingObject() {
    return _getCacheArray('lasttrainingobj');

}
function GetTrainingActivityBasedDetailObj() {
    return _getCacheArray('lasttrainingactivityobj');
}
function getCurrentTrainingWeekObj() {
    var traingobj = GetLastTrainingObject();
    var weekobj = null;
    if (!$.isEmptyObject(traingobj)) {
        if (!traingobj.length) {
            traingobj = [traingobj];
        }
        /**
         * @param firsttraingobj.currentweekno This is current week no
         * @param firsttraingobj.weekdatebreak This is weak date break
         */
        var firsttraingobj = traingobj[0];
        if (firsttraingobj.currentweekno) {
            $.each(firsttraingobj.weekdatebreak, function (key, trainobj) {
                if (trainobj.f_weeknr == firsttraingobj.currentweekno) {
                    weekobj = trainobj;
                    return false;
                }
            });
            // return firsttraingobj.currentweekno;
        }
    }
    return weekobj;
}

function getCurrentFitlevel() {
    var traingobj = GetLastTrainingObject();
    if (traingobj) {
        return traingobj.fitness_level;
    }
    return 0;
}

function getCurrentDayPointsFromTrainingWeek() {
    var cweek = getCurrentTrainingWeekObj();
    var daypoint = 0;
    /**
     * @param  cweek.daypoints This is current week day point
     * @param  pointobj.date  This is point object date
     */
    if (cweek) {
        if (cweek.daypoints) {

            var crdate = getDateTimeByFormat('Y-m-d');
            if(cweek.daypoints.length && cweek.daypoints.length>1)
            {
                var cweekdaypointsdata = cweek.daypoints;
            }else
            {
                var cweekdaypointsdata = [cweek.daypoints];
            }
            $.each(cweekdaypointsdata, function (key, pointobj) {
                if (pointobj.date == crdate) {
                    daypoint = pointobj.point;
                    return false;
                }
            });
        }
    }
    return daypoint;
}

 function getNoOfTraingsCompletedFromTrainingWeek() {
    var cweek = getCurrentTrainingWeekObj();
    if (cweek) {
        if (cweek.daypoints) {
            if (cweek.daypoints.length){
                return cweek.daypoints.length;
            }
            return 0;
        }
    }
    return 0;
} 

function formatHHMMSSFromSec(sec) {
    var sec_num = parseInt(sec, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return hours + ':' + minutes + ':' + seconds;
}

function getGoalPerDayBasedOnFitLevelAndWeekGoal(fitlevel, weekgoal) {
    if (fitlevel <= 2) {
        return Math.round(weekgoal / 2);
    } else {
        return Math.round(weekgoal / 3);
    }
}

function GetAnalysedTest() {
    return _getCacheArray('usertestobj');
}
function UpdateQuestionariesObject(grouplist) {
    _Questierdatainfo = grouplist;
    updateQuestionSessionObj(grouplist);
}
function GetAllQuestionsCountByGroupId(GroupId, selDate) {
    logStatus("GetAllQuestionsCountByGroupId", LOG_FUNCTIONS);
    try {
        var quescountobj = {};
        var groupobj = getLastGroupObject();
        var totalquestions = 0;
        $.each(groupobj, function (gidx, eachgroup) {
            if ((GroupId && (eachgroup.group_id == GroupId)) || (!GroupId)) {
                var quescount = eachgroup._phase._questionscount;
                var questions = (quescount == 1) ? [eachgroup._phase._questions] : eachgroup._phase._questions;
                var questioncnt = getQuestionPercentageObjectByQuestion(questions, quescount, selDate);
                questioncnt.groupname = eachgroup.group_name;
                totalquestions += parseInt(quescount);
                quescountobj[gidx] = questioncnt;
				
            }
        });
        return getPercentageAppliedObj(quescountobj, totalquestions);
    } catch (e) {
        ShowExceptionMessage("GetAllQuestionsCountByGroupId", e);
    }
}
function getQuestionPercentageObjectByQuestion(questions, quescount, selDate) {
    logStatus("getQuestionPercentageObjectByQuestion", LOG_FUNCTIONS);
    try {
        var questioncnt = {};
		if(IsRequestSentForTestAppointment())
				{
				var quescount = quescount-1;	
				}
        questioncnt.questions = quescount;
        questioncnt.answers = 0;
        questioncnt.cons_qpercentage = 0;
        questioncnt.percentage = 0;
        var formateddt = '';
        if ((selDate) && (selDate != '')) {
            formateddt = getDateTimeByFormat('Y-m-d 00:00:00', new Date(selDate));
        }
        if (quescount > 0) {
            $.each(questions, function (qidx, ques) {
                var isInLocal = isSavedAnswer(ques.monitorvariable_id, formateddt);//Saved in local
                if ((parseInt(ques._answercount) > 0) || (isInLocal)) {
                    if (selDate && selDate != '') {
                        var answers = (ques._answercount == 1) ? [ques._answers] : ques._answers;
                        questioncnt.answers += ( (!IsAnswerEditable(answers, selDate)) || (isInLocal) ) ? 1 : 0;
                    } else {
                        questioncnt.answers += 1;
                    }
                }


            });

            var percentage = ((parseInt(questioncnt.answers) / parseInt(quescount)) * 100).toFixed(2);
				questioncnt.percentage = !isNaN(percentage) ? percentage : 0;
        }
        return questioncnt;
    } catch (e) {
        ShowExceptionMessage("getQuestionPercentageObjectByQuestion", e);
    }
}
function getOptionColor(value) {
    logStatus("getOptionColor", LOG_FUNCTIONS);
    try {
        if (value <= 20) {
            return 'rgb(255,0,0)';
        } else if ((value > 20) && (value <= 60)) {
            return 'rgb(255,180,0)';
        } else {
            return 'rgb(0,255,0)';
        }
    } catch (e) {
        ShowExceptionMessage("getOptionColor", e);
    }
}
function getOptionColorPane(value) {
    logStatus("getOptionColorPane", LOG_FUNCTIONS);
    try {
        if (value <= 20) {
            return 'rgb(255,207,207)';
        } else if ((value > 20) && (value <= 60)) {
            return 'rgb(255,234,183)';
        } else {
            return 'rgb(197,255,206)';
        }
    } catch (e) {
        ShowExceptionMessage("getOptionColorPane", e);
    }
}

//function getCircleChart(selector,addsettings,indexval,group){
function getCircleChart(selector, indexval, group) {
    logStatus("getCircleChart", LOG_FUNCTIONS);
    try {
        if (typeof ($(selector).html()) === "undefined"){
            return;
        }
        var totalpoints = '';
        var creditindexobject = getCurrentGroupwiseIndexPercentage();
        if (!$.isEmptyObject(creditindexobject)) {
            //Need To change 3 Group May changed when included General
            totalpoints = ((creditindexobject['total']) / 3) / 10;
            if (isNaN(totalpoints))
                totalpoints = 'N/A';
        }
        var gaugeOptions = {
            chart: {
                'type': 'solidgauge'
            },
            title: null,
            tooltip: {
                enabled: false
            },
            pointFormat: '<span style="color: #575b5b !important; position: absolute;top:0; margin-bottom: -20px; " >{series.name}</span>',
            positioner: function (labelWidth, labelHeight) {
                var $curactcreditchart = $("#curactcreditchart");
                var xpos = Math.round($curactcreditchart.width() / 2);
                var ypos = Math.round($curactcreditchart.height() / 2);
                return {
                    x: xpos - labelWidth / 2,
                    y: ypos - (labelHeight / 2 - 20)
                };
            },
          /* 

	  pane: {
                center: ['50%', '50%'],
                startAngle: 0,
                endAngle: 360,
                background: {
                    backgroundColor: getGroupColor(group),
                    innerRadius: '80%',
                    outerRadius: '100%',
                    borderWidth: 0
                }
            },
			
			*/
			
			pane: {
				startAngle: 0,
				endAngle: 360,
				background: [{ // Track for Move
					outerRadius: '112%',
					innerRadius: '88%',
					//backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.3).get(),
					backgroundColor: '#3b3e47',
					borderWidth: 0
				}, { // Track for Exercise
					outerRadius: '87%',
					innerRadius: '63%',
					//backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.3).get(),
					backgroundColor: '#767678',
					borderWidth: 0
				}, { // Track for Stand
					outerRadius: '62%',
					innerRadius: '38%',
					//backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[2]).setOpacity(0.3).get(),
					backgroundColor: '#c2c3c5',
					borderWidth: 0
				}]
			},

            yAxis: {
                min: 0,
                max: 100,
                labels: {
                    enabled: false
                },
                lineWidth: 0,
                minorTickInterval: null,
                tickPixelInterval: 400,
                tickWidth: 0
            },
            series: [{
                data: [{
                    y: (totalpoints * 10),//indexval,// /10,
                    color: getOptionColor(indexval),
                    radius: '80%',
                    innerRadius: '100%',
                    borderWidth: 0
                }],
                dataLabels: {
                    enabled: true,
                    useHTML: true,
                    //format:'<span class="circleindex">'+ parseFloat((indexval/100) * 10).toFixed(1)+'</span>'
                    format: '<span class="circleindex">' + ( (totalpoints != 'N/A') ? totalpoints.toFixed(1) : totalpoints ) + '</span>'
                }
            }]
        };
        $(selector).highcharts(gaugeOptions);
    } catch (e) {
        ShowExceptionMessage("getCircleChart", e);
    }
}
function getGroupColor(group) {
    logStatus("getGroupColor", LOG_FUNCTIONS);
    try {
        var retcolor = '#71b0e0';
        switch (group) {
            case GROUP_MOVESMART:
                retcolor = '#71b0e0';
                break;
            case GROUP_EATFRESH:
                retcolor = '#3c3e46';
                break;
            case GROUP_MINDSWITCH:
                retcolor = '#8ecb7f';
                break;
        }
        return retcolor;
    } catch (e) {
        ShowExceptionMessage("getGroupColor", e);
    }
}
function getSolidGaugeChart(selector, addsettings,group) {
    logStatus("getSolidGaugeChart", LOG_FUNCTIONS);
    try {
        if (typeof ($("#"+selector).html()) === "undefined"){
            return;
        }
        var creditindexobject = getCurrentGroupwiseIndexPercentage();
        
        
        if (!$.isEmptyObject(creditindexobject)) {
            //Need To change 3 Group May changed when included General
           // var totalpoints = ((creditindexobject['total']) / 3) / 10;
            var tot = selector;
            var totalpers = Math.round((creditindexobject['total']) / 3);
            $("#hiddenScore").val(totalpers);
            if(tot=="curactcreditchart_move")
            {
                var totalpoints = "MOVE.";
            }
            else if(tot=="curactcreditchart_eat")
            {
                var totalpoints = "EAT!";
            }
            else if(tot=="curactcreditchart_mind")
            {
                var totalpoints = "MIND?";
            }
            else
            {
                var totalpoints = Math.round((creditindexobject['total']) / 3);
                //alert(totalpoints);
                if (isNaN(totalpoints)) {
                totalpoints = 'N/A';
                }
            }
            
            /*Highcharts.setOptions({
             chart: {
             backgroundColor: 'none'
             },
             colors: [getOptionColor(creditindexobject[''+GROUP_MOVESMART].creditindex ),getOptionColor(creditindexobject[''+GROUP_EATFRESH].creditindex ),getOptionColor(creditindexobject[''+GROUP_MINDSWITCH].creditindex )],//'#ee2c2b', '#f39a29', '#1fa549'
             title: {
             style: {
             color: 'silver'
             }
             },
             tooltip: {
             style: {
             color: 'silver'
             }
             }
             });*/
            //var chart	=	Highcharts.chart(selector, {
            return Highcharts.chart(selector, {
                    chart: {
                        type: 'solidgauge',
                        marginTop: 0,
                        /*marginBottom: 0,*/
                        marginLeft: 0,
                        marginBottom: 0,
                        spacingTop: 0,
                        backgroundColor: 'none',
                        spacingBottom: 0
                        // Explicitly tell the width and height of a chart
                        /*width: 250,
                         height:250*/
                        // width: 300,
                        // height:300
                    },
                    title: {
                        text: null/*,
                         style: {
                         fontSize: '24px'
                         }*/
                    },
                    /**/
                    tooltip: {
                        borderWidth: 0,
                        backgroundColor: 'none',
                        shadow: false,
                        style: {
                            fontSize: '12px'
                        },
                        /*formatter:function(){
                         /*MK Added : Tool Tip Should Show the Index* /
                         var showpoint	=	parseFloat(this.y/10).toFixed(1);
                         var pointtext	=	'<br/><span style="color: #575b5b !important; position: absolute;top:30; margin-bottom: -20px; font-size:10px " >'+this.series.name+'</span>';
                         $(".totalprecentage").html(showpoint +''+pointtext);
                         if(_TimerShowIndexValue){
                         clearTimeout(_TimerShowIndexValue);
                         }
                         _TimerShowIndexValue	=	setTimeout(function(){
                         showpoint	=	( (totalpoints!='N/A') ? totalpoints.toFixed(1): totalpoints );
                         $(".totalprecentage").html(showpoint);
                         clearTimeout(_TimerShowIndexValue);
                         },3000);
                         },*/
                        //	pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}</span>',
                        pointFormat: '',
                        positioner: function (labelWidth, labelHeight) {
                            var $resultchart = $("#resultchart");
                            var xpos = Math.round($resultchart.width() / 2);
                            var ypos = Math.round($resultchart.height() / 2);

                            return {
                                x: xpos - labelWidth / 2,
                                y: ypos - (labelHeight / 2 - 20)
                            };
                            /*
                             return {
                             x: 200 - labelWidth / 2,
                             y: 180
                             };
                             */
                        }
                    },
                    /**/
                    /*setOptions:{
                     chart: {
                     backgroundColor: 'none'
                     },
                     colors: [getOptionColor(creditindexobject[''+GROUP_MOVESMART].creditindex ),getOptionColor(creditindexobject[''+GROUP_EATFRESH].creditindex ),getOptionColor(creditindexobject[''+GROUP_MINDSWITCH].creditindex )],//'#ee2c2b', '#f39a29', '#1fa549'
                     title: {
                     style: {
                     color: 'silver'
                     }
                     },
                     tooltip: {
                     style: {
                     color: 'silver'
                     }
                     }

                     },*/
                    pane: {
                        startAngle: 0,
                        endAngle: 360,
                        background: [{ // Track for Move
                            outerRadius: '112%',
                            innerRadius: '88%',
                            //backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.3).get(),
                            backgroundColor: (group && group!=GROUP_MOVESMART) ?'#ebebec':getOptionColorPane(creditindexobject['' + GROUP_MOVESMART].creditindex),
                            borderWidth: 0
                        }, { // Track for Exercise
                            outerRadius: '87%',
                            innerRadius: '63%',
                            //backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.3).get(),
                            backgroundColor: (group && group!=GROUP_EATFRESH) ?'#ebebec':getOptionColorPane(creditindexobject['' + GROUP_EATFRESH].creditindex),
                            borderWidth: 0
                        }, { // Track for Stand
                            outerRadius: '62%',
                            innerRadius: '38%',
                            //backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[2]).setOpacity(0.3).get(),
                            backgroundColor: (group && group!=GROUP_MINDSWITCH) ?'#ebebec':getOptionColorPane(creditindexobject['' + GROUP_MINDSWITCH].creditindex),
                            borderWidth: 0
                        }]
                    },
                    /**/
                    yAxis: {
                        min: 0,
                        max: 100,
                        lineWidth: 0,
                        verticalAlign: 'middle',
                        tickPositions: []
                    },

                    plotOptions: {

                        solidgauge: {
                            /*borderWidth: '34px',*/
                            top: "12px",
                            borderWidth: (addsettings && addsettings.plotBorderWidth ? addsettings.plotBorderWidth : '24px'),
                            dataLabels: {
                                enabled: true,
                                useHTML: true,
                                y: -20,
                                borderWidth: 0,
                                /*<div style="text-align:center">total<br>*/
                                /*format: '<span class="totalprecentage" style="font-size:2em; color: {point.color}; font-weight: bold">'+Math.round(totalpoints)+'</span>',*/
                                format: '<span class="totalprecentage" style="font-size:2em; color: {point.color}; font-weight: bold" onclick="openResultSlidePage()">' + totalpoints + '</span>'
                            },
                            linecap: 'round',
                            stickyTracking: false
                            //	enableMouseTracking: true
                        },
                          series: {
                            animation: {
                                complete: function () {
                                    var seriespath = this.d;
                                    var pathfillvalue =seriespath.split(' ');
                                    var $element = $(this.element.nextSibling);
                                    var widthpos = $($element.context).attr('width');
                                    var  x_axispath =pathfillvalue[9]-(widthpos/2); 
                                    var  y_axispath =pathfillvalue[10]-(widthpos/2); 
                                    $($element.context).attr('x',x_axispath);
                                    $($element.context).attr('y',y_axispath);
                                }
                            }
                        }
                    },
                   
                    series: [{
                        name: creditindexobject['' + GROUP_MOVESMART].groupname,
                        borderColor: (group && group!=GROUP_MOVESMART) ?'#ebebec':getOptionColor(creditindexobject['' + GROUP_MOVESMART].creditindex),
                        data: [{
                            //color: Highcharts.getOptions().colors[0],
                            color: (group && group!=GROUP_MOVESMART) ?'#ebebec':getOptionColor(creditindexobject['' + GROUP_MOVESMART].creditindex),
                            radius: '100%',
                            innerRadius: '100%',
                            y: creditindexobject['' + GROUP_MOVESMART].creditindex
                        }]
                        // dataLabels: {
                        // y: -30,
                        // borderWidth: 0,
                        // format: '<div style="text-align:center">total<br><span class="totalprecentage" style="font-size:2em; color: {point.color}; font-weight: bold">{totalpoints}%</span></div>',
                        // },
                        /*tooltip: {
                         valueSuffix: '%'
                         }*/
                    }, {
                        name: creditindexobject['' + GROUP_EATFRESH].groupname,
                        borderColor: (group && group!=GROUP_EATFRESH) ?'#ebebec':getOptionColor(creditindexobject['' + GROUP_EATFRESH].creditindex),
                        data: [{
                            color: (group && group!=GROUP_EATFRESH) ?'#ebebec':getOptionColor(creditindexobject['' + GROUP_EATFRESH].creditindex),
                            radius: '75%',
                            innerRadius: '75%',
                            y: creditindexobject['' + GROUP_EATFRESH].creditindex
                        }]
                        // dataLabels: {
                        // y: -30,
                        // borderWidth: 0,
                        // format: '<div style="text-align:center">total<br><span class="totalprecentage" style="font-size:2em; color: {point.color}; font-weight: bold">{totalpoints}%</span></div>',
                        // },
                        /*tooltip: {
                         valueSuffix: '%'
                         }*/
                    }, {
                        name: creditindexobject['' + GROUP_MINDSWITCH].groupname,
                        borderColor: (group && group!=GROUP_MINDSWITCH) ?'#ebebec':getOptionColor(creditindexobject['' + GROUP_MINDSWITCH].creditindex),
                        data: [{
                            color: (group && group!=GROUP_MINDSWITCH) ?'#ebebec':getOptionColor(creditindexobject['' + GROUP_MINDSWITCH].creditindex),
                            radius: '50%',
                            innerRadius: '50%',
                            y: creditindexobject['' + GROUP_MINDSWITCH].creditindex
                        }]
                    }]
                },

                /**
                 * In the chart load callback, add icons on top of the circular shapes
                 */
                 
                function callback() {
                       
		/*SN commanded graph group icon*/
                    /**/
                     // Move icon

                     var pathheight = 33;  //96//67 = 33//16
                     var xpos = Math.round($("#resultchart").width()/2);
                     var ypos = (Math.round($("#resultchart").height()/3))-pathheight;
                     var path1 = ypos+10;
                     var path2 = (ypos-pathheight)+7;
                     var path3 = ypos-(pathheight*2)-7;
                     if ($("#resultchart").height() <= 250){
                        path1 = 67;
                        path2 = 45;
                        path3 = 20;
                    }
                   //  this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
                     this. renderer.image('./images/move_logo.png', 0, 0, 25, 25)
                     .attr({
                        'stroke': '#303030',
                        'stroke-linecap': 'round',
                        'stroke-linejoin': 'round',
                        'stroke-width': 2,
                        'zIndex': 10
                    })
                    // .translate(xpos, path3)
                     .translate(0, 0)
                     .add(this.series[0].group);

                     // Exercise icon
                    // this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8, 'M', 8, -8, 'L', 16, 0, 8, 8])
                 
                    this. renderer.image('./images/eat_logo.png', 0, 0, 25, 25)
                     .attr({
                        'stroke': '#303030',
                        'stroke-linecap': 'round',
                        'stroke-linejoin': 'round',
                        'stroke-width': 2,
                        'zIndex': 10
                    })
                    // .translate(xpos, path2)
                     .translate(0, 0)
                     .add(this.series[1].group);

                     // Stand icon
                    // this.renderer.path(['M', 0, 8, 'L', 0, -8, 'M', -8, 0, 'L', 0, -8, 8, 0])
                    this. renderer.image('./images/mind_logo.png', 0, 0, 25, 25)
                     .attr({
                        'stroke': '#303030',
                        'stroke-linecap': 'round',
                        'stroke-linejoin': 'round',
                        'stroke-width': 2,
                        'zIndex': 10
                    })
                     //.translate(xpos, path1)
                     .translate(0, 0)
                     .add(this.series[2].group);
                    
                     /**/
                });
               
            /**/
            //return chart;
        }
         
    } catch (e) {
        ShowExceptionMessage("getSolidGaugeChart", e);
    }
}
/*MK - Added Bottom Menu Refresh*/
function openHomePage() {

    logStatus("Calling openHomePage", LOG_FUNCTIONS);
    try {
        var template = GetCurrentPath();
        if (template == 'dashboard') {
            if (_DashboardSliderObj[0]) {
                _DashboardSliderObj[0].slideTo(0);
            }
        } else {
            MoveActiveUserPage();
        }
    }
    catch (e) {

        ShowExceptionMessage(
            "openHomePage", e);
    }
}
function openMoveSmart() {
    logStatus(
        "Calling openMoveSmart", LOG_FUNCTIONS);
    try {
        var ActiveFaseId = getCurrentFaseForGroupId(GROUP_MOVESMART);
        var a = IsTrainingActive();
        if (IsTrainingActive()) {//Need To Check Once
			_setCacheValue('lastactivetab', GROUP_MOVESMART);
		    GoForTestTraining();
            return;
        }
        if (IsPhaseQuestionComplete(PHASE_Check)) {
            if (
                ActiveFaseId == PHASE_phase2) {
               /* ShowAlertMessage(GetLanguageText(
                    MSG_INFO_PHASE_2_BEFORE_TEST))
                ;
                return;*/
                if (IsTrainingActive()) {//Need To Check Once
                    GoForTestTraining();
                } else {
                    ShowAlertMessage(GetLanguageText(MSG_INFO_PHASE_2_BEFORE_TEST));
                    return;
                }
            }
			_setCacheValue('lastactivetab', GROUP_MOVESMART);
            var pagemoveto = (ActiveFaseId == PHASE_Check) ? 'startphase-movesmart' : 'activity-movesmart';
            if (IsActivitySartedPhaseOne()) {
                pagemoveto = 'activity-movesmart';
            }
            CheckDashboardAndMovePageTo(pagemoveto, false);
            RefreshCurrectPageGroupDet();
        } else {
            _setCacheValue('lastactivetab', GROUP_MOVESMART);
            if (ActiveFaseId == PHASE_phase2) {
                if (IsTrainingActive()) {//Need To Check Once
                    GoForTestTraining();
                } else {
                    ShowAlertMessage(GetLanguageText(MSG_INFO_PHASE_2_BEFORE_TEST));
                }
            }
        }
    } catch (e) {
        ShowExceptionMessage("openMoveSmart", e);
    }
}
function GoForTestAppointment() {
                                                                                                   
    logStatus("Calling GoForTestAppointment", LOG_FUNCTIONS);
    try {
        //$( ".user" ).trigger( "click" );
        if (!IsRequestSentForTestAppointment()) {
            CheckDashboardAndMovePageTo('appointment-request', true);
        } else {
            //ShowAlertMessage(GetLanguageText(MSG_INFO_PHASE_2_BEFORE_TEST));
            ShowAlertMessage("You have to do the test, consult your coach");
        }
    } catch (e) {
        ShowExceptionMessage("GoForTestAppointment", e);
    }
}
                                                                                                
function GoForTestAppointmentFromMenu(){
   logStatus("Calling GoForTestAppointmentFromMenu", LOG_FUNCTIONS);
   try {
   $( ".user" ).trigger( "click" );
   if (!IsRequestSentForTestAppointment()) {
   CheckDashboardAndMovePageTo('appointment-request', true);
   } else {
   ShowAlertMessage(GetLanguageText(MSG_COACH_CONSULT));
   //ShowAlertMessage("You already have an outstanding request for a Coach Consult");
   }
   } catch (e) {
   ShowExceptionMessage("GoForTestAppointmentFromMenu", e);
   }
                                                                                                   
}

function GoForTestAppointmentFromCheckphase(){
   logStatus("Calling GoForTestAppointmentFromCheckphase", LOG_FUNCTIONS);
   try {
   
   if (!IsRequestSentForTestAppointment()) {
   //HideLoader(); 
   CheckDashboardAndMovePageTo('appointment-request', true);
   } else {
   ShowAlertMessage(GetLanguageText(MSG_COACH_CONSULT));
   //ShowAlertMessage("You already have an outstanding request for a Coach Consult");
   }
   } catch (e) {
   ShowExceptionMessage("GoForTestAppointmentFromCheckphase", e);
   }
                                                                                                   
}

function GoForTestTraining() {
    logStatus("Calling GoForTestTraining", LOG_FUNCTIONS);
    try {
        var currenttrainingweekobj = getCurrentTrainingWeekObj();
        if (currenttrainingweekobj) {
            _movePageTo("test-training");
        } else {
            ShowAlertMessage(EXCEPTION_TRAINING_NOT_ACTIVE);
        }
        /*
         var reqtrin = IsTrainingActive();
         if (reqtrin){
         _movePageTo('test-training');
         }else{
         ShowAlertMessage(EXCEPTION_TRAINING_NOT_ACTIVE);			
         }
         */
        //CheckDashboardAndMovePageTo('appointment-request');
    } catch (e) {
        ShowExceptionMessage("GoForTestTraining", e);
    }
}
function openEatfresh() {
    logStatus("Calling openEatfresh", LOG_FUNCTIONS);
    try {
        var ActiveFaseId = getCurrentFaseForGroupId(GROUP_EATFRESH);

        if(IsFase1CourseCompleted(GROUP_EATFRESH)) {
            if (IsPhaseQuestionComplete(PHASE_Check)) {
                var pagemoveto = (ActiveFaseId == PHASE_Check) ? 'startphase-eatfresh' : 'activity-eatfresh';
                CheckDashboardAndMovePageTo(pagemoveto, false);
                _setCacheValue('lastactivetab', GROUP_EATFRESH);
            } else {
                if (ActiveFaseId == PHASE_phase2) {
                    _setCacheValue('lastactivetab', GROUP_EATFRESH);
                    //CheckDashboardAndMovePageTo('dashboard', false);
                    //CheckDashboardAndMovePageTo('result-eatfresh', false);
                    CheckDashboardAndMovePageTo('activity-movesmart', false);
                    RefreshCurrectPageGroupDet();
                }
            }
        } else {
          ShowAlertMessage(GetLanguageText(EXP_EXPIRED_GO_FOR_TEST));
           _movePageTo('dashboard');
            return;
        }
        RefreshCurrectPageGroupDet();
    } catch (e) {
        ShowExceptionMessage("openEatfresh", e);
    }
}
function openMindswitch() {
    logStatus("Calling openMindswitch", LOG_FUNCTIONS);
    try {
		setUserWantCoachAnswer();
		 var answer = _getCacheArray('coachquesanswer');
		if(IsRequestSentForTestAppointment()!=true){
		if(getUserWantCoachAnswer())
		{  
			openCoachListFromCheckFase();
		}
		else if(answer && answer=="1")
		{  
			openCoachListFromCheckFase();
		}
		else{
			/*if answer is no*/
					var loginInfos = getCurrentLoginInfo();
					var postdata= {};
					postdata.user_id = loginInfos.user.user_id;
					postdata.is_reporting = BASE_URL+'reporting/index.php/mind_switch/checkIsactive';
					
					 mindTabAct(postdata,function(response){
					if(response.status==1){
					   CheckDashboardAndMovePageTo('active_mind_questions', false);
					   var a = response.data.mindlink;
						$("#hidenquesval").val(a);

					}
				else{
							var ActiveFaseId = getCurrentFaseForGroupId(GROUP_MINDSWITCH);
						if (IsPhaseQuestionComplete(PHASE_Check)) {
							 _setCacheValue('lastactivetab', GROUP_MINDSWITCH);
							 var pagemoveto = (ActiveFaseId == PHASE_Check) ? 'startphase-mindswitch' : 'activity-mindswitch';
							 CheckDashboardAndMovePageTo(pagemoveto, false);
							 RefreshCurrectPageGroupDet();
						 } else {
							 if (ActiveFaseId == PHASE_phase2) {
								 _setCacheValue('lastactivetab', GROUP_MINDSWITCH);
								 CheckDashboardAndMovePageTo('activity-mindswitch', false);
								 RefreshCurrectPageGroupDet();
							 }
						 }
				}
        });
			/*end*/
		}
		}
		else{
        var loginInfos = getCurrentLoginInfo();
        var postdata= {};
        postdata.user_id = loginInfos.user.user_id;
        postdata.is_reporting = BASE_URL+'reporting/index.php/mind_switch/checkIsactive';
        
         mindTabAct(postdata,function(response){
            if(response.status==1){
               //localStorage.setItem('mind-ques-status',response.status);
               CheckDashboardAndMovePageTo('active_mind_questions', false);
               var a = response.data.mindlink;
               //alert(a);
           $("#hidenquesval").val(a);
               // $('#frst_sd').css('display','block');
               //          var a = response.data.mindlink;
               // $("#hidenquesval").val(a);
       
       
            }
            else{
                //localStorage.setItem('favoriteflavor',response.status);
                //return response.status;
                //$('#scf_fg').css('display','block');
                        var ActiveFaseId = getCurrentFaseForGroupId(GROUP_MINDSWITCH);
                if (IsPhaseQuestionComplete(PHASE_Check)) {
                     _setCacheValue('lastactivetab', GROUP_MINDSWITCH);
                     var pagemoveto = (ActiveFaseId == PHASE_Check) ? 'startphase-mindswitch' : 'activity-mindswitch';
                     //CheckDashboardAndMovePageTo('dashboard', false);
                     //CheckDashboardAndMovePageTo('result-mindswitch', false);
                     CheckDashboardAndMovePageTo(pagemoveto, false);
                     RefreshCurrectPageGroupDet();
                 } else {
                     if (ActiveFaseId == PHASE_phase2) {
                         _setCacheValue('lastactivetab', GROUP_MINDSWITCH);
                         //CheckDashboardAndMovePageTo('dashboard', false);
                         //CheckDashboardAndMovePageTo('result-mindswitch', false);
                         CheckDashboardAndMovePageTo('activity-mindswitch', false);
                         RefreshCurrectPageGroupDet();
                     }
                 }
            }
        });
        
        /*
         if(CLIENT_CURRENT_QUESPHASE && CLIENT_CURRENT_QUESPHASE!=PHASE_Check) {
         _movePageTo('group-mindwsitch');
         }
         */
		}
    } catch (e) {
        ShowExceptionMessage("openMindswitch", e);
    }
}
function openCreditPage() {
    logStatus("Calling openCreditPage", LOG_FUNCTIONS);
    try {
        /*SN ADDED- footer coin click to dashboard-20160503*/
        if (IsPhaseQuestionComplete(PHASE_Check)) {
            //openResultSlidePage();
            //CheckDashboardAndMovePageTo('dashboard', false);
            CheckDashboardAndMovePageTo('dashboard-credits');
            RefreshCurrectPageGroupDet();
        }
    } catch (e) {
        ShowExceptionMessage("openCreditPage", e);
    }
}
function CheckDashboardAndMovePageTo(redirectpage, isshowmessage, isreturn) {
    var selectedgrpid = getlastActiveGroupId();
    var ActiveFaseId = getCurrentFaseForGroupId(selectedgrpid);
    if (ActiveFaseId >= PHASE_phase2) {
        //var analysedtest = GetAnalysedTest();
        var reqtrin = IsTrainingActive();
        if (reqtrin) {
            if (selectedgrpid == GROUP_MOVESMART) {
                redirectpage = 'credit-dashboard';
            }
        } else {
            if (isshowmessage) {
                ShowAlertMessage(GetLanguageText(MSG_INFO_PHASE_2_BEFORE_TEST));
                redirectpage = '';
            }
        }
    }
    if (redirectpage != "") {
        if (isreturn) {
            return redirectpage;
        } else {
            _movePageTo(redirectpage);
        }
    }
}
/**/
function updateUserStartCheck(obj) {
//function updateUserStartCheck(groupID, obj){
    logStatus("Calling updateUserStartCheck", LOG_FUNCTIONS);
    try {
        AddProcessingLoader(obj);
        var user = getCurrentUser();
        updateUserPhase({userId: user.user_id, phaseId: PHASE_Check, groupId: GROUP_MOVESMART}, function () {
            updateUserPhase({userId: user.user_id, phaseId: PHASE_Check, groupId: GROUP_EATFRESH}, function () {
                updateUserPhase({userId: user.user_id, phaseId: PHASE_Check, groupId: GROUP_MINDSWITCH}, function () {
                    RemoveProcessingLoader();
                    _movePageTo('phase-check');
                });
            });
        });
    } catch (e) {
        ShowExceptionMessage("updateUserStartCheck", e);
    }
}
function updateUserStartPhase(groupID, phaseid, successhandler, stepTypeId) {
    logStatus("Calling updateUserStartPhase", LOG_FUNCTIONS);
    try {
        var user = getCurrentUser();
        var postData = {};
        if (stepTypeId != '') {
            postData.stepTypeId = stepTypeId;
        }
        updateUserPhase({userId: user.user_id, phaseId: phaseid, groupId: groupID}, function () {
            if (successhandler) {
                successhandler();
            }
        });
    } catch (e) {
        ShowExceptionMessage("updateUserStartPhase", e);
    }
}
function updatePhaseCompleted(groupID, phase, successhandler) {
    logStatus("Calling updatePhaseCompleted", LOG_FUNCTIONS);
    try {
        var user = getCurrentUser();
        var phasesel = PHASE_Check;
        if (phase) {
            phasesel = phase;
        }
        if (groupID == 0) {
            updateUserPhase({
                userId: user.user_id,
                phaseId: phasesel,
                groupId: GROUP_MOVESMART,
                completed: 1
            }, function () {
                updateUserPhase({
                    userId: user.user_id,
                    phaseId: phasesel,
                    groupId: GROUP_EATFRESH,
                    completed: 1
                }, function () {
                    updateUserPhase({
                        userId: user.user_id,
                        phaseId: phasesel,
                        groupId: GROUP_MINDSWITCH,
                        completed: 1
                    }, function () {
                        LoadAllQuestionerdata({}, function () {
                            CheckDashboardAndMovePageTo('dashboard', false);
                        });
                    });
                });
            });
        } else {
            updateUserPhase({userId: user.user_id, phaseId: phasesel, groupId: groupID, completed: 1}, function () {
                LoadAllQuestionerdata({}, function () {
                    if (successhandler) {
                        successhandler();
                    } else {
                        CheckDashboardAndMovePageTo('dashboard', false);
                    }
                });
            });
        }
    } catch (e) {
        ShowExceptionMessage("updatePhaseCompleted", e);
    }
}
function getGroupPageBasedonGroupId(selectedgrpid) {
    var redirectpage = "activity-movesmart";
    if (selectedgrpid == GROUP_EATFRESH) {
        redirectpage = "activity-eatfresh";
    } else if (selectedgrpid == GROUP_MINDSWITCH) {
        redirectpage = "activity-mindswitch";
    }
    return redirectpage;
}
//function GetAcitvityDashBoardData(answers,points,activitysteps){
/**
 * @return {number}
 */
function GetAcitvityDashBoardData(answers, points) {
    logStatus("Calling GetAcitvityDashBoardData", LOG_FUNCTIONS);
    try {
        var activity_len = (answers) ? answers.length : 0;
        var totaldays = 0;
        if (activity_len > 0) {
            var ans_start_date = answers[0]['date'];
            var ans_last_date = getDateTimeByFormat('Y-m-d H:i:s');
            if (ans_start_date && ans_start_date != '') {
                var stdate = new Date(ans_start_date.split(" ")[0]);
                var lstdate = new Date(ans_last_date.split(" ")[0]);
                totaldays = DateDifferece(lstdate, stdate);
            }
            if (totaldays == 0) {
                totaldays = 1;
            }
        }
        var pointweek = points.perweek;
        var creditweek = points.weekcredits;
        var daycredit = (creditweek / pointweek);
        var weeks = (totaldays / 7); //MK Weeks mining by '7'
        var days = (totaldays % 7);
        var total_creditweeks = parseInt(weeks);
        if (days > pointweek) {
            days = pointweek;
        }
        //var totaldays = (total_creditweeks * pointweek) + days;
        totaldays = (total_creditweeks * pointweek) + days;
        return totaldays * daycredit;
    } catch (e) {
        ShowExceptionMessage("GetAcitvityDashBoardData", e);
    }
}
/* function updateprofileimage(givenurl) {
    var user = getCurrentUser();
    console.log("user type"+JSON.stringify(_LoggedUser));
    if(user.logintype==2){
        
        $('#userimage').attr('src', url);
        $('#userprofile').attr('src', url);
        $(".user_profileimage").removeClass("user_profileimageloader");
        $('#popupuserprofile').attr('src', url);
        $('.footuser_icon.user').css({'background-image': 'url("' + url + '")'});
        setMenuIcon(url, true);

                var url=user.image_url;
                console.log("profile url"+url);
                $(".user_profileimage").removeClass("user_profileimageloader");
                console.log("TheRemoveclass after this "+url);
               // $('.footuser_icon.user').css({'background-image': 'url("' + url + '")'});
                console.log('url fo image')
                console.log(url);
                $('#userimage').attr("src", url);
                $('#userprofile').attr("src", url);
                $('#popupuserprofile').attr("src", url); 
                 setMenuIcon(url, true);
            
    }else{
        
        getValidImageUrl(givenurl, function (url) {
        
            $('#userimage').attr('src', url);
            $('#userprofile').attr('src', url);
            $(".user_profileimage").removeClass("user_profileimageloader");
            console.log("Removeclass after this ");
            $('#popupuserprofile').attr('src', url);
            $('.footuser_icon.user').css({'background-image': 'url("' + url + '")'});
            setMenuIcon(url, true);
         });
    }
} */
function updateprofileimage(givenurl) {
    var user = getCurrentUser();
    if(user.logintype==2){
    var image = user.userimage.length;
               if(givenurl.indexOf('.jpg') > -1){
               getValidImageUrl(givenurl, function (url) {

                           $('#userimage').attr('src', url);
                           $('#userprofile').attr('src', url);
                           $(".user_profileimage").removeClass("user_profileimageloader");
                           console.log("Removeclass after this ");
                           $('#popupuserprofile').attr('src', url);
                           $('.footuser_icon.user').css({'background-image': 'url("' + url + '")'});
                           setMenuIcon(url, true);
                        });
               }
               else
               {
                var url=user.image_url;
                $(".user_profileimage").removeClass("user_profileimageloader");
                //$('.footuser_icon.user').css({'background-image': 'url("' + url + '")'});
                $('#userimage').attr("src", url);
                $('#userprofile').attr("src", url);
                $('#popupuserprofile').attr("src", url); 
                 setMenuIcon(url, true);
                }

            
    }else{
        
        getValidImageUrl(givenurl, function (url) {
        
            $('#userimage').attr('src', url);
            $('#userprofile').attr('src', url);
            $(".user_profileimage").removeClass("user_profileimageloader");
            console.log("Removeclass after this ");
            $('#popupuserprofile').attr('src', url);
            $('.footuser_icon.user').css({'background-image': 'url("' + url + '")'});
            setMenuIcon(url, true);
         });
    }
}
function getPercentageAppliedObj(obj, totalcount) {
    var retobj = obj;
	if(IsRequestSentForTestAppointment())
                {
                var totalcount = totalcount-1;    
                }
    if (totalcount != 0) {
        $.each(obj, function (gidx, qrow) {
            var per = ((parseInt(qrow.questions) / totalcount) * 100);
            var ansper = ((parseInt(qrow.answers) / totalcount) * 100);
            qrow.cons_qpercentage = isNaN(per) ? 0 : per.toFixed(2);
            qrow.cons_anspercentage = isNaN(ansper) ? 0 : ansper.toFixed(2);
            retobj[gidx] = qrow;
        });
    }
	//change on 17-05-17
	$('#totalquesvalue').val(totalcount);
    return retobj;
	
}
/*Edison Page Fixed Height*/
function pageHeight() {
    logStatus("Calling pageHeight", LOG_FUNCTIONS);
    try {
        var windowHeight = $(window).height();
        var headerHeight = $("header").outerHeight();
        var footerHeight = $("footer").outerHeight();
        var $main_panel_header = $(".main_panel_header");
        var $panel_cnt_header = $(".panel_cnt_header");
        var $circle_blue_btn = $(".circle_blue_btn");
        var $btn_center_box = $(".btn_center_box");
        var $slider_btn_bx = $(".slider_btn_bx");
        //var main_panel_header = $(".main_panel_header").outerHeight();
        var main_panel_header = ($main_panel_header.outerHeight()) ? $main_panel_header.outerHeight() : 0;
        //var panel_cnt_header = $(".panel_cnt_header").outerHeight();
        var panel_cnt_header = ($panel_cnt_header.outerHeight()) ? $panel_cnt_header.outerHeight() : 0;
        //var circle_blue_btn = $(".circle_blue_btn").outerHeight();
        var circle_blue_btn = ($circle_blue_btn.outerHeight()) ? $circle_blue_btn.outerHeight() : 0;
        var btn_center_box = ($btn_center_box.outerHeight()) ? $btn_center_box.outerHeight() : 0;
        var slider_btn_bx = ($slider_btn_bx.outerHeight()) ? $slider_btn_bx.outerHeight() : 0;

        var cntHeight = (windowHeight - headerHeight - footerHeight - 1);
        //var innerboxHeight = (cntHeight - main_panel_header - circle_blue_btn - panel_cnt_header - btn_center_box);
        var home_slider_inner = (windowHeight - headerHeight - footerHeight - slider_btn_bx);

        $(".main_panel").css({
            "height": cntHeight
        });
        //slider_btn_bx		
        $(".home_slider_inner").css({
            /*"height" : home_slider_inner */
            "height": home_slider_inner - 10
        });
        /*ED
         $(".welcome_ctn_bx").css({
         "height" : innerboxHeight 
         });*/
    } catch (e) {
        ShowExceptionMessage("pageHeight", e);
    }
}
function LoadApplicationData(onLoadSuccessData) {
    logStatus("Calling LoadApplicationData", LOG_FUNCTIONS);
    try {
        loadLanguage(function (response) {
            LANGUAGE = response;
            //_PageContentDetails
            GetAllPageContentData(function () {
                //_Questierdatainfo				
                LoadAllQuestionerdata({}, function () {
                    if (onLoadSuccessData) {
                        onLoadSuccessData()
                    }
                });
            });
        });
    } catch (e) {
        ShowExceptionMessage("LoadApplicationData", e);
    }
}
function initPageTemplate() {
    logStatus("Calling initPageTemplate", LOG_FUNCTIONS);
    try {
        var template = getPathHashData();
		 if(is_login() && template.path=='movesmart-login'){
		navigator.app.exitApp();
        }
        else{
        var path = (template.path != '') ? template.path : GetUserActiveTemplate();
        loadView(path, function () {
            _onPageLoadHandler(path);
			
        });
		}
    } catch (e) {
        ShowExceptionMessage("initPageTemplate", e);
    }
}
function loadView(path, onPathLoadSuccess) {
    logStatus("Calling loadView", LOG_FUNCTIONS);
    try {
        $(".page_content").load(PATH_VIEWS + path + ".html", function () {
            if (onPathLoadSuccess) {
                onPathLoadSuccess();
            }
			pagetitelanimation();
        });
    } catch (e) {
        ShowExceptionMessage("loadView", e);
    }
}
function getDummyGroupObject(dummygroupid, questionsobj, dummygroupname) {
    logStatus("Calling getDummyGroupObject", LOG_FUNCTIONS);
    try {
        var groupname = (dummygroupname) ? dummygroupname : 'General';
        var user = getCurrentUser();
        var user_id = ((!$.isEmptyObject(user)) && user.user_id) ? user.user_id : 0;
        return {
            group_id: dummygroupid,
            group_name: groupname,
            is_delete: 0,
            created_by: user_id,
            created_date: "0000-00-00 00:00:00",
            modified_by: user_id,
            modified_Date: "0000-00-00 00:00:00",
            _phasestartdate: getDateTimeByFormat('Y-m-d H:i:s'),
            _generalquestions: {},
            _generalquestioncount: 0,
            _coachcount: 0,
            _phase: {
                phase_id: PHASE_Check,
                phase_name: "Check",
                options: "2",
                is_delete: "0",
                created_by: "0",
                created_date: "0000-00-00 00:00:00",
                modified_by: "0",
                modified_Date: "0000-00-00 00:00:00",
                _activitiescount: "0",
                _activities: "",
                _phaserange: "",
                _phaserangecount: "0",
                _creditinfo: "",
                _creditinfocount: "0",
                _questionscount: questionsobj._questionscount,
                _questions: questionsobj._questions
            },
            _coachlist: {}
        }
    } catch (e) {
        ShowExceptionMessage("getDummyGroupObject", e);
    }
}
/*
 function pushQuestionToObject(sq,q){
 var qcount		=	q._questionscount;
 var questions	=	(qcount==1 || qcount==0) ? [q._questions]:q._questions;
 questions.push(sq);
 qcount++;
 var retobj	=	{};
 retobj._questionscount	=	qcount;
 retobj._questions		=	questions;
 return retobj;
 }
 */
/**
 * @return {boolean}
 */
function IsStaticQuestion(questionid) {
    return ([QUESTION_STATIC_preference, QUESTION_STATIC_target, QUESTION_STATIC_take_test].indexOf(parseInt(questionid)) != -1);
}
function ApplyPrefferedGroup(data) {
    logStatus("Calling ApplyPrefferedGroup", LOG_FUNCTIONS);
    try {
        /**
         * @param preference.app_group_preference This is app group preference
         * @param data.userpreference This is app userpreference
         */
        var preference = data.userpreference;
        var element = "";
        _setCacheValue('preferedgroup', preference.app_group_preference);
        switch (parseInt(preference.app_group_preference)) {
            case GROUP_MOVESMART:
                element = ".move_log";
                break;
            case GROUP_EATFRESH:
                element = ".eat_log";
                break;
            case GROUP_MINDSWITCH:
                element = ".mind_log";
                break;
        }
        $(".preferred_btn").remove("");
        if (element != '') {
            var $element = $(element);
            if (IsAnyGroupCompletedCheckPhase()) {
                // $element.html('');
            } else {
                $element.append('<label class="preferred_btn">!</label>')
            }
        }
    } catch (e) {
        ShowExceptionMessage("ApplyPrefferedGroup", e);
    }
}
/**
 * @return {boolean}
 */
function IsAnyGroupCompletedCheckPhase() {
    logStatus("Calling IsAnyGroupCompletedCheckPhase", LOG_FUNCTIONS);
    try {
        var activephase_move = getCurrentFaseForGroupId(GROUP_MOVESMART);
        var activephase_eat = getCurrentFaseForGroupId(GROUP_EATFRESH);
        var activephase_mind = getCurrentFaseForGroupId(GROUP_MINDSWITCH);
        return !!(activephase_move != PHASE_Check || activephase_eat != PHASE_Check || activephase_mind != PHASE_Check);

    } catch (e) {
        ShowExceptionMessage("IsAnyGroupCompletedCheckPhase", e);
    }
}
function GetPreferredGroup() {
    logStatus("Calling GetPreferredGroup", LOG_FUNCTIONS);
    try {
        var prefgroup = _getCacheValue('preferedgroup');
        return ((prefgroup) && (prefgroup != 0)) ? prefgroup : GROUP_MOVESMART;
    } catch (e) {
        ShowExceptionMessage("GetPreferredGroup", e);
    }
}
function sequrityQuestionsLoad(handleOnSuccess) {
    logStatus("Calling sequrityQuestionsLoad", LOG_FUNCTIONS);
    try {
        getsecurityQuestions('getSequirityQuestions', function (reponse) {
            var _sqQuestions = reponse.securityQuestions;
            var quesHTML = "";
            $.each(_sqQuestions, function (_index, _ques) {
                //to make and appear questions during registration
                //quesHTML += '<div class="input_box quescompulsary ">' + '<input class="input_blue input_blue_fontsize sq_ques req-nonempty" type="text" id="sqQues_' + _ques.secure_question_id + '" placeholder="' + _ques.question + '" />' + '</div>';
                quesHTML += '<div class="input_box  ">' + '<input class="input_blue input_blue_fontsize sq_ques " type="text" id="sqQues_' + _ques.secure_question_id + '" placeholder="' + _ques.question + '" />' + '</div>';
            });
            $(".questions_list").html(quesHTML);
              TranslateText("#sqQues_1", PHTRANS, TXT_SQ1);
              TranslateText("#sqQues_2", PHTRANS, TXT_SQ2);
              TranslateText("#sqQues_3", PHTRANS, TXT_SQ3);
              $(".comm_banner_img_box h4").text(GetLanguageText(TEX_SECURITY_QUESTIONS));
            if (handleOnSuccess) {
                handleOnSuccess();
            }
            /*
             setCaptchaContent(".recapchabox",function(reponse){
             /*On load Captcha Success* /
             }); /*Temp commented MK*/
        });
    } catch (e) {
        ShowExceptionMessage("sequrityQuestionsLoad", e);
    }
}
/**
 * @return {boolean}
 */
function IsAnswerEditable(answers, selectedDate) {
    logStatus("IsAnswerEditable", LOG_FUNCTIONS);
    try {
        var editable = true;
        if (!IsPhaseQuestionComplete(PHASE_Check)) {
            if ((answers.length > 0) && (!$.isEmptyObject(answers[0]))) {
                editable = false;
            }
            return editable;
        }
        if ((!selectedDate) || (selectedDate == "undefined") || (selectedDate == '')) {
            //var sdate 	=	new Date();
            selectedDate = getDateTimeByFormat('Y-m-d', new Date());
        }
        var ansdate = '';
        $.each(answers, function (idx, answer) {
            if (answer) {
                ansdate = (answer.date) ? answer.date : '';
                if (ansdate != '') {
                    ansdate = ansdate.split(" ")[0];
                }
                if (selectedDate == ansdate) {
                    editable = false;
                    return false;
                }
            }
        });
        return editable;
    } catch (e) {
        ShowExceptionMessage("IsAnswerEditable", e);
    }
}
function GetAnswerByDate(answers, selectedDate) {
    logStatus("GetAnswerByDate", LOG_FUNCTIONS);
    try {
        var retans = {};
        if (!IsPhaseQuestionComplete(PHASE_Check)) {
            retans = answers[0];
        }
        else {
            if ((!selectedDate) || (selectedDate == '')) {
                //var sdate 	=	new Date();
                selectedDate = getDateTimeByFormat('Y-m-d', new Date());
            }
            var ansdate = '';
            $.each(answers, function (idx, eachanswer) {
                ansdate = (eachanswer.date) ? eachanswer.date : '';
                if (ansdate != '') {
                    ansdate = ansdate.split(" ")[0];
                }
                if (selectedDate == ansdate) {
                    retans = eachanswer;
                    return false;
                }
            });
        }
        return retans;
    } catch (e) {
        ShowExceptionMessage("GetAnswerByDate", e);
    }
}
/**
 * @return {boolean}
 */
function IsRequestSentForTestAppointment() {
    logStatus("Calling IsRequestSentForTestAppointment", LOG_FUNCTIONS);
    try {
        var user = getCurrentUser();
        var isSent = false;
        if (!$.isEmptyObject(user)) {
            if (user.r_club_id != 0) {
                isSent = true;
            }
        }
        return isSent;
    } catch (e) {
        ShowExceptionMessage("IsRequestSentForTestAppointment", e);
    }
}
/*
 function ShowTipsPopup(content){
 logStatus("Calling ShowTipsPopup",LOG_FUNCTIONS);
 try{
 _popup(content,{close:false});
 } catch (e) {
 ShowExceptionMessage("ShowTipsPopup", e);
 }
 }
 */
function GetCredits(GroupId) {
    logStatus("Calling GetCredits", LOG_FUNCTIONS);
    try {
        var credit = GetLastCreditObject();
        var pointstogredit = 0;
        var totalinprocess = 0;
        var totalredeemed = 0;
        var totalcredits = 0;
        if (GroupId && GroupId != 0 && GroupId != GROUP_GENERAL) {
            if (credit['group' + GroupId]) {
                if (credit['group' + GroupId].redeemable) {
                   
                    pointstogredit = +credit['group' + GroupId].redeemable;
                    totalcredits += pointstogredit;
                }
                if (credit['group' + GroupId].redeemed) {
                   
                    totalredeemed = +credit['group' + GroupId].redeemed;
                    totalcredits += totalredeemed;
                }
                if (credit['group' + GroupId].inprocess) {

                    totalinprocess = +credit['group' + GroupId].inprocess;
                    totalcredits += totalinprocess;
                }
            }
            return {
                redeemable: pointstogredit,
                redeemed: totalredeemed,
                inprocess: totalinprocess,
                totalcredits: totalcredits
            };
        }
        $.each(credit, function (groupidx, row) {
            if (groupidx != 'group' + GROUP_GENERAL) {
                pointstogredit += +parseInt(row.redeemable);
                totalredeemed += +parseInt(row.redeemed);
                totalinprocess += +parseInt(row.inprocess);
            }
        });

        totalcredits = (pointstogredit + totalredeemed + totalinprocess);
        return {
            redeemable: pointstogredit,
            redeemed: totalredeemed,
            inprocess: totalinprocess,
            totalcredits: totalcredits
        };
    } catch (e) {
        ShowExceptionMessage("GetCredits", e);
    }
}
/*
 function GetCreditAndCreditRate(groupid,phaseid){
 logStatus("Calling GetCreditAndCreditRate",LOG_FUNCTIONS);
 try {
 var phase	=	GetPhaseCredits(groupid,phaseid);
 } catch (e) {
 ShowExceptionMessage("GetCreditAndCreditRate", e);
 }
 }
 */
function GetPhaseCredits(groupid, phaseid) {
    logStatus("Calling GetPhaseCredits", LOG_FUNCTIONS);
    try {
        var credit = GetLastPhaseCreditObject();
        var pointstogredit = 0;
        var totalinprocess = 0;
        var totalredeemed = 0;
        var totalcredits = 0;
        if ((phaseid && phaseid != 0) && (groupid && groupid != 0)) {
            if (credit['group' + groupid]['phase' + phaseid]) {
                if (credit['group' + groupid]['phase' + phaseid].redeemable) {
                    pointstogredit = +credit['group' + groupid]['phase' + phaseid].redeemable;
                    totalcredits += pointstogredit;
                }
                if (credit['group' + groupid]['phase' + phaseid].redeemed) {
                    totalredeemed = +credit['group' + groupid]['phase' + phaseid].redeemed;
                    totalcredits += totalredeemed;
                }
                if (credit['group' + groupid]['phase' + phaseid].inprocess) {
                    totalinprocess = +credit['group' + groupid]['phase' + phaseid].inprocess;
                    totalcredits += totalinprocess;
                }
            }
            var credits = (credit['group' + groupid]['phase' + phaseid]) ? credit['group' + groupid]['phase' + phaseid] : {};
            /**
             * @param credits.weightage This is weightage
             */
            return {
                redeemable: pointstogredit,
                redeemed: totalredeemed,
                inprocess: totalinprocess,
                totalcredits: totalcredits,
                initalcredits: credits.weightage,
                activitycredits: credits.activitycredits
            };
        }
        /*$.each(credit,function(groupidx,row){
         if(groupidx!='group'+GROUP_GENERAL){
         pointstogredit+=	+row.redeemable;
         totalredeemed+=	+row.redeemed;
         totalinprocess+=	+row.inprocess;
         }
         });*/
        totalcredits = (pointstogredit + totalredeemed + totalinprocess);
        return {
            redeemable: pointstogredit,
            redeemed: totalredeemed,
            inprocess: totalinprocess,
            totalcredits: totalcredits
        };
    } catch (e) {
        ShowExceptionMessage("GetPhaseCredits", e);
    }
}
function swipePagination(selector, noPagination) {
    logStatus("Calling swipePagination", LOG_FUNCTIONS);
    try {
        var settings = {
            preventClicks: true,
            slideToClickedSlide: false,
            mode: 'horizontal',
            loop: false,
            autoHeight: false,
            spaceBetween: 4
        };
        if (!noPagination) {
            settings.pagination = '.swiper-pagination';
            settings.paginationClickable = true;
        }
        //var intropage = new Swiper (selector,settings);
        new Swiper(selector, settings);
    } catch (e) {
        ShowExceptionMessage("swipePagination", e);
    }
}

function getActiveTrainingActivityObj() {
    var returnactobj = [];
    var trainingactobj = GetTrainingActivityBasedDetailObj();
    var lastseltraingid = GetLastSelectedActivityIdSession();
    if (lastseltraingid > 0) {
        if (!$.isEmptyObject(trainingactobj)) {
            $.each(trainingactobj, function (key, obj) {
                if (obj.activityid == lastseltraingid) {
                    returnactobj = obj;
                    return false;
                }
            });
        }
    }
    return returnactobj;
}
function updateLoginUserOnRefresh(onRefreshSuccess) {
    logStatus("Calling updateLoginUserOnRefresh", LOG_FUNCTIONS);
    try {
        var user = getCurrentUser();
        if (!$.isEmptyObject(user)) {
            var profileimage = user.userimage;
            var username = user.first_name;
            updateprofileimage(PROFILEPATH + profileimage);
            $('.loginusername').text(username);
            USER_LANG = user.r_language_id;
        }
        if (onRefreshSuccess) {
            onRefreshSuccess();
        }
    } catch (e) {
        ShowExceptionMessage("updateLoginUserOnRefresh", e);
    }
}
/**
 * @return {boolean}
 */
function IsActivitySartedPhaseOne() {
    logStatus("Calling IsActivitySartedPhaseOne", LOG_FUNCTIONS);
    try {
        var groupobj = getLastGroupObject();
        var selectgroup = groupobj['group' + GROUP_MOVESMART];
        var phase = selectgroup._phase;
        var isActivityType = (phase.phase_id == PHASE_phase1);
        var isExists = false;
        if (isActivityType) {
            if (phase._activitiescount > 0) {
                var activities = (phase._activitiescount == 1) ? [phase._activities] : phase._activities;
                $.each(activities, function (idx, activity) {
                    if (activity._answerscount > 0) {
                        isExists = true;
                    }
                    return (!isExists);//Loop Only If Answers not Exist
                });
            }
        }
        return isExists;
    } catch (e) {
        ShowExceptionMessage("IsActivitySartedPhaseOne", e);
    }
}
/*
 function IsAnswerStartedPhaseOne(){
 logStatus("Calling IsAnswerStartedPhaseOne", LOG_FUNCTIONS);
 try {

 } catch (e) {
 ShowExceptionMessage("IsAnswerStartedPhaseOne", e);
 }
 }
 */
function updateAppLogin(data) {
    logStatus("Calling updateAppLogin", LOG_FUNCTIONS);
    try {
        var isCheckRemindMe = true;//$("#is_remindme").is(":checked");
        _LoginInfo = data;
        _LoggedUser = data.user;
        USER_LANG = (data.user.r_language_id && data.user.r_language_id != 0) ? data.user.r_language_id : APP_LANG_ID;

        if (isCheckRemindMe) {
            _setCacheArray('user', data.user);
            _setCacheArray('login_info', data);
        }
        //var user_reqpage=	$(".tickouter .tick .selected_page");
        //var logintopage	=	'home';
        var loginfo = getCurrentLoginInfo();
        if (!$.isEmptyObject(loginfo)) {
            var loginuser = loginfo.user;
            var first_name = loginuser.first_name;
            var userimage = loginuser.userimage;
            var isclubmedicalinfo = loginuser.isclubmedicalinfo;
            $('.regusername').text(first_name);
            $('.loginusername').text(first_name);
            $('#isclubmedicalinfo').val(isclubmedicalinfo);
            updateprofileimage(PROFILEPATH + userimage);
			/****for version and devices****/
					var userid  = loginuser.user_id;
					var device_type = device.platform;
					var device_version = device.version;
					var app_version = APP_VERSION;
                    var DEVICE_ID = $('#device_token').val();
						/*if(device_type == 'iOS' || device_type == 'Android' || device_type == 'browser'){
                    updateDeviceId(userid,DEVICE_ID,device_type,device_version,app_version);
						}
						/**end**/
        }
        //LoadApplicationData(function(){
        TranslateGenericText();
        profileIncomplete('welcome');
        //});
    } catch (e) {
        ShowExceptionMessage("updateAppLogin", e);
    }
}

//Auto Register Login
function updateAppRegister(data) {
    logStatus("Calling updateAppRegister", LOG_FUNCTIONS);
    try {
        var isCheckRemindMe = true;//$("#is_remindme").is(":checked");
        _LoginInfo = data;
        _LoggedUser = data.user;
        USER_LANG = (data.user.r_language_id && data.user.r_language_id != 0) ? data.user.r_language_id : APP_LANG_ID;

        if (isCheckRemindMe) {
            _setCacheArray('user', data.user);
            _setCacheArray('login_info', data);
        }
        //var user_reqpage= $(".tickouter .tick .selected_page");
        //var logintopage   =   'home';
        var loginfo = getCurrentLoginInfo();
        if (!$.isEmptyObject(loginfo)) {
            var loginuser = loginfo.user;
            var first_name = loginuser.first_name;
            var userimage = loginuser.userimage;
            var isclubmedicalinfo = loginuser.isclubmedicalinfo;
            $('.regusername').text(first_name);
            $('.loginusername').text(first_name);
            $('#isclubmedicalinfo').val(isclubmedicalinfo);
            updateprofileimage(PROFILEPATH + userimage);
        }
        //LoadApplicationData(function(){
        TranslateGenericText();
        _movePageTo('welcome');
        //profileIncomplete('welcome');
        //});
    } catch (e) {
        ShowExceptionMessage("updateAppRegister", e);
    }
}



/*MK Added Check Summary Added Or not*/
function SetCheckSummaryViewed() {
    logStatus("Calling SetCheckSummaryViewed", LOG_FUNCTIONS);
    try {
        _setCacheValue('checksummaryviewed', 1);
    } catch (e) {
        ShowExceptionMessage("SetCheckSummaryViewed", e);
    }
}
/**
 * @return {boolean}
 */
function IsCheckSummaryViewed() {
    logStatus("Calling IsCheckSummaryViewed", LOG_FUNCTIONS);
    try {
        var viewed = _getCacheValue('checksummaryviewed');
        return (viewed && viewed == 1) ? true : true;
    } catch (e) {
        ShowExceptionMessage("IsCheckSummaryViewed", e);
    }
}

function GetSwiperContent(contentstring, selector, customclass) {
    logStatus("Calling GetSwiperContent", LOG_FUNCTIONS);
    try {
        /*copy to a function*/
        var contents = contentstring.split(MARKER_QUERY_PAGEBREAK);
        var ccls = customclass ? customclass : '';
        var HTML = '';
        if (contents.length > 0) {
            HTML = '<div class="swiper-wrapper">';
			var multing = 1;
            $.each(contents, function (i, eachcontent) {
                if (eachcontent != '') {
                    HTML += '<div class="swiper-slide ' + ccls + '">'+
					'<div class="comm_banner_img_box">'
                   +'<div class="transparent-stripe"></div>'
						+'<img src="images/banner/eat'+multing+'.jpg" alt="move2">'
						//+'<h4 style="bottom: 0px;">Thank you</h4>'
						//<img class="banner_logo" src="images/movesmartlogo.png" alt="movesmartlogo"> -->
					+'</div><div class="home_ctn_padding" >'+
					eachcontent +
					'</div></div>';
                }
				multing++;
            });
            HTML += '</div>';
            if(multing>2){
                HTML += '<div class="swiper-pagination"></div>';
            }
        }
        if (HTML != '') {
            $(selector).html(HTML);
            if(selector=='.summarycnt'){
                getSolidGaugeChart('yourscore_box');
            }
            //$(selector).css("width","100%");
            $(selector).css("overflow", "hidden");
            $(selector).css("margin", "none");
            //var ThankyouSwiper =	new Swiper(selector,{
            new Swiper(selector, {
                slideToClickedSlide: false,
                mode: 'horizontal',
                loop: false,
                autoHeight: false,
                pagination: '.swiper-pagination',
                paginationClickable: true
            });
        }
    } catch (e) {
        ShowExceptionMessage("GetSwiperContent", e);
    }
}
function closeActivityPopup() {
    $(".activityinfo_popup").hide();
}

function CheckAndDisconnectBT() {
    var deviceid = _getCacheValue('lasttestdeviceid');
    if (deviceid != '') {
        isHR_Connected(deviceid, function(){
            BLEDeviceDisConnect(deviceid);
        },function(){});
    }
}

function IsFase1CourseCompleted(groupId){
    logStatus("Calling IsFase1CourseCompleted", LOG_FUNCTIONS);
    try {
        var iscoursecomp = true;
        var groupobj	=	_Questierdatainfo['group'+groupId];
        if(!$.isEmptyObject(groupobj)){
            var phasestartdt	=	groupobj['_phasestartdate'];
            var dateExpiration	=	new Date(phasestartdt.split(" ")[0]);
            var today			=	new Date();
            dateExpiration.setDate(dateExpiration.getDate()+QUESTION_OPEN_DAYS);
            if(ComapreDate(dateExpiration,today,'less_than_or_equal')) {
                iscoursecomp = false;
            }

        }
        return iscoursecomp;
    }
    catch (e) {
        ShowExceptionMessage("IsFase1CourseCompleted", e);
    }
}
function getAgeByDOB(date) {
    logStatus("getAgeByDOB",LOG_FUNCTIONS);
    try {
        var today = new Date();
        var birthDate = new Date(date);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
        {
            age--;
        }

        return isNaN(age) ? 'N/A':age;
        
    } catch(e){
        ShowExceptionMessage("getAgeByDOB", e);
    }
}
function GoToVouchers() {
    logStatus("Calling GoToVouchers", LOG_FUNCTIONS);
    try {   
            CheckDashboardAndMovePageTo('vouchers-page');
           
        }
    catch (e) {
        ShowExceptionMessage("GoToVouchers", e);
    }
}
function validPhone(phone){
    logStatus("validPhone",LOG_FUNCTIONS);
    var filter = /^[0-9-+]+$/;
    if(phone)
    {   
        if (phone.length >=9 && phone.length<=12 && filter.test(phone)) {
        return true;
        }
        else{
            return false;
        }
    }
    else
    {
        return true;
    }
    
}  
function saveLastCoachSelectAnswerFromDiary(quesID,answer) {
    _setCacheArray('coachquesID', quesID);
    _setCacheArray('coachquesanswer', answer);
}
/*to change coach again*/
function ChangeCoachForTestAppointmentFromMenu(){
   logStatus("Calling GoForTestAppointmentFromMenu", LOG_FUNCTIONS);
   try {
    $('#is_change_coach').val(1);
   //$( ".user" ).trigger( "click" ); 
   CheckDashboardAndMovePageTo('appointment-request');
  
   } catch (e) {
   ShowExceptionMessage("GoForTestAppointmentFromMenu", e);
   }
                                                                                                   
}
/*to update device id*/
/*function updateDeviceId(userid,DEVICE_ID,device_type,device_version,app_version){
     logStatus("Calling MindUpdateActivities", LOG_FUNCTIONS);
        try {
        var postdata = {}; 
         postdata.user_id   =   userid;
         postdata.device_id  =   DEVICE_ID;
          postdata.device_type  =   device_type;
		  postdata.device_version = device_version;
		  postdata.app_version =  app_version;
         postdata.is_reporting=  base_url+"/reporting/index.php/voucherlist/addupdateDeviceIdforcoach";
         updatedeviceCoach(postdata,function(response){
                if(response.status==1){
              console.log("data saved");
                }
                else{
                console.log("data not saved");
                }
        });
    }
     catch(e) {
        ShowExceptionMessage("MindUpdateActivities", e);
    }
}*/