//var APP_ID = '38849b0714fa44e89da35294cb618dd5';
var IS_ENV_PROD = true;
//var IS_WEBAPP	=	true;
//var APP_ID		=	1;
var APPLICATION_NAME = (IS_ENV_PROD) ? "MOVESMART" : "clientapp_local";
//var _HOTEL_PREFX_ORDER = "SHP";
SHOW_EXCEPTION_ALERT = 1; //Set 1 to receive alert message when there is an exception or 0 to Just Log the exception (Invisible to the user)

var ENCKEY = '1234567shanetha123456789';

var _DefultHomePage = 'login';
var TOKEN_KEY = '7d8ab0663b9ecc43bd2835e409e532c9';
var GEN_TOKEN = '';

var LOCAL_LANG_TRANS = {
	"1" :{
		"0":"Welcome",//
		"1":"CHECK",//
		"2":"Profile",//
		"3":"Take the consult",//
		"4":"Privacy policy",//
		"5":"SELECT A COACH",//
		"6":'<p class="coach_head">Select coach from list below</p>',//
		"7":"MEETING REQUEST",//
		"8":"CREDIT",//
		"9":"YOUR CREDITS",//
		"10":"TO THE SHOP",//
		"11":"GET A CONSULT",//
		"12":"The credits can be achieved in different ways",//
		"14":" <p class='comm_para' >Thank you for entering all info we need to initiate your test. Your personal coach will guide you thru this test and will select togheter with you your goals and discusse  your achivements this far.</p><p class='comm_para' >Every 12 weeks you will get a retest and your goals will be adjusted to your personal needs and goals.</p>",
		"15":"Proceed",//
		"16":"MOVE ACTIVITY",//
		"17":"General Total",//
		"18":"MOVE LEVEL",//
		"19":"POINTS TO ACHIEVE",//
		"20":"FIND OTHER",//
		"21":"on your first training",
		"22":"CLIENT INFO",//
        "23": "SEARCH AGAIN",
        "24":"SELECT DEVICE",
        "25":"You can monitor your steps with a steps counter. This can be your Phone or a seperate device. If you have a Phone (list of supported phones) or device already you can start right away!",
        "26":"I don not want to use a stepmonitor",
        "27": "GET A COACH",
        "28": "SUPPORT",
		"29":"Today",
		"30":"VITALITY score",		// Added code 22-05-17
		"31":"Good that you choose to go back to work",
		"32":"Show top activities"
	},
	"3" :{
		"0":"Welkom",
		"1":"CHECK",
		"2":"Profiel",
		"3":"Kies voor Consult",
		"4":"Privacybeleid",
		"5":"SELECTEER COACH",
		//"6":"Selecteer Coach uit lijst",
		"6":'<p class="coach_head">Uit onderstaande lijst met coaches kun je de jouwe kiezen</p>', //23-may-2017
		"7":"VERZOEK AFSPRAAK",
		"8":"CREDITS",
		"9":"JOUW CREDITS",
		"10":"WEBSHOP",
		"11":"CONSULT",
		"12":"De Credits kunnen op verschillende manieren worden bereikt",
		"14":"<p class='comm_para' >Dank je wel voor je gegevens! We kunnen nu een consult voor je voorbereiden. Je persoonlijke coach zal spoedig contact met je opnemen.</p>",
		"15":"Doorgaan",
		"16":"MOVE ACTIVITY",
		"17":"TOTAAL",
		"18":"MOVE LEVEL",
		/* "19":"TE BEHALEN CREDITS DEZE WEEK", */
		/*-------Change on 5-5-17 -----*/
		"19":"TE BEHALEN FIT-punten DEZE WEEK",
		"20":"ANDERE",
		"21":"op je eerste training",
		"22":"CLIENT INFO",//
        "23": "OPNIEUW ZOEKEN",
        "24": "SELECTEER APPARAAT",
        "25":"Je kunt je stappen bijhouden met een stappenteller. Dit kan met je telefoon of een ander apparaat. Wanneer je een geschikte telefoon of apparaat hebt (zie lijst), dan kun je direct starten!",
        "26":"Ik maak geen gebruik van een stappenteller",
        "27": "ZOEK EEN COACH",
        "28": "SUPPORT",
		"29":"Vandaag",
		"30":"VITALITY score", // Added code 22-05-17
		"31":"Goed dat je ervoor kiest om weer te gaan trainen",
		"32":"Toon top activiteiten"
	}

};
