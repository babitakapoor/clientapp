/*SN added 20160321-select option append span*/
 $(document).ready(function(){
	 if (!IsRequestSentForTestAppointment()){
		 $('.coachinfoshow').css('display','none');
		 $('.findcoachshow').css('display','block');
	 }
	 else{
		  $('.coachinfoshow').css('display','block');
		 $('.findcoachshow').css('display','none');
	 }
   setInterval(function(){
     if(is_login()){
      var loginInfo = getCurrentLoginInfo();
      $('#username_sidemenu').html("");
      $('#username_sidemenu').html(loginInfo.user.first_name);
	  // checkAppversion(loginInfo.user.user_id);
     }
     else{
      $('#username_sidemenu').html("");
     }
   },8000);
   
  setInterval(function(){
	   if(is_login()){
      var login = getCurrentLoginInfo();
	  checkAppversion(login.user.user_id);
	   }
	   },100000);
});
function _onPageLoadHandler(path) {
    logStatus("Callling _onPageLoadHandler", LOG_FUNCTIONS);
    try {
        var page = __IsPageTemplateValid(path);
		if(page=='register'){
			 $('.logo').html('<img src="images/logo_arrow1.png"/>');
		 }
		 else{
			 $('.logo').html('<img src="images/logo.png"/>');
		 }
        if (__PageReqFooter.indexOf(page) != -1) {
            $(".ftr_bx_one").show();
            $(".ftr_bx_two").hide();
        } else {
            $(".ftr_bx_one").hide();
            $(".ftr_bx_two").show();
        }
        if (is_login()) {
            $(".reqlogin").show();
            $(".reqnologin").hide();
            $("#newuser").addClass("addnewuser");
            $(".lfb").css("display","none");
            
            //$(".menu_icon").show();
			hideInfoIcon();
        } else {
            $(".reqlogin").hide();
            $(".reqnologin").show();
            $("#newuser").removeClass("addnewuser");
            $(".lfb").css("display","block");

            //$(".menu_icon").hide();
			setInfoIcon();
            setMenuIcon();
        }
        if (IsPhaseQuestionComplete(PHASE_Check)) {
            
            $(".reqphasecheck").show();
			$(".ftr_bx_one").hide();
			$(".ftr_bx_two").show();
        } else {
            
            $(".reqphasecheck").hide();
			$(".ftr_bx_one").show();
			$(".ftr_bx_two").hide();
        }
        var reqtrin = IsTrainingActive();
        if (reqtrin) {
            $(".reqactivetrain").show();
        } else {
            $(".reqactivetrain").hide();
        }
        if(path=='dashboard-credits'){
            getCoinFillAnimation();
        }
        pageHeight();
        $(window).on('resize', function () {
            pageHeight();
        });

       // pagetitelanimation();
		footermeun_act();
		// editprofile
		editprofile();

    } catch (e) {
        ShowExceptionMessage("_onPageLoadHandler", e);
    }
}
function uploadProfileImage() {
    logStatus("Calling uploadProfileImage", LOG_FUNCTIONS);
    try {
        $("input#profilepic").trigger("click.#profilepic");
    } catch (e) {
        ShowExceptionMessage("uploadProfileImage", e);
    }
}
/**
 * @return {boolean}
 */
function IsWebLoad() {
    return (typeof(_doWebLoad) != 'undefined');

}

/* Uploading the Profile Picture by SNK*/
function UploadProfilePic(filepath, uploadservicepath) {
	
    logStatus("Calling UploadProfilePic", LOG_FUNCTIONS);
    try {
        var postdata = {};
        postdata.imagefile = 'image';
        postdata.uploadtype = UPLOAD_TYPE_USER_PROFILE;
        var User = getCurrentUser();
        postdata.modeUploadonly = 1;
        if (!$.isEmptyObject(User) && User.user_id) {
            postdata.modeUploadonly = 1;
            postdata.id = User.user_id;
        } else {
            postdata.responseAsJson = 1;
        }
        if (filepath == '') {
            postdata.image = $('input[type=file]')[0].files[0];
        }
        var getdata = {};
        getdata.action = 'updateProfileimage';
        $(".profileimages span").html("Uploading...");
        $(".user_profileimage img").attr('src','images/loading.gif');
        $(".user_profileimage").addClass("user_profileimageloader");

		//console.log('filepathh' +filepath);
		//console.log('fileurl' +uploadservicepath);
		
		//alert(filepath);
		//alert(uploadservicepath);
        UploadJpgImageToServer(filepath, uploadservicepath, function (response) {
			console.log("inside UploadJpgImageToServer");
            if (response) {
				console.log("aaaaaa==="+response);
                $(".profileimages span").html("Get Picture");
                var profileimage = '';
                if ((!postdata.modeUploadonly) || (postdata.responseAsJson)) {
                    if (typeof response !== 'object') {
                        try {
                            response = $.parseJSON(response);
                        } catch (e) {

                        }
                    }
                    /**
                     * @param response.status_message This is response status_message
                     */
                    if (response.status) {
                        if (response.status == AJAX_STATUS_TXT_SUCCESS) {

                            ShowToastMessageTrans(IMAGE_UPLOAD_SUCCESS, _TOAST_LONG);
                            if (response.imagefile) {
                                profileimage = response.imagefile;
                            }
                        } else {
                            ShowAlertMessage(response.status_message);
                        }
                    }
                } else {
                    profileimage = response;
					//console.log("pro==="+profileimage);
                }
				
				
                if (profileimage != '') {
                    /*SN commanded-after login we can use it*/
                    //updateUserInfoLocal({userimage:profileimage});						
                    updateprofileimage(PROFILEPATH + profileimage);
					console.log("aaaa"+profileimage);
                    $('#imgval').val($.trim(profileimage));
                     var User = getCurrentUser();
                     var userid= User.user_id;
                    if (is_login()) {
                       updateUserInfoLocal({userimage:profileimage});
                    }
                }
			
            }
        }, function () {
        }, getdata, postdata);
    } catch (e) {
		
        ShowExceptionMessage("UploadProfilePic", e);
    }
}
function is_login() {
    logStatus("Calling is_login", LOG_FUNCTIONS);
    try {
        if ($.isEmptyObject(_LoggedUser) && $.isEmptyObject(_LoginInfo)) {
            _LoggedUser = getCurrentUser();
            _LoginInfo = getCurrentLoginInfo();
            
            //TranslateGenericText();
			
        }
        return !!( (!$.isEmptyObject(_LoggedUser)) && (!$.isEmptyObject(_LoginInfo)));
    } catch (e) {
        ShowExceptionMessage("is_login", e);
    }
}
function gotoEditProfile(){
$( ".user" ).trigger( "click" );
_movePageTo('edit-profile');
}

function gotoCoachProfile(){
$( ".user" ).trigger( "click" );
_movePageTo('edit-coachprofile');
}
function gotoCoachProfiledash(){
//$( ".user" ).trigger( "click" );
_movePageTo('edit-coachprofile');
}
function signout() {
    logStatus("Callling signout", LOG_FUNCTIONS);
    try {
	$( ".user" ).trigger( "click" );
        localStorage.clear();
        _LoginInfo = {};
        _LoggedUser = {};
        _movePageTo('intro');
        TranslateGenericText();
    } catch (e) {
        ShowExceptionMessage("signout", e);
    }
}
function TranslateGenericText() {
    TranslateText(".transfirstname", PHTRANS, PH_FIRST_NAME);
    TranslateText(".translastname", PHTRANS, PH_LAST_NAME);
    TranslateText(".nextcls", TEXTTRANS, LBL_NEXT);
    TranslateText(".transsave", VALTRANS, BTN_SAVE);
    TranslateText(".transselect", TEXTTRANS, LBL_SELECT);
    TranslateText(".transsearch", PHTRANS, TEX_SEARCH);
    TranslateText(".refresh_btn", TEXTTRANS, PH_REFRESH_BTN);
    TranslateText(".savecls", TEXTTRANS, BTN_SAVE);
    TranslateText(".transemail", TEXTTRANS, PH_EMAIL);
    TranslateText(".transaction", TEXTTRANS, LBL_ACTION);
    TranslateText(".transgender", TEXTTRANS, LBL_GENDER);
    TranslateText(".transfirstname", TEXTTRANS, PH_FIRST_NAME);
    TranslateText(".transphone", PHTRANS, PH_PHONE);
    TranslateText(".transemail", PHTRANS, PH_EMAIL);
    TranslateText(".transcountry", TEXTTRANS, TXT_COUNTRY);
    TranslateText(".transtxtexit", TEXTTRANS, TXT_EXIT);
    TranslateText(".transmandatory", TEXTTRANS, TXT_MANDATORY);
    TranslateText(".transgallery", TEXTTRANS, TXT_GALLERY);
    TranslateText(".transcamera", TEXTTRANS, TXT_CAMERA);
    TranslateText(".transtextmale", TEXTTRANS, TXT_MALE);
    TranslateText(".transtextselgender", TEXTTRANS, TXT_SELECT_GENDER);
    TranslateText(".transtextfemale", TEXTTRANS, TXT_FEMALE);
    TranslateText(".transsearch", TEXTTRANS, TEX_SEARCH);
    TranslateText(".transgofortest", TEXTTRANS, BTN_GO_FOR_TEST);
    TranslateText(".transouradvice", TEXTTRANS, BTN_OUR_ADVICE);
    TranslateText(".transyourgoal", TEXTTRANS, TXT_YOUR_OWN_GOAL);
    TranslateText(".transcopyyesterday", TEXTTRANS, TXT_COPY_YESTERDAY);
    TranslateText(".transsave", TEXTTRANS, BTN_SAVE);
    TranslateText(".transgetpicture", TEXTTRANS, TEX_GET_IMAGE);
    TranslateText(".transbirthday", PHTRANS, PH_BIRTHDAY);
    TranslateText(".transbirthday", TEXTTRANS, PH_BIRTHDAY);
    TranslateText(".translength", PHTRANS, PH_LENGTH);
    TranslateText(".translength", TEXTTRANS, PH_LENGTH);
    TranslateText(".transweight", PHTRANS, LBL_WEIGHT);
    TranslateText(".transweight", TEXTTRANS, LBL_WEIGHT);
    TranslateText(".transtepgame", TEXTTRANS, BTN_STEP_GAME);
    TranslateText(".transback", TEXTTRANS, BTN_BACK);
    TranslateText(".transregiter", TEXTTRANS, BTN_REGISTER);
    TranslateText(".transintro", TEXTTRANS, BTN_INTRO);
    TranslateText(".transgofortraining", TEXTTRANS, BTN_GO_FOR_TRAINING);
    TranslateText(".transinfo", TEXTTRANS, BTN_INFO);
    TranslateText(".transprofile", TEXTTRANS, TXT_PROFILE);
    TranslateText(".transpolicy", TEXTTRANS, TXT_PRIVACY_POLICY);
    TranslateText(".transplcycond", TEXTTRANS, TXT_PRIVACY_POLICY_TERMS_CONDITIONS);
    TranslateText(".transcancel", TEXTTRANS, BTN_CANCEL);
    TranslateText(".transtoshop", TEXTTRANS, TXT_TO_SHOP);
    TranslateText(".transvouchers", TEXTTRANS, TXT_VOUCHERS);
    TranslateText(".transforconsult", TEXTTRANS, TXT_CONSULT);
    TranslateText(".transcontinue", TEXTTRANS, BTN_CONTINUE);
    TranslateTextLocal(".transprofilenew",TEXTTRANS,"2");
    TranslateTextLocal(".transtakeconsult",TEXTTRANS,"3");
    TranslateTextLocal(".transprivacypolicy",TEXTTRANS,"4");
    TranslateTextLocal(".transSupportPage",TEXTTRANS,"28");
    if (is_login()) {
        TranslateText(".translogout", TEXTTRANS, PAGE_TTL_LOGOUT);
    } else {
        TranslateText(".translogout", TEXTTRANS, PAGE_TTL_LOGIN_ON);
    }
}
function TranslateVoucherPageText(){
    TranslateText(".transvoucherhead", TEXTTRANS, TXT_VOUCHERS_HEAD);
    TranslateText(".transVoucherbtn", TEXTTRANS, TXT_VOUCHERS_BTN);

}

function setMenuIcon(url, isFull) {
    var backgroundSize = isFull ? "100% 100%" : '50% 50%';
    var imageurl = (url) ? url : 'images/menuicon.png';
    $(".menu_icon").css({
        "background-image": 'url(' + imageurl + ')',
        "background-size": backgroundSize
    });
}

function setInfoIcon(){
$(".info_icon").css({
        "background": 'url(images/info.png) no-repeat center center',
        "background-size": "100%"
    });
	 try{
		UpdateDynamicPageForId('.home_slider_inner .content',INTRO_PAGE_CONTENT_ID,function(content,pagetitle){
            var introcntstr = content.toString();
            var introcnt = introcntstr.split(MARKER_QUERY_PAGEBREAK);
            //XX var intropagecnt = '<div class="swiper-wrapper">';
            var intropagecnt = '';
			intropagecnt+='<div class="welcome_ctn_bx" style="overflow: auto; max-height: 90%;"> ';
			var multing = 1;
            $.each(introcnt,function(key,value){
                
				
				intropagecnt+='<div class="home_ctn_padding" style="padding: 0px !important;    margin: 0 0px 60px !important;" >'+value+'</div>';
				
				
				multing++;
            });
			intropagecnt+='</div><div class="footer-info"><img src="images/bottom-info.png"></div>';
            //XX intropagecnt+='</div><div class="swiper-pagination"></div>';
            $('.info-content').html(intropagecnt);
           
           
        });
		
        
    } catch (e) {
        ShowExceptionMessage("Welcome.Document.Event.Ready", e);
    }
	
	$(".info_icon").show();
}

function hideInfoIcon(){
$(".info_icon").hide();
}



/*PK Added Privacy policy */
function GoPrivacyPolicy() {
    //ConfirmPrivacyPolicy(function(){alert(1111111);
	$( ".user" ).trigger( "click" );
    $(".privacypopup").show();
    $(".privacy_bg").show();
    /*$("#idagreeprivacy").on("click",function(){
     closeTermsPopup();
     onAgreeTerms(this);
     });
     $("#iddisagreeprivacy").on("click",function(){
     closeTermsPopup();
     onDisagreeTerms(this);
     });*/
    //});
}
function ConfirmPrivacyPolicy(onAgreeTerms, onDisagreeTerms) {
    $(".resitboxpop_outer").show();
    $(".privacy_bg").show();
    $("#idagreeprivacy").on("click", function () {
        closeTermsPopup();
        onAgreeTerms(this);
    });
    $("#iddisagreeprivacy").on("click", function () {
        closeTermsPopup();
        onDisagreeTerms(this);
    });
}
function closeTermsPopup() {
    $(".resitboxpop_outer").hide();
    $(".privacypopup").hide();
    $(".privacy_bg").hide();
    $("#iddisagreeprivacy, #idagreeprivacy").off("click");
}
function openPrivacyPolicy() {
    var $termscond = $("#termscond");
    ConfirmPrivacyPolicy(function () {
        $termscond.addClass("checkbtn");
        $termscond.find('input').attr("checked", "checked");
    }, function () {
        $termscond.removeClass("checkbtn");
        $termscond.find('input').removeAttr("checked");
    });
}

/*
 function checkSpcialChar(event){
 if(!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 48) && (event.keyCode <= 57))){
 event.returnValue = false;
 return;
 }
 event.returnValue = true;
 }
 */
/*PK- AutoLogin(2016/05/11)*/
/*
 function handleOpenURL(url){
 logStatus("Callling handleOpenURL",LOG_FUNCTIONS);
 try{
 //var valfirst = APP_LINK.split('//?');
 var valfirst = url.split('//?');
 var namepassword = valfirst[1].split('&');
 var namesplit = namepassword[0].split('=');
 var pswsplit = namepassword[1].split('=');
 var username = namesplit[1];
 var password = pswsplit[1];
 //ShowLoader("Authenticating... Please wait");		
 LoadApplicationData(function(){
 ShowLoader(GetLanguageText(MSG_AUTHENTICATING));
 getLoginByUsernamePassword(base64_decode(username),base64_decode(password),function(response){
 HideLoader();
 if(response.status_code==1){
 updateAppLogin(response);					
 } 
 });
 });
 } catch(e) {
 ShowExceptionMessage("handleOpenURL", e);
 }
 }

 */

// function pagetitelanimation() {
    // setTimeout(function(){
        // $(".comm_banner_img_box h4").animate({
            // "bottom": 0
        // })

    // }, 3000);
// }


function editprofile(){
	/*
	$(".neweditprofile").on("click",function(e){
		if($(".editprofile").is(":visible")){			
			//$(".new_profile, .new_profile_bg").slideUp(),1000;
			//e.preventDefault();
			MoveActiveUserPage();
		}else{
			$(".new_profile, .new_profile_bg").slideDown(),1000;					
		}
	});
	
	$(".new_profile_bg, .new_profile ul li").on("click",function(){
		$(".new_profile, .new_profile_bg").slideUp(),500;
	});
	*/
}


/*
* Selected Slot Scroll
* */
function applyScrollPanel(selector){
    var $selector  =   $(selector);
    $selector.hide();
    var HTML    =   '';
    $selector.each(function(){
        var $self   =   $(this);
        var $options =    $self.find('option');
        var HTML    =   '<ul class="questionselect_list questionans_list liselectoptions">';
        $options.each(function(){
            var $option =   $(this);
            var text =   $option.text();
            var value =   $option.attr('value');
            var class_selected  =   '';
            if(value!='pick_no_selected') {
                if ($self.val() == value) {
                    class_selected = 'act_select';
                }
                HTML += '<li class="' + class_selected + '" ' +
                    'onclick="setValueSelected(this,\'' + $self.attr('id') + '\')" ' +
                    'selected-value="' + value + '" text="' + text + '">' + text + '</li>';
            }
        });
        HTML+=   '</ul>';
        $self.after(HTML);
    });
}
function setValueSelected(self,targetId){
    var $self   =   $(self);
    var $parent   =   $self.parent();
    $("#" + targetId).val($self.attr('selected-value'));
    /*
    * Enable once Scroll bottom is fixed
    * * /
    var top     =   $self.offset().top ? $self.offset().top - $parent.offset().top + $parent.scrollTop():0;

    $parent.animate({
        scrollTop:(top>0) ? top:0
    });
    */
}
/*
* Selected Scroll Panel - Ends
* */
//* Open support link
function GoToSupportPage()
{	
	do_service({is_reporting: BASE_URL+"reporting/index.php/Report/get_supportlink"},{is_reporting: BASE_URL+"reporting/index.php/Report/get_supportlink"},
	function (response) 
	{   
		var url=response.link;
		window.open(url, '_system');
		// navigator.app.loadUrl('https://google.com/', { openExternal:true });
	},'json');
}
//* Open privacy policy link
function GoToprivacylink()
{
	var url="http://www.movesmart.company/gebruikersvoorwaarden-en-privacy/";
	window.open(url, '_system');
}
function AndroidIosPlatform(){
    var string = device.platform;
       return string;
}
/**********************************/
function checkAppversion(user_id)
{
    /*alert(user_id);
    alert(device.platform);
    alert(device.version);
    alert(device.model);
    alert(APP_VERSION);*/
    var postdata= {};
    postdata.user_id = user_id;
    postdata.app_platform = device.platform;
    postdata.device_version = device.version;
    postdata.device_model = device.model;
    postdata.app_version = APP_VERSION;
    addAppVersion_type(postdata,function(response){
                      console.log(response.status_message);
                       });
}