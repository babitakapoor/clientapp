
function getToken(){
	logStatus("Get Token Device ID == "+device.uuid, LOG_FUNCTIONS);
	var formData = new FormData(); 
	if(device.uuid == null){
	
	 device.uuid = "qwrettyyrtyrtursss";
	}
	formData.append("device_id", device.uuid);
	formData.append("secret_key", TOKEN_KEY);

	$.ajax({
	  url: BASE_URL+'backoffice/service/generate_token.php',
	  data: formData,
	  processData: false,
	  contentType: false,
	  type: 'POST',
	  success: function(data){
		//alert(data);
		
		var resultJson = JSON.parse(data);
		GEN_TOKEN = resultJson.token;
		logStatus("Get Token RESULT == "+resultJson.token, LOG_FUNCTIONS);
		LoadApplicationData(function () {
			
            //FB Login
            openFB.init({appId: '946205342143418', tokenStore: window.localStorage});
            //Remove - Splash Screen Init loading completed
            var RedeemCredits = GetCredits();
            //XX var selectedGroupID	=	getlastActiveGroupId();
            //XX var RedeemCredits	=	GetCredits(selectedGroupID);
            updateLoginUserOnRefresh(function () {
                //Need to be used for user data update later.
            });
            if (RedeemCredits.redeemable != 0) {
                //Need to workout
                _movePageTo('phase-credits');
            }
            initPageTemplate();
            $(window).bind('hashchange', function () {
                initPageTemplate();
            });
			
			
			
        });
		
	  }
	});
}
