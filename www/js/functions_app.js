/******************************************************************************************/
/********************************* Application Level  - Start **********************************/
/******************************************************************************************/
/*
 var Num=1;
 var GlobalActiveScope = null;
 var GlobalFooterScope = null;
 */
/*
 function DeviceLoadCompleted(){ 
 _IS_SYNC_PROGRESS = 0;
 CheckAndBlockScreenNotSyncronized(); 
 setTimeout(function() {stopprogress()}, 2000);
 setTimeout(function() {HideSplashScreen();}, 2000);
 var GlobalActiveScope;
 if (GlobalActiveScope){
 if (GlobalActiveScope.refresh){
 GlobalActiveScope.refresh();        
 }
 }
 var GlobalFooterScope;
 if (GlobalFooterScope){
 if (GlobalFooterScope.refresh){
 GlobalFooterScope.refresh();        
 }
 }
 //setUserData(FormatedDate(new Date(),FORMAT_ONLY_DATE)); //By Sankar
 }
 function SetActivePageLoadHandler(LoadHandler){
 _ACTIVEPAGEHANDLER = LoadHandler;
 }
 function InitApp(){
 logStatus("Execute InitApp ",LOG_FUNCTIONS);
 stopprogress();
 /**
 * @param angular This is angular
 * @param angular.bootstrap  This is angular bootstrap
 */
/*angular.element(document).ready(function() {
 angular.bootstrap(document, ["MovesmertclientApp"]);
 _ISAPPLOADED = true;
 });	* /
 }
 */
/*
 function RefreshScroll(){
 /**
 * @param myScroll2 This is scroll value
 * @param myScroll  This is scroll value
 * /
 myScroll2.scrollTo(0, 0, 1000);
 //alert($("#wrapper").height());
 setTimeout(function(){         
 myScroll.refresh();
 myScroll2.refresh();
 //alert(1);
 }, 1000)
 }
 */
/*
 function LoadCurrentPageData(){
 logStatus("LoadCurrentPageData", LOG_FUNCTIONS);    
 try{
 if (_ACTIVEPAGEHANDLER){
 //SetWrapperHeight();
 _ACTIVEPAGEHANDLER();
 //ApplyAppTheme();
 stopprogress();
 }else{
 // ExecuteMainCartContent();
 } 
 } catch (e) { 
 stopprogress();
 ShowExceptionMessage("LoadCurrentPageData", e); 
 }        

 }
 */
/******************************************************************************************/
/********************************* Main Cart Screen  - Start ******************************/
/******************************************************************************************/

/*
 function rTrim(text,token){
 return text.substring(0,text.lastIndexOf(token));
 }*/


$(document).delegate('.custom_checkbox', 'click', function () {
    var check = $(this).find('.check');
    var $this = $(this);
    if (check.is(':checked')) {
        check.attr('checked', false);
        $this.removeClass("selected");
    } else {
        check.attr('checked', true);
        $this.addClass("selected");
    }
});
/*Function created and implemented by MK*/
__sortMe = function (originalObject, _sortHandler) {
    try {
        var _objectAsArray = [];
        var _objectAsObject = {};
        $.each(originalObject, function (key, rows) {
            _objectAsArray.push({data: rows, key: key})
        });
        if (_sortHandler) {
            _objectAsArray.sort(_sortHandler);
        } else {
            _objectAsArray.sort();
        }
        for (var _iter = 0; _iter < _objectAsArray.length; _iter++) {
            _objectAsObject[_objectAsArray[_iter].key] = _objectAsArray[_iter].data;
        }
        return _objectAsObject;
    } catch (e) {
        ShowExceptionMessage("Object.prototype.__sortMe", e);
    }
};

/******************************************************************************************/
/********************************* Main Cart Screen  - End ******************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Application Level  - End **********************************/
/******************************************************************************************/