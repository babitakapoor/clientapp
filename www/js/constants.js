
//var BASEURL	=	'https://www.movesmart.company/backoffice/';
//var BASEURL	=	'http://192.168.1.27:8080/fitclass_apps/adminnew/';
//var BASEURL	=	'https://www.movesmart.company/staging/';

//For testquestionaies uncomment this and comment the above
//var BASEURL	=	'http://movesmart.offshoresolutions.nl/livetest/backoffice/';
//var BASE_URL = 'http://movesmart.offshoresolutions.nl/livetest/';
//----------------------------------------------
//For LIVE uncomment this and comment the STAGING
//var BASEURL	=	'https://movesmart.offshoresolutions.nl/backoffice/';
//var BASE_URL = 'https://movesmart.offshoresolutions.nl/';
// --------------------------------------------
//For STAGING uncomment this and comment the LIVE
//var BASEURL	=	'http://movesmart.offshoresolutions.nl/movesmart_staging/backoffice/';
//var BASE_URL = 'http://movesmart.offshoresolutions.nl/movesmart_staging/';
// --------------------------------------------
//For DEV uncomment this and comment the above
//var BASEURL	= 'http://movesmart.offshoresolutions.nl/movesmart_dev/backoffice/';
//var BASE_URL = 'http://movesmart.offshoresolutions.nl/movesmart_dev/';
///----------------------------------------------
//For Acceptance uncomment this and comment the above
var BASEURL	=	'http://movesmart.offshoresolutions.nl/movesmart_acceptance/backoffice/';
var BASE_URL = 'http://movesmart.offshoresolutions.nl/movesmart_acceptance/';
///----------------------------------------------
//For HR test, Livetest uncomment this and comment the above
//var BASEURL = 'http://movesmart.offshoresolutions.nl/livetest/backoffice/';
//var BASE_URL = 'http://movesmart.offshoresolutions.nl/livetest/';
    
//For local environment
//var BASEURL = 'http://movesmart/backoffice/';
//var BASE_URL = 'http://movesmart/';
//For Offshore Local
//var BASEURL='http://192.168.1.11/movesmart/backoffice/';
//var BASE_URL='http://192.168.1.11/movesmart/';
//----------------------------------------------
var APP_VERSION = '2.0.59';
var SHOW_EXCEPTION_SOCIALMEDIA_NO_TELPHONE = "Vul alstublieft de Geboortedatum in.";
var URL_SERVICE_NOTI = BASEURL + 'service/notification.php';
var URL_SERVICE = BASEURL + 'service/service.php';
//var URL_SQ		=	BASEURL+'service/_sq.php';
var PROFILEPATH = BASEURL + 'images/uploads/movesmart/profile/';
//var QUESTIONPATH= BASEURL+'images/uploads/movesmart/quesicon/';
var ACTIVITYIMGPATH = BASEURL + 'images/activity/';
/*Allows only client user CLIENT*/
var APP_USERTYPE = 3;
var APP_COMPANY_ID = 2;
var APP_LANG_ID = 3;
var USER_LANG = APP_LANG_ID;
var UPLOAD_TYPE_USER_PROFILE = 1;//MK Upload IMAGES for User Profile image

var QUESTION_OPEN_DAYS = 14;//MK May be changed Later

//var TOTAL_CLIENT_REGSTEPS			=	12;
//var TOTAL_CLIENT_REGSTEPS_wo_medical=	2;

//var DEVICE_TYPE_HR_monitor	=	1;

var AJAX_STATUS_TXT_SUCCESS = "success";
//var AJAX_STATUS_TXT_ERROR	= "error";

/*Message Success */
//var SUCCESS_CLIENT_ADD		=	"Thanks for the registration. Your details are sent to MOVESMART.company";
//var SUCCESS_ADDRESSINFO 	=	"Address Information Added sucessfully ";
//var SUCCESS_PERSONALINFO  	=	"Personal Information Added sucessfully ";
//var SUCCESS_PROFESSION_WORK_TYPE= "ProfessionWorkType Information Added sucessfully ";
//var SUCCESS_CLIENTACTIVITY 		= "ClientActivity Information Added sucessfully ";
//var SUCCESS_GPINFOMORMATION		= "GPInfomormation Information Added sucessfully ";
//var SUCCESS_MEDICALINJURY		=	"MedicalInjury Information Added sucessfully";
//var SUCCESS_MEDICAL_INFO		=	"Medical Information Added sucessfully";
//var SUCCESS_STRESSTESTINFO		=	"StressTestInfo Information Added sucessfully";
//var SUCCESS_SMOKEINFO			=	"Smoke Information Added sucessfully";
//var SUCCESS_CLIENTCURRENTFEEL	=	"ClientCurrentFeel Information Added sucessfully";
//var SUCCESS_SIGNATURE_UPDATED	=	"Registeration completed successfully";
//var SUCCESS_APPOINTMENTSENT_EMAIL	=	"Appointment request E-mail sent successfully";
/*Allows Exception*/
//var EXCEPTION_INCOMPLETE_PROFILE=	"Profile is incomplete. Please continue to complete.";
//var EXCEPTION_EMAIL_EXISTS		=	"Client - Email is already asssociated with us.";
//var EXCEPTION_ACCEPT_DECLARATION=	"Please check 'declare for real and read' to accept declaration.";
//var EXCEPTION_SIGNATURE_MISSING	=	"Please signature of the client is required.";
var EXCEPTION_TRAINING_NOT_ACTIVE = "Currently you don't have any active training week.";

//var MSG_INFO_PHASE_2_BEFORE_TEST	= 	"You have to do the test, consult your MOVESMART.coach";

//var USER_SELECT_NONE		=	0;
//var USER_SELECT_CARDIOTEST	=	1;
//var USER_SELECT_STRENGTHTEST=	2;

var PATH_VIEWS = 'src/views/';

/*HOW DO YOU FEEL TODAY ? - CONSTANTS - Need to load FROM Master Table */
/*
 var FEEL_EXCELLENT		=	1;
 var FEEL_GOOD			=	2;
 var FEEL_AVERAGE		=	3;
 var FEEL_POOR			=	4;
 var FEEL_BAD			=	5;
 var FEEL_LIGHT_PHYSICAL_LABOR	=	6;
 */
/* QUESTION TYPE MAY LOAD FROM - DB FUTURE*/
var QUESTION_TYPE_NUMBER = 1;
var QUESTION_TYPE_OPTION = 2;
var QUESTION_TYPE_YN = 3;
var QUESTION_TYPE_SMILEY = 4;
var QUESTION_TYPE_CHECKBOX = 5;
var QUESTION_TYPE_LIKDISLIK = 6;

/*PHASE ID*/
var PHASE_Check = 1;
var PHASE_phase1 = 2;
var PHASE_phase2 = 3;
var PHASE_phase3 = 4;
//var PHASE_phase4=	5;

/*PHASE RANGE FOR SUCCESS*/
var PHASE_SUCCESS = 1;
var PHASE_QUICKWIN = 2;
var PHASE_GOAL = 3;
var PHASE_ADVICE = 4;
var STATUS_TEXT = {};
STATUS_TEXT[PHASE_SUCCESS] = 'Success';
STATUS_TEXT[PHASE_QUICKWIN] = 'Quick Win';
STATUS_TEXT[PHASE_GOAL] = 'Goal';
STATUS_TEXT[PHASE_ADVICE] = 'Advice';

var GROUP_MOVESMART = 1;
var GROUP_EATFRESH = 2;
var GROUP_MINDSWITCH = 3;
var GROUP_GENERAL = 999999;//Dummy Group

//var STEP_GAME_BYSTEPS	=	0;
var STEP_GAME_BYMINUTES = 1;

/*MK Added - APP Static Questions - Starts*/
var QUESTION_STATIC_preference = 41;//Static Question ID:
var QUESTION_STATIC_take_test = 42;//Static Question ID;
var QUESTION_STATIC_target = 9999999;//Static Question ID
/*MK Added - APP Static Questions - Ends*/

var MARKER_QUERY_PAGEBREAK = '[MQUERY PAGEBREAK]';
var MARKER_QUERY_QUSBREAK = '[MQUERY QUSBREAK]';
var MARKER_QUERY_SCORE = '[MQUERY SCORE]';
var MARKER_QUERY_QWTEXT = '[MQUERY QUIKWINTEXT]';
var MARKER_QUERY_SUCTEXT = '[MQUERY SUCCESSTEXT]';
var MARKER_QUERY_QWTOPIC = '[MQUERY QUIKWINTOPIC]';
var MARKER_QUERY_SUCTOPIC = '[MQUERY SUCCESSTOPIC]';
var MARKER_QUERY_CONTINUE = '[MQUERY BTNCONTINUE]';
//var MARKER_QUERY_QWEDU		=	'[MQUERY QWEDU]';

var URL_SUFIX_FOR_BACK = '&_back=back';

var POINT_STATUS_NEW = 0;

/*Language*/
//var USER_LANG	   =	1;
/*SN added default language in dutch*/
var DEFAULT_LANG = 3;
/*translation variable types */
var HTMLTRANS = 1;
var VALTRANS = 2;
var TEXTTRANS = 3;
var PHTRANS = 4;
/*language traslation Key*/
var BTN_SUBMIT = 1;
//var LBT_TABLE	                               = 2;
//var CLICK_TABLE	                               = 3;
var BTN_REMEMBER_ME = 5;
var LBL_FORGOT_PASSWORD = 6;
var PH_USERNAME = 143;
var PAGE_TTL_LOGIN_ON = 8;
var PAGE_TTL_HOME = 9;
var PH_PASSWORD = 10;
//var LBL_SELECT_LOCATION                        = 11;
//var BTN_GO                                     = 12;
var BTN_CONTINUE                               = 13;
//var TEXT_INCOMPLETE_PROFILE                    = 14;
var PH_FIRST_NAME = 15;
var PH_LAST_NAME = 16;
var PH_EMAIL = 17;
var PH_PHONE = 18;
//var TEXT_LINKED                                = 19;
//var LBL_ADMINISTRATOR                          = 20;
//var LBL_STRENGTH                               = 22;
//var LBL_NUTRITION                              = 23;
//var LBL_GROUP_MESSAGES                         = 24;
//var LBL_All_CLIENTS                            = 25;
//var TEX_RIGHT                                  = 26;
var TEX_GET_IMAGE = 27;
//var TEX_ACTIVE                                 = 28;
//var TEX_NON_ACTIVE                             = 29;
//var PH_EXTRA_PARAMETERS                        = 30;
var BTN_SAVE = 31;
//var PH_SEARCH                                  = 32;
//var BTN_MEMBER_LIST                            = 33;
//var BTN_CARDIO_TEST                            = 34;
//var BTN_BODY_COMP                              = 35;
//var BTN_FLEXIBILITY_TEST                       = 36;
//var BTN_REPORTS                                = 37;
//var BTN_OVERTRAINING                           = 38;
var BTN_CANCEL = 39;
//var BTN_MACHINES                               = 40;
//var BTN_EXERCISES                              = 41;
//var BTN_ACTIVITIES                             = 42;
//var BTN_DUMBELLS                               = 43;
var PAGE_TTL_LOGOUT = 44;
//var PAGE_TTL_MANAGE_MEMBER                     = 45;
//var PAGE_TTL_MANAGE_TEST                       = 46;
//var PAGE_TTL_MANAGE_REPORTS                    = 47;
//var PAGE_TTL_MANAGE_NEWMACHINESEXERCISE        = 48;
//var PAGE_TTL_MANAGE_POINT                      = 49;
//var PAGE_TTL_MANAGE_CALENTER                   = 50;
//var PAGE_TTL_MANAGE_MESSAGES                   = 51;
//var PAGE_TTL_CONTACT                           = 52;
var TEX_SEARCH = 55;
//var BTN_TEST_PAGE                              = 56;
//var BTN_REPORT_PAGE                            = 57;
//var BTN_MESSAGES_PAGE                          = 58;
//var PAGE_TTL_MANAGE_MEMBER_DETAILS             = 59;
//var BTN_START_NEW_TEST                         = 60;
//var PAGE_TTL_MANAGE_BODYCOMPOSITION            = 61;
//var BTN_BODYCOMPOSITION                        = 62;
//var BTN_FLEXIBILITY                            = 63;
//var BTN_CARDIO                                 = 64;
//var BTN_STRENGTH                               = 65;
//var LBL_TEST_DATE          	                   = 66;
//var LBL_TEST_METHOD          			       = 67;
var LBL_GENDER = 68;
//var LBL_AGE          			               = 69;
//var LBL_HEGHT          			              = 70;
var LBL_WEIGHT = 71;
//var LBL_BODY_FAT               		          = 72;
//var LBL_BMI          		   	               = 73;
//var LBL_BELLYGROWTH          	   	           = 74;
//var LBL_FATFREEWEIGHT          	        	   = 75;
//var LBL_PREOFESSION          		           = 76;
//var LBL_ACTIVITY          		    	       = 77;
//var LBL_SPORTS          		    	       = 78;
//var LBL_LIFESTYLE          		    	       = 79;
//var LBL_STRESS          		    	       = 80;
//var LBL_LABORCHARGE_PER          		       = 81;
//var LBL_SPORT_CHARGEE          		    	   = 82;
//var LBL_LABORCHARGE          		           = 83;
//var LBL_BASICMETABOLISM          	 	       = 84;
//var LBL_SPORTS_SPECIFIC              	       = 85;
//var LBL_ENERGY          		    	       = 86;
var LBL_NEXT = 87;
//var LBL_FREQWEEK          		    	       = 88;
//var LBL_INTENSITY          		    	       = 89;
//var LBL_AGE_TEST          		    	       = 90;
//var LBL_WEIGHT_TEST          		    	   = 91;
//var LBL_TEST_LEVEL          		    	   = 92;
//var LBL_TEST_OPTION          		    	   = 93;
//var LBL_COACH                		    	   = 94;
var BTN_NEXT_ARROW = 95;
//var BTN_SAVE_START_TEST                	       = 96;
//var PAGE_TTL_CARDIOTEST   					   = 97;
//var TEX_WELCOME_STRONG	     				   = 98;
//var PH_ENTER_CLIENT_CODE	     	     	   = 99;
//var PH_SEARCH_CLIENT	     	     	       = 100;
var PH_REFRESH_BTN = 101;
//var LBL_CODE_LABEL                             = 102;
var BTN_GOOD = 103;
//var BTN_FLEX_LEVE                              = 104;
//var BTN_FLEX_CAT                               = 105;
//var BTN_CALFS                                  = 106;
//var BTN_ILLIPSOAS                              = 107;
//var BTN_HAMSTRINGS                             = 108;
//var BTN_BREAST                                 = 109;
//var BTN_QUADRICEPS                             = 110;
//var BTN_REMARKS                                = 111;
var TEXT_FORGOTPASSWORD = 112;
var LBL_CLOSE = 113;
//var LBL_SEND                                   = 114;
//var PAGE_TTL_FORGOTPASSWORD                    = 115;
//var EXCP_UNDER_CONSTUCTION_PAGE                = 117;
//var EXCP_INVALID_CAPTCHA_ERROR                 = 118;
//var EXCP_INVALID_CLICK_EVENT_MSG               = 119;
//var EXCP_NO_MEMBER_MAPPED                      = 110;
//var EXCP_DEL_COFRM_MSG                         = 111;
//var CONFIRM_SUBMIT_DUPLICATETEST               = 112;
//var LBL_STRENGTH_WEEK						   = 122;
//var PAGE_TTL_STRENGTH_PROGRAM_OVERVIEW 		   = 123;
//var LBL_NAME								   = 124;
//var LBL_IMAGE_URL						       = 125;
//var LBL_MOVEI_URL						       = 126;
//var LBL_CLEAR      						       = 127;
var TXT_EXIT = 128;
//var LBL_OUT_OF_ORDER      				       = 129;
//var LBL_USE_MIX_TRAINING_ALLOW      	       = 130;
//var LBL_USE_CIRCUIT_TRAINING_ALLOW		       = 131;
//var LBL_USE_FREE_TRAINING_ALLOW                = 132;
//var LBL_MOVESMART_CERTIFIET     		       = 133;
//var LBL_ONLY_ALLOWED_WITH_MACHINES		       = 134;
//var LBL_ENTER_URL						       = 135;
//var PH_ENTER_URL						       = 136;
var LBL_SELECT = 137;
//var LBL_MEMBER_ID							   = 138;
//var LBL_PERSONAL_ID							   = 139;
var TXT_MALE = 140;
var TXT_FEMALE = 141;
//var LBL_CLUB_ORG							   = 142;
var LBL_ACTION = 144;
//var LBL_CURRENT_TEST						   = 145;
//var LBL_ANALYSED							   = 146;
//var LBL_REPORTS								   = 147;
//var PAGE_TTL_SETTINGS					       = 148;
//var PAGE_TTL_STRENGTH						   = 149;
//var PAGE_TTL_HEALTHCHECK			           = 150;
//var LBL_CALCULATE_AUTOMETICALY	               = 151;
//var LBL_STRENGTHTEST			               = 152;
var BTN_REGISTER = 153;
var LBL_REGISTER_MANUALLY_OR_FACEBOOK = 154;
var BTN_FACEBOOK = 155;
var TEX_SECURITY_QUESTIONS = 156;
var TEX_ANSWER_SEQURITY_QUESTION = 157;
var PH_BIRTHDAY = 158;
var PH_LENGTH = 159;
var MSG_INFO_PHASE_2_BEFORE_TEST = 161;
var PH_STREET = 162;
var PH_NUMBER = 163;
var PH_BUS = 164;
var PH_ZIPCODE = 165;
var TEX_PLACE = 166;
//var BTN_NEXT_ARROW                             = 167;
var TXT_COUNTRY = 168;
var PH_TYPE_HERE = 169;
var TEX_YES = 170;
var TEX_NO = 171;
var PH_SELECT_TEST_DAY = 172;
var PH_AMOUNT = 173;
var PH_DATE_ENTRY = 174;
var LBL_SIGNATURE_FULL_NAME = 175;
var BTN_OK = 176;
var LBL_SELECT_ACTIVITY = 177;
var LBL_MONTHLY = 178;
var LBL_WEEKLY = 179;
var LBL_AVERAGE_TIME = 180;
var PH_MIN = 181;
var BTN_ADD_NEXT_ACTIVITY = 182;
var PH_NAME_GP = 183;
//var PAGE_TTL_USER_PROFILE    				   = 184;
/*selva*/
var LBL_HEAVY_PHY_LABOUR = 186;
var LBL_SEDENTARY_JOBS = 188;
var LBL_EXCELLENT = 189;
var LBL_AVERAGE = 191;
var LBL_POOR = 192;
var LBL_BAD = 193;
var LBL_LIGHT_PHYSICAL_LABOUR = 194;
var LBL_DECLARE_FOR_REAL_READ = 195;
var PLEASE_ENTER_USERNMAE = 196;
var PLEASE_ENTER_PASSWORD = 197;
var LOGIN_USERNAMEPASSWORD_INCORRECT = 198;
var USERNAMEPASSWORD_INCORRECT = 199;
//var CONFIRM_SUBMIT_DUPLICATETEST			   = 200;
var TXT_MANDATORY = 201;
//var MENU_MANAGE_MEMBERS						   = 202;

//var MENU_HEALTH_CHECK						   = 204;
//var MENU_MESSAGE						   	   = 206;
//var MENU_AGENDA_CALLENTER					   = 207;
//var MENU_SETTINGS						   	   = 208;
//var MENU_SWITCH_USER					   	   = 209;
//var MSG_SUCCESS_CLIENT_ADD					   = 215;

var MSG_SUCCESS_ADDRESSINFO = 216;
var MSG_SUCCESS_PERSONALINFO = 217;
var MSG_SUCCESS_PROFESSION_WORK_TYPE = 218;
var MSG_SUCCESS_CLIENTACTIVITY = 219;
var MSG_SUCCESS_GPINFOMORMATION = 220;
var MSG_SUCCESS_MEDICALINJURY = 221;
var MSG_SUCCESS_MEDICAL_INFO = 222;
var MSG_SUCCESS_STRESSTESTINFO = 223;
var MSG_SUCCESS_SMOKEINFO = 224;
var MSG_SUCCESS_CLIENTCURRENTFEEL = 225;
var MSG_SUCCESS_SIGNATURE_UPDATED = 226;
var MSG_SUCCESS_APPOINTMENTSENT_EMAIL = 227;

/*Allows Exception*/
//var MSG_EXCEPTION_INCOMPLETE_PROFILE           = 228;
var MSG_EXCEPTION_EMAIL_EXISTS = 229;
var MSG_EXCEPTION_ACCEPT_DECLARATION = 230;
var MSG_EXCEPTION_SIGNATURE_MISSING = 231;


var TXT_GALLERY = 232;
var TXT_CAMERA = 233;
var BTN_START_CHECK = 234;
var TXT_WELCOME = 235;

var BTN_INFO = 236;
var TXT_MEDICAL_INFO = 237;
var TXT_TESTS = 238;
var TXT_HOW_DO_FEEL_TODAY = 239;
var TXT_AWARNESS = 240;
var TXT_PHYSICAL_ACT = 241;
var TXT_HOW_EXERCISE_REG = 242;
var LBL_AVG_TIME = 243;
var TXT_INJURY_PBLMS = 244;
var TXT_MEDICATION = 245;
var TXT_GP_INFO = 246;
//var TXT_SUCCES_QUICK_WIN				       = 247;

var TXT_SMOKE = 248;
var TXT_SELECT_PROFESSION = 249;
var TXT_CITY_ZIPCODE = 250;
var BTN_GO_FOR_TEST = 251;
var BTN_OUR_ADVICE = 252;
var TXT_YOUR_OWN_GOAL = 253;
var TXT_LOAD_WAITING_MSG				       = 254;
//var TXT_SELECTED_RADIUS				       	   = 255;
//var TXT_SEARCH_SELECT_CLUB				       = 256;
//var TXT_SENT_REQ_APPIONMENT				       = 257;
//var TXT_STEPS_GAMES				               = 258;
var TXT_COPY_YESTERDAY = 259;
//var EXCEPTION_DATACONNECTION_LOST              = 264;
//var EXCEPTION_UNEXPECTED_PROCESSING_ERROR      = 265;
var MSG_SUCCESS_PROFILE_UPDATED = 266;
var MSG_PROFILE_UPDATED_FAILED = 267;
var MSG_EXCEPTION_EMAIL_ERROR = 268;
var TXT_ENTER_EMAIL = 347;


/*2016-03-16*/
//var LBL_DO_NOT_COACH_YET			           = 370;
//var LBL_NEW_RESULTS_CHECK			           = 371;
//var LBL_TOP_IMPROVEMENTS			           = 372;
//var LBL_STATISTICS			                   = 373;
var BTN_SELECT_PLACE = 374;
var BTN_SELECT_CITY = 375;

/*2016-03-17*/
var MSG_CHOOSE_ATLEAST_ANY_ONE_OPTIONS = 376;

/*2016-03-21*/
var BTN_SELECT_ACTIVITY = 382;
var BTN_FIND_ACTIVITY = 383;

/*2016-03-26*/
var TEX_SECURITY = 384;
var BTN_NEW_USER = 385;
//var BTN_DETAIL    							   = 386;
var LBL_NEEDED_TEST_INFO = 387;
var LBL_CREDIT = 388;
var LBL_TOTAL_CREDITS = 389;
var BTN_BACK = 390;
var BTN_STEP_GAME = 391;
var LBL_CHECK = 392;
//var BTN_START_CHECK                    	       = 393;
var BTN_INTRO = 394;
var BTN_GO_FOR_TRAINING = 395;

/*2016-03-29*/
var TEX_DIFFERENT_WAYS_CREDITS = 396;
var TEX_MOVE = 397;
var TEX_EAT = 398;
var TEX_MIND = 399;
var TEX_CLOUD = 400;
var TEX_POCKET = 401;
var LBL_SHOP = 402;

/*PK- ADD 20160502*/
var EXP_EXPIRED_GO_FOR_TEST = 410;
var MSG_MISMATCH_PASSWORD = 411;
var TEXT_DOB = 412;
var TXT_AGREE = 413;
var TXT_DISAGREE = 414;
var TXT_PROFILE = 415;
//var TXT_GENERAL_TOTAL                          = 416;
var TXT_ENTER_STEPS_HERE = 417;
var TXT_START_STEP_COUNTER = 418;
var TXT_LIST = 419;
var TXT_SELECT_DAILY_REMINDER = 420;
var TXT_TRAINING = 421;
var PH_RE_PASSWORD = 425;
var PLEASE_ENTER_USERNMAE_AND_PASSWORD = 449;

//var MSG_CONFN_MAIL 					 		    =450;
//var MSG_EMAIL_INVALID 						    =451;
var TXT_ENTER_MINUTES_HERE = 452;
var MSG_PLZ_CONFIRM_TERMS 						=453;
var TXT_PRIVACY_POLICY = 454;
var MSG_MONITOR_YOUR_STEPS = 455;
var MSG_DONT_WANT_USE_STEPMONITOR = 456;
var MSG_CAN_MONITOR_YOUR_STEPS = 457;
var MSG_DO_YOU_WANT_SAVE_ANYWAY = 458;
var TXT_PRIVACY_POLICY_TERMS_CONDITIONS = 459;
var TXT_AGREE_PRIVACY_POLICY = 460;
/*PK 10 May 2016*/
var TXT_SUNDAY = 461;
var TXT_MONDAY = 462;
var TXT_TUESDAY = 463;
var TXT_WEDNESDAY = 464;
var TXT_THURSDAY = 465;
var TXT_FRIDAY = 466;
var TXT_STATURDAY = 467;
var TXT_TODAY = 468;
/*PK Translation*/
var TXT_GET_COACH = 469;
var TXT_TO_STEPCOUNTER = 470;
var TXT_TO_EAT_DIARY = 471;
var TXT_TO_MIND_ACTIVITY = 472;
var TXT_START_MIND_ACTIVITY = 473;
var TXT_START_DIARY = 474;
var TXT_START_STEPCOUNTER = 475;
//var MSG_AUTHENTICATING   						= 476;
//var TXT_SELECT_GENDER = 477;
var TXT_SELECT_GENDER = 521;
var TXT_SQ1 = 478;
var TXT_SQ2 = 479;
var TXT_SQ3 = 480;
var YES_BTN = 481;
var NO_BTN = 482;
var TXT_TO_SHOP = 483;
var TXT_CONSULT = 484;
var TXT_PLEASE_WAIT = 485;
var TXT_SELECT_LANGUAGE = 486;
var TEX_SEARCHING = 487;
var TEX_COACH_SERACH_RESULT = 488;
var TEX_CREDIT_INFO_CNT = 489;
var MSG_SUCCESS_CONSULT_EMAIL = 490;
var TXT_FITNESS = 491;
var TXT_POWER = 492;
var TXT_AGILITY = 493;
var MSG_EXCEPTION_AGE =494; 
var TXT_VOUCHERS = 523;
var TXT_VOUCHERS_HEAD = 524;
var TXT_VOUCHERS_BTN = 525;

/*PAGE CONTENT */
var INTRO_PAGE_CONTENT_ID = 1;
var WELCOME_PAGE_CONTENT_ID = 2;
var MOVESMART_FASE = 3;
var EATFRESH_FASE = 4;
var MINDSWITCH_FASE = 5;
var TERMS_AND_CONDITIONS_ID = 6;
var PAGE_STEPCOUNTER = 7;
var CHECK_FASE_SUMMARY = 8;
var STEPS_REGISTRATION = 9;
var THANKYOU_REGISTRATION = 10;
var START_PAHSE1_EATFRESH = 11;
var START_PAHSE1_MINDSWITCH = 12;
var PAGE_CONTENT_UPCC_INFO = 13;
var APPOINTMENT_REQUEST_ID = 14;
var APPOINTMENT_CLIENTINFO = 15;

var TEXTBEFORESELECTCOACH = 16;
/*SN Login Error Message need to update once the Translation applied*/
//var EXCEPTION_LOGIN_USERNAMEPASSWORD_INCORRECT_MSG='Login UserName/Password is incorrect.';
//var EXCEPTION_USERNAMEPASSWORD_INCORRECT_MSG=' Please Enter Login UserName AND Password.';
//var EXCEPTION_ENTER_USERNMAE_MSG=' Please Enter Login UserName ';
//var EXCEPTION_ENTER_PASSWORD_MSG=' Please Enter Login Password ';

var TRAINING_REFTYPE_Question = 1;
var TRAINING_REFTYPE_Activity = 2;
//var TRAINING_REFTYPE_training	=	3;
/*SN added register logintype 20160505*/
var LOGINTYPE_SOCIALMEDIA = 2;

var SHOW_EXCEPTION_SOCIALMEDIA_EMAIL = "Email is invalid with the seleced social media. Please check the email.";
var SHOW_EXCEPTION_SOCIALMEDIA_NO_EMAIL = "Vul het e-mailadres en telefoonnummer in.";
var SHOW_EXCEPTION_SOCIALMEDIA_EMAIL_EXISTS = "Already registered this app.You can use reset password";
var SHOW_EXCEPTION_WRONG_EMAIL = "Email is ongeldig.";
var APP_EXIT = "Press again to exit";
var FORMAT_ONLY_DATE = 1;
var FORMAT_ONLY_TIME = 2;
var FORMAT_DATE_TIME = 3;
var FORMAT_SLASH_DATE_DMY_HIS = 4;
var TXT_DAY = 510;
var TXT_OF = 512;
var TXT_SUPPORT = 513;
var TXT_STEPS = 520;
var TXT_Wearables = 522;
var MSG_INSUF_CREDITS = 526;
var MSG_SUCCESS_VOUCHER_CREDITS = 527;
var MSG_SELECT_VOUCHER = 528;
var MSG_EXCEPTION_PHONE = 529;
var TXT_SHARE = 530;
var LOGIN_ACTIVATE_ACCOUNT = 531;
var TXT_DONE = 533;
var IMAGE_UPLOAD_SUCCESS = 535;
var TXT_LOGIN_FB_BTN = 537;
var MSG_COACH_CONSULT = 538;
var TXT_NOHR_TRAIN = 540;
var TXT_NOHR_TIME_ERR = 541;
var MSG_NOCOACH_SELECT = 370;
var APPID = 'movesmart';