var _CurrTime = 0;
var _WARM_UP_SECOND = 150;

/* Const Start  */

//var _ClientData_g;

/* TTestStatus */
var _C_tsToSpecify = 0;
var _C_tsTodo = 1;
var _C_tsToAnalyze = 2;
var _C_tsAnalyzed = 3;
var _C_tsTraining = 4;
var _C_tsFinished = 5;


/* TTestMachine */
_C_tmDummy = 0;
_C_tmCycle = 1;
_C_tmThreadmil = 2;
_C_tmField = 3;
_C_tmSpecCycle = 4;
_C_tmManual = 5;

/* TTestMachine */
_C_Slim_Fit = 0;
_C_Cond_Train = 1;

/* TAppearType */
_C_taFullData = 0;
_C_taTestData = 1;

/* TTrimFlag */
_C_TrimBoldFlag = 0;//Jump
_C_TrimJustFlag = 1;

_C_StartSpeedBike = [1, 2, 3, 4, 5, 6];
_C_StartSpeedThreadmill = [1, 2, 3, 4, 5, 6, 7, 8];


/* No Of HR Captured per Min. Assumming that that HR is captured every 5 Sec */
_C_SampleRate_ = 12;

/* Const End */


var THRChart = {
    ChartHRData_WARMUP: [],
    ChartHRData_TEST: [],
    ChartHRData_COOLDWN: [],
    ChartHRData_FULL: [],
    ChartObj: null
};

var THRChartAnalysis = {
    ChartHRStripline: [],
    ChartHRData: [],
    ChartObj: null
};

var TCATChartAnalysis = {
    REPDATA1: [],
    REPDATA2: [],
    TRAINZONEMIN: [],
    TRAINZONEMAX: [],
    ChartObj: null,
    XAxisMin: 0,
    SPMData: [],
    IANTHR: 0,
    IANTWATT: 0,
    TRIMBOLDOption: _C_TrimBoldFlag
};

function TCardioTestData() {
    this.DataBg_ = 0; //Begining Index of the text data in HR
    this.CoolDnBg_ = 0; //Start point number of testdata & cooldown // in HR_Data
    this.HRData_ = [];

    this.getTestHRCount = function () {
        return this.CoolDnBg_ - this.DataBg_;
    };

    this.getWarmUpTimeForAppearType = function (AppearType) {
        if (AppearType == _C_taTestData) {
            return 0;
        } else {
            return (this.DataBg_ - 1) / _C_SampleRate_;
        }
    };

    this.getTestTime = function () {
        var testtime = this.getTestHRCount();
        return testtime / _C_SampleRate_;
    };

    this.getCooldownTime = function () {
        return this.CoolDnBg_ / _C_SampleRate_;
    };

    this.getFullTimeForAppearType = function (AppearType) {
        if (AppearType == _C_taTestData) {
            return this.getTestTime();
        } else {
            return this.getHRCount() / _C_SampleRate_;
        }
    };

    this.PushHRData = function (data) {
        this.HRData_.push(data);
    };

    this.getHRCount = function (data) {
        logStatus(data, LOG_DEBUG);
        return this.HRData_.length;
    };

    /*
     bOldCard_ :Boolean;

     TimeIntervals_ :array [0..50] of Word; //polar intervals after editing
     IntervalCount_ : byte;
     SamplesInterval_ : byte;  // sample interval
     Lap_ : word;
     Surface_ : Integer; // 1..5 type of surface for fields tests
     OrigTimeIntervals_ : array [0..50] of word; // original intervals read from watch must be last blob data
     WeightOnTestDate_ : byte;
     public
     procedure SetHR(src:Pointer; Count:word);
     function GetHR:Pointer;
     function GetOrigTimeInt:Pointer;
     function GetTimeInt:Pointer;
     // makes HRData from card record;
     procedure LoadHR (src: Pointer);
     // makes HR Data from Polar watch record
     procedure LoadPolarHR (src: Pointer ;Count: word ;pInterval :Pointer;IntervalCount :Word;SamplesInterval:byte );
     procedure SaveToDB(tc:TTable);// saves data to Blob stream
     function ReadFromCardioDB(tc :TTable) :Boolean;
     */
}

function TSportMedData() {
    this.TrainType_ = 0;
    this.SPM_ = [];
    this.Regr1_ = {};
    this.Regr2_ = {};
    this.RLP1 = {};
    this.RLP2 = {};
    this.RLP3 = {};
    this.RLP4 = {};
    this._DeflPoint_ = [];
}

function TCardioTrainingPlan() {

}

function TStrengthData() {

}

function TPersonalActivities() {
}

function TClientData() {
    this.TestMachine_ = 0; // test option : tmDummy = 0,tmCycle = 1,tmThreadmil = 2,tmField = 3,tmSpecCycle = 4,tmManual = 5.
    this.TestStart_ = 0; //Selected Test Speed Index;
    this.weight_ = 0;
    this.age_ = 0;
    this.CardioTest_ = new TCardioTestData(); //person's cardiotest data
    this.SPMed_ = new TSportMedData();// SportMed points
    //XX this.Train_ = new TCardioTrainingPlan(); // person's cardiotraining data
    //XX this.Strength_ = new TStrengthData(); // persons strengthtest and training data
    //XX this.NextTestDate_ = null; //TDate;
    this.TestStatus_ = 0; // tospecify=0,todo=1,toanalyze=2,analyzed=3, intraining=4,finished=5
    //XX this.TestNumber_= ''; //current cardio test number
    //XX this.HasCard_=false; // true if member has card
    //XX this.HasPW_ =false; // true if members has polar watch
    //XX this.WriteToCardCardio_ = false;// true if write cardio info to card during checkin
    //XX this.WriteToPW_  = false;
    //XX this.ClearPointsPW_  = false;
    //XX this.WriteToCardStrength_ = false;// true if write strength info to card during checkin
    //XX this.FitLevPart_ = null;//: byte ; // fit level % part completed (max 100)
    //XX this.KeepFit_  = false; // true if mode is on
    //XX this.Activities_ : new TPersonalActivities(); // external activities selected

    //XX this.PActivities_ : new TPersonalActivities(); // external activities selected for Pupil

    //XX this.bAlien_  = false; // true if alien club card
    //XX this.LimCodes_ = new Array();// array [0..9] of byte; // person's limitations codes
    //XX this.bLimitationsEntered_ = false// true if client answered limitations questionary
    //XX this.LimitationAnswers_ = new Array();//: String[105]; // The questions in questionary answered "Yes" by the person
    //XX this.Country_= 0 
    //XX this.MotherCountry_ = 0;
    //XX this.TrainLocation_ = 0; 
    //XX this.TrainPerWeek_ = 0;
    //XX this.TestPayment_ = 0;
    //XX this.TestPackageStart_ : TDateTime;
    //XX this.PackageStarted_ = 0;
    //XX this.AdditTraining_ = 0;
    //XX this.WantsNewTest_  = false;
    //XX this.FreeTest_  = false;

    this.WriteRegFactor = function (xRegF1, xRegF0) {
        /* XX NEED TO IMPLEMENT insertion
         var
         xxRegF1, xxRegF0 : String ;
         Begin
         xxRegF1 := FormatFloat('#0.00',xRegF1) ;
         xxRegF0 := FormatFloat('#0.00',xRegF0) ;

         if not GetClientsCardioTable then  // get clients cardio table
         begin
         Result := False;
         Exit;
         end;

         try

         with tc_ do
         begin
         if recordcount > 0 then
         begin
         tc_.Edit;
         {Issue 271: 27/03/09 Decimal Issue in regr1_b1 and regr1_b0 are stored in regrfactor1 and regrfactor2
         in cardio and cardio test - URGENT issue Included }
         {
         FieldByName('RegrFactor1').AsFloat := round(xRegF1) ;
         FieldByName('RegrFactor2').AsFloat := round(xRegF0) ;
         }

         FieldByName('RegrFactor1').AsFloat := StrToFloat(xxRegF1) ;
         FieldByName('RegrFactor2').AsFloat := StrToFloat(xxRegF0) ;
         {Issue 271: 27/03/09 Decimal Issue in regr1_b1 and regr1_b0 are stored in regrfactor1 and regrfactor2
         in cardio and cardio test - URGENT issue Included }

         FieldByName('DateModified').AsDateTime := Now;
         if CoachName = '' then
         FieldByName('UserModified').AsString :=
         'S' + ' ' + Inttostr(Clientdata_g.PersonalNumber_)
         else
         FieldByName('UserModified').AsString := CoachName;

         Post;
         end ;
         CloseCardio;
         end;
         Except
         ErrorMessage(eCARDIODB);
         CloseCardio;
         Result := False;
         Exit;
         end;
         Result := True;

         */
    }
}
/*
 TPersonalData = class(TPersonOnCard)
 public
 SecretNumber_ :word; // obsolete, left for compatibility with older versions
 AddressAndStreet_:string;
 ZipCode_:string;
 City_:string;
 TelNumber_:string;
 TelNumber2_ : String;
 FaxNumber_:string;
 EMail_:string;
 BankAccount_:string;
 Coach_: byte;// coach's number
 Status_:TPersStatus;// active, notactive,personal,copiedout

 constructor Create;
 end;

 pTPersonalData = ^TPersonalData;

 // the main class describing the client and processing personal,
 // cardio, strength

 TClientData = class(TPersonalData)

 */

var _ClientData_g = new TClientData();
