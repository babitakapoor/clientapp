var __PathHistory = [];
var __PageReqNoLogin = ['register', 'intro', 'complete-registeration', 'login', 'security', 'thankspage'];
var __PageReqFooter = ['register', 'intro', 'complete-registeration', 'login', 'welcome', 'thankspage'];
//var __LoginTemplate	=	'login';
var __TemplateLogin = 'login';
__GetValidPageTemplate = function (template, arguments) {
    logStatus("Calling __GetValidPageTemplate", LOG_FUNCTIONS);
    try {
        var path = '';
        if (__IsPageTemplateValid(template) != '') {
            var page = __IsPageTemplateValid(template);
            var argstring = __BuildArgsString(arguments);
            path = '#/' + page + argstring;
        }
        return path;
    } catch (e) {
        ShowExceptionMessage("__GetValidPageTemplate", e);
    }

};
__BuildArgsString = function (args) {
    var _argstr = '';
    if (args && ( !$.isEmptyObject(args) )) {
        _argstr = '?';
        $.each(args, function (key, value) {
            _argstr += key + '=' + value + '&';//encodeURIComponent()
        });
        _argstr = rTrim(_argstr, '&');
    }
    return _argstr;
};
__IsLoggedIn = function () {

    return is_login();
};
__IsPageTemplateValid = function (template) {
    logStatus("Calling __IsPageTemplateValid", LOG_FUNCTIONS);
    try {
        var temp = template;
        if (template && template != '') {
            if (!is_login()) {
                if (__PageReqNoLogin.indexOf(template) != -1) {
                    temp = template;
                } else {
                    temp = _DefultHomePage;
                }
            }
        }
        return temp;
    } catch (e) {
        ShowExceptionMessage("__IsPageTemplateValid", e);
    }
};
__trackPagePathHistory = function (pathurl) {
    __PathHistory.push(pathurl);
};
__getHistoryPagePath = function () {
    return __PathHistory.pop();
};
__setPagesRequiredLogin = function (pages, loginTemplate) {
    __PageReqNoLogin = pages;
    __TemplateLogin = loginTemplate;
    logStatus(__TemplateLogin, LOG_DEBUG);
};
function app_tempvars(key, value) {
    logStatus("Calling app_tempvars", LOG_FUNCTIONS);
    try {
        var $key = $("#" + key);
        if (value) {
            var HTML = "";
            if (typeof($key.val()) != 'undefined') {
                $key.val(value);
            } else {
                HTML = '<input type="hidden" id="' + key + '" value="' + value + '" />';
                $('body').append(HTML);
            }
        } else {
            return $key.val();
        }
    } catch (e) {
        ShowExceptionMessage("app_tempvars", e);
    }
}
/*
 function rangeInput(selector){
 logStatus("Calling updateRangeInput",LOG_FUNCTIONS);
 try{
 var $selector	=	$(selector);
 var minval	=	0;
 var maxval	=	1000;
 if($selector.attr('_rangeMin')){
 minval	=	$selector.attr('_rangeMin');
 }
 if($selector.attr('_rangeMax')){
 maxval	=	$selector.attr('_rangeMax');
 }
 $selector.on("keyup",function(){
 var $this	=	$(this);
 if (!(($this.val() <= minval) || ($this.val() >= maxval))) {
 }
 })
 }catch (e) {
 ShowExceptionMessage("updateRangeInput", e);
 }
 }
 */
function getValidImageUrl(url, returnURL, _default) {
    
    logStatus("Calling getValidImageUrl", LOG_FUNCTIONS);
    try {
        var __default = (_default && _default != '') ? _default : 'images/no_image.png';
        $("<img>", {
            src: url,
            error: function () {
                returnURL(__default);
            },
            load: function () {
                returnURL(url);
            }
        });
    } catch (e) {
        ShowExceptionMessage("getValidImageUrl", e);
    }
}
function timestamp() {
    if (!Date.now) {
        Date.now = function () {
            return new Date().getTime();
        }
    }
    return Math.floor(Date.now() / 1000);
}

/*FWORK - Prototypes*/
String.prototype.to_ucfirst = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};


/**
 * @return {string}
 */
function GetUrlArg(key, isEncoded, defValue) {
    logStatus("GetUrlArg", LOG_FUNCTIONS);
    try {
        var urldata = getPathHashData();
        if (urldata.args && urldata.args[key]) {
            return isEncoded ? base64_decode(urldata.args[key]) : urldata.args[key];
        }
        if (defValue) {
            return defValue;
        }
        return '';
    } catch (e) {
        ShowExceptionMessage("GetUrlArg", e);
    }
}
function GetCurrentPath() {
    logStatus("GetCurrentPath", LOG_FUNCTIONS);
    try {
        var urldata = getPathHashData();
        return urldata.path;
    } catch (e) {
        ShowExceptionMessage("GetCurrentPath", e);
    }
}