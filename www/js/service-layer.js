function getLoginByUsernamePassword(username, password, onSuccessLogin) {
    logStatus("Calling getLoginByUsernamePassword", LOG_FUNCTIONS);
    try {
        do_service({method: 'authenticate', 'p': 'login'}, {
            username: username,
            password: password,
            usertype: APP_USERTYPE
        }, function (response) {
            if (onSuccessLogin) {
                onSuccessLogin(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("getLoginByUsernamePassword", e);
    }
}
/**************************************
        Save Chart Value
**************************************/
function postchartvaluedata(postdata, onsuccesresponsehandler) {
    logStatus("Calling postchartvaluedata", LOG_FUNCTIONS);
    try {
var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuccesresponsehandler){
                onsuccesresponsehandler(response);
            }
        },'json');
    } catch (e) {
        ShowExceptionMessage("postchartvaluedata", e);
    }
}
/*
 function _appendClubName(){
 logStatus("Calling _appendClubName", LOG_FUNCTIONS);
 try{
 //$('#clubname_inputfield').html('');
 var clubid =	getCurrentUser();
 //$('#clubname_inputfield').attr('value',clubid.club_name);
 $('#users_club').val(app_tempvars('club_selected'));
 }catch (e) {
 ShowExceptionMessage("_appendClubName", e);
 }	

 }*/
/*
 function doLogoutUser(){
 logStatus("Calling doLogoutUser", LOG_FUNCTIONS);
 try{
 do_service({method:'logout','p':'login'},{},function(response){
 _movePageTo('login');
 },'json');
 }catch (e) {
 ShowExceptionMessage("doLogoutUser", e);
 }		
 }

 function getManageMembersData(action,_element,tab,pageindex){
 logStatus("Calling getManageMembersData", LOG_FUNCTIONS);
 try{
 $('.tabsbox').on('click','span',function(){
 $('.tabsbox > span').removeClass("active");
 $(this).addClass("active");
 });
 var _data	=	{};
 var _datapost	=	{};
 var selectedClub	=	$("#users_club").val();
 _data.action=	action;
 _data.p		=	'members';
 if(tab){
 _data.tab	=	tab;
 }
 if(pageindex){
 _datapost.page=pageindex;
 }
 if(selectedClub){
 _data.clubId=selectedClub;
 }
 do_service(_data,_datapost,function(respsonse){
 $(_element).find('tbody').html(respsonse);
 _appendClubName();
 });
 }catch (e) {
 ShowExceptionMessage("getManageMembersData", e);
 }	
 }

 function getSearchResult(action,_element){
 logStatus("Calling getSearchResult", LOG_FUNCTIONS);
 try{
 var searchValue	=	$("#searchinputvalue").val();
 var clubid	 	=  $("#users_club").val();
 var filterby = $('#selecrfiltervalue').val();
 do_service({action:action,p:'members',searchType:filterby,searchValue:searchValue,clubId:clubid},{},function(respsonse){
 $(_element).find('tbody').html(respsonse);
 });
 }catch (e) {
 ShowExceptionMessage("getSearchResult", e);
 }	
 }

 function getEditMember(action,mid,testid,clubid,onsuccessresponse){
 logStatus("Calling getEditMember", LOG_FUNCTIONS);
 try{
 var _data = {};
 _data.action = action;
 _data.p = 'members';
 _data.id = mid;
 _data.testId = testid;	
 var logininfo	=	getCurrentLoginInfo();
 _data.authorizedClubId	=	clubid;
 do_service(_data,{},function(respsonse){	
 if(onsuccessresponse){
 onsuccessresponse(respsonse);
 }
 },'json');
 }catch (e) {
 ShowExceptionMessage("getEditMember", e);
 }
 }

 function newTestDetails(action,mid,teststatus,onsuccessresponse){
 logStatus("Calling newTestDetails", LOG_FUNCTIONS);
 try{
 var _data = {};
 _data.action 	= action;
 _data.p			= 'members';
 _data.id 		= mid;
 _data.newTest	= teststatus; 
 do_service(_data,{},function(respsonse){		
 if(onsuccessresponse){
 onsuccessresponse(respsonse);
 }
 },'json');	
 }catch (e) {
 ShowExceptionMessage("newTestDetails", e);
 }	
 }
 */
function getsecurityQuestions(action, onsuceessresponse) {
    logStatus("Calling getsecurityQuestions", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.method = action;
        _data.p = 'login';
        do_service(_data, {}, function (respsonse) {
            if (onsuceessresponse) {
                onsuceessresponse(respsonse);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("getsecurityQuestions", e);
    }
}
function sendPasswordrequest(action, email, onsuccessdatahandler) {
    logStatus("Calling sendPasswordrequest", LOG_FUNCTIONS);
    try {
        var _data = {};
        var _datapost = {};
        _data.method = action;
        _data.p = 'login';
        _data.email = email;
        var question = {};
        $(".sq_ques").each(function (i, eachitem) {
            var idstr = $(eachitem).attr("id");
            var answer = $(eachitem).val();
            var _qid = idstr.split('sqQues_')[1];
            question[_qid] = answer;
        });
        _datapost.question = JSON.stringify(question);
        do_service(_data, _datapost, function (response) {
            if (onsuccessdatahandler) {
                onsuccessdatahandler(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("sendPasswordrequest", e);
    }
}
/* Save Security Question*/
function _saveSequrityAnswers(action, id, question, onsuccessdatahandler) {
    logStatus("Calling sendPasswordrequest", LOG_FUNCTIONS);
    try {
        var _data = {};
        var _datapost = {};
        _data.action = action;
        _data.p = 'members';
        _data.regid = id;
        _datapost.question = JSON.stringify(question);
        do_service(_data, _datapost, function (response) {
            if (onsuccessdatahandler) {
                onsuccessdatahandler(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("sendPasswordrequest", e);
    }
}
function regUserData(id, onsuccessdatahandler) {
    logStatus("Calling regUserData", LOG_FUNCTIONS);
    try {
        var _data = {};
        var _datapost = {};
        _data.action = 'getRegUserData';
        _data.p = 'members';
        _data.regid = id;
        do_service(_data, _datapost, function (response) {
            if (onsuccessdatahandler) {
                onsuccessdatahandler(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("regUserData", e);
    }
}
/*
 function _getUserImage(action,mid,onsuccessimageresponse){
 logStatus("Calling _getUserImage", LOG_FUNCTIONS);
 try{
 var _data = {};
 _data.action 	= action;
 _data.p			= 'members';
 _data.id 		= mid;
 do_service(_data,{},function(respsonse){	
 if(onsuccessimageresponse){
 onsuccessimageresponse(respsonse);
 }
 },'json');
 }catch (e) {
 ShowExceptionMessage("_getUserImage", e);
 }	
 }
 */
function _registerMember(data, onsucessResponse) {
    logStatus("Calling _registerMember", LOG_FUNCTIONS);
    try {
        //var _datapost	= {};
        var _data = (data) ? data : {};
        _data.action = 'registerClient';
        _data.p = 'members';
        do_service(data, _data, function (response) {
            if (onsucessResponse) {
                onsucessResponse(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("memberSignup", e);
    }
}
/*SN added profile image 20160125*/
/*
 function updateProfileImage(action,formdata,onsuceessresponse){
 logStatus("Calling updateProfileImage ", LOG_FUNCTIONS);
 var _data = {};
 var _datapost = {};
 _data.action = action;
 _data.p 	 = 'userProfile';
 do_service_upload(_data,formdata,function(respsonse){
 if(onsuceessresponse){
 onsuceessresponse(respsonse);
 }
 });
 }
 */
/*MK Get Nationality List*/
function loadMemberOptionList(action, handleOnSuccess) {
    logStatus("Calling loadMemberOptionList", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = action;
        /*action may be 'getActivityList','getProfessionList','getNationalityList'*/
        _data.p = 'members';
        do_service(_data, {}, function (response) {
            if (action == 'getActivityList') {
            }
            if (handleOnSuccess) {
                handleOnSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("loadMemberOptionList", e);
    }
}
function getCityListByZipCode(data, handleOnSuccess) {
    logStatus("Calling getCityListByZipCode", LOG_FUNCTIONS);
    try {
        var _data = (data) ? data : {};
        _data.action = 'getCityListByZipCode';
        _data.p = 'members';
        do_service(_data, {}, function (response) {
            if (handleOnSuccess) {
                handleOnSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("getCityListByZipCode", e);
    }
}
function _updateClientInformation(data, handleOnSuccess) {
    logStatus("Calling _updateClientInformation", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'updatePersonalInfo';
        _data.p = 'members';
        var user = getCurrentUser();
        data.userId = (user.user_id) ? user.user_id : 0;
        do_service(_data, data, function (response) {
            if (handleOnSuccess) {
                handleOnSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("_updateClientInformation", e);
    }
}
function _saveClientActivity(data, handleOnSuccess) {
    logStatus("Calling _saveClientActivity", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'updateActivityMember';
        _data.p = 'members';
        var user = getCurrentUser();
        _data.userId = (user.user_id ) ? user.user_id : 0;
        do_service(_data, data, function (response) {
            if (handleOnSuccess) {
                handleOnSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("_saveClientActivity", e);
    }
}
function _updateMemberMedicalInformation(data, handleOnSuccess) {
    logStatus("Calling _updateMemberMedicalInformation", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'updateMemberMedicalInfo';
        _data.p = 'members';
        do_service(_data, data, function (response) {
            if (handleOnSuccess) {
                handleOnSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("_updateMemberMedicalInformation", e);
    }
}
/*Load Questionaries Starts*/
function _loadClientQuestionaries(data, onLoadSuccess) {
    logStatus("Calling _loadClientQuestionaries", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'loadClientQuestionaries';
        _data.p = 'questionaries';
        var user = getCurrentUser();
        _data.userId = user.user_id;
        do_service(_data, data, function (response) {
            if (onLoadSuccess) {
                onLoadSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("_loadClientQuestionaries", e);
    }
}
/*Questions By - Phase, Group ID */
/*
 function _getQuestionsByPhaseGroup(data,onLoadSuccess){
 logStatus("Calling _getQuestionsByPhaseGroup", LOG_FUNCTIONS);
 try {
 var _data	=	{};
 _data.action=	'getQuestionsByPhaseGroup';
 _data.p		=	'questionaries';
 var user	=	getCurrentUser();
 _data.userId=	user.user_id;
 do_service(_data,data,function(response){
 if(onLoadSuccess){
 onLoadSuccess(response)
 }
 },'json');
 } catch(e){
 ShowExceptionMessage("_getQuestionsByPhaseGroup", e);
 }
 }
 */
/*Update Answer for the question*/
function updateAnswerForQuestion(data, onSaveSuccess) {
    logStatus("Calling updateAnswerForQuestion", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'updateAnswerForQuestion';
        _data.p = 'questionaries';
        var user = getCurrentUser();
        data.userId = user.user_id;
        do_service(_data, data, function (response) {
            if (onSaveSuccess) {
                onSaveSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("updateAnswerForQuestion", e);
    }
}
function updateUserPhase(data, onSaveSuccess) {
    logStatus("Calling updateUserPhase", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'updateUserPhase';
        _data.p = 'questionaries';
        do_service(_data, data, function (response) {
            if (onSaveSuccess) {
                onSaveSuccess(response);
            }
        });
    } catch (e) {
        ShowExceptionMessage("updateUserPhase", e);
    }
}
function _searchCoachAndClub(data, onSaveSuccess) {
    logStatus("Calling _searchCoachAndClub", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'searchCoachAndClub';
        _data.p = 'questionaries';
        var user = getCurrentUser();
        _data.userId = user.user_id;
        do_service(_data, data, function (response) {
            if (onSaveSuccess) {
                onSaveSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("_searchCoachAndClub", e);
    }
}
function _sendAppoinmentRequest(data, onSaveSuccess) {
    logStatus("Calling _sendAppoinmentRequest", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'sendAppoinmentRequest';
        _data.p = 'questionaries';
        var user = getCurrentUser();
        _data.userId = user.user_id;
        do_service(_data, data, function (response) {
            if (onSaveSuccess) {
                onSaveSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("_sendAppoinmentRequest", e);
    }
}
function updateClientProfile(dataProfile, onUpdateSuccess) {
    logStatus("Calling userProfile", LOG_FUNCTIONS);
    try {
        var user = getCurrentUser();
        var GetData = {};
        GetData.action = 'updateProfiledata';
        GetData.p = 'userProfile';
        GetData.id = user.user_id;
        do_service(GetData, dataProfile, function (response) {
            if (onUpdateSuccess) {
                onUpdateSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("userProfile", e);
    }
}
function loadLanguage(Onsuccesshandler) {
    logStatus("calling loadLanguage", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = "getLanguageDetails";
        _data.p = "settings";
        do_service(_data, {}, function (response) {
            if (Onsuccesshandler) {
                Onsuccesshandler(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("loadLanguage", e);
    }
}
/*
 function loadLanguagePageContent(Onsuccesshandler){
 logStatus("calling loadLanguagePageContent",LOG_FUNCTIONS);
 try{
 var _data={};
 _data.action="getLanguagePageContent";
 _data.p		 ="settings";
 do_service(_data,{},function(response){
 if(Onsuccesshandler){
 Onsuccesshandler(response);
 }
 },'json');
 } catch(e){
 ShowExceptionMessage("loadLanguagePageContent", e);
 }
 }

 function updateAnswersFromPreviousDay(postData,onUpdateSuccess){
 logStatus("calling updateAnswersFromPreviousDay",LOG_FUNCTIONS);
 try{
 var _data={};
 var user	=	getCurrentUser();
 _data.action=	"updateAnswersFromPreviousDay";
 _data.p		=	"questionaries";
 _data.userId=	user.user_id;
 do_service(_data,postData,function(response){
 if(onUpdateSuccess){
 onUpdateSuccess(response);
 }
 },'json');
 } catch(e){
 ShowExceptionMessage("updateAnswersFromPreviousDay", e);
 }
 }
 */
/*PK Added - getlanguages to Update in User Language*/
function getlanguages(Onsuccesshandler) {
    logStatus("Calling getlanguages Result", LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'getLanguagelist';
        _data.p = 'settings';
        do_service(_data, {}, function (response) {
            if (Onsuccesshandler) {
                Onsuccesshandler(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("getlanguages", e);
    }
}
/*Get All qustionaries data from Server*/
function _loadAllPageContents(postData, onGetDataSuccess) {
    logStatus("Calling _loadAllPageContents", LOG_FUNCTIONS);
    logStatus(postData, LOG_DEBUG);
    try {
        var getData = {};
        getData.action = 'getLanguagePageContent';
        getData.p = 'settings';
        do_service(getData, {}, function (response) {
            if (onGetDataSuccess) {
                onGetDataSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("_loadAllPageContents", e);
    }
}
/*MK Added - Update Redeemed points on server*/
function updateAchivePoints(postdata, OnUpdateSuccess) {
    logStatus("Calling updateRedeemCreditsPoints", LOG_FUNCTIONS);
    try {
        var getdata = {};
        var user = getCurrentUser();
        getdata.p = 'questionaries';
        getdata.action = 'updateAchivePoints';
        getdata.userId = user.user_id;
        do_service(getdata, postdata, function (response) {
            if (OnUpdateSuccess) {
                OnUpdateSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("updateRedeemCreditsPoints", e);
    }
}
/*
 function getUserFitlevelAndPoints(userid,onGetDataSuccess){
 logStatus("Calling getUserFitlevelAndPoints", LOG_FUNCTIONS);
 try {
 var getData={};
 getData.action	=	'getUserFitlevel';
 getData.p		=	'settings';
 getData.userid		=	userid;
 do_service(getData,{},function(response){
 if(onGetDataSuccess){
 onGetDataSuccess(response);

 }
 },'json');
 } catch(e){
 ShowExceptionMessage("getUserFitlevelAndPoints", e);
 }
 }

 function updateMappedMemberDevice(postData,onUpdateSuccess){
 logStatus("Calling updateMappedMemberDevice", LOG_FUNCTIONS);
 try {
 var getData		=	{};
 getData.action	=	'updateMappedMemberDevice';
 getData.p		=	'members';
 getData.clubId	=	getselectedclubid();
 getData.UserId	=	getCurrentLoggedinUserId();
 do_service(getData,postData,function(response){
 if(onUpdateSuccess){
 onUpdateSuccess(response);
 }
 },'json');
 } catch(e){
 ShowExceptionMessage("updateMappedMemberDevice", e);
 }
 }
 */
function postpointachieveddata(trainingid, trainingtype, phaseid, groupid, curstatus, hrdata, creditdttm, points, refid, reftypeid, onUpdateSuccess) {
    logStatus("Calling postpointachieveddata", LOG_FUNCTIONS);
    try {
        var User = getCurrentUser();
        var getData = {
            "userid": User.user_id,
            "trainingid": trainingid,
            "trainingtype": trainingtype,
            "phaseid": phaseid,
            "groupid": groupid,
            "status": curstatus,
            "hrdata": hrdata,
            "creditdttm": creditdttm,
            "points": points,
            "refid": refid,
            "reftypeid": reftypeid
        };
        getData.action = 'postpointachieveddata';
        getData.p = 'members';
        do_service(getData, {}, function (response) {
            if (onUpdateSuccess) {
                onUpdateSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("postpointachieveddata", e);
    }
}
function SaveAllAnswersQuestions(postData, onUpdateSuccess) {
    logStatus("Calling SaveAllAnswersQuestions", LOG_FUNCTIONS);
    try {
        var User = getCurrentUser();
        var getData = {};
        getData.action = 'SaveAllAnswersQuestions';
        getData.p = 'questionaries';
        getData.userId = User.user_id;
        do_service(getData, {'allanswers': JSON.stringify(postData)}, function (response) {
            if (onUpdateSuccess) {
                onUpdateSuccess(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("SaveAllAnswersQuestions", e);
    }
}
/**************************************
       Mind Tab Activation
**************************************/
function mindTabAct(postdata, onsuccesresponsehandler) {
    logStatus("Calling mindTabAct", LOG_FUNCTIONS);
    try {
var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuccesresponsehandler){
                onsuccesresponsehandler(response);
            }
        },'json');
    } catch (e) {
        ShowExceptionMessage("mindTabAct", e);
    }
}

/**************************************
       Notification
**************************************/
function getNotificatonsInapp(postData,Onsuccesshandler) {
    logStatus("Calling getNotificatonsInapp", LOG_FUNCTIONS);
    try {
        var _dataa = {};
		var _data = {};
  var User = getCurrentUser();
  var creditindexobject = getCurrentGroupwiseIndexPercentage();
  var screenscore = creditindexobject['' + postData.screen].creditindex;
        _dataa.action = 'getInappNotification';
        _dataa.p = 'notification';
  _data.userid = User.user_id;
  _data.screen = postData.screen;
  _data.screenscore = screenscore;
  _data.totalcredit = postData.totalcredit;
  _data.day = postData.day;
  _data.question = JSON.stringify(postData.question);
  
  do_notification(_dataa,_data, function (response) {
            if (Onsuccesshandler) {
                Onsuccesshandler(response);
            }
        }, 'json');
    } catch (e) {
        ShowExceptionMessage("getNotificatonsInapp", e);
    }
}

function _getNoHrTestResult(postData,Onsuccesshandler)
{
    logStatus("Calling _getNoHrTestResult", LOG_FUNCTIONS);
    try {
        var data = {};
        var data = postData;
        data.is_reporting = BASE_URL+"reporting/index.php/VoucherCredit/get_points_per_activity";

        do_service(postData,data,function(response){
            if(Onsuccesshandler){
                Onsuccesshandler(response);
            }
        },'json');
    }
    catch(e){
        ShowExceptionMessage("_getNoHrTestResult", e);
    }
}
function _getCoachInfo(postData,Onsuccesshandler)
{
    logStatus("Calling _getCoachInfo", LOG_FUNCTIONS);
    try {
        var data = {};
        var data = postData;
        
        data.is_reporting = BASE_URL+"reporting/index.php/report/coach_info";
        
        do_service(postData,data,function(response){
            if(Onsuccesshandler){
                Onsuccesshandler(response);
            }
        },'json');
    }
    catch(e){
        ShowExceptionMessage("_getCoachInfo", e);
    }
}
function SendVoucherMail(data, onsucessResponse) {
        var urlVoucher = BASE_URL+"reporting/index.php/VoucherCredit/voucher/";
        
        logStatus("SendVoucherMail", LOG_FUNCTIONS);
        try {
            //var _datapost = {};
            var _data = (data) ? data : {};
            _data.is_reporting = urlVoucher;
            do_service(data, _data, function (response) {
                if (onsucessResponse) {
                    onsucessResponse(response);
                }
            }, 'json');
        } catch (e) {
            ShowExceptionMessage("SendVoucherMail", e);
     }
}
function saveStepdata(postData,Onsuccesshandler)
{
    logStatus("Calling saveStepdata", LOG_FUNCTIONS);
    try {
        var data = {};
        var data = postData;
        do_service(data,postData,function(response){
            if(Onsuccesshandler){
                Onsuccesshandler(response);
            }
        },'json');
    }
    catch(e){
        ShowExceptionMessage("saveStepdata", e);
    }
}

function getdeviceid(postData,Onsuccesshandler){
    logStatus("Calling getdevice", LOG_FUNCTIONS);
    try {
        var data = {};
        var data = postData;
        do_service(data,postData,function(response){
            if(Onsuccesshandler){
                Onsuccesshandler(response);
            }
        },'json');
    }
    catch(e){
        ShowExceptionMessage("getdevice", e);
    }
}

//update device
//------------
function updatedeviceCoach(postdata,onsuceessresponse)
{
    logStatus("Calling updatedevicedata",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("updatedevicedata", e);
    }
}
//update device type version
//------------
function addAppVersion_type(postdata,onsuceessresponse)
{
    logStatus("Calling addAppVersion_type",LOG_FUNCTIONS);
    try {
        var data = {};
        data.p = 'members';
        data.action = 'saveUserDevice';
        data.user_id = postdata.user_id;
        data.app_platform = postdata.app_platform;
        data.device_version = postdata.device_version;
        data.device_model = postdata.device_model;
        data.app_version = postdata.app_version;
        do_service(data,postdata,function(response){
                   if(onsuceessresponse){
                   onsuceessresponse(response);
                   }
                   },'json');
    } catch (e){
        ShowExceptionMessage("addAppVersion_type", e);
    }
}