var defaultSlideIndex;
var incSlideIndex	=	0;
var _QuestionBackInfo;
var _BackQuestionIndex;
var _SplitCharOptionValue   =   '__';
var undefined;//unset variable / mark as undefined
//var isLastNeedSave	=	false;
$(document).ready(function(){
    var appversion = '';
    $('.versionnums').text(appversion);
    TransQuestion();
    $(".formfocus_ipentry input,.formfocus_ipentry select").trigger("blur");
    logStatus("Calling Questions.Document.Event.Ready", LOG_FUNCTIONS);
    try{
        TranslateGenericText();
        InitPageEvents();
        defaultSlideIndex=undefined;
        var selectedgrpid = getlastActiveGroupId();
        
        var ActiveFaseId = getCurrentFaseForGroupId(selectedgrpid);
        var isback = GetUrlArg('_back', false, '');
        if (isback != ''){
            //Need to set the data from session
            _QuestionBackInfo = GetSelectedQuestionBackInfoObj();
            defaultSlideIndex	=	_BackQuestionIndex;

            //UpdateQuestionBackInfo({});
        }
        if(ActiveFaseId==PHASE_Check) {
            BuildQuestionsForPhaseAndGroup(PHASE_Check, 0);
			
        }
	//var id = $('.swiper-slide-active').attr('data-slide-index');
	//$('.q_no').html(id);
		var tot = $('#totalquesvalue').val();
		//$('.swiper-containers').html('<p>'+ 1 + '/' + tot +'</p>');
		//change on 17-05-17 
		var int_startslide = $(".swiper-slide-active").attr("data-slide-index");
		localStorage.setItem('int_startslide',int_startslide);
		if(int_startslide>1){
			var startslide = parseInt(int_startslide) + 1;
			$('.swiper-containers').html('<p>'+ startslide + '/' + tot +'</p>');
		}
		else{$('.swiper-containers').html('<p>'+ 1 + '/' + tot +'</p>');}	
    } catch (e) {
        ShowExceptionMessage("Questions.Document.Event.Ready", e);
    }
});
/*$(document).delegate(".checked_icon_cls","click",function(){
    //openQuestinfoForm(this,\''+questiondata.r_quesgroup_id+'\', \''+questiondata.id+'\', \''+questiondata.type+'\', \''+questiondata.monitorvariable_id+'\')
    _movePageTo('question_smily_info');
});*/
function BuildQuestionsForPhaseAndGroup(PhaseId, GroupId){
    logStatus("Calling BuildQuestionsForPhaseAndGroup", LOG_FUNCTIONS);
    logStatus(PhaseId, LOG_DEBUG);
    try{
        var HTML	=	'';
        var progressHtml	=	'<div class="perfill_inner"><div class="perfill"></div></div>';
        var $questionpanel	=	$("#question-forms");
        var $progresspanel	=	$("#progress_group");
        var IsUPCCDone = false;
        if(GroupId==0){
            if(_Questierdatainfo) {
                HTML	+='<div class="swiper-wrapper">';
                $.each(_Questierdatainfo,function(groupidx,egroup){
                    var phase	=	(egroup._phase) ? egroup._phase:{};
                    if(phase._questionscount>0) {
                        var questions	=	(phase._questionscount==1) ? [phase._questions]:phase._questions;
				
                        if ((!IsUPCCDone) && (egroup.group_id == GROUP_GENERAL)){
                        // if ((!IsUPCCDone) && (egroup.group_id == GROUP_MOVESMART)){
                            IsUPCCDone = true;
                            HTML+=	GetGenericTextContentForQuestions(PAGE_CONTENT_UPCC_INFO);
                        }
                        HTML+=	GetBuiltQuestionariesTemplate(questions,egroup.group_id);
                    }
                    progressHtml+=	'<label id="progtabgroup' + egroup.group_id + '" >&nbsp;</label>';
                    //Since they dont want it now progressHtml+=	'<label id="progtabgroup' + egroup.group_id + '" >' + egroup.group_name + '</label>';
                });
                HTML+=	'</div>';
            }
        }
        $questionpanel.html(HTML);
        $progresspanel.html(progressHtml);
        var slideroptions	=	{
            preventClicks:true,
            slideToClickedSlide:false,
            mode:'horizontal',
            loop: false,
            autoHeight:true,
            onSlideNextEnd:CheckFaseSldierMove,
			onSlidePrevEnd:getslidenumber,
            //onSlideNextStart:CheckFaseSldierMove,
            //onSlideChangeStart:CheckFaseSldierMove
            //onSliderMove:CheckFaseSldierMove
            //onTransitionStart:TrackSaveLaseSlide
            onTransitionEnd:TrackSaveLaseSlide
            //onReachEnd:TrackSaveLaseSlide
            //onTouchEnd:CheckFaseSldierMove

        };
        slideroptions.initialSlide=defaultSlideIndex;
        LoadPercentageCompleteForPhaseAndGroup();
        _QuestionSliderObj[0]	= new Swiper ('#question-forms',slideroptions);
        //_QuestionSliderObj[0].on('onSlideNextEnd',CheckFaseSldierMove);
        //InitOptionPicker(".question_option");
        UpdateRetainedValuesOnBack();
    } catch (e) {
        ShowExceptionMessage("BuildQuestionsForPhaseAndGroup", e);
    }
}

function InitOptionPicker(selector){
    var $selector   =   $(selector);
    if( (typeof($selector.html())==='undefined') && (typeof($selector.val())=='undefined')) {
        return;
    }
    //MK Added Generic Scroll Panel Option
    //applyScrollPanel(selector);
    //return;

    $selector.each(function(){
        var $self   =   $(this);
        var panel_count  =$self.find('option').length;
        $self.drum({
            panelCount:30,
	    dail_w: 35, dail_h: 10, dail_stroke_width: 2,
            applySelectedClass:'figureOptionSelected',
            onChange:function(elem,$optionElem){

                /*$optionElem.addClass("");
                $optionElem.siblings().removeClass("figureOptionSelected");*/

            }
        });
    });/**/
    /**/
    /*$selector.iPhonePicker({
        width: '300px',
        reset:false/*
        imgRoot: 'images/option-box/'* /
    });/**/
    //MK Debug $selector.show();
}

function getPercentageDetailsForQuestion(prog){
    var minlabelpxarry = [52, 40, 55, 31];
    //var minlabelpxarry = [75, 60, 80, 44];
    var fullwidthpx = $("#progress_group").width();
    var minwidthreqpx = 0;
    $.each(minlabelpxarry, function(idx, val){
        minwidthreqpx += val;
    });
    var balavailwidthpx = (fullwidthpx - minwidthreqpx);
    var balavailper = parseFloat((balavailwidthpx/fullwidthpx)*100);
    var ajustedperc = 0;
    var excessadjperc = 0;
    var sharedobj = {};
    var returnperarry = {};
    var incarr = 0;
    $.each(prog,function(groupidx,row){
        var minpx = minlabelpxarry[incarr];
        var minper = (minpx/fullwidthpx)*100;
        sharedobj[groupidx] =  (minper - row.cons_qpercentage);
        returnperarry[groupidx] = minper;
        if (minper > row.cons_qpercentage){
            ajustedperc += (minper - row.cons_qpercentage);
        }else{
            excessadjperc += (row.cons_qpercentage - minper);
        }
        incarr++;
    })	;
    var count	=	Object.keys(prog).length;//MK To Share the Width by Percentage In Equal
    $.each(sharedobj,function(groupidx,val){
        if (val < 0){
            var posval = val * -1;
            var shareper = ((posval/excessadjperc)*100);
            var additionalper = ((balavailper * shareper)/100);
            if (count == 1){
                returnperarry[groupidx] = 100;

            }else{
                returnperarry[groupidx] += additionalper;
            }
        }
    });
    return returnperarry;
}

function LoadPercentageCompleteForPhaseAndGroup(GroupId,selDate){
    logStatus("Calling LoadPercentageCompleteForPhaseAndGroup", LOG_FUNCTIONS);
    try{
        var	prog		=	GetAllQuestionsCountByGroupId(GroupId,selDate);
        var returnperarry = getPercentageDetailsForQuestion(prog);
        var percentage	=	0;
        var count	=	Object.keys(prog).length;//MK To Share the Width by Percentage In Equal
        logStatus(count, LOG_DEBUG);
        //var grouplabelwidth	=	(100/count);//MK Since to be calculated as percentage
        $.each(prog,function(groupidx,row) {
            var $glabel	=	$("#progtab"+groupidx);
            if(row.percentage==100){
                $glabel.addClass("ac_text");
            }
            var perc = parseFloat(returnperarry[groupidx]).toFixed(2);
            var incpercentage	=	(row.percentage/100) * perc;
            //XX $glabel.css("width",grouplabelwidth+"%");
            $glabel.css("width",perc+"%");

            percentage+=parseFloat(incpercentage);
            //$glabel.css("width",row.cons_qpercentage+"%");
            //percentage+=parseFloat(row.cons_anspercentage);

        });
        /*
        console.log(returnperarry);
        var percentage	=	0;

        var minlabelpxarry = [75, 60, 80, 44];
        //XX var minlabelpx = 80;
        //XX var maxgroupcnt = 4;//prog.length;
        var fullwidth = $("#progress_group").width();

        //var minwidthreq = minlabelpx * maxgroupcnt;
        var minwidthreq = 0;
        $.each(minlabelpxarry, function(idx, val){
            minwidthreq += val;
        });

        var balavailwidth = (fullwidth - minwidthreq);
        //XX var minper = parseFloat((minlabelpx/fullwidth)*100);
        var balavailper = parseFloat((balavailwidth/fullwidth)*100);

        //console.log("maxgroupcnt:"+maxgroupcnt+" fullwidth:"+fullwidth+" minwidthreq:"+minwidthreq+" balavailwidth:"+balavailwidth+" minper:"+minper+" balavailper:"+balavailper);

        var incarr = 0;
        var remper = 0;
        $.each(prog,function(groupidx,row){
            var $glabel	=	$("#progtab"+groupidx);
            if(row.percentage==100){
                $glabel.addClass("ac_text");
            }
            var balper = ((balavailper * row.cons_qpercentage)/100);
            var minpx = minlabelpxarry[incarr];
            var minper = (minpx/fullwidth)*100;
            if (minper > row.cons_qpercentage){
                remper += balper;
                balper = 0;
            }else{
                balper = (row.cons_qpercentage - minper);
                remper = remper - balper;
            }

            var perc = parseFloat(minper+balper).toFixed(2);
            console.log(minper + " - " + perc+" - "+ remper +" - "+balper+" - "+row.cons_qpercentage);
            $glabel.css("width",perc+"%");
            // $glabel.css("width",row.cons_qpercentage+"%");
            percentage+=parseFloat(row.cons_anspercentage);
            incarr++;
        });
        */
        applyProgressPercentage('.footer_graph_box',percentage);
    } catch (e) {
        ShowExceptionMessage("LoadPercentageCompleteForPhaseAndGroup", e);
    }
}
function getQuestionTemplate(questiondata,nextQuesId,prevQuesId,parentid,selecteddate){
	//console.log("quess === "+JSON.stringify(questiondata));
	//alert(questiondata.question_info
    logStatus("Calling getQuestionTemplate", LOG_FUNCTIONS);
    try {
		//console.log("questiondata");
		//console.log(questiondata);
        var template	=	'<p>Something Went Wrong!!!</p>';
        var cusparentid = '';
        switch(parseInt(questiondata.type)){
            case QUESTION_TYPE_NUMBER:
                template	=	getNumberTypeTemplate(questiondata,nextQuesId,prevQuesId,parentid, selecteddate);
                break;
            case QUESTION_TYPE_OPTION:
                template	=	getRadioTypeTemplate(questiondata,nextQuesId,prevQuesId, parentid,selecteddate);
                break;
            case QUESTION_TYPE_CHECKBOX:
                template	=	getCheckBoxTypeTemplate(questiondata,nextQuesId,prevQuesId, parentid,selecteddate);
                break;
            case QUESTION_TYPE_YN:
                cusparentid=	parentid + ' #questiontemp_'+questiondata.id;
                template	=	getYNTypeTemplate(questiondata,nextQuesId,prevQuesId, cusparentid, selecteddate);
                break;
            case QUESTION_TYPE_SMILEY:
                 cusparentid=	parentid + ' #questiontemp_'+questiondata.id;
                template	=	getSmilyTypeTemplate(questiondata,nextQuesId,prevQuesId, cusparentid, selecteddate);
                break;
            case QUESTION_TYPE_LIKDISLIK:
                 cusparentid=	parentid + ' #questiontemp_'+questiondata.id;
                template	=	getLikdislikTypeTemplate(questiondata,nextQuesId,prevQuesId, cusparentid, selecteddate);
                break;
        }
        return template;
    } catch(e){
        ShowExceptionMessage("getQuestionTemplate", e);
    }
}
/**
 * Description: Get Number Type Template
 */
function getNumberTypeTemplate(questiondata, nextQuesId, prevQuesId, parentid, selecteddate){
	
    logStatus("Calling getNumberTypeTemplate", LOG_FUNCTIONS);
    logStatus(nextQuesId, LOG_DEBUG);
    logStatus(prevQuesId, LOG_DEBUG);
    try {
        var nparentid = (parentid)?parentid:'';
        /**
        * @param   questiondata.formelement_option  This is questiondata formelement_option
        * @param   questiondata._answercount  This is questiondata _ answer count
        * @param   questiondata._answers  This is questiondata answer
        * @param   questiondata.monitorvariable_id  This is questiondata monitorvariable id
        * @param   questiondata.r_quesgroup_id  This is question group id
        * @param   questiondata.prameter_value  This is parameter value
        * @param   questiondata.parameter_goal  This is parameter goal
        */
        var ansoption	=	questiondata.formelement_option;
        var arr_ansoption	=	(ansoption && ansoption!='')  ? ansoption.split("_"):[];
        var start	=	(arr_ansoption[0]) ? arr_ansoption[0]:0;
        var end		=	(arr_ansoption[1]) ? arr_ansoption[1]:10;
        var step	=	(arr_ansoption[2]) ? arr_ansoption[2]:1;
        var clickable	=	true;
        var answer		=	start;
        var answerdata	=	{};
        if(questiondata._answercount>0)  {
            var answers		=	(questiondata._answercount==1) ? [questiondata._answers]:questiondata._answers;
            answerdata	=	GetAnswerByDate(answers,selecteddate);
            /**
             *  @param   answerdata.actmv_otherdata  This is questiondata minitor variable other data
             */
            answer	=	answerdata.actmv_otherdata ? answerdata.actmv_otherdata:0;
            if (! IsPhaseQuestionComplete(PHASE_Check)){
                clickable = false;
            }
        }
        var ans	=	getAnswersLocalByMV(questiondata.monitorvariable_id);
        if(ans){
            answer	=	ans;
        }
        var questtransobj = getQuestionTransObjForGroupIdAndQuestId(questiondata.r_quesgroup_id, questiondata.id);
        var rangeId	=	'rangerinput'+questiondata.id;
        var questionstring	=	questtransobj.question.split(MARKER_QUERY_QUSBREAK);
        var question_notes	=	getQuestionNotesByQuestionString(questionstring);
        question_notes	=	(question_notes!='') ? '<div class="questionnote-wrapper">' + question_notes + '</div>' :'';
		var ques_info = questtransobj.question_info;
		//console.log('aaa' + questtransobj);
		
	   var questionStyle ='';
		if(ques_info=='' || typeof ques_info == 'undefined')
		{   
	       questionStyle = 'style="display:none!important;"';
		}
        /*+
         '<div class="btn_center_box">'+
                            '<button class="white_btn" parentid="'+nparentid+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' onclick="saveMonitorNumberAnswer(' + questiondata.monitorvariable_id + ',' + clickable + ', this,\'' + selecteddate + '\')" nextId="' + nextQuesId + '">'+GetLanguageText(BTN_SAVE)+'</button>'+
                        '</div>';*/
        return '<div class="welcome_ctn_bx"> ' +
             '<div class="welcome_ctn_header" >' +
			  '<div class="text-wrap">'+
				'<div class="q-mark mark2" '+questionStyle+'><img  parentsel="' + nparentid + '" src="images/question-mark.png" onclick="openQuestinfoForm(this,\'' + questiondata.r_quesgroup_id + '\', \'' + questiondata.id + '\', \'' + questiondata.type + '\', \'' + questiondata.monitorvariable_id + '\')">  </div><div class="swiper-containers"><img src="images/loading.gif"/></div>'+
                '<h3>' + questionstring[0] + '</h3>'+
				'</div>'+
			/*'<h4 class="comm_h4" >' + questionstring[0] + '</h4>' +
                '<span class="question_icon" parentsel="' + nparentid + '" ' +
                    'onclick="openQuestinfoForm(this,\'' + questiondata.r_quesgroup_id + '\', \'' + questiondata.id + '\', \'' + questiondata.type + '\', \'' + questiondata.monitorvariable_id + '\')">?</span>' +
            '</div>' +
			*/
            '<input type="hidden" id="parameter_value' + questiondata.monitorvariable_id + '" name="parameter_value' + questiondata.monitorvariable_id + '" value="' + questiondata.prameter_value + '" />' +
            '<input type="hidden" id="parameter_goal' + questiondata.monitorvariable_id + '" name="parameter_goal' + questiondata.monitorvariable_id + '" value="' + questiondata.parameter_goal + '" />' +
            '<div class="counter-container" >' +
			'<div class="arrow-left"></div>'+
            //'<span id="' + rangeId + '" class="ques_count ip_numberval' + questiondata.monitorvariable_id + '" >' + answer + '</span>' +
            '<button id="decrmtstep" class="qus_fl_btn decrease decrmtstep" start="' + start + '" step="' + step + '"  ' + ( (!clickable) ? 'disabled="disabled"' : '') + ' associd="' + rangeId + '"> - </button>' +
            '<div id="' + rangeId + '" class="ques_count counter ip_numberval' + questiondata.monitorvariable_id + '">' + answer + '</div>'+
			'<button id="incrmentstep" class="qus_fr_btn increase incrmentstep" end="' + end + '"  step="' + step + '" ' + ( (!clickable) ? 'disabled="disabled"' : '') + ' associd="' + rangeId + '"> + </button>' +
            '<div class="arrow-right"></div>'+
			'</div>' +
            question_notes +
			'</div>'+
			/*
			 '<input type="hidden" id="parameter_value' + questiondata.monitorvariable_id + '" name="parameter_value' + questiondata.monitorvariable_id + '" value="' + questiondata.prameter_value + '" />' +
            '<input type="hidden" id="parameter_goal' + questiondata.monitorvariable_id + '" name="parameter_goal' + questiondata.monitorvariable_id + '" value="' + questiondata.parameter_goal + '" />' +
			 '<div class="text-wrap">'+
                
                '<div class="q-mark"><img src="images/question-mark.png">  </div>'+
                '<h3>I have had at least 30 minutes of exercise with moderate intensity</h3>'+
                '<div class="counter-container">'+
                    
                    '<div class="arrow-left"></div>'+
                    
					'<button class="qus_fl_btn decrease decrmtstep" start="' + start + '" step="' + step + '"  ' + ( (!clickable) ? 'disabled="disabled"' : '') + ' associd="' + rangeId + '"> - </button>'+
                    '<div class="counter ip_numberval' + questiondata.monitorvariable_id + '">' + answer + '</div>'+
                    
					'<button class="qus_fr_btn increase incrmentstep" end="' + end + '"  step="' + step + '" ' + ( (!clickable) ? 'disabled="disabled"' : '') + ' associd="' + rangeId + '"> + </button>' +
                    '<div class="arrow-right"></div>'+
                    
               ' </div>'+
               ' <h3>'+question_notes+'</h3>'+
			   '</div>'+
                */
               
               
            
            '</div>';
    } catch(e){
        ShowExceptionMessage("getNumberTypeTemplate", e);
    }
}
/*SN - Load Yes Or No Type*/
function getYNTypeTemplate(questiondata,nextQuesId,prevQuesId, parentid, selecteddate){
    logStatus("Calling getYNTypeTemplate", LOG_FUNCTIONS);
    logStatus(nextQuesId, LOG_DEBUG);
    logStatus(prevQuesId, LOG_DEBUG);
    try {
        var nparentid = (parentid)?parentid:'';
        //var ansoption	=	questiondata._options;
        var clickable	=	true;
        var quesoptions	=	questiondata._options;
        var imgcnt = '';
        var answer;
        if(questiondata._answercount>0)  {
            var answers		=	(questiondata._answercount==1) ? [questiondata._answers]:questiondata._answercount;
            var answerdata	=	GetAnswerByDate(answers,selecteddate);
            answer	=	answerdata.actmv_otherdata ? answerdata.actmv_otherdata:'';
            if (! IsPhaseQuestionComplete(PHASE_Check)){
                clickable = false;
            }
        }
        var ans	=	getAnswersLocalByMV(questiondata.monitorvariable_id);
        if(ans){
            answer	=	ans;
        }
        /**
        * @param   row.range_points This is range points
        */
        
        $.each(quesoptions,function(key,row){
            var img = (key==0) ?GetLanguageText(YES_BTN):GetLanguageText(NO_BTN);
            var selectclass = '';
            if (answer == row.range_points){
                selectclass = 'checked';
            }
           
           // imgcnt+='<span class="yesno_btn imgcls_clk '+selectclass+'" option_id="'+row.id+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' custvalue="'+row.range_points+'" >'
            // +'<img src="images/'+img+'.png" alt="'+img+'" class="yesnoimgouter"/>'
            //+'</span>';
            imgcnt+='<span class="yesno_btn imgcls_clk yesno_btn_fix yesno_btn_text_fix '+selectclass+'" option_id="'+row.id+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' custvalue="'+row.range_points+'" >'+img+'</span>';

        });
        var questtransobj = getQuestionTransObjForGroupIdAndQuestId(questiondata.r_quesgroup_id, questiondata.id);
        var questionstring	=	questtransobj.question.split(MARKER_QUERY_QUSBREAK);
        var question_notes	=	getQuestionNotesByQuestionString(questionstring);
        question_notes	=	(question_notes!='') ? '<div class="questionnote-wrapper">' + question_notes + '</div>' :'';
		var ques_info = questtransobj.question_info;
	
		   var questionStyle ='';
		if(ques_info=='' || typeof ques_info == 'undefined')
		{   
	       questionStyle = 'style="display:none!important;"';
		}
        /*+
         '<div class="btn_center_box">'+
                            '<button class="white_btn" questionid="'+questiondata.id+'" parentid="'+nparentid+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' onclick="saveMonitorSmilyAnswer(' + questiondata.monitorvariable_id + ',' + clickable + ', this,\''+selecteddate+'\')" nextId="' + nextQuesId + '"> Save </button>'+
                        '</div>';*/
        return '<div class="welcome_ctn_bx" >' +
            '<div class="welcome_ctn_header text-wrap" >' +
			'<div class="q-mark mark2" '+questionStyle+'><img parentsel="' + nparentid + '" src="images/question-mark.png" onclick="openQuestinfoForm(this,\'' + questiondata.r_quesgroup_id + '\', \'' + questiondata.id + '\', \'' + questiondata.type + '\', \'' + questiondata.monitorvariable_id + '\')">  </div><div class="swiper-containers"><img src="images/loading.gif"/></div>'+
            //'<h4 class="comm_h4" >' + questionstring[0] + '</h4>' +
			'<div  ><h3 >' + questionstring[0] + '</h3></div>' +
           // '<span class="question_icon"  parentsel="' + nparentid + '" onclick="openQuestinfoForm(this,\'' + questiondata.r_quesgroup_id + '\', \'' + questiondata.id + '\', \'' + questiondata.type + '\', \'' + questiondata.monitorvariable_id + '\')">?</span>' +
            '</div>' +

            '<div class="question_yesno_bx counter-container">' +
			'<div class="arrow-left"></div>'+
			'<div class="yesno">'+
            /*'<span class="yesno_btn">'+
             '<img src="images/yes_icon.png" alt="yes_icon" />'+
             '</span>'+
             '<span class="yesno_btn">'+
             '<img src="images/no_icon.png" alt="no_icon" />'+
             '</span>'+*/
            imgcnt +
			'</div>'+
			' <div class="arrow-right"></div>'+
            '</div>' +
            question_notes +
            '</div>';
    } catch(e){
        ShowExceptionMessage("getYNTypeTemplate", e);
    }
}/*SN - Load Like or Dislike Type -20160503*/
function getLikdislikTypeTemplate(questiondata,nextQuesId,prevQuesId, parentid, selecteddate){
    logStatus("Calling getLikdislikTypeTemplate", LOG_FUNCTIONS);
    logStatus(nextQuesId, LOG_DEBUG);
    logStatus(prevQuesId, LOG_DEBUG);
    try {
        var nparentid = (parentid)?parentid:'';
        //var ansoption	=	questiondata._options;
        var clickable	=	true;
        var quesoptions	=	questiondata._options;
        var imgcnt = '';
        var answer;
        if(questiondata._answercount>0)  {
            var answers		=	(questiondata._answercount==1) ? [questiondata._answers]:questiondata._answercount;
            var answerdata	=	GetAnswerByDate(answers,selecteddate);
            answer	=	answerdata.actmv_otherdata ? answerdata.actmv_otherdata:'';
            if (! IsPhaseQuestionComplete(PHASE_Check)){
                clickable = false;
            }
        }
        var ans	=	getAnswersLocalByMV(questiondata.monitorvariable_id);
        if(ans){
            answer	=	ans;
        }
        $.each(quesoptions,function(key,row){
            var img = (key==0) ?'like':'dislike';
            var selectclass = '';
            if (answer == row.range_points){
                selectclass = 'checked';
            }
            imgcnt+='<span class="yesno_btn imgcls_clk '+selectclass+'" option_id="'+row.id+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' custvalue="'+row.range_points+'" ><img src="images/'+img+'.png" alt="'+img+'" class="yesnoimgouter"/></span>';
        });
        var questtransobj = getQuestionTransObjForGroupIdAndQuestId(questiondata.r_quesgroup_id, questiondata.id);
        var questionstring	=	questtransobj.question.split(MARKER_QUERY_QUSBREAK);
        var question_notes	=	getQuestionNotesByQuestionString(questionstring);
        question_notes	=	(question_notes!='') ? '<div class="questionnote-wrapper">' + question_notes + '</div>' :'';
        /*+
         '<div class="btn_center_box">'+
                            '<button class="white_btn" questionid="'+questiondata.id+'" parentid="'+nparentid+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' onclick="saveMonitorSmilyAnswer(' + questiondata.monitorvariable_id + ',' + clickable + ', this,\''+selecteddate+'\')" nextId="' + nextQuesId + '"> Save </button>'+
                        '</div>';*/
        return '<div class="welcome_ctn_bx" >' +
            '<div class="welcome_ctn_header" >' +
            '<h4 class="comm_h4" >' + questionstring[0] + '</h4>' +
            '<span class="question_icon"  parentsel="' + nparentid + '" onclick="openQuestinfoForm(this,\'' + questiondata.r_quesgroup_id + '\', \'' + questiondata.id + '\', \'' + questiondata.type + '\', \'' + questiondata.monitorvariable_id + '\')">?</span>' +
            '</div>' +

            '<div class="question_yesno_bx">' +
			' <div class="arrow-left" style="margin: 9% 7px;"></div>'+
            /*'<span class="yesno_btn">'+
             '<img src="images/yes_icon.png" alt="yes_icon" />'+
             '</span>'+
             '<span class="yesno_btn">'+
             '<img src="images/no_icon.png" alt="no_icon" />'+
             '</span>'+*/
            imgcnt +
			'<div class="arrow-right" style="margin: 9% 7px;"></div>'+
            '</div>' +
            question_notes +
            '</div>';
    } catch(e){
        ShowExceptionMessage("getLikdislikTypeTemplate", e);
    }
}
/**
* Description:Get smily type Template
*/
function getSmilyTypeTemplate(questiondata,nextQuesId,prevQuesId, parentid, selecteddate){
    logStatus("Calling getSmilyTypeTemplate", LOG_FUNCTIONS);
    logStatus(nextQuesId, LOG_DEBUG);
    logStatus(prevQuesId, LOG_DEBUG);
    try {
        var clickable	=	true;
        var quesoptions	=	questiondata._options;
        var nparentid = (parentid)?parentid:'';
        var imgcnt = '';
        var imgcount = quesoptions.length;
        var answer;
        if(questiondata._answercount>0)  {
            var answers		=	(questiondata._answercount==1) ? [questiondata._answers]:questiondata._answercount;
            var answerdata	=	GetAnswerByDate(answers,selecteddate);
            answer	=	answerdata.actmv_otherdata ? answerdata.actmv_otherdata:'';
            if (! IsPhaseQuestionComplete(PHASE_Check)){
                clickable = false;
            }
        }
        var ans	=	getAnswersLocalByMV(questiondata.monitorvariable_id);
        if(ans){
            answer	=	ans;
        }
        var countimg  = imgcount;
        /**
         * @param   row.range_points This is range points
         */
        $.each(quesoptions,function(key,row){
            var i = key+1;
            var selectclass = '';
            if (answer == row.range_points){
                selectclass = 'checked';
            }

            imgcnt+='<span class="imgcls_clk imgnum_'+i+' '+selectclass+'" option_id="'+row.id+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' custvalue="'+row.range_points+'"><img src="images/smile'+countimg+'.png" alt="smile2" />';
            /*if(imgcount==i){
                imgcnt+='<a href="javascript:void(0)" class="question_icon checked_icon_cls" parentsel="'+nparentid+'"  onclick="OpenQuestHelpInfo(this,\''+questiondata.r_quesgroup_id+'\', \''+questiondata.id+'\', \''+questiondata.type+'\', \''+questiondata.monitorvariable_id+'\')" >?</a>';
            }*/
            imgcnt+='</span>';
            countimg = countimg-1;
        });
        var questtransobj = getQuestionTransObjForGroupIdAndQuestId(questiondata.r_quesgroup_id, questiondata.id);
        var questionstring	=	questtransobj.question.split(MARKER_QUERY_QUSBREAK);
        var question_notes	=	getQuestionNotesByQuestionString(questionstring);
        question_notes	=	(question_notes!='') ? '<div class="questionnote-wrapper">' + question_notes + '</div>' :'';
        var HTML 	='<div class="welcome_ctn_bx" >'+
                            '<div class="welcome_ctn_header" >'+
                                '<h4 class="comm_h4" >' + questionstring[0] + '</h4>'+
                                '<span class="question_icon" parentsel="'+nparentid+'"  onclick="openQuestinfoForm(this,\''+questiondata.r_quesgroup_id+'\', \''+questiondata.id+'\', \''+questiondata.type+'\', \''+questiondata.monitorvariable_id+'\')">?</span>'+
                            '</div>'+
                            '<input type="hidden" id="parameter_value' + questiondata.monitorvariable_id + '" name="parameter_value' + questiondata.monitorvariable_id + '" value="' + questiondata.prameter_value + '" />'+
                            '<input type="hidden" id="parameter_goal' + questiondata.monitorvariable_id + '" name="parameter_goal' + questiondata.monitorvariable_id + '" value="' + questiondata.parameter_goal + '" />'+
                            '<div class="question_smile_btn_bx">';

        HTML+=	imgcnt+'</div>'+question_notes+'</div>';/*+
                        '<div class="btn_center_box">'+
                            '<button class="white_btn" questionid="'+questiondata.id+'" parentid="'+nparentid+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' onclick="saveMonitorSmilyAnswer(' + questiondata.monitorvariable_id + ',' + clickable + ', this,\''+selecteddate+'\')" nextId="' + nextQuesId + '"> Save </button>'+
                        '</div>';*/
        return HTML;
    } catch(e){
        ShowExceptionMessage("getSmilyTypeTemplate", e);
    }
}
/*
function OpenQuestHelpInfo(selfobj,grpid, qsid,questiontype,mvid){
    logStatus("Calling OpenQuestHelpInfo", LOG_FUNCTIONS);
    try {
        var $pnl_cquestion	=	$("#questiontemp_"+qsid);
        var parent	=	$(selfobj).attr('parentsel');
        var slideindex		=	$pnl_cquestion.attr("data-slide-index");
        _BackQuestionIndex	=	parseInt(slideindex);
        SaveQuestionFormInfo(questiontype,qsid,mvid,parent);
        _movePageTo('question_smily_info',{grpid:base64_encode(grpid),qsid:base64_encode(qsid)});
    } catch(e){
        ShowExceptionMessage("OpenQuestHelpInfo", e);
    }
}*/
/**
 *Description:Open Question info form
 */
function openQuestinfoForm(selfobj,grpid, qsid,questiontype,mvid){
    logStatus("Calling openQuestinfoForm", LOG_FUNCTIONS);
    logStatus(questiontype, LOG_DEBUG);
    logStatus(selfobj, LOG_DEBUG);
    logStatus(mvid, LOG_DEBUG);
    try {
        var questtransobj = getQuestionTransObjForGroupIdAndQuestId(grpid, qsid);
        var quesinfo = '';
        // if (questtransobj){
            // //$("#idinfotext").html(questtransobj.question_info);
            // quesinfo = questtransobj.question_info;
        // }
        /**
         * @param    questtransobj.question_info This is question info
         */
        if(questtransobj && questtransobj.question_info) {
             quesinfo = questtransobj.question_info;
            var pcobj	=	quesinfo.split(MARKER_QUERY_PAGEBREAK);
			/* Add Image code to show on info page 08-05-17 */
			var img = getQuestionImage(grpid);
            var quesinfopagecnt	=	'<div class="swiper-wrapper">';
            $.each(pcobj,function(i,pc){
                quesinfopagecnt+=	'<div class="swiper-slide edu_inner">'
                +'<div class="comm_banner_img_box">'
                   /*  +'<img src="images/banner/move9.jpg" alt="move9" /> */
				   /* Add Image code to show on info page 08-05-17 */
					 /* +'<img src="'+img+'" alt="home_img1" />' */
					
                    /* +'<h4 style="bottom: 0px;" >MOVESMART.chect</h4>' */
                +'</div>'
                +'<div class="home_ctn_padding info-content" style="height:100%" >' + pc + '</div></div>';
            });
            //quesinfopagecnt+=	'</div><div class="swiper-pagination"></div>';
			quesinfopagecnt+=	'</div>';
        }

        // var quesinfopagecnt = '<div class="swiper-wrapper">';
        // //$.each(introcnt,function(key,value){
            // quesinfopagecnt+='<div class="swiper-slide" >'+quesinfo+'</div>';
    // //	});
        //quesinfopagecnt+='</div><div class="swiper-pagination"></div>';
		quesinfopagecnt+='</div>';
        $('#idinfotext').html(quesinfopagecnt);
        //$(".info_popupbox_bg").slideDown();
		//$(".info_popupbox_inner").slideDown();
		$(".info_popupbox_bg").show();
        $(".info_popupbox_inner").show();
        var imageHeight = $(".comm_banner_img_box").innerHeight();
        var headerHeight = $("header").innerHeight();
        var infoTo = imageHeight + headerHeight;
        $(".clsquestioncontent, .main_panel").scrollTop(0);
        //$(".info_popupbox_bg").css("top", infoTo);
        //$(".info_popupbox_inner").css("top", infoTo + 10);
		$(".info_popupbox_bg").css("top", 0);
        $(".info_popupbox_inner").css("top", 0);
		$(".info_popupbox_bg").css("z-index", 1200);
        $(".info_popupbox_inner").css("z-index", 1250);
		$(".info_popupbox_bg").css("background", 'rgba(0,149,172,0.95)');
		$(".info_popupbox_inner").css("background", 'none');

        $(".clsquestioncontent, .main_panel").css("overflow", " hidden ");
        swipePagination('#idinfotext',false);

        /* var $pnl_cquestion	=	$("#questiontemp_"+qsid);
        var parent	=	$(selfobj).attr('parentsel');
        var slideindex		=	$pnl_cquestion.attr("data-slide-index");
        _BackQuestionIndex	=	parseInt(slideindex);//MK Way 2 - to mark last question slide index
        //defaultSlideIndex
        SaveQuestionFormInfo(questiontype,qsid,mvid,parent);
        /*If we save the last question info May Be able to get the last question info on the
        /#question_info without URL Values* /
        _movePageTo("question_info",{grpid:base64_encode(grpid),qsid:base64_encode(qsid)}); */
    } catch(e){
        ShowExceptionMessage("openQuestinfoForm", e);
    }
}
/**
 * Description:Get checkbox type template
 */
function getCheckBoxTypeTemplate(questiondata,nextQuesId,prevQuesId, parentid,selecteddate){
    logStatus("Calling getCheckBoxTypeTemplate", LOG_FUNCTIONS);
    logStatus(nextQuesId, LOG_DEBUG);
    logStatus(prevQuesId, LOG_DEBUG);
    try {
        var nparentid = (parentid)?parentid:'';
        //var IsQuestionStatic	=	IsStaticQuestion(questiondata.id);
        var quesoptions=	[];
        /**
         * @param    questiondata._optionscounts This is question options  count
         * @param   _eachques.range_points  This is question range point
         */
        if(questiondata._optionscounts==1) {
            quesoptions.push(questiondata._options);
        } else if(questiondata._optionscounts>1){
            quesoptions	=	questiondata._options;
        }
        //var clickable	=	true;
        var answeroptions	=	[];
        if(questiondata._answercount>0){
            var answers	=	(questiondata._answercount==1 ) ? [questiondata._answers]:questiondata._answers;
            var answerdata	=	GetAnswerByDate(answers, selecteddate);
            var answer	=	answerdata.actmv_otherdata ? answerdata.actmv_otherdata:'';

            /*if (! IsPhaseQuestionComplete(PHASE_Check)){
                clickable = false;
            }*/
        }
        var ans	=	getAnswersLocalByMV(questiondata.monitorvariable_id);
        if(ans){
            answer	=	ans;
        }
        if(answer && answer!=''){
            answer	=	answer.split("_");
            if(answer.length>0) {
                $.each(answer,function(i,opitonval){
                    if(opitonval!=''){
                        var ov	=	opitonval.split(":");
                        if(ov[1]) {
                            answeroptions.push(ov[1]);
                        }
                    }
                });
            }
        }
		
        var questtransobj = getQuestionTransObjForGroupIdAndQuestId(questiondata.r_quesgroup_id, questiondata.id);
        var questionstring	=	questtransobj.question.split(MARKER_QUERY_QUSBREAK);
        var question_notes	=	getQuestionNotesByQuestionString(questionstring);
        question_notes	=	(question_notes!='') ? '<div class="questionnote-wrapper">' + question_notes + '</div>' :'';
		
        var ques_info = questtransobj.question_info;
	     var questionStyle ='';
		if(ques_info=='' || typeof ques_info == 'undefined')
		{   
	       questionStyle = 'style="display:none!important;"';
		}
		var HTML	=	'<div class="welcome_ctn_bx" >'+
                            //'<div class="welcome_ctn_header" >'+
                            //    '<h4 class="comm_h4" >' + questtransobj.question +'<span class="question_icon"  parentsel="'+nparentid+'" onclick="openQuestinfoForm(this,\''+questiondata.r_quesgroup_id+'\', \''+questiondata.id+'\', \''+questiondata.type+'\', \''+questiondata.monitorvariable_id+'\')">?</span></h4>'+
                            //'</div>'+
							'<div class="welcome_ctn_header text-wrap" >' +
							'<div class="q-mark mark2" '+questionStyle+'><img parentsel="' + nparentid + '" src="images/question-mark.png" onclick="openQuestinfoForm(this,\'' + questiondata.r_quesgroup_id + '\', \'' + questiondata.id + '\', \'' + questiondata.type + '\', \'' + questiondata.monitorvariable_id + '\')">  </div><div class="swiper-containers"><img src="images/loading.gif"/></div>'+
							//'<h4 class="comm_h4" >' + questionstring[0] + '</h4>' +
							'<h3 >' + questtransobj.question + '</h3>' +
						   // '<span class="question_icon"  parentsel="' + nparentid + '" onclick="openQuestinfoForm(this,\'' + questiondata.r_quesgroup_id + '\', \'' + questiondata.id + '\', \'' + questiondata.type + '\', \'' + questiondata.monitorvariable_id + '\')">?</span>' +
							'</div>' +
                            '<input type="hidden" id="parameter_value' + questiondata.monitorvariable_id + '" name="parameter_value' + questiondata.monitorvariable_id + '" value="' + questiondata.prameter_value + '" />'+
                            '<input type="hidden" id="parameter_goal' + questiondata.monitorvariable_id + '" name="parameter_goal' + questiondata.monitorvariable_id + '" value="' + questiondata.parameter_goal + '" />';

        if(quesoptions.length>0){
            //ED > questionans_list UL Option with fixed max-height List
			HTML+= '<div class="counter-container">';
			HTML+= '<div class="arrow-left" style="top:60px;"></div>';
			HTML+= ' <div class="radios">';
            HTML+=	'<ul class="questionselect_list questionans_list licheckoptions radioptlist">';
            $.each(quesoptions,function(i,_eachques){
                var optlang	=	getOptionLangText(_eachques);
                if(optlang && optlang.options){
                    var activeclass	=	'';
                    if(answeroptions.length>0){
                        if (answeroptions.indexOf(_eachques.id)!=-1){
                            activeclass='act_select';
                        }
                    }else{
                        activeclass=	'';
                    }
                   /* HTML+=	'<li class="' + activeclass + '" onclick=""><span class="markbtn_check"></span><input type="hidden" class="optionval'+questiondata.monitorvariable_id+'" value="'+ _eachques.range_points + ':' + _eachques.id + '" />' + optlang.options + '</li>';*/
                    HTML+=	'<li class="wslot-item ' + activeclass + '" onclick=""><span class="markbtn_check"></span><input type="hidden" class="optionval'+questiondata.monitorvariable_id+'" value="'+ _eachques.range_points + ':' + _eachques.id + '" /><span class="chkboxopttxt">' + optlang.options + '</span></li>';
                }
            });
            HTML+=	'</ul></div><div class="arrow-right" style="top:60px;"></div></div>';
			
			HTML+= '<span class="arrowtop_icon">&darr;</span>';
        }
        HTML+=			question_notes+
                    '</div>' ;/*+
                    '<div class="btn_center_box">'+
                        '<button class="white_btn" parentid="'+nparentid+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' onclick="saveMonitorCheckboxAnswer(' + questiondata.monitorvariable_id + ',' + clickable + ', this,\''+selecteddate+'\')" nextId="' + nextQuesId + '" questionid=""> Save </button>'+
                    '</div>'
                    '';*/
        return HTML;
    } catch(e){
        ShowExceptionMessage("getCheckBoxTypeTemplate", e);
    }
}
/**
 * Description:Get Radio Type Template
 */
function getRadioTypeTemplate(questiondata,nextQuesId,prevQuesId, parentid,selecteddate){
    logStatus("Calling getRadioTypeTemplate", LOG_FUNCTIONS);
    logStatus(nextQuesId, LOG_DEBUG);
    logStatus(prevQuesId, LOG_DEBUG);
    try {
        var nparentid = (parentid)?parentid:'';
        //var IsQuestionStatic	=	IsStaticQuestion(questiondata.id);
        var quesoptions=	[];
        if(questiondata._optionscounts==1) {
            quesoptions.push(questiondata._options);
        } else if(questiondata._optionscounts>1){
            quesoptions	=	questiondata._options;
        }
    //	var clickable	=	true;
        var answer		=	0;
        if(questiondata._answercount>0){
            var answers	=	(questiondata._answercount==1 ) ? [questiondata._answers]:questiondata._answers;
            var answerdata	=	GetAnswerByDate(answers, selecteddate);
            answer	=	answerdata.actmv_otherdata ? answerdata.actmv_otherdata:'';
        /*	if (! IsPhaseQuestionComplete(PHASE_Check)){
                clickable = false;
            }*/
        }
        var ans	=	getAnswersLocalByMV(questiondata.monitorvariable_id);
        if(ans){
            answer	=	ans;
        }
        var questtransobj = getQuestionTransObjForGroupIdAndQuestId(questiondata.r_quesgroup_id, questiondata.id);
        var questionstring	=	questtransobj.question.split(MARKER_QUERY_QUSBREAK);
        var question_notes	=	getQuestionNotesByQuestionString(questionstring);
        question_notes	=	(question_notes!='') ? '<div class="questionnote-wrapper">' + question_notes + '</div>' :'';
		var ques_info = questtransobj.question_info;
		   var questionStyle ='';
		if(ques_info=='' || typeof ques_info == 'undefined')
		{   
	       questionStyle = 'style="display:none!important;"';
		}
        var HTML	=	'<div class="welcome_ctn_bx" >'+
                            //'<div class="welcome_ctn_header" >'+
                            //    '<h4 class="comm_h4" >' + questtransobj.question +'<span class="question_icon"  parentsel="'+nparentid+' .liselectoptions" onclick="openQuestinfoForm(this,\''+questiondata.r_quesgroup_id+'\', \''+questiondata.id+'\', \''+questiondata.type+'\', \''+questiondata.monitorvariable_id+'\')">?</span></h4>'+
                            //'</div>'+
							'<div class="welcome_ctn_header text-wrap" >' +
							'<div class="q-mark mark2" '+questionStyle+ '><img parentsel="' + nparentid + '" src="images/question-mark.png" onclick="openQuestinfoForm(this,\'' + questiondata.r_quesgroup_id + '\', \'' + questiondata.id + '\', \'' + questiondata.type + '\', \'' + questiondata.monitorvariable_id + '\')">  </div><div class="swiper-containers"><img src="images/loading.gif"/></div>'+
							//'<h4 class="comm_h4" >' + questionstring[0] + '</h4>' +
							'<h3 >' + questtransobj.question + '</h3>' +
						   // '<span class="question_icon"  parentsel="' + nparentid + '" onclick="openQuestinfoForm(this,\'' + questiondata.r_quesgroup_id + '\', \'' + questiondata.id + '\', \'' + questiondata.type + '\', \'' + questiondata.monitorvariable_id + '\')">?</span>' +
							'</div>' +
                            '<input type="hidden" id="parameter_value' + questiondata.monitorvariable_id + '" name="parameter_value' + questiondata.monitorvariable_id + '" value="' + questiondata.prameter_value + '" />'+
                            '<input type="hidden" id="parameter_goal' + questiondata.monitorvariable_id + '" name="parameter_goal' + questiondata.monitorvariable_id + '" value="' + questiondata.parameter_goal + '" />';

        if(quesoptions.length>0){
            //ED > questionans_list UL Option with fixed max-height List
            
			HTML+= '<div class="counter-container">';
			HTML+= '<div class="arrow-left" style="top:60px;"></div>';
			HTML+= ' <div class="radios">';
			HTML+=	'<ul class="questionselect_list questionans_list liselectoptions">';
            /*HTML+=  '<div class="clsoptionbox-skin">' +
                '<select id="optionval'+questiondata.monitorvariable_id+'" ' +
                'class="question_option optionval'+questiondata.monitorvariable_id+'">' +
                    '<option value="pick_no_selected">Please Select</option>';*/
                $.each(quesoptions,function(i,_eachques){
                var optlang	=	getOptionLangText(_eachques);
                if(optlang.options){
                    var activeclass	=	'';
                    var selected	=	'';
					var activeRadioClass = '';

                    if((questiondata._answercount<=0) && (!answer)){
                        activeclass=	'';
                        selected    =   '';
						activeRadioClass = '';
                    }else{
                        if (answer==_eachques.range_points){
                            activeclass='act_select';
                            selected    =   ' selected="selected"';
							activeRadioClass = 'checkbtn';
                        }

                    }
                    //HTML+=	'<li>
                    // <span class="radiobox">
                    // <input type="radio" name="quesmv' + questiondata.monitorvariable_id + '"
                    // id="quesmv' + questiondata.monitorvariable_id + '"
                    // value="'+ _eachques.range_points + ':' + _eachques.id + '" ' +
                    // ( (answer==_eachques.range_points) ? 'checked' : '' ) + '/></span>' + optlang.options +
                    // '</li>';
                    //HTML+=	'<li><input type="radio" name="quesmv' + questiondata.monitorvariable_id + '"
                    // id="quesmv' + questiondata.monitorvariable_id + '"
                    // value="'+ _eachques.range_points + ':' + _eachques.id + '" ' + checked + '/>
                    // </span>' + optlang.options + '</li>';
                   /* HTML+=	'<option ' +
                                'value="'+ _eachques.range_points + _SplitCharOptionValue + _eachques.id + '" ' +  selected + '>' +
                                optlang.options +
                            '</option>';*/
                    
					
					HTML+=	'<li class="privacy_bx_1 radioans ' + activeclass + '" onclick="">'+
								

								'<span  class="radiooptiontxt checkouter '+activeRadioClass+'">'+
								
								'</span>'+
                                '<input type="hidden" ' +
                                    'class="optionval'+questiondata.monitorvariable_id+'" ' +
                                    'value="'+ _eachques.range_points + ':' + _eachques.id + '"/>' +
									
                                '<label class="r-questions radiooptiontxt">'+optlang.options +'</label><div class="clear"></div>'+
                        '</li>';
						
						
					/*HTML+=  '<li class="' + activeclass + '" onclick="">'+
						'<div class="privacy_bx_1">'+

                            '<span  class="checkouter">'+
                                '<input type="checkbox"  class="" id="answer2" type="hidden" ' +
                                    'class="optionval'+questiondata.monitorvariable_id+'" ' +
                                    'value="'+ _eachques.range_points + ':' + _eachques.id + '">'+
                            '</span>'+
                            '<label for="answer2" class="r-questions">'+optlang.options +'</label>'+
                            '<div class="clear"></div>'+
						'</div></li>';*/
					
                }
            });
           // HTML+=  '</select></div>';
            HTML+=  '</ul>';
			HTML+= '</div>';
			HTML+='<div class="arrow-right" style="top:60px;"></div>';
			HTML+='</div>';
        }

        HTML+=		question_notes+
                    '</div>' ;/*+
                    '<div class="btn_center_box">'+
                        '<button class="white_btn" parentid="'+nparentid+'" ' + ( (!clickable) ? 'disabled="disabled"':'') + ' onclick="saveMonitorRadioAnswer(' + questiondata.monitorvariable_id + ',' + clickable + ', this,\''+selecteddate+'\')" nextId="' + nextQuesId + '" questionid=""> Save </button>'+
                    '</div>'
                    '';*/
        return HTML;
    } catch(e){
        ShowExceptionMessage("getRadioTypeTemplate", e);
    }
}
function getNextGroupQuestionId(prevgid){
    logStatus("Calling getNextGroupQuestionId", LOG_FUNCTIONS);
    try {
        var nxtqid	=	0;
        if(prevgid!=GROUP_GENERAL){
            var groupid	=	(parseInt(prevgid)+1);
            var groupobj = getLastGroupObject();
            var group	=	groupobj['group'+groupid];
            group	=	( (!group) || ($.isEmptyObject(group))) ? groupobj['group'+GROUP_GENERAL]:group;
            if( group && (!$.isEmptyObject(group)) ){
                if(group._phase._questionscount>0) {
                    var questions =	(group._phase._questionscount==1) ? [group._phase._questions]:group._phase._questions;
                    nxtqid	=	questions[0].id;
                } else {
                    nxtqid	=	getNextGroupQuestionId(groupid);
                }
            }
        }
        return nxtqid;
    } catch(e){
        ShowExceptionMessage("getNextGroupQuestionId", e);
    }
}
/**
 * Description:Save monitor number answer
 */
function saveMonitorNumberAnswer(monitorquestion,clickable, obj, selecteddate,handleOnSwipeSuccess){
    logStatus("Calling saveMonitorNumberAnswer", LOG_FUNCTIONS);
    logStatus(clickable, LOG_DEBUG);
    try {

        /**
         * @param    selectedOption.max_value This is select option max value
         */
        //var templatepath=	GetCurrentPath();
        var updDate	=	null;
        if((!selecteddate) || (selecteddate=='') || (selecteddate=='undefined')){
            updDate=getDateTimeByFormat('Y-m-d 00:00:00');
        } else {
            updDate	=	getDateTimeByFormat('Y-m-d 00:00:00',new Date(selecteddate));
        }
        //var nxtqId	=	$(obj).attr("nextId");
        var parentid=	$(obj).attr("parentid");
        //var $elem	=	$(parentid+" .ip_numberval" + monitorquestion);
        var $elem	=	$(obj).find(".ip_numberval" + monitorquestion);
        var deseaseParam=	$(obj).find("#parameter_value" + monitorquestion).val();
        var goalParam	=	$(obj).find("#parameter_goal" + monitorquestion).val();
        var value		=	$.trim($elem.html());
        var questionId  =   $(obj).attr('questionId');
        //var proceed	=	true;
        GetValidationConfirmOnlyIf( ( (value==0) && (!isSavedAnswer(monitorquestion,selecteddate) ) ) ,function(){
            var options	=	getQuestionOptionsQMonitorID(monitorquestion);
			var end = options.length-1;
			var start_point = options[0].min_value;
			var end_point = options[end].max_value;
            var selectedOption	=	getSelectedRangeOption(options,value);
            if ($.isEmptyObject(selectedOption)) {
            } else {
                if ((!deseaseParam) || (deseaseParam == '')) {
                    deseaseParam = 1;
                }
                if ((!goalParam) || (goalParam == '')) {
                    goalParam = 1;
                }
                var rangepoint = (selectedOption.range_points * deseaseParam);
                var credit = rangepoint * value;
                if (selectedOption.max_value == '') {
                    credit = selectedOption.range_points;
                }
                var optionId = selectedOption.id;
                UpdateLocalAnswers({
                        mvkey: monitorquestion,
                        value: credit,
                        optionId: optionId,
                        actualPoint: credit,
                        actualDeseaseParameter: deseaseParam,
                        actualOtherData: value,
                        reftype: TRAINING_REFTYPE_Question,
                        updDate: updDate,
                        question:questionId,
						question_type:'number',
						start_point:start_point,
						end_point:end_point
                    //}, function (response) {
                    }, function () {
                        /*updateAnswerForQuestion({mvkey:monitorquestion,value:credit,optionId:optionId,actualPoint:credit,actualDeseaseParameter:deseaseParam,actualOtherData:actualOtherData,reftype:TRAINING_REFTYPE_Question,updDate:updDate},function(response){
                                     UpdateQuestionariesObjectWithGeneralQuestions(response.data);*/
                                   // var cques			=	0;

                                    var selectedgrpid = getlastActiveGroupId();
                                    var cphase = getCurrentFaseForGroupId(selectedgrpid);
                                    /*MK Check once current Fase - "Check" & Completed */
                                    if(cphase!= PHASE_Check){//MK Need to make a new way for GroupId base percentage calculation
                                        LoadPercentageCompleteForPhaseAndGroup(selectedgrpid,selecteddate);
                                    } else {
                                        LoadPercentageCompleteForPhaseAndGroup();
                                    }
                                    if(cphase==PHASE_Check && IsPhaseQuestionComplete(PHASE_Check)){
                                        _IsLastQuestionAnswered = false;
                                        //saveAllAnswersInServer(function(){
                                        CheckTakeTestOptionAndMove('dashboard');
                                        return;
                                        //});
                                    } else {
                                        /*if(nxtqId!=0) {
                                         goToTemplateQuestion(nxtqId,parentid, false, updDate);
                                    } else{
                                        goToTemplateQuestion(nxtqId,parentid, true, updDate);
                                        //_movePageTo(templatepath,{tm:timestamp()});
                                    }*/
                                }
                                if(handleOnSwipeSuccess){
                                handleOnSwipeSuccess(
                                    true);
                                }
                            }
                                );
                        }
        },function(){
            if(handleOnSwipeSuccess){
                handleOnSwipeSuccess(false);
            }
        },GetLanguageText(MSG_DO_YOU_WANT_SAVE_ANYWAY))
    } catch(e){
        ShowExceptionMessage("saveMonitorNumberAnswer", e);
    }
}
function saveMonitorSmilyAnswer(monitorquestion,clickable, obj,selecteddate,handleOnSwipeSuccess){
    logStatus("Calling saveMonitorSmilyAnswer", LOG_FUNCTIONS);
    try {
        var templatepath=	GetCurrentPath();
        //----------
        var group_object= getLastGroupObject();
        var isCheckPhaseActive  =   (!IsAnyGroupCheckFaseComplete(group_object));
        
        var questgroup = (!isCheckPhaseActive) ? getlastActiveGroupId() : '';

        //----------

        if(!clickable){
            _movePageTo(templatepath,{tm:timestamp()});
            return;
        }
        var updDate='';
        if((!selecteddate) || (selecteddate=='') || (selecteddate=='undefined')){
            updDate=getDateTimeByFormat('Y-m-d 00:00:00');
        } else {
            updDate	=	getDateTimeByFormat('Y-m-d 00:00:00',new Date(selecteddate));
        }
        var $elem	=	$(obj).find('.imgcls_clk.checked');
        if((typeof($elem.html())!='undefined')) {
            var deseaseParam=	$("#parameter_value" + monitorquestion).val();
            //var goalParam	=	$("#parameter_goal" + monitorquestion).val();
            var value	=	$elem.attr("custvalue");
			var value = value.replace(",", ".");
            var options	=	getQuestionOptionsQMonitorID(monitorquestion);
            var ques_type = getQuestionType(monitorquestion);
                       
            var start_point = options[0].range_points;
            var end_point = options[1].range_points;
            
            //logStatus(options, LOG_DEBUG);
            //var selectedOption	=	getSelectedRangeSmilyOption(options,value);
            if( (!deseaseParam) || (deseaseParam=='') ) {
                deseaseParam=1;
            }
            var credit	=	(value * deseaseParam);
            /*if (selectedOption.max_value == ''){
                credit	=	selectedOption.range_points;
            }*/
            var optionId=	$elem.attr("option_id");
            var actualOtherData	=	value;
            //var nxtqId		=	$(obj).attr("nextId");
            var parentid	=	$(obj).attr("parentid");
            var questionId  =   $(obj).attr('questionId');

            saveLastCoachSelectAnswerFromDiary(questionId,actualOtherData);
            AddProcessingLoader(obj);
            //UpdateLocalAnswers({mvkey:monitorquestion,value:credit,optionId:optionId,actualPoint:credit,actualDeseaseParameter:deseaseParam,actualOtherData:actualOtherData,reftype:TRAINING_REFTYPE_Question,updDate:updDate},function(response){
            UpdateLocalAnswers({
                mvkey:monitorquestion,
                value:credit,
                optionId:optionId,
                actualPoint:credit,
                actualDeseaseParameter:deseaseParam,
                actualOtherData:actualOtherData,
                reftype:TRAINING_REFTYPE_Question,
                updDate:updDate,
                question:questionId,
                questgroup:questgroup,
                ques_type:ques_type,
                start_point:start_point,
                end_point:end_point
            },function(){
            //updateAnswerForQuestion({mvkey:monitorquestion,value:credit,optionId:optionId,actualPoint:credit,actualDeseaseParameter:deseaseParam,actualOtherData:actualOtherData,reftype:TRAINING_REFTYPE_Question,updDate:updDate},function(response){
                //UpdateQuestionariesObjectWithGeneralQuestions(response.data);
                //var cques			=	0;
                RemoveProcessingLoader(obj);
                var selectedgrpid = getlastActiveGroupId();
                var cphase = getCurrentFaseForGroupId(selectedgrpid);
                /*MK Check once current Fase - "Check" & Completed */
                //if(parentid!=''){//MK Need to make a new way for GroupId base percentage calculation

                if(cphase!=PHASE_Check){
                    LoadPercentageCompleteForPhaseAndGroup(selectedgrpid,selecteddate);
                } else {
                    LoadPercentageCompleteForPhaseAndGroup();
                }
                if(cphase==PHASE_Check && IsPhaseQuestionComplete(PHASE_Check)){
                    _IsLastQuestionAnswered = false;
                    //saveAllAnswersInServer(function(){
                        CheckTakeTestOptionAndMove('dashboard');
                        return;
                    //});
                } else {
                    /*if(nxtqId!=0) {
                        goToTemplateQuestion(nxtqId,parentid, false, updDate);
                    } else{
                        goToTemplateQuestion(nxtqId,parentid, true, updDate);
                    }*/
                }
                if(handleOnSwipeSuccess){
                    handleOnSwipeSuccess(true);
                }
            });
        } else {
            ShowToastMessage(GetLanguageText(MSG_CHOOSE_ATLEAST_ANY_ONE_OPTIONS));
            if(handleOnSwipeSuccess){
                handleOnSwipeSuccess(false);
            }
        }
    } catch(e){
        ShowExceptionMessage("saveMonitorSmilyAnswer", e);
    }
}
function saveMonitorRadioAnswer(monitorquestion,clickable, obj, selecteddate,handleOnSwipeSuccess){
    logStatus("Calling saveMonitorRadioAnswer", LOG_FUNCTIONS);
    try {
        
        var templatepath=	GetCurrentPath();
        if(!clickable){
            _movePageTo(templatepath,{tm:timestamp()});
            return;
        }
        var updDate = '';
        if((!selecteddate) || (selecteddate=='') || (selecteddate=='undefined')){
            updDate=getDateTimeByFormat('Y-m-d 00:00:00');
        } else {
            updDate	=	getDateTimeByFormat('Y-m-d 00:00:00',new Date(selecteddate));
        }
    //	var $rawelem=	$("#quesmv" + monitorquestion);
        var $elem	=	$(obj).find(".act_select .optionval" + monitorquestion);
        var value	=	$elem.val();
        if(!value)  {
            //ShowAlertMessage(GetLanguageText(MSG_CHOOSE_ATLEAST_ANY_ONE_OPTIONS));
            ShowToastMessage(GetLanguageText(MSG_CHOOSE_ATLEAST_ANY_ONE_OPTIONS));
            if(handleOnSwipeSuccess){
                handleOnSwipeSuccess(false);
            }
        } else {
            var splitdata	=	(value!='') ? value.split(':'):{};
           // var splitdata	=	(value!='') ? value.split(_SplitCharOptionValue):{};
            var _value		=	(splitdata[0]) ? splitdata[0]:0;
            var _optionid	=	(splitdata[1]) ? splitdata[1]:0;
            //var splitdata	=	(value!='') ? value.split(':'):{};
            var deseaseParam=	$("#parameter_value" + monitorquestion).val();
            //var goalParam	=	$("#parameter_goal" + monitorquestion).val();
            AddProcessingLoader(obj);
            //var nxtqId	=	$(obj).attr("nextId");
            var parentid	=	$(obj).attr("parentid");
            var questionId  =   $(obj).attr('questionId');
            //UpdateLocalAnswers({mvkey:monitorquestion,value:_value,optionId:_optionid,actualPoint:_value,actualDeseaseParameter:deseaseParam,actualOtherData:_value,reftype:TRAINING_REFTYPE_Question,updDate:updDate},function(response){
            UpdateLocalAnswers({
                mvkey:monitorquestion,
                value:_value,
                optionId:_optionid,
                actualPoint:_value,
                actualDeseaseParameter:deseaseParam,
                actualOtherData:_value,
                reftype:TRAINING_REFTYPE_Question,
                updDate:updDate,
                question:questionId
            },function(){
            //updateAnswerForQuestion({mvkey:monitorquestion,value:_value,optionId:_optionid,actualPoint:_value,actualDeseaseParameter:deseaseParam,actualOtherData:_value,reftype:TRAINING_REFTYPE_Question,updDate:updDate},function(response){
                //UpdateQuestionariesObjectWithGeneralQuestions(response.data);
                RemoveProcessingLoader(obj);
                /*MK Check once current Fase - "Check" & Completed */
                var selectedgrpid = getlastActiveGroupId();
                var cphase = getCurrentFaseForGroupId(selectedgrpid);
                /*MK Check once current Fase - "Check" & Completed */
                //if(parentid!=''){//MK Need to make a new way for GroupId base percentage calculation
                if(cphase!=PHASE_Check){
                    LoadPercentageCompleteForPhaseAndGroup(selectedgrpid,selecteddate);
                } else {
                    LoadPercentageCompleteForPhaseAndGroup();
                }
                if(cphase==PHASE_Check && IsPhaseQuestionComplete(PHASE_Check)){
                    _IsLastQuestionAnswered = false;
                    //saveAllAnswersInServer(function(){
                        CheckTakeTestOptionAndMove('dashboard');
                        return;
                    //});
                } else {
                    /*if(nxtqId!=0) {
                        goToTemplateQuestion(nxtqId,parentid, false, updDate);
                    } else{
                        goToTemplateQuestion(nxtqId,parentid, true, updDate);
                    }*/
                }
                if(handleOnSwipeSuccess){
                    handleOnSwipeSuccess(true);
                }
            });
        }
    } catch(e){
        ShowExceptionMessage("saveMonitorRadioAnswer", e);
    }
}

function saveMonitorCheckboxAnswer(monitorquestion,clickable, obj, selecteddate,handleOnSwipeSuccess){
    logStatus("Calling saveMonitorCheckboxAnswer", LOG_FUNCTIONS);
    try {
        
        var templatepath=	GetCurrentPath();
        if(!clickable){
            _movePageTo(templatepath,{tm:timestamp()});
            return;
        }
        var updDate = '';
        if((!selecteddate) || (selecteddate=='') || (selecteddate=='undefined')){
            updDate=getDateTimeByFormat('Y-m-d 00:00:00');
        } else {
            updDate	=	getDateTimeByFormat('Y-m-d 00:00:00',new Date(selecteddate));
        }
        //var $rawelem=	$("#quesmv" + monitorquestion);
        var $elem	=	$(".act_select .optionval"+monitorquestion);
        //var value	=	$elem.val();
        if($elem.length>0)  {
            var value	=	0;
            var option_selected	=	0;
            var actualotherdata	=	'';
            $elem.each(function(i,elem){
                var eachvalue	=	$(elem).val();
                var splitdata	=	(eachvalue!='') ? eachvalue.split(':'):{};
                var splitval	=	(splitdata[0]) ? splitdata[0]:0;
                value+=+splitval;
                actualotherdata+='_'+eachvalue;
                if(option_selected==0){
                    option_selected	=	(splitdata[1]) ? splitdata[1]:0;
                }
            });
            var deseaseParam	=	$("#parameter_value" + monitorquestion).val();
            //var goalParam	=	$("#parameter_goal" + monitorquestion).val();
            AddProcessingLoader(obj);
            //var nxtqId	=	$(obj).attr("nextId");
            var parentid	=	$(obj).attr("parentid");
            var questionId  =   $(obj).attr('questionId');
            //UpdateLocalAnswers({mvkey:monitorquestion,value:value,optionId:option_selected,actualPoint:value,actualDeseaseParameter:deseaseParam,actualOtherData:actualotherdata,reftype:TRAINING_REFTYPE_Question,updDate:updDate},function(response){
            UpdateLocalAnswers({
                mvkey:monitorquestion,
                value:value,
                optionId:option_selected,
                actualPoint:value,
                actualDeseaseParameter:deseaseParam,
                actualOtherData:actualotherdata,
                reftype:TRAINING_REFTYPE_Question,
                updDate:updDate,
                question:questionId

            },function(){
            //updateAnswerForQuestion({mvkey:monitorquestion,value:value,optionId:option_selected,actualPoint:value,actualDeseaseParameter:deseaseParam,actualOtherData:actualotherdata,reftype:TRAINING_REFTYPE_Question,updDate:updDate},function(response){
                //UpdateQuestionariesObjectWithGeneralQuestions(response.data);
                RemoveProcessingLoader(obj);
                /*MK Check once current Fase - "Check" & Completed */
                var selectedgrpid = getlastActiveGroupId();
                var cphase = getCurrentFaseForGroupId(selectedgrpid);
                /*MK Check once current Fase - "Check" & Completed */
                //if(parentid!=''){//MK Need to make a new way for GroupId base percentage calculation
                if(cphase!=PHASE_Check){
                    LoadPercentageCompleteForPhaseAndGroup(selectedgrpid,selecteddate);
                } else {
                    LoadPercentageCompleteForPhaseAndGroup();
                }
                if(cphase==PHASE_Check && IsPhaseQuestionComplete(PHASE_Check)){
                    _IsLastQuestionAnswered = false;
                    //saveAllAnswersInServer(function(){
                        CheckTakeTestOptionAndMove('dashboard');
                    //})
                    return;
                } else {
                    /*if(nxtqId!=0) {
                        goToTemplateQuestion(nxtqId,parentid, false, updDate);
                    } else{
                        goToTemplateQuestion(nxtqId,parentid, true, updDate);
                    }*/
                }
                if(handleOnSwipeSuccess){
                    handleOnSwipeSuccess(true);
                }
            });
        } else {
            ShowToastMessage(GetLanguageText(MSG_CHOOSE_ATLEAST_ANY_ONE_OPTIONS));
            if(handleOnSwipeSuccess){
                handleOnSwipeSuccess(false);
            }
        }
    } catch(e){
        ShowExceptionMessage("saveMonitorCheckboxAnswer", e);
    }
}
function saveMonitorActivityAnswer(obj,date,day){
    logStatus("Calling saveMonitorActivityAnswer", LOG_FUNCTIONS);
    try {

        var updDate		=	date.split(" ")[0];
        var appendstr	=	day ? day:'';
        var exactval 	=	+$("#exipactivityval"+appendstr+"").val();
        var activityval =	+$("#ipactivityval"+appendstr+"").val();
        var total_seconds =	+$("#total_seconds"+appendstr+"").val();
        var credit		=	$("#credit"+appendstr+"").val();
        var credit_lmt	=	$("#credit_limit"+appendstr+"").val();
        var mvkey =	$("#mvkey"+appendstr+"").val();
        
        //var earncredit		=	$("#lastearned_credit"+appendstr+"").val();
        //var weeklycredits	=	$("#week_credits"+appendstr+"").val();
        var user	=	getCurrentUser();
        if(user.stepgame_type==STEP_GAME_BYMINUTES){
            activityval	=	parseInt((credit_lmt/total_seconds) * (activityval*60));
        }
        var actvalue	=	(activityval + exactval);
        var value =	(actvalue / credit_lmt) * credit;
        var actualval = value;
        if(value>credit){
            value	=	credit;
        }
        var optionId	=	$("#activityId"+appendstr+"").val();
        var actualOtherData	=	parseInt(actvalue);//activityval;
        var ActiveFaseId = getCurrentFaseForGroupId(GROUP_MOVESMART);
        var questgroup = getlastActiveGroupId();
        AddProcessingLoader(obj);
        //var group_object= getLastGroupObject();
        //var isCheckPhaseActive  =   (!IsAnyGroupCheckFaseComplete(group_object));

        
        //if(!(ActiveFaseId == PHASE_Check))
            //{   
               /* UpdateLocalAnswers({
                mvkey:mvkey,
                value:value,
                optionId:optionId,
                actualPoint:actualval,
                actualDeseaseParameter:"",
                actualOtherData:actualOtherData,
                reftype:TRAINING_REFTYPE_Activity,
                updDate:updDate,
                question:"",
                questgroup:questgroup,
                start_point:"",
                end_point:""
            });*/
            
        //}
        
        updateAnswerForQuestion({
                mvkey:mvkey,
                value:value,
                optionId:optionId,
                actualPoint:actualval,
                actualOtherData:actualOtherData,
                updDate:updDate,
                reftype:TRAINING_REFTYPE_Activity},function(response){
            UpdateQuestionariesObjectWithGeneralQuestions(response.data);
            RemoveProcessingLoader(obj);
            UpdateLocalAnswers({
                mvkey:mvkey,
                value:value,
                optionId:optionId,
                actualPoint:actualval,
                actualDeseaseParameter:"",
                actualOtherData:actualOtherData,
                reftype:TRAINING_REFTYPE_Activity,
                updDate:updDate,
                question:"",
                questgroup:questgroup,
                ques_type:"",
                start_point:credit,
                end_point:0
            });
            getNotification();
            var template	=	GetCurrentPath();
            //_movePageTo(template,{tm:timestamp()});
            _movePageTo('dashboard',{tm:timestamp()});
        });
    } catch(e){
        ShowExceptionMessage("saveMonitorActivityAnswer", e);
    }
}
function UpdateLocalAnswers(data,onUpdateLocal){
    logStatus("Calling UpdateLocalAnswers", LOG_FUNCTIONS);
    try {
        var savedanswers=	getSavedQuestionsInLocal();						
        var na	=	[];//New answers build to be saved
        if(!$.isEmptyObject(savedanswers)){
            na	=	savedanswers;
        }
        var ni	=	true;//need to insert
        if(na.length>0) {
            $.each(na,function(i,a){
                if(a.mvkey==data.mvkey){
                    na[i]=	data;
                    ni=false;//Updated data
                }
            })
        }
        if(ni){
            na.push(data);
        }
        updateSavedQuestionsInLocal(na);
        if(onUpdateLocal) {
            onUpdateLocal({});
        }
    } catch(e){
        ShowExceptionMessage("UpdateLocalAnswers", e);
    }
}
/*
* Description:Get option language text
*/
function getOptionLangText(option){
    logStatus("Calling getOptionLangText", LOG_FUNCTIONS);
    try {
        var	language	=	{};
        /**
        * @param   option._optlangcount  This is option language count
        * @param   option._optlang  This is option language
        */
        if(option._optlangcount>0){
            language	=	option._optlang['lang_' + USER_LANG];
        }
        return language;
    } catch(e){
        ShowExceptionMessage("getOptionLangText", e);
    }
}
/**
 * Description:Copy prevoius day
 */
/*
function copyPreviousDay(){
    logStatus("Calling copyPreviousDay", LOG_FUNCTIONS);
    try {
        var selectedGroupID	=	getlastActiveGroupId();
        var phaseID			=	getCurrentFaseForGroupId(selectedGroupID);
        var selectedDate	=	$("#dteatfresh").val();
        var qRealIDs	=	[];
        var groupobj	=	getLastGroupObject();
        var groupdata	=	groupobj['group' + selectedGroupID];
        var phase		=	groupdata._phase;
        if(phase._questionscount>0){
            var questions	=	(phase._questionscount==1) ? [phase._questions]:phase._questions;
            $.each(questions,function(i,question){
                qRealIDs.push(question.monitorvariable_id);
            });
        }
        updateAnswersFromPreviousDay({phaseID:phaseID,GroupID:selectedGroupID,mvkeys:JSON.stringify(qRealIDs),answeredDate:selectedDate},function(response){
            if ((response) && (response.data)){
                /**
                 * @param   response.data.grouplist  This is group list
                 * /
                if (response.data.grouplist){
                    UpdateQuestionariesObject(response.data.grouplist);
                }
            }
            var template	=	getPathHashData();
            _movePageTo(template.path,{tm:timestamp()});
        });
    } catch(e){
        ShowExceptionMessage("copyPreviousDay", e);
    }
}
*/
function getQuestionOptionsQMonitorID(qmonitor_id){
    logStatus("Calling getQuestionOptionsQMonitorID", LOG_FUNCTIONS);
    try {
        var groupobj= getLastGroupObject();
        
        var options	=	{};
        $.each(groupobj,function(groupidx,egroup){
            var phase	=	egroup._phase;
            if(phase._questionscount>0){
                var questions	=	(phase._questionscount==1) ? [phase._questions]:phase._questions;
                $.each(questions,function(qidx,ques){
                    if(ques.monitorvariable_id==qmonitor_id){
                        if(ques._optionscounts>0) {
                            options	=	(ques._optionscounts==1) ? [ques._options]:ques._options;
                            return false;
                        }
                    }
                });
                if(!$.isEmptyObject(options)){
                    return false;
                }
            }
        });
        return options;
    } catch(e){
        ShowExceptionMessage("getQuestionOptionsQMonitorID", e);
    }
}

function getSelectedRangeOption(options,value){
    logStatus("Calling getSelectedRangeOption", LOG_FUNCTIONS);
    try {
        var selectedoption	=	{};
        var fvalue 			=	+value;
        fvalue	=	isNaN(fvalue) ? 0:fvalue;
        /**
         * @param  eachrow.max_value This is option max value
         * @param  eachrow.min_value This is option min value
         */
        $.each(options,function(i,eachrow){
            var maxval	=	+eachrow.max_value;
            var minval	=	+eachrow.min_value;
            if(((fvalue<=maxval) || (maxval=='') || (maxval==0) || (isNaN(maxval))) && (fvalue>=minval)){
                selectedoption	=	eachrow;
                return false;
            }
        });
        return selectedoption;
    } catch (e) {
        ShowExceptionMessage("getSelectedRangeOption", e);
    }
}
/**
 *Description:Get select range smily option
 */
/*
function getSelectedRangeSmilyOption(options,value){
    logStatus("Calling getSelectedRangeOption", LOG_FUNCTIONS);
    try {
        var selectedoption	=	{};
        /**
         * @param eachrow.ui_order This is option ui_order
         * /
        $.each(options,function(i,eachrow){
            if(eachrow.ui_order==value){
                selectedoption	=	eachrow;
                return false;
            }
        });
        return selectedoption;
    } catch (e) {
        ShowExceptionMessage("getSelectedRangeOption", e);
    }
}
*/
/*
function GetUansweredNextQuestionIndex(currdate){
    logStatus("Calling GetUansweredNextQuestionIndex", LOG_FUNCTIONS);
    try {
        var index	=	-1;
        var iscontinueloop	=	true;
        var groupobj=	getLastGroupObject();
        $.each(groupobj,function(groupidx,egroup){
            var phase	=	egroup._phase;
            var questions	=	(phase._questionscount>1) ? phase._questions:[phase._questions];
            $.each(questions,function(qidx,question){
                var editable	= true;
                if(question._answercount>0){
                    var answers	=	(question._answercount>1) ? question._answers:[question._answers];
                    editable =	IsAnswerEditable(answers, currdate);
                }
                if(editable){
                    index	=	$("#questiontemp_"+question.id).attr('data-slide-index');
                    iscontinueloop=false;
                }
                return iscontinueloop;
            });
            return iscontinueloop;
        });
        return index;
    } catch(e){
        ShowExceptionMessage("GetUansweredNextQuestionIndex", e);
    }
}
*/
/*
function goToTemplateQuestion(questionid,parentid,islastindex, updDate){
    logStatus("Calling goToTemplateQuestion", LOG_FUNCTIONS);
    try {
        if (islastindex){
            _IsLastQuestionAnswered = true;
        }
        // console.log("_IsLastQuestionAnswered"+_IsLastQuestionAnswered);
        if (_IsLastQuestionAnswered){
            var date = '';

            var selectedgrpid = getlastActiveGroupId();
            var cphase = getCurrentFaseForGroupId(selectedgrpid);
            if(cphase!=PHASE_Check){
                date = updDate;
            }

            var pendingqsindex	=	GetUansweredNextQuestionIndex(date);
            console.log("pendingqsindex:"+pendingqsindex);
            if ((pendingqsindex == -1) || (!pendingqsindex)){
                _IsLastQuestionAnswered = false;
                //saveAllAnswersInServer(function(){
                    MoveActiveUserPage();
                //});
            }else{
                if(_QuestionSliderObj[_QuestionSliderObjIndex]){
                    _QuestionSliderObj[_QuestionSliderObjIndex].slideTo(pendingqsindex);
                }
            }
        }else {
            if(_QuestionSliderObj[_QuestionSliderObjIndex]){
                _QuestionSliderObj[_QuestionSliderObjIndex].slideNext();
            }
        }
    } catch(e){
        ShowExceptionMessage("goToTemplateQuestion", e);
    }
}
*/
function InitPageEvents(){
    logStatus("Calling InitPageEvents", LOG_FUNCTIONS);
    try {
        //noinspection JSUnresolvedFunction
		if(device.platform=="Android" || device.platform=="browser"){
          $(".incrmentstep").live("click",function(event){
            logStatus("Calling Event.incrmentstep.Click", LOG_FUNCTIONS);
            try {
                event.stopImmediatePropagation();
                event.preventDefault();
                if ($(this).is(":disabled")){
                    return;
                }
                var $self	=	$(this);
                var end	=	+$self.attr("end");
                var step	=	+$self.attr("step");
                //var selectorid	=	($self.attr("associd")) ? $self.attr("associd"):null;
                //decInput(start, step, selectorid);
                // incInput(end, step, selectorid)
                incInput(end, step, $self.parent().find('.ques_count'));
            } catch (e) {
                ShowExceptionMessage("Event.decrea.Click", e);
            }
        });
        //noinspection JSUnresolvedFunction
         $(".decrmtstep").live(" click",function(event){
            logStatus("Calling Event.decrmtstep.Click", LOG_FUNCTIONS);
            try {
                event.stopImmediatePropagation();
                event.preventDefault();
                if ($(this).is(":disabled")){
                    return;
                }
                var $self	=	$(this);
                var start = +$self.attr("start");
                var step = +$self.attr("step");
                //var selectorid	=	($self.attr("associd")) ? +$self.attr("associd"):null;
                //incInput(end, step, selectorid);
                // decInput(start, step, selectorid);
                decInput(start, step, $self.parent().find('.ques_count'));
            } catch (e) {
                ShowExceptionMessage("Event.decrea.Click", e);
            }
        });
        //noinspection JSUnresolvedFunction
        $(".liselectoptions>li>.radiooptiontxt").live("click",function(){
			//alert('===');
            var $inherit=	$(this).parent();
            var $this	=	$(this);
            $inherit.siblings().removeClass("act_select");
			var sibArr = $inherit.siblings();
			$.each(sibArr,function(i,lirow){
				$(lirow).find('.checkouter').removeClass("checkbtn");
			});
            $inherit.addClass("act_select");
			$inherit.find('.checkouter').addClass("checkbtn");
        });
        //$(".licheckoptions>li").die("click").die("touchstart").live("touchstart click",function(){
        //noinspection JSUnresolvedFunction
         $(".licheckoptions>li .chkboxopttxt,.licheckoptions>li .markbtn_check").live("click",function(){//As touchstart & click detects on the devices twise
            //var $inherit=	$(this).siblings();
			
            var $this	=	$(this).parent();
            if($this.hasClass("act_select")){
                $this.removeClass("act_select");
            } else {
                $this.addClass("act_select");
            }
        });
		
		}
		//For IOS
		if(device.platform=="iOS"){
           $(".incrmentstep").die("click").die("touchstart").live("touchstart click",function(event){
            logStatus("Calling Event.incrmentstep.Click", LOG_FUNCTIONS);
            try {
                event.stopImmediatePropagation();
                event.preventDefault();
                if ($(this).is(":disabled")){
                    return;
                }
                var $self	=	$(this);
                var end	=	+$self.attr("end");
                var step	=	+$self.attr("step");
                //var selectorid	=	($self.attr("associd")) ? $self.attr("associd"):null;
                //decInput(start, step, selectorid);
                // incInput(end, step, selectorid)
                incInput(end, step, $self.parent().find('.ques_count'));
            } catch (e) {
                ShowExceptionMessage("Event.decrea.Click", e);
            }
        });
        //noinspection JSUnresolvedFunction
         $(".decrmtstep").die("click").die("touchstart").live("touchstart click",function(event){
            logStatus("Calling Event.decrmtstep.Click", LOG_FUNCTIONS);
            try {
                event.stopImmediatePropagation();
                event.preventDefault();
                if ($(this).is(":disabled")){
                    return;
                }
                var $self	=	$(this);
                var start = +$self.attr("start");
                var step = +$self.attr("step");
                //var selectorid	=	($self.attr("associd")) ? +$self.attr("associd"):null;
                //incInput(end, step, selectorid);
                // decInput(start, step, selectorid);
                decInput(start, step, $self.parent().find('.ques_count'));
            } catch (e) {
                ShowExceptionMessage("Event.decrea.Click", e);
            }
        });
        //noinspection JSUnresolvedFunction
        $(".liselectoptions>li>.radiooptiontxt").die("click").die("touchstart").live("touchstart click",function(){
			//alert('===');
            var $inherit=	$(this).parent();
            var $this	=	$(this);
            $inherit.siblings().removeClass("act_select");
			var sibArr = $inherit.siblings();
			$.each(sibArr,function(i,lirow){
				$(lirow).find('.checkouter').removeClass("checkbtn");
			});
            $inherit.addClass("act_select");
			$inherit.find('.checkouter').addClass("checkbtn");
        });
        //$(".licheckoptions>li").die("click").die("touchstart").live("touchstart click",function(){
        //noinspection JSUnresolvedFunction
         $(".licheckoptions>li .chkboxopttxt,.licheckoptions>li .markbtn_check").die("click").live("click",function(){//As touchstart & click detects on the devices twise
            //var $inherit=	$(this).siblings();
			
            var $this	=	$(this).parent();
            if($this.hasClass("act_select")){
                $this.removeClass("act_select");
            } else {
                $this.addClass("act_select");
            }
        });
		
		}
		
    } catch(e){
        ShowExceptionMessage("InitPageEvents", e);
    }
}
function applyProgressPercentage(selector,percentage){
    logStatus("Calling applyProgressPercentage", LOG_FUNCTIONS);
    try {
        var $selector	=	$(selector+" .perfill");
        if(percentage>100){
            percentage=100;
        }
        var updpercentage	=	parseFloat(percentage).toFixed(2);
        var $progress	=		$selector.find(".cpercentage");
        if(percentage>10 && percentage<85) { //Temp Fix For UI Will Show greater 5% lesser 95%
            if(typeof($progress.html())=="undefined"){
                $selector.html('<span class="cpercentage">'+parseInt(updpercentage)+'%</span>');
            } else {
                $progress.html(parseInt(updpercentage)+"%");
            }
        }
        /*SN added progress question answer bar-20160509*/
        else{
            $selector.html('');
        }
        $selector.css("width",updpercentage+"%");
    } catch(e){
        ShowExceptionMessage("applyProgressPercentage", e);
    }
}

/**
 * @return {string}
 */
function GetGenericTextContentForQuestions(pagecontentid){
    logStatus("Calling GetBuiltQuestionariesTemplate", LOG_FUNCTIONS);
    try {
        var TemplateHtml	=	'';
        var pagecontent= _PageContentDetails[USER_LANG];
        if(!$.isEmptyObject(pagecontent)){
            var epage	=	pagecontent[pagecontentid];
            if(!$.isEmptyObject(epage)) {
                if (epage.length > 0){
                    var pagecontentobj = epage[0];
                    var pagecontentdata = pagecontentobj.content;
                    if (pagecontentdata){
                        var contents	=	pagecontentdata.split(MARKER_QUERY_PAGEBREAK);
                        $.each(contents,function(i,eachcontent){
                            if(eachcontent!='') {
                                TemplateHtml+=	'<div class="clsstaticcontent swiper-slide clsstaticcnt" data-slide-index="'+incSlideIndex+'" id="questiontemp_static_' + i + '_' + pagecontentid + '"><div class="comm_banner_img_box"><div class="transparent-stripe"></div><img src="images/banner/move11.jpg" alt="home_img1"></div><div class="comm_contentbox">' +eachcontent+'</div></div>';
                                //TemplateHtml+=	'<div class="clsstaticcontent swiper-slide" data-slide-index="'+incSlideIndex+'" id="questiontemp_static_' + i + '_' + pagecontentid + '">' +eachcontent+'</div>';
                            }
                        });
                    }
                }
            }
        }
        return TemplateHtml;
    } catch(e){
        ShowExceptionMessage("GetBuiltQuestionariesTemplate", e);
    }
}
/**
 * @return {string}
 */
function GetBuiltQuestionariesTemplate(questions,groupid){
    logStatus("Calling GetBuiltQuestionariesTemplate", LOG_FUNCTIONS);
    try {
        var TemplateHtml	=	'';
        var mvkeyarr		=	{};
        var multing = 1;
        $.each(questions,function(quesidx,question){
            var nextQuesId	=	(questions[quesidx+1] && questions[quesidx+1].id) ? questions[quesidx+1].id:getNextGroupQuestionId(groupid);
            var prevQuesId	=	(questions[quesidx-1] && questions[quesidx-1].id) ? questions[quesidx-1].id:0;//getNextGroupQuestionId(egroup.group_id);
            var template	=	getQuestionTemplate(question,nextQuesId,prevQuesId, '');
            //var Editable	=	true;
            if(question._answercount>0){
                //var answers	=	(question._answercount==1) ? [question._answers]:question._answers;
            //	Editable	=	IsAnswerEditable(answers);
            }
            if(template){
                //monitorquestion,clickable, obj, selecteddate,parentid
                var img = getQuestionImage(groupid);
                /*
                * move,mind, eat
                * 1-20, 1-17, 1-6
                *  */
              // var qu_no = incSlideIndex+1;
               TemplateHtml+=	'<div class="swiper-slide" ' +
               'data-slide-index="'+incSlideIndex+'" ' +
               'id="questiontemp_' + question.id + '" ' +
               'questionId="' + question.id + '">' +
               '<div class="clsquestioncontent">'
               +'<div class="comm_banner_img_box">'
               +'<div class="transparent-stripe"></div>'
               +'<img src="'+img+'" alt="home_img1" />'
               //+'<h4>Gym Check</h4>'
               //+'<img class="banner_logo" src="images/movesmartlogo.png" alt="movesmartlogo" />'
               +'</div>'
               + addIPFormAttributes({
                                     monitorquestion:question.monitorvariable_id,
                                     nextId:nextQuesId,
                                     parentid:('#questiontemp_' + question.id),
                                     questiontype:question.type
                                     },question.id) + ''
               + template +
               '</div></div>';
               mvkeyarr[question.monitorvariable_id]	=	incSlideIndex;
               if( (!isSavedAnswer(question.monitorvariable_id)) && ( (typeof(defaultSlideIndex)=='undefined') || defaultSlideIndex==null) ){
               defaultSlideIndex	=	incSlideIndex;
               }
               
               
                /*if( (Editable) && ( (typeof(defaultSlideIndex)=='undefined') || defaultSlideIndex==null)) {
                    defaultSlideIndex	=	incSlideIndex;
                }/**/
                incSlideIndex++;

            }
            multing++;
        });

        return TemplateHtml;
    } catch(e){
        ShowExceptionMessage("GetBuiltQuestionariesTemplate", e);
    }
}
function getQuestionImage(groupid){
    var max = 0;
    var group ='';
    var min =1;
    var mygrouparr = [1,2,3];
    if(groupid==GROUP_GENERAL){
       groupid= mygrouparr[Math.floor(Math.random() * mygrouparr.length)];
    }
    if(groupid==GROUP_MOVESMART){
        max=20;
        group = 'move';
    }
    if(groupid==GROUP_EATFRESH){
        max=7;
        group = 'eat';
    }
    if(groupid==GROUP_MINDSWITCH){
        max=17;
        group = 'mind';
    } 
    
   var rand = (Math.floor(Math.random() * (max - min + 1)) + min);
   // var rand = Math.random() * (max - min) + min;
   // var imgurl = (groupid!=GROUP_GENERAL) ?'images/banner/'+group+rand+'.jpg':'images/banner/move11.jpg';
    var imgurl = 'images/banner/'+group+rand+'.jpg';
    return imgurl;
}
function TransQuestion(){
    logStatus("TransQuestion", LOG_FUNCTIONS);
    try {
        TranslateText(".transneededinfo", TEXTTRANS, LBL_NEEDED_TEST_INFO);
        TranslateText(".transcheck", TEXTTRANS, LBL_CHECK);
    } catch(e){
        ShowExceptionMessage("TransQuestion", e);
    }
}
function CheckTakeTestOptionAndMove(template){
    logStatus("Calling CheckTakeTestOptionAndMove", LOG_FUNCTIONS);
    try {
        if(GetAnswersByQuestionID(QUESTION_STATIC_take_test)==1){
            GoForTestAppointment();
            //return;
        } else {
            _movePageTo(template);
            //return;
        }
    } catch(e){
        ShowExceptionMessage("CheckTakeTestOptionAndMove", e);
    }
}
/**
 * @return {number}
 */
function GetAnswersByQuestionID(questionid){
    logStatus("Calling GetAnswersByQuestionID", LOG_FUNCTIONS);
    try {
        var selanswer   =   null;
        var iscontinue  =   true;
        var groupobj = getLastGroupObject();
        $.each(groupobj,function(gidx,group){
            //var phase	=	group._phase;
            if(group._phase._questionscount>0){
                var questions=	(group._phase._questionscount==1) ? [group._phase._questions]:group._phase._questions;
                $.each(questions,function(qidx,question){
                    if(questionid==question.id){
                        if(question._answercount>0){
                            var answer	=	(question._answercount==1) ? question._answers:question._answers[0];
                            if(answer.value){
                                selanswer	=	answer.value;
                                iscontinue=false;
                                return iscontinue;
                            }
                        }
                    }

                })
            }
            return iscontinue;
        });
        return selanswer;
    } catch(e){
        ShowExceptionMessage("GetAnswersByQuestionID", e);
    }
}

/*
function SaveQuestionFormInfo(qtype,qsid,mvid,parentsel){
    logStatus("Calling SaveQuestionFormInfo", LOG_FUNCTIONS);
    try {
        var selected	='';
        switch(parseInt(qtype)){
            case QUESTION_TYPE_NUMBER:
                selected = +$(parentsel + ' .triangle_question_bx #rangerinput'+qsid).html();
                break;
            case QUESTION_TYPE_OPTION:
                selected=$(parentsel + " .act_select").find('input').val();
                break;
            case QUESTION_TYPE_CHECKBOX:
                selected	=	'';
                var $elem	=	$(parentsel + " .act_select");
                $elem.each(function(i,elm){
                    selected+= '_' + $(elm).find('input').val();
                });
                break;
            case QUESTION_TYPE_YN:
                selected=$(parentsel + ' .imgcls_clk.checked').attr('option_id');
                break;
            case QUESTION_TYPE_SMILEY:
                selected=$(parentsel + ' .imgcls_clk.checked').attr('option_id');
                break;
            case QUESTION_TYPE_LIKDISLIK:
                selected=$(parentsel + ' .imgcls_clk.checked').attr('option_id');
                break;
        }
        UpdateQuestionBackInfo({qtype:qtype,selectedval:selected,qsid:qsid,mvid:mvid,sliderobjidx:_QuestionSliderObjIndex,parentsel:parentsel});
    } catch(e){
        ShowExceptionMessage("SaveQuestionFormInfo", e);
    }
}
*/
/**
 * Description:Update retained values on back
 */
function UpdateRetainedValuesOnBack(){
    /**
     * @param _QuestionBackInfo.sliderobjidx This is slider object index
     * @param _QuestionBackInfo.parentsel This is parent selector
     * @param _QuestionBackInfo.qtype This is question type
     * @param _QuestionBackInfo.qsid This is question id
     * @param _QuestionBackInfo.selectedval This is select value
     * @param _QuestionBackInfo.mvid This is monitor variable id
     */
    if ( (_QuestionBackInfo) && (!$.isEmptyObject(_QuestionBackInfo))){
        _QuestionSliderObjIndex	=	_QuestionBackInfo.sliderobjidx;
        var $clsdaytabs = $(".clsdaytabs");
        if(typeof($clsdaytabs.html())!='undefined'){
            $clsdaytabs.removeClass('scroll_tabs_active');
            $clsdaytabs.eq((_QuestionSliderObjIndex+1)).addClass('scroll_tabs_active');
        }
        var parentsel=	_QuestionBackInfo.parentsel;
        switch(parseInt(_QuestionBackInfo.qtype)){
            case QUESTION_TYPE_NUMBER:
                $(parentsel + ' .triangle_question_bx #rangerinput'+_QuestionBackInfo.qsid).html(+_QuestionBackInfo.selectedval);
                break;
            case QUESTION_TYPE_OPTION:
                var optionval = '.optionval' + _QuestionBackInfo.mvid;
                var $element=	$(parentsel + ' '+  optionval+'[value="' + _QuestionBackInfo.selectedval + '"]');
                //$element.parent('li').siblings().removeClass('act_select');
                //$element.parent('li').addClass('act_select');
                $element.parent('li').trigger("click");
                break;
            case QUESTION_TYPE_CHECKBOX:
                var clsoption	=	'.optionval'+_QuestionBackInfo.mvid;
                var optionsstr = _QuestionBackInfo.selectedval;
                var checkoptionval	=	[];
                if(optionsstr!=''){
                    checkoptionval	=	optionsstr.split("_");
                }
                $.each(checkoptionval,function(i,optval){
                    if(optval!='') {
                        var $element=	$(parentsel + ' '+  clsoption+'[value="' + optval + '"]');
                        $element.parent('li').addClass('act_select');
                    }
                });
                break;
            case QUESTION_TYPE_SMILEY:
                /*performs (or) condition to next case*/
            case QUESTION_TYPE_YN:
                /*performs (or) condition to next case*/
            case QUESTION_TYPE_LIKDISLIK:
                $(parentsel + ' .imgcls_clk').each(function(i,row){
                    $(row).removeClass("checked");
                    if($(row).attr('option_id')==_QuestionBackInfo.selectedval){
                        $(row).addClass('checked');
                        //$(row).trigger("click");
                    }
                });
                break;
        }
    }
}
function getQuestionNotesByQuestionString(questionstring){
    logStatus("Calling getQuestionNotesByQuestionString", LOG_FUNCTIONS);
    try {
        var question_notes	=	'';
        $.each(questionstring,function(i,questiontext){
            if(i>0) {
                question_notes+='<div class="questionnote">'+questiontext+'</div>';
            }
        });
        return question_notes;
    } catch(e){
        ShowExceptionMessage("getQuestionNotesByQuestionString", e);
    }
}
function TrackSaveLaseSlide(cSwiper){
    $("#question-forms").scrollTop(0);
    $(".activity_todaybox").scrollTop(0);
    pagetitelanimation();
    if(cSwiper.isEnd && (cSwiper.snapIndex==cSwiper.previousIndex) ) {
        CheckFaseSldierMove(cSwiper);
    }
}
function CheckFaseSldierMove(cSwiper){
    logStatus("Calling CheckFaseSldierMove", LOG_FUNCTIONS);
    try {
        $("#question-forms").scrollTop(0);
        $(".activity_todaybox").scrollTop(0);
        $("section").scrollTop(0);
        if(cSwiper.swipeDirection=="next") {
            cSwiper.lockSwipeToNext();
            //var swipe	=	true;
            //var $panel1	=	$(cSwiper.slides[cSwiper.snapIndex]);
            var $panel	=	$(cSwiper.slides[cSwiper.previousIndex]);

            if ($panel.hasClass("clsstaticcontent")){ //Just to skip if it just a static question. by Sankar
                cSwiper.unlockSwipeToNext();
                return;
            }

            var suffday		=	($panel.attr("dsuffix") && $panel.attr("dsuffix")!='' && $panel.attr("dsuffix")!=0) ? '_d_' + $panel.attr("dsuffix"):'';
            var attr	=	getIPFormAttributes($panel.attr("id").split("_")[1] + suffday);
			
            var selansdate	=	attr.hasOwnProperty('selansdate') ? attr.selansdate:null;
            switch(parseInt(attr.questiontype)){
                case QUESTION_TYPE_NUMBER:
                    saveMonitorNumberAnswer(attr.monitorquestion,true, $panel, selansdate,function(swipe){
                        validateMoveSwipe(swipe,cSwiper,selansdate);
                    });
                    break;
                case QUESTION_TYPE_OPTION:
                    saveMonitorRadioAnswer(attr.monitorquestion,true, $panel, selansdate,function(swipe){
                        validateMoveSwipe(swipe,cSwiper,selansdate);
                    });
                    break;
                case QUESTION_TYPE_CHECKBOX:
                    saveMonitorCheckboxAnswer(attr.monitorquestion,true, $panel, selansdate,function(swipe){
                        validateMoveSwipe(swipe,cSwiper,selansdate);
                    });
                    break;
                case QUESTION_TYPE_YN:
                    saveMonitorSmilyAnswer(attr.monitorquestion,true, $panel,selansdate,function(swipe){
                        validateMoveSwipe(swipe,cSwiper,selansdate);

                    });
                    break;
                case QUESTION_TYPE_SMILEY:
                    saveMonitorSmilyAnswer(attr.monitorquestion,true, $panel,selansdate,function(swipe){
                        validateMoveSwipe(swipe,cSwiper,selansdate);
                    });
                    break;
                case QUESTION_TYPE_LIKDISLIK:
                    saveMonitorSmilyAnswer(attr.monitorquestion,true, $panel,selansdate,function(swipe){
                        validateMoveSwipe(swipe,cSwiper,selansdate);
                    });
                    break;
            }
					//var id = $('.swiper-slide-active').attr('data-slide-index');
				//	$('.q_no').html(id);
			// SCROLL BOTTOM QUESTION
			var $questionContent = $(".clsquestioncontent");
			$questionContent.each(function(){			
			var thislist = $(this).find(".radioptlist li").length;
			var questionans_listHeight = $(this).find(".questionans_list").outerHeight();
			if(questionans_listHeight > (thislist*40)){
				$(this).find(".arrowtop_icon").hide();
					//alert("hide");
				}else{
					$(this).find(".arrowtop_icon").show();
				//	alert("show");
				}
			});
        }
		getslidenumber(cSwiper);	//change on 17-05-17
    } catch(e){
        ShowExceptionMessage("CheckFaseSldierMove", e);
    }
}
function validateMoveSwipe(doswipe,swiper,selansdate){
    swiper.unlockSwipeToNext();
    $("#question-forms").scrollTop(0);
    $(".activity_todaybox").scrollTop(0);
    pagetitelanimation();
    var checkDate	=	'';
    if(selansdate){
        checkDate	=	getDateTimeByFormat('Y-m-d 00:00:00',new Date(selansdate))
    }
    var group_object= getLastGroupObject();
    var isCheckPhaseActive  =   (!IsAnyGroupCheckFaseComplete(group_object));
    if(!doswipe){
        //swiper.slidePrev()
        swiper.slideTo(swiper.previousIndex);
    } else {
        //XX if(completedCheckPhaseQuestion(checkDate) && (swiper.isEnd)){
        var isEnd   =   isEndSlideSwiped(swiper);
        if(completedCheckPhaseQuestion(checkDate) && ( isCheckPhaseActive || isEnd)){
            ShowLoader();
            saveAllAnswersInServer(function(response){
                UpdateQuestionariesObjectWithGeneralQuestions(response.data);
				if(!isCheckPhaseActive)
				{  // openCoachListFromDiary();
					getNotification();
                }
                $(".menuspan").removeClass("act");
                $(".frt").addClass("act");
                HideLoader();
                
                MoveActiveUserPage();
                //CheckTakeTestOptionAndMove('dashboard');
                //return;
            });
        } else {
            if(isCheckPhaseActive) {
                if (GetLocalAnswerByQuestionID(QUESTION_STATIC_take_test) !== null) {
                    var slide_index = GetUnAnsweredQuestionSlideIndex(checkDate);
                    if (slide_index != 0) {
                        swiper.slideTo(slide_index);
                    }
                }
            }
        }
    }
}
function addIPFormAttributes(attributes,questionid){
    logStatus("Calling addIPFormAttributes", LOG_FUNCTIONS);
    try {
        var HTML	=	'<input type="hidden" id="form_attributes'+questionid+'"';
        $.each(attributes,function(key,value){
            HTML	+=	' '+ key +'="' + value +'"';
        });
        HTML+=' />';
        return HTML;
    } catch(e){
        ShowExceptionMessage("addIPFormAttributes", e);
    }
}
function getIPFormAttributes(questionid){
    logStatus("Calling getIPFormAttributes", LOG_FUNCTIONS);
    try {
        var $elem	=	$("#form_attributes" + questionid);
        var attributes=	$elem[0].attributes;
        var attr	=	{};
        $.each(attributes,function(i,row){
            attr[row.name]	=	row.nodeValue;
        });
        return attr;
    } catch(e){
        ShowExceptionMessage("getIPFormAttributes", e);
    }
}
function isSavedAnswer(mvkey,checkdate){
    logStatus("Calling isSavedAnswer", LOG_FUNCTIONS);
    try {
        var issavedalready	=	false;
        var sa	=	getSavedQuestionsInLocal();
        var sdt	=	(checkdate && checkdate!='') ? checkdate.split(" ")[0]:'';
        if($.isEmptyObject(sa)){
            issavedalready	=	false;
        } else {
            $.each(sa,function(i,row){
                var updt	=	(row.updDate && row.updDate!='' ) ? row.updDate.split(" ")[0]:'';
                if( (row.mvkey==mvkey) && (sdt=='' || sdt==updt) ){
                    issavedalready	=	true;
                    return false;
                }
            });
        }
        return issavedalready;
    } catch(e){
        ShowExceptionMessage("isSavedAnswer", e);
    }
}
function completedCheckPhaseQuestion(checkdate){
    logStatus("Calling completedCheckPhaseQuestion", LOG_FUNCTIONS);
    try {
        //isAllAnswersSaved();
        //return (IsPhaseQuestionComplete(PHASE_Check) || isAllAnswersSaved());
        return (isAllAnswersSaved(checkdate));
    } catch(e){
        ShowExceptionMessage("completedCheckPhaseQuestion", e);
    }
}
function isAllAnswersSaved(checkdate){
    logStatus("Calling isAllAnswersSaved", LOG_FUNCTIONS);
    try {
        var isallquessaved	=	true;
        var groupobj = getLastGroupObject();
        if (IsAnyGroupCompletedCheckPhase()){
            var selectedgrpid	= getlastActiveGroupId();
            groupobj = [groupobj['group'+selectedgrpid]];
        }
        var iterloop = true;
        $.each(groupobj,function(gi,g){
            var phase	=	g._phase;
			if(IsRequestSentForTestAppointment()){
												var ques_type = 'coach';
												phase._questions = $.grep(phase._questions, function(e){ 
												return e.ques_type != ques_type; 
												});
										} 
            if(phase._questionscount>0){
                var questions	=	(phase._questionscount==1) ? [phase._questions]:phase._questions;
                $.each(questions,function(i,q){
                    var isInLocal=	isSavedAnswer(q.monitorvariable_id,checkdate);//Saved in local
                    if( (parseInt(q._answercount)<=0) && (!isInLocal)) {
                    // if( (!isSavedAnswer(q.monitorvariable_id)) ){
                        isallquessaved	=	false;
                        iterloop	=	false
                    }
                    return iterloop;
                })
            } else {
                isallquessaved	=	false;
            }
            return iterloop;
        });
        return isallquessaved;
    } catch(e){
        ShowExceptionMessage("isAllAnswersSaved", e);
    }
}
function saveAllAnswersInServer(onAllQuestionSaveSuccess){
    logStatus("Calling saveAllAnswersInServer", LOG_FUNCTIONS);
    try {
        var sa	=	getSavedQuestionsInLocal();
        SaveAllAnswersQuestions(sa,function(response){
            onAllQuestionSaveSuccess(response);
        });
    } catch(e){
        ShowExceptionMessage("saveAllAnswersInServer", e);
    }
}
function getAnswersLocalByMV(mvkey){
    logStatus("Calling getAnswersLocalByMV", LOG_FUNCTIONS);
    try {
        var ansreturn =	null;
        var iscontinue=	true;
        var sa	=	getSavedQuestionsInLocal();
        $.each(sa,function(i,row){
            if(row.mvkey==mvkey){
                ansreturn	=	row.actualOtherData;
                iscontinue	=	false;
            }
            return iscontinue;
        });
        return ansreturn;
    } catch(e){
        ShowExceptionMessage("getAnswersLocalByMV", e);
    }
}
/*MK Added
* Answers from local storage by Question Id*/
function GetLocalAnswerByQuestionID(question_id){
    logStatus("Calling GetLocalAnswerByQuestionID", LOG_FUNCTIONS);
    try {
        var answer  =	null;
        var isContinue=	true;
        var sa	=	getSavedQuestionsInLocal();
        $.each(sa,function(i,row){
            if(row.question==question_id){
                answer      =	row.actualOtherData;
                isContinue	=	false;
            }
            return isContinue;
        });
        return answer;
    } catch(e){
        ShowExceptionMessage("GetLocalAnswerByQuestionID", e);
    }
}
function GetValidationConfirmOnlyIf(condition,onAllowProceed,onRestrictProceed,Message){
    logStatus("Calling GetValidationConfirmOnlyIf", LOG_FUNCTIONS);
    try {
        if(!condition){
            if(onAllowProceed){
                onAllowProceed();
            }
        } else {
            ShowAlertConfirmMessage(Message,function(status){
                if(status==1){
                    if(onAllowProceed){
                        onAllowProceed()
                    }
                } else {
                    if(onRestrictProceed){
                        onRestrictProceed();
                    }
                }
            });
        }
    } catch(e){
        ShowExceptionMessage("GetValidationConfirmOnlyIf", e);
    }
}
/*MK Added Get Un-Answered Question Slider Index*/
function GetUnAnsweredQuestionSlideIndex(checkDate){
    logStatus("Calling GetUnAnsweredQuestionSlideIndex", LOG_FUNCTIONS);
    try {
        var unSavedQuestionIndex	=	0;
        var groupobj = getLastGroupObject();
        if (IsAnyGroupCompletedCheckPhase()){
            var selectedgrpid	= getlastActiveGroupId();
            groupobj = [groupobj['group'+selectedgrpid]];
        }
        var iterLoop = true;
        $.each(groupobj,function(gi,g){
            var phase	=	g._phase;
            if(phase._questionscount>0){
                var questions	=	(phase._questionscount==1) ? [phase._questions]:phase._questions;
                $.each(questions,function(i,q){
                    var isInLocal=	isSavedAnswer(q.monitorvariable_id,checkDate);//Saved in local
                    if( (parseInt(q._answercount)<=0) && (!isInLocal)) {
                        unSavedQuestionIndex=GetSlideIndexByQuestion(q.id);
                        iterLoop	=	false;
                    }
                    return iterLoop;
                })
            }
            return iterLoop;
        });
        return unSavedQuestionIndex;
    } catch(e){
        ShowExceptionMessage("GetUnAnsweredQuestionSlideIndex", e);
    }
}
/*MK Added Get Slider Index By Question ID */
function GetSlideIndexByQuestion(questionId){
    logStatus("Calling GetUnAnsweredQuestionSlideIndex", LOG_FUNCTIONS);
    try {
        var $elem       = $("#questiontemp_" + questionId);
        if(typeof($elem.html())!=='undefined'){
            return $elem.attr('data-slide-index') ? $elem.attr('data-slide-index'):0;
        }
        return 0;
    } catch(e){
        ShowExceptionMessage("GetSlideIndexByQuestion", e);
    }
}
function infoClosedBtn() {
    $(".clsquestioncontent, .main_panel").css("overflow", " auto ");
    //$(".info_popupbox_bg").slideUp();
    //$(".info_popupbox_inner").slideUp();
	$(".info_popupbox_bg").hide();
    $(".info_popupbox_inner").hide();
}
/*MK Fix Swiper should Allow the Last Question to Edit*/
function isEndSlideSwiped(swiper){
    return ( (swiper.previousIndex==swiper.snapIndex) && swiper.isEnd );
}
/*function checkCoachReqanswer()
{
    var b = _getCacheArray('coachquesID');
    var a = _getCacheArray('coachquesanswer');//getLastCoachSelectAnswerFromDiary();
    
    console.log(b);
    console.log(a);
}*/
function openCoachListFromDiary(){
        logStatus("Calling openCoachListFromDiary", LOG_FUNCTIONS);

        try{
            //var sa  =   getSavedQuestionsInLocal();
            var b = _getCacheArray('coachquesID');
            var answer = _getCacheArray('coachquesanswer');
            
            if(!IsRequestSentForTestAppointment()){
                if(answer && answer=="0.0")
                {
                    //setTimeout(function(){ GoForTestAppointmentFromCheckphase(); }, 7000);
                                         
                }
            }else{
                return;
            }

        }catch(e){
            ShowExceptionMessage("openCoachListFromDiary", e);
        }
    }

function getQuestionType(qmonitor_id){
    logStatus("Calling getQuestionType", LOG_FUNCTIONS);
    try {
        var groupobj= getLastGroupObject();
        var ques_type;
        $.each(groupobj,function(groupidx,egroup){
            var phase   =   egroup._phase;
            if(phase._questionscount>0){
                var questions   =   (phase._questionscount==1) ? [phase._questions]:phase._questions;
                $.each(questions,function(qidx,ques){
                    if(ques.monitorvariable_id==qmonitor_id){
                        //if(ques._optionscounts>0) {
                            ques_type =   (ques.ques_type) ? ques.ques_type:"";
                            return false;
                        //}
                    }
                });
            }
        });
        return ques_type;
    } catch(e){
        ShowExceptionMessage("getQuestionType", e);
    }
}
//change on 17-05-17
function getslidenumber(cSwiper)
{
	var totalquestion = $('#totalquesvalue').val();
	 //var string = AndroidIosPlatform();
   // $(".swiper-containers").addClass(string);
	//var totalquestion = $('#totalquesvalue').val();
	var startslide = localStorage.getItem('int_startslide');
	var startslide = parseInt(startslide) + 1;
	
	var slidenumber = $(".swiper-slide-active").attr("data-slide-index");
	var slidenumber = parseInt(slidenumber) + 1;
	if(startslide>1)
	{
	$('.swiper-containers').html('<p>'+ startslide + '/' + totalquestion +'</p>');
	}
	else
	{
		$('.swiper-containers').html('<p>'+ 1 + '/' + totalquestion +'</p>');
	}
	if (cSwiper.swipeDirection == 'next') {
		$('.swiper-containers').html('<p>'+slidenumber + '/' + totalquestion +'</p>');
	}
	if (cSwiper.swipeDirection == 'prev') {
		$('.swiper-containers').html('<p>'+slidenumber + '/' + totalquestion +'</p>');

	}
}
	