$(function() {
  TranslateGenericText();
  TranslateLogin(); 
   var appversion = APP_VERSION;
	$('.versionnums').html(appversion);
  //$(".comm_banner_img_box h4").text(GetLanguageText(PAGE_TTL_LOGIN_ON));
  $(".page-title h4 .one").text(GetLanguageText(PAGE_TTL_LOGIN_ON));
  $(".formfocus_ipentry input,.formfocus_ipentry select").trigger("blur");
});
/*
$(document).delegate(".linkforgots",'click',function(){	
    var emailplaceholder = GetLanguageText(TXT_ENTER_EMAIL);		
    var POPUP_HTML	= '<div class="popuptitle comm_h4">'+GetLanguageText(TEXT_FORGOTPASSWORD)+'</div>'
                    +'<div class="input_box"><input type="email" value="" id="forgetemail" class="input_blue" placeholder="'+emailplaceholder+'" >'
                    +'<div class="pupopfooter"><button class="white_btn" onclick="forgotPassword()">'+GetLanguageText(LBL_NEXT)+'</button>'
                    +'<button class="white_btn" onclick="_close_popup()">'+GetLanguageText(LBL_CLOSE)+'</button></div></div>';
    _popup(POPUP_HTML,{close:false},function(){},'forget');
});
*/
$('.linkforgot').click(function(){
						
	$('.password-forgot').show();
		
						
		});
/*Functions - starts */
function doUserLogin(eobj){
    logStatus("Calling doUserLogin", LOG_FUNCTIONS);
    try{
        var $loginid = $("#loginid");
        var $loginpswd = $("#loginpswd");
        var $errortext = $('.errortext');
        var $loginmessage = $('.loginmessage');
        var username 	=	$loginid.val();
        var password	=	$loginpswd.val();
        //var user		=	getCurrentUser();
        var errortxt = '<label class="errortext" style="color:red;"></label>';
        if($loginid.val()=='' && $loginpswd.val()==''){
            $errortext.remove();
            $loginid.before(errortxt);
            $errortext.html(GetLanguageText(PLEASE_ENTER_USERNMAE_AND_PASSWORD));
            return;
        }
        if($loginid.val()==''){
            $errortext.remove();
            $loginid.before(errortxt);
            $errortext.html(GetLanguageText(PLEASE_ENTER_USERNMAE));
            return;
        }
        if($loginpswd.val()==''){
            $errortext.remove();
            $loginpswd.before(errortxt);
            $errortext.html(GetLanguageText(PLEASE_ENTER_PASSWORD));
            return;
        }
        if(validateSteps('#loginform1')){
            //DisableProcessBtn("#idbuttonlogin", false,"Please Wait..",true);
            AddProcessingLoader(eobj);
            getLoginByUsernamePassword(username,password,function(response){
                //XX RemoveProcessingLoader(eobj);
                //DisableProcessBtn("#idbuttonlogin", true, "Login",true);
                TranslateLogin();
                /**
                 * @param   response.status_code This is status code
                 */
                if(response.status_code==1){
                    updateAppLogin(response);
                } 
                else if(response.status_code==5)
                {
                    RemoveProcessingLoader(eobj);
                    //tstmk@stech.com
                    //$('.loginmessage').find('label').html(GetLanguageText(LOGIN_USERNAMEPASSWORD_INCORRECT));
                    $loginmessage.find('span').html(GetLanguageText(LOGIN_ACTIVATE_ACCOUNT));
                }

                else {
                    //Invalid Login Should Stop the Loader
                    RemoveProcessingLoader(eobj);
                    //tstmk@stech.com
                    //$('.loginmessage').find('label').html(GetLanguageText(LOGIN_USERNAMEPASSWORD_INCORRECT));
                    $loginmessage.find('span').html(GetLanguageText(LOGIN_USERNAMEPASSWORD_INCORRECT));
                }
            });
        } else {
            RemoveProcessingLoader(eobj);
            //$('.loginmessage').find('label').html(GetLanguageText(USERNAMEPASSWORD_INCORRECT));
            $loginmessage.find('span').html(GetLanguageText(USERNAMEPASSWORD_INCORRECT));
        }
    } catch (e) {
        ShowExceptionMessage("doUserLogin", e);
        RemoveProcessingLoader(eobj);
    }
}
function doUserLoginFacebook(data){
    logStatus("Calling doUserLoginFacebook", LOG_FUNCTIONS);
    try{
        var username    =   data.email;
        var password    =   data.password;
        //AddProcessingLoader(eobj);
        getLoginByUsernamePassword(username,password,function(response){   
            TranslateLogin();     
            if(response.status_code==1){
                updateAppLogin(response);
            }    
        }); 
    } catch (e) {
        ShowExceptionMessage("doUserLoginFacebook", e);
        //RemoveProcessingLoader(eobj);
    }
}
function TranslateLogin(){
    logStatus("Calling TranslateLogin", LOG_FUNCTIONS);
    try {
        TranslateText(".transname", PHTRANS, PH_USERNAME);
        TranslateText(".transsubmit", TEXTTRANS, BTN_SUBMIT);
        TranslateText(".transpassword", PHTRANS, PH_PASSWORD);
        TranslateText(".transrememberme", TEXTTRANS, BTN_REMEMBER_ME);
        TranslateText(".transforgot", TEXTTRANS, LBL_FORGOT_PASSWORD);
        TranslateText(".transregister", TEXTTRANS, BTN_REGISTER);
    } catch (e) {
        ShowExceptionMessage("TranslateLogin", e);
    }
}
function forgotPassword(){
    logStatus("Callling forgotPassword",LOG_FUNCTIONS);
    try{
	$('.password-forgot').hide();
        var $forgetemail = $('#forgetemail');
        var email = $forgetemail.val();
        if(IsEmailValid(email)){
            _close_popup();
            _movePageTo('security',{email:base64_encode(email)});
            
        }  else {
            ShowToastMessageTrans(MSG_EXCEPTION_EMAIL_ERROR,_TOAST_LONG);
        }
    }catch (e) {
        ShowExceptionMessage("forgotPassword", e);
    }	
}
/*16-june-2017*/
  function fb_login_confirm()
      {
        var result = confirm("Log je uit de facebook-applicatie in je telefoon?");
        if(result)
          {  
            fb_login();
          }
        else
          {
            return false;
          }
      } 
