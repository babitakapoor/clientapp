/*
* Controller JS file 
* Page : 'edit-profile'*/

/*jQuery Events - Starts*/

//Aneetha

$(document).ready(function(){
	var template = GetCurrentPath();
	if(template == 'edit-profile'){
		$(".page-title h4 .one").text('PROFILE');
	}
    TranslateEditProfile();
    TranslateGenericText();
	$(".edit_ctn_one").slideUp();
	$(".edit_ctn_two").slideDown();
    editProfilePage();	
$(".formfocus_ipentry input,.formfocus_ipentry select").trigger("blur");


$(".edit_twoshow").click(function(){
	$(".edit_ctn_one").slideUp();
	$(".edit_ctn_two").slideDown();
});
$("#useremail").blur(function() 
			{
			 var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;  
			 var emailaddress = $("#useremail").val();
			 if(!emailReg.test(emailaddress)) {
			   $('#useremail').css('border','1px solid red !important');
			ShowToastMessage(SHOW_EXCEPTION_WRONG_EMAIL, _TOAST_SHORT);
			 }
			else
			{
				$('#useremail').css('border','1px solid #0095ac');
			}
			});

});
$(document).delegate("#usergenderdata","change",function(){
    var selecttext = $( "#usergenderdata").find("option:selected" ).text();
    $("#usergenderdata_text").html(selecttext);
});

$(document).delegate("#language","change",function(){
	var selecttexts = $( "#language").find("option:selected" );
	
    var selecttext = $( "#language").find("option:selected" ).text();
	
    $("#language_text").html(selecttext);
});
/*jQuery Events - Ends*/

/*Function - Starts*/	
//Aneetha
function TranslateEditProfile(){
    logStatus("TranslateEditProfile", LOG_FUNCTIONS);
    try {
     TranslateText(".transgetpicture", TEXTTRANS, TEX_GET_IMAGE);
     }catch(e) {
        ShowExceptionMessage("TranslateEditProfile", e);
    }
}

function editProfilePage(){
    logStatus("Calling editProfilePage", LOG_FUNCTIONS);
    try{
        var login_info = getCurrentLoginInfo();
        var user = getCurrentUser();	
        if(!$.isEmptyObject(user) && !$.isEmptyObject(login_info)){

            updateprofileimage(PROFILEPATH + user.userimage);
            
            //var gender	=	(user.gender==0 ) ? 'Male':'Female';
            var $userfirstname = $('#userfirstname');
            var $userlastname = $('#userlastname');
            var $useremail = $('#useremail');
            var $loginemail = $('#loginemail');
            var $userphone = $('#userphone');
            var $usergenderdata = $('#usergenderdata');
            $userfirstname.val(login_info.user.first_name);
             $('.loginusername').text(login_info.user.first_name);
            $userlastname.val(login_info.user.last_name);
            $useremail.val(login_info.user.email);
            $loginemail.val(login_info.user.email);
            $userphone.val(login_info.user.phone);
			var gender = user.gender;  
			   if(gender==0){
			   $('select[name^="gender_details"] option[value="0"]').attr("selected","selected");
			   }
			   else{
				$('select[name^="gender_details"] option[value="1"]').attr("selected","selected");
			   }
            $usergenderdata.val(user.gender);
            $usergenderdata.trigger("change");
			$("#imgval").val(login_info.user.userimage);
            /**
             * @param  login_info.user.is_deleted This is user isdeleted
             */
            var isactive = login_info.user.is_deleted;
            var $isactive= $('#isactive');
            var $isnotactive= $('#isnotactive');
            if(isactive){
                $isactive.addClass('radiobtn');
                $isnotactive.removeClass('radiobtn');
            } else {
                $isnotactive.addClass('radiobtn');
                $isnotactive.removeClass('radiobtn');
            }
            // var associativecluns = login_info.user_session.associatedClubs;
            // var assoc_clubli	=	"";
            // $.each(associativecluns,function(key,value){
                // assoc_clubli	+= '<li class="clubdetails">'+
                    // '<span class="radiobox ">'+'<input type="radio"  name="inp_proff" class="rdgrp_clubdetails"  ></span>'+
                    // '<label>'+value.club_name+'</label>'+
                // '</li>';
            // });
            // if(assoc_clubli==''){
                // assoc_clubli	=	'<li>No Clubs associated</li>';
            // }
            // $('.associativeclubs .clublist').html(assoc_clubli);
            /**
             * @param user.security_questions This is user security questions
             * @param login_info.securityQuestions This is securityQuestions
             */
            var _sqQuestions=	login_info.securityQuestions;
            
            var quesHTML	=	"";
            var existSeqQuesStr	=	user.security_questions;
            var seq_questions	=	(existSeqQuesStr && existSeqQuesStr!='') ? JSON.parse(existSeqQuesStr):{};
            
            $.each(_sqQuestions,function(_index,_ques){
                var isMandatory	=	( (seq_questions[_ques.secure_question_id]) &&
                                        (seq_questions[_ques.secure_question_id]!='') ) ? false:true;
                var seq_answer = "";
                if(seq_questions[_ques.secure_question_id] && seq_questions[_ques.secure_question_id]!="")
                {
                    var seq_answer = seq_questions[_ques.secure_question_id];
                }
                 
                var reqclass	=	isMandatory ? 'req-nonempty':'';
                var comclass	=	isMandatory ? 'compulsory':'';
                //var isMandatory	=	( (seq_questions[_ques.secure_question_id]) && (seq_questions[_ques.secure_question_id]!='') ) ? '':'req-nonempty';
                //quesHTML+='<div class="input_box '+comclass+'">'+'<input class="sq_ques '+reqclass+' input_blue input_blue_fontsize" type="text" id="sqQues_'+_ques.secure_question_id+'" placeholder="'+_ques.question+'" value="'+seq_answer+'"/></div>';
                quesHTML+='<div class="input_box ">'+'<input class="sq_ques input_blue input_blue_fontsize" type="text" id="sqQues_'+_ques.secure_question_id+'" placeholder="'+_ques.question+'" value="'+seq_answer+'"/></div>';
            });
            var langHTML = "";
            var $language = $("#language");
            //var $languagetext = ("#language_text");
            //var $questionslist = (".questions_list");
            $language.html('<option selected="selected">'+GetLanguageText(TXT_PLEASE_WAIT)+'</option>');
            getlanguages(function(response){
                /**
                 * @param value.language_id This is language id
                 */
				langHTML += '<option value="0" >'+GetLanguageText(TXT_SELECT_LANGUAGE)+'</option>';
                $.each(response,function(key,value){
                    langHTML+= '<option value="'+value.language_id+'">'+value.title+'</option>';
                });
                $language.html(langHTML);
                $language.val(login_info.user.r_language_id);
                var language = login_info.user.r_language_id;
                $language.find('option[value='+language+']').attr('selected', 'selected');
                var selecttext = $language.find("option:selected" ).text();
                //$languagetext.html(selecttext);
                $("#language_text").html(selecttext);
            });
            $(".questions_list").html(quesHTML);
            TranslateText("#sqQues_1", PHTRANS, TXT_SQ1);
            TranslateText("#sqQues_2", PHTRANS, TXT_SQ2);
            TranslateText("#sqQues_3", PHTRANS, TXT_SQ3);
            /*SN commanded -setCaptchaContent undefined error
            setCaptchaContent(".recapchabox",function(reponse){
                /*On load Captcha Success*
            });
            */
        }
    } catch (e) {
        ShowExceptionMessage("editProfilePage", e);
    }
}
function saveClientProfile(elemobj){
    logStatus("Calling saveClientProfile", LOG_FUNCTIONS);
    try {
        if(validateSteps('.editprofile')){
            var question = {};
            $(".sq_ques").each(function(i,eachitem){
                var idstr	=	$(eachitem).attr("id");
                var answer	=	$(eachitem).val();
                if($.trim(answer)!=""){
                    var _qid	=	idstr.split('sqQues_')[1];
                    question[_qid]=answer;
                }
            });
            var clientProfile	=	{};
            clientProfile.first_name = $('#userfirstname').val();
            clientProfile.last_name = $('#userlastname').val();
            clientProfile.email = $('#useremail').val();
            clientProfile.phone = $('#userphone').val();
            //clientProfile.gender = $('#usergenderdata').val();
            /*********************************************************************
            Change the Minus Value into Null and By default go 0 in database
            ********************************************************************/
            if($('#usergenderdata').val()==-1){
             clientProfile.gender = '';
            }
            else{
              clientProfile.gender = $('#usergenderdata').val();
            }
            /*********************************************************************
            Change the Minus Value into Null and By default go 0 in database
            ********************************************************************/
            clientProfile.question	=	JSON.stringify(question);
            clientProfile.language	=	$("#language").val();
			clientProfile.userimage	=	$("#imgval").val();
            //DisableProcessBtn("#saveProfieldata", false,AddProcessingLoader('.transsave'));
            if(validateSteps('.editprofile')){
            AddProcessingLoader(elemobj);
            updateClientProfile(clientProfile,function(response){
                if(response.status==AJAX_STATUS_TXT_SUCCESS){
                        updateUserInfoLocal({
                            first_name:clientProfile.first_name,
                            last_name:clientProfile.last_name,
                            email:clientProfile.email,
                            gender:clientProfile.gender,
                            r_language_id:clientProfile.language,
                            phone:clientProfile.phone,
                            security_questions:clientProfile.question,
                            userimage:$("#imgval").val()
                        },function(){
                            USER_LANG	=	clientProfile.language;
                            RemoveProcessingLoader(elemobj);
                        });
                        ShowToastMessageTrans(MSG_SUCCESS_PROFILE_UPDATED,_TOAST_LONG);
                    } else {
                        ShowToastMessageTrans(MSG_PROFILE_UPDATED_FAILED,_TOAST_LONG);
                    }
                    _movePageTo('edit-profile',{tm:timestamp()});
                });
            }
        }
    } catch (e) {
        ShowExceptionMessage("saveClientProfile", e);
    }
}
/*Function - Ends*/