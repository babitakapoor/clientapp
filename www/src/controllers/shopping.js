$(document).ready(function(){
    TranslateGenericText();
    TransCreditInfo();
    logStatus("shopping", LOG_FUNCTIONS);
    try{
        var User	=	getCurrentUser();
        var first_name = User.first_name;
        $('.username').text(first_name);
        var RedeemCredits	=	GetCredits();
        var percentage	=	(RedeemCredits.redeemed/RedeemCredits.totalcredits)*100;
        $(".graph_center_bg").css("height",percentage+"%");
        $("#shopcredited").val(Math.round(RedeemCredits.redeemed));
    } catch (e) {
        ShowExceptionMessage("shopping", e);
    }
});
function goHealthShop(){
    logStatus("Calling goHealthShop", LOG_FUNCTIONS);
    try {
        _movePageTo("health_shop");
    } catch(e){
        ShowExceptionMessage("goHealthShop", e);
    }
}

function TransCreditInfo(){
    logStatus("TransCreditInfo", LOG_FUNCTIONS);
    try {
    TranslateText(".tanstotalcredits", TEXTTRANS, LBL_TOTAL_CREDITS);
    TranslateText(".transshop", TEXTTRANS, LBL_SHOP);
    } catch(e){
        ShowExceptionMessage("TransCreditInfo", e);
    }
}