var RES_MOVESMART_INFO ='<p class="comm_para" >You\'ve choosen to MOVESMART. as your goal.</p>'+
                        '<p class="comm_para" >Starting the 10.000 stepgame will give you the'+
                        'needed amount of movement to start your healthier way of life.'+
                        'Keep track of how many steps you make every day and reach your 10.000 steps.'+
                        'Try not to sit all the time, move around and improve your mobility.'+
                        'The less you sit the better.</P>'+
                        '<p class="comm_para" >Good luck with your 14-day stepgame and start now!</P>';

/*
var RES_MINDSWITCH_INFO = '<div class="lamp_popup">'+

                            '<p class="comm_para txt_cntr">'+
                            'Great that you want to start with the'+
                            'MINDSWITCH?activity. Don\'t forget your'+
                            'goal and enter your steps in the stepsgame!'+
                        '</p>'+
                        '<button class="white_btn close_btn"  onclick="">'+GetLanguageText(LBL_CLOSE)+'</button></div>';

var RES_EATFRESH_INFO ='<div class="lamp_popup">'+
                                '<p class="comm_para txt_cntr">'+
                                'Great that you want to start with the EATFESH!diary. Don\'t forget your goal.Enter your steps in the stepsgame!'+
                            '</p>'+
                            '<button class="white_btn close_btn"  onclick="">Close</button></div>';
var RES_MOVESMART_TIPS = '<div class="lamp_popup" >'+
                '<div class="lamp_popup_header" >'+
                    '<span class="light_icon"></span>'+
                    '<span class="lamp_popup_close" onclick="">X</span>'+
                '</div>'+
                '<p class="comm_para">MOVE SMART</p>'+
                '<p class="comm_para"> Even though exercising is the most benificial for you we advice you to start moving a bit more</p>'+
                '<p class="comm_para">Excercise,a smart move?</p>'+
                '<p class="comm_para">The easiest way to find out what moving means for you is to walk a bit more.</p>'+
                '<p class="comm_para">Keen to find out?</p>'+
                '<div class="hints_img">'+
                    '<img src="images/hints_img3.png" alt="hints_img1" />'+
                '</div>'+
            '<button class="white_btn close_btn"  onclick="" >Close</button></div>';	*/
$(document).ready(function(){
    logStatus("Calling Questions.Document.Event.Ready", LOG_FUNCTIONS);
    try{
        TranslateGenericText();
        TransStartphaseMovesmart();
        TransStartphaseEatFresh();
        TransStartphaseMindActivity();
        InitPageEvents();
        UpdateDynamicPageForId('.stepregistercont',STEPS_REGISTRATION,function(content,pagetitle){
            $(".comm_banner_img_box .startfasemovetrans").text(pagetitle);
        });
        UpdateDynamicPageForId('.stepregistereatfreshcont',START_PAHSE1_EATFRESH,function(content,pagetitle){
            $(".comm_banner_img_box .startfaseeattrans").text(pagetitle);
        });
        UpdateDynamicPageForId('.startphsemindcont',START_PAHSE1_MINDSWITCH,function(content,pagetitle){
            $(".comm_banner_img_box .startfasemindtrans").text(pagetitle);
        });
        var topics	=	GetTopthreeTopics();
        var selectedgrpid = getlastActiveGroupId();
        if(topics['group_'+selectedgrpid]){
            var topic	=	topics['group_'+selectedgrpid];
            var HTML	=	'';
            /**
             * @param  tpc.topic_name This is toic name
             */
            $.each(topic,function(idx,tpc){
                HTML+=	'<li>'+
                            '<span class="light_icon lamp_icon" onclick="" >'+
                                '<!-- <label class="question_icon result_movesmart_info"  onclick="">?</label> -->'+
                            '</span>'+
                            '<h4 class="light_label clsapplyellipsis" >'+(idx+1)+', '+tpc.topic_name+'</h4>'+
                        '</li>';
            });
            $("#toptopics").html(HTML);
        }
        var resmovepagecnt = '<div class="swiper-wrapper">';
        //$.each(introcnt,function(key,value){
            resmovepagecnt+='<div class="welcome_ctn_bx swiper-slide" >'+RES_MOVESMART_INFO+'</div>';
        //});
        resmovepagecnt+='</div><div class="swiper-pagination"></div>';
        $('#resmoveinfo').html(resmovepagecnt);
        swipePagination('#resmoveinfo');
        /*MK Added*/
        var  $showresulttipef = $(".showresulttipef");
        var  $showresulttipms = $(".showresulttipms");
        var  $showresultmvs = $(".showresultmvs");
        if(typeof($showresulttipef.html())!='undefined'){
            $showresulttipef.trigger("click");
        }
        if(typeof($showresulttipms.html())!='undefined'){
            $showresulttipms.trigger("click");
        }
        if(typeof($showresultmvs.html())!='undefined'){
            $showresultmvs.trigger("click");
        }
        //var dashinfopage = new Swiper ('.startphasecls',{
        new Swiper ('.startphasecls',{
                preventClicks:true,
                slideToClickedSlide:false,
                mode:'horizontal',
                loop: false,
                autoHeight:false,
                pagination: '.swiper-pagination',
                paginationClickable: true,
                spaceBetween:5,
				onSlideNextEnd: swipeScrollTop,
				onTransitionEnd: swipeScrollTop
            });
    } catch (e) {
        ShowExceptionMessage("Questions.Document.Event.Ready", e);
    }
});
/*Need Change the Delegate Event to common or InitPageEvents*/
$(document).delegate(".result_movesmart_info",'click',function(event){
    event.preventDefault();
    event.stopPropagation();
    _movePageTo("result_movesmart_info");
});

$(document).delegate(".que_question_btn",'click',function(event){
    event.preventDefault();
    event.stopPropagation();
    // _movePageTo("result_movesmart_info");
    $(".que_popupbox_bg").show();
    $(".que_popupbox").show();
});

$(document).delegate(".que_popupbox .lamp_popup_close, .que_popupbox_bg",'click',function(event){
    event.preventDefault();
    event.stopPropagation();
    // _movePageTo("result_movesmart_info");
    $(".que_popupbox_bg").hide();
    $(".que_popupbox").hide();
});



function InitPageEvents(){
    logStatus("Calling InitPageEvents", LOG_FUNCTIONS);
    try{
        $("#idfasecompletedbutton").click(function(){//#idfasecompletedbutton_mind,#idfasecompletedbutton_move,#idfasecompletedbutton_eat
            logStatus("Calling Event#idfasecompletedbutton.Click", LOG_FUNCTIONS);
            var selectedgrpid = getlastActiveGroupId();
            var ActiveFaseId  = getCurrentFaseForGroupId(selectedgrpid);
            var isNextPhaseTypeActivity	=	!!( (ActiveFaseId == PHASE_Check) && (selectedgrpid == GROUP_MOVESMART));
            var userupdatedata	=	{};
            var steptypeid = $('#startype').is(":checked") ? 1:0;
            //var dailytimer =$("#dailysteptimer").val();
           // if(isNextPhaseTypeActivity){
         //       userupdatedata	=	{stepgame_type:steptypeid};
         //       //steptypeid	=	'';
         //   }
            if (ActiveFaseId == PHASE_Check){//Since keep pressing Step Game by clicking on the back gives problem
                //var nextphaseid = getNextPhaseIdForGroupId(selectedgrpid);
                var eobj = $(this);
                if(validateSteps(".startphasetimer")){
                    //alert(dailytimer);
                    AddProcessingLoader(eobj);
                    updateUserStartPhase(selectedgrpid, PHASE_phase1, function(){//nextphaseid
                        updatePhaseCompleted(selectedgrpid, ActiveFaseId, function(){
                            RemoveProcessingLoader(eobj);
                            var template	=	getGroupPageBasedonGroupId(selectedgrpid);
                            updateUserInfoLocal(userupdatedata,function(){

                            });
                            CheckDashboardAndMovePageTo(template, false);
                        });
                    },steptypeid);
                }
            } else {
                var template	=	getGroupPageBasedonGroupId(selectedgrpid);
                CheckDashboardAndMovePageTo(template, false);
            }
           // if(isNextPhaseTypeActivity)
            //{
           //     stepdataupdate(steptypeid);
          //  }
            
        })
    } catch (e) {
        ShowExceptionMessage("InitPageEvents", e);
    }
}
/*
function resEatFreshTips(){
    logStatus("Calling resEatFreshTips", LOG_FUNCTIONS);
    try {

        ShowTipsPopup(RES_EATFRESH_INFO);
    }catch(e) {
        ShowExceptionMessage("resEatFreshTips", e);
    }
}
function resMindSwitchTips(){
    logStatus("Calling resMindSwitchTips", LOG_FUNCTIONS);
    try {
        ShowTipsPopup(RES_MINDSWITCH_INFO);
    }catch(e) {
        ShowExceptionMessage("resMindSwitchTips", e);
    }
}
function resultMovesmartTips(){
    logStatus("Calling resultMovesmartTips", LOG_FUNCTIONS);
    try {
        ShowTipsPopup(RES_MOVESMART_TIPS);
    }catch(e) {
        ShowExceptionMessage("resultMovesmartTips", e);
    }
}*/
function TransStartphaseMovesmart(){
    logStatus("Calling TransStartphaseMovesmart", LOG_FUNCTIONS);
    try {
        TranslateText(".transcounter", TEXTTRANS,TXT_START_STEP_COUNTER);
        TranslateText(".translist", TEXTTRANS,TXT_LIST);
        TranslateText(".transreminder", TEXTTRANS,TXT_SELECT_DAILY_REMINDER);
        TranslateText(".transstepcounter", TEXTTRANS,MSG_CAN_MONITOR_YOUR_STEPS);
        TranslateTextLocal(".transstepcounter", TEXTTRANS, "25");
        TranslateText(".transmonitorstep", TEXTTRANS,MSG_MONITOR_YOUR_STEPS);
        TranslateText(".transdontuse", TEXTTRANS,MSG_DONT_WANT_USE_STEPMONITOR);
        //TranslateTextLocal(".transdontuse", TEXTTRANS,"26");
        TranslateText(".transtextwearable", TEXTTRANS,TXT_Wearables);
    }catch(e) {
        ShowExceptionMessage("TransStartphaseMovesmart", e);
    }
}
function TransStartphaseEatFresh(){
    logStatus("Calling TransStartphaseEatFresh", LOG_FUNCTIONS);
    try {
         TranslateText(".transeatdiary", TEXTTRANS,TXT_START_DIARY);
    }catch(e) {
        ShowExceptionMessage("TransStartphaseEatFresh", e);
    }
}
function TransStartphaseMindActivity(){
    logStatus("Calling TransStartphaseMindActivity", LOG_FUNCTIONS);
    try {
         TranslateText(".transmindactivity", TEXTTRANS,TXT_START_MIND_ACTIVITY);
    }catch(e) {
        ShowExceptionMessage("TransStartphaseMindActivity", e);
    }
}

function swipeScrollTop(){
	$(".startphasecntslide").scrollTop(0);
}

function stepdataupdate(steptypeid){
  logStatus("Calling stepdataupdate", LOG_FUNCTIONS);
 try{
  var user = getCurrentUser();
  var userid = user.user_id;
   if(steptypeid==1)
   {
    var steptypeids = 1;
   }
   else
   {
    var steptypeids = 0;
   }
  var postdata = {};
  postdata.user_id   =   userid;
  postdata.is_reporting=  BASE_URL+"reporting/index.php/report/stepchecknew";
  postdata.stp_id = steptypeids;
  saveStepdata(postdata,function(response){
     if(response.status==1){
      console.log('Step field is updated successfulling');
     }
     else
     {
      console.log('Step field is not updated successfulling');
     }
    });
  
 }
 catch(e) {
        ShowExceptionMessage("stepdataupdate", e);
    }
}