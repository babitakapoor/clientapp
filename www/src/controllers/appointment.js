var _CoachList	=	{};
$(document).ready(function(){
    HideLoader(); 
     TranslateGenericText();
	 $(".page-title h4 .one").text(GetLanguageTextLocal("5"));
     $(".formfocus_ipentry input,.formfocus_ipentry select").trigger("blur");
    logStatus("Calling Event.AppointmentRequest.Load", LOG_FUNCTIONS);
    try { 
          UpdateDynamicPageForId('.appointment_cnt',APPOINTMENT_REQUEST_ID,function(content,pagetitle){
            var appointcntstr = content.toString();
            var appointcnt = appointcntstr.split(MARKER_QUERY_PAGEBREAK);
            //XX var intropagecnt = '<div class="swiper-wrapper">';
            var appointpagecnt = '';
			var multing = 1;
			
            $.each(appointcnt,function(key,value){
                appointpagecnt+='<div class="welcome_ctn_bx home_welcome_ctn_bx swiper-slide" >'
				+'<div class="comm_banner_img_box">'
				+'<div class="transparent-stripe"></div>'
					+'<img src="images/banner/move'+multing+'.jpg" alt="move12" />'
					//+'<h4>A healthy life </h4>'
					//+'<img class="banner_logo" src="images/movesmartlogo.png" alt="movesmartlogo" />'
				+'</div>'
				+'<div class="home_ctn_padding" >'+value+'</div></div>';
				multing++;
				return false;
            });
           if(multing>2){
                appointpagecnt += '<div class="swiper-pagination"></div>';
            }
            $('.appointment_cnt').html(appointpagecnt);
            //$(".comm_banner_img_box h4").text(pagetitle);
            //var intropage = new Swiper ('.home_slider_inner',{
            new Swiper ('.coachdetail',{
                preventClicks:true,
                slideToClickedSlide:false,
                mode:'horizontal',
                loop: false,
                autoHeight:false,
                pagination: '.swiper-pagination',
                paginationClickable: true,
				onSlideNextEnd: swipeScrollTop,
				onTransitionEnd: swipeScrollTop
            });
        });
        InitPageEvents();//Inherited from question.js
        /*Implement Later
        ShowCoachBasedOnTheDistance();*/
        searchCoachAndClub();
        LoadCoachInformation();
        var selectedcoach = GetSelectedCoachObj();
        
        if(!$.isEmptyObject(selectedcoach)){
            getValidImageUrl(PROFILEPATH+selectedcoach.userimage,function(url){
                $('#coachicon').attr('src',url);
                $('.coach_name').html(selectedcoach.first_name);
            });
        }
        etidPersonalInfo();
    } catch(e){
        ShowExceptionMessage("Event.AppointmentRequest.Load", e);
    }
});
/*Need to change the Delegate tot the InitPage Event Later*/
$(document).delegate(".coachdetailinfo",'click',function(){
    //_movePageTo("coachdetail_info");
    $('.coachinfodetail').show();
    $('#pnlcoachresult').hide();
    $('#appointment_title').hide();
});
$(document).delegate(".personalclientinfo",'click',function(){
    //_movePageTo("personalclient_info");
});
/**
 * description: Search Coach and Club
 */
function searchCoachAndClub(){
    logStatus("Calling searchCoachAndClub", LOG_FUNCTIONS);
    try {
        //var locationRange	=	$("#rangerinput").html();
        var locationRange	=	0;//$("#rangerinput").html();
        var $search_label = $("#search_label");
        $search_label.html('<img src="images/loading.gif" alt="Loading.." />'+GetLanguageText(TEX_SEARCHING)+'...');
        _searchCoachAndClub({companyId:APP_COMPANY_ID,locationRange:locationRange/*$.trim(locationRange)*/},function(response){
            var clubs 	=	(response) ? response:{};
            var $reqcoachlist	=	$("#reqcoachlist");
            var CoachListHtml	=	'';
            //var coachimageobj = [];
            if(clubs.length>0) {
                /**
                * @param club.commercial_name This is club commercial name
                * @param club.street This is club street
                * @param club.club_id This is club id
                * @param club.coachlist This is coach list
                * @param club.citycountry This is city country
                * @param coach.user_id This is user id
                * @param coach.nationality_name This is nationality name
                */
                $.each(clubs,function(i,club){
                    //var clubname	=	club.commercial_name;
                    //var clubstreet	=	club.street;
                    var clubid		=	club.club_id;
                    if(club._coachcount>0) {
                        var coachlist	=	(club._coachcount==1) ? [club.coachlist]:club.coachlist;
                        //var address   =  club.citycountry;
                        var _clubname = club.club_name;
                        $.each(coachlist,function(coachidx,coach){
                            _CoachList[coach.user_id]	=	coach;
                            if(clubid==coach.r_club_id)
                            {
                                var clubname = _clubname;
                            }
                            var city = (coach.city_name) ?coach.city_name:'';
                            var nationality = (coach.nationality_name) ?coach.nationality_name:'';
                            //var citynation =city +'-'+nationality;
                            var citynation = city;
                            // if(city=='' && nationality==''){
                            //     citynation='Unavailable';
                            // }
                            if(city==''){
                                citynation='Unavailable';
                               }
                            //var coachimg = (coach.userimage) ?PROFILEPATH+coach.userimage:'images/no_image.png';
                            //coachimageobj.push({id:coach.user_id,icon:coach.userimage});
                            //getValidImageUrl(PROFILEPATH+coach.userimage,function(url){
									/*
                                CoachListHtml	+=	'<li class="openreqappt" onclick="openReqAppointmentSteps(\''+clubid+'\',\''+coach.user_id+'\',this)">'+
                                                        '<div class="useicon">'+
                                                            '<img src="" class="coachicon_'+coach.user_id+'" alt="..." />'+
                                                        '</div>'+
                                                        '<div class="coach_name_box" >'+
                                                            '<h4 class="comm_h4" >'+  coach.first_name + ' '+  coach.last_name + '</h4>'+
                                                            '<p class="comm_para" >'+citynation+'</p>'+
                                                        '</div>'+
                                                        '<span class="spanloading"></span>'+
                                                        //'<button class="white_btn coachdetailinfo" >'+GetLanguageText(BTN_DETAIL)+'</button>'+
                                                        //'<button class="white_btn" onclick="sendAppoinmentRequest(\''+clubid+'\',\''+coach.user_id+'\',this)">Send</button>'+
                                                        //'<button class="white_btn" onclick="">'+GetLanguageText(LBL_SEND)+'</button>'+
                                                        '<div class="clear"></div>'+
                                                    '</li>';
													*/
									CoachListHtml	+=  '<div class="coach-row" onclick="openReqAppointmentSteps(\''+clubid+'\',\''+coach.user_id+'\',this)">'+
														'<div class="coach-photo-container">'+
															'<div class="hasphoto"><img src="" class="coachicon_'+coach.user_id+'" alt="..."></div>'+
														'</div>'+
														'<a href="javascript:void(0)" class="coach-name">'+
															'<p><strong>'+  coach.first_name + ' '+  coach.last_name + '</strong><br>'+
																'<span class="red">'+citynation+'</span><br>'+'<span style="font-size:12px;font-family:open_sanslight;">'+clubname+'</span>'+
															'</p>'+
														'</a>'+
														'<span class="spanloading"></span>'+
														'<div class="clear"></div>'+
													'</div>';			
													
													
                            //});
                        });
                    }
                });
            } else {
                CoachListHtml	=	'<p>No Coach Found</p>';
            }
            $("#SearchClubs").hide();
            $reqcoachlist.html(CoachListHtml);
            $.each(_CoachList,function(coach_id,eachcoach){
				
                 getValidImageUrl(PROFILEPATH+eachcoach.userimage,function(url){
					if(url === 'images/no_image.png'){
						$('.coachicon_'+coach_id).attr('src',"");
					}
					else{
					$('.coachicon_'+coach_id).attr('src',url);
					}
                     
                 });
            });
            $("#pnlcoachresult").show();
            //$("#search_label").html(GetLanguageText(TEX_COACH_SERACH_RESULT))
			$("#search_label").html(GetLanguageTextLocal("6"));
			
        });
    } catch(e) {
        ShowExceptionMessage("searchCoachAndClub", e);
    }
}
/**
 * description: Send Appointment for Client

 */
function sendAppoinmentRequest(ClubID,CoachID,eobj){
    logStatus("Calling sendAppoinmentRequest", LOG_FUNCTIONS);
    try {
		var is_new_coach = 0;
		  var is_change_coach = $('#is_change_coach').val();
		  if(is_change_coach==1){
		  var is_new_coach = 1;
		  }
        if(CoachID!=0){
            /*Check coach ID enough to validate club*/
            var user	=	getCurrentUser();
            var groupid	=	GROUP_MOVESMART; //XX for now test appointment is only for Movesmart getlastActiveGroupId();
            var phaseid	=	getCurrentFaseForGroupId(groupid);
			var user_id = user.user_id;
            AddProcessingLoader(eobj);
            ShowLoader();
            _sendAppoinmentRequest({ClubID:ClubID,CoachID:CoachID,groupID:groupid,is_new_coach:is_new_coach},function(response){
            //_sendAppoinmentRequest({ClubID:ClubID,CoachID:CoachID,groupID:groupid},function(response){
               // updateUserPhase({userId:user.user_id,phaseId:phaseid,groupId:groupid,completed:1},function(){
                    var nextphaseid = getNextPhaseIdForGroupId(groupid, true);
                    if (nextphaseid == PHASE_phase1){
                        nextphaseid = PHASE_phase2;
                    }
                   /* Commented Temp to allow the user to continue with the Step counter  */
		           // updateUserStartPhase(groupid, nextphaseid, function(){

                        RemoveProcessingLoader(eobj);
                        //HideLoader();
                        LoadAllQuestionerdata({}, function(){
                            ShowToastMessageTrans(MSG_SUCCESS_APPOINTMENTSENT_EMAIL,_TOAST_LONG);
                            var isclubmedicalinfo	=	0;
                            /**
                             * @param SEARCH_CLUBS[ClubID].t_ismedicalinfo This is club ismedicalinfo
                             */
                            if(SEARCH_CLUBS[ClubID] && SEARCH_CLUBS[ClubID].t_ismedicalinfo){
                                isclubmedicalinfo	=	SEARCH_CLUBS[ClubID].t_ismedicalinfo;
                            }
                            updateUserInfoLocal({r_club_id:ClubID,isclubmedicalinfo:isclubmedicalinfo},function(){
                                //_movePageTo('appointment-coach');
                                HideLoader();
                                _movePageTo('appointment-clientinfo');
                            });
                        });
                   // });
               // });
            });

			getDeviceIdOfCoach(CoachID,user_id);

        }
    } catch(e) {
        ShowExceptionMessage("sendAppoinmentRequest", e);
    }
}
/*
function ShowCoachBasedOnTheDistance(){
    logStatus("Calling ShowCoachBasedOnTheDistance", LOG_FUNCTIONS);
    try {

    } catch(e) {
        ShowExceptionMessage("ShowCoachBasedOnTheDistance", e);
    }
}*/
function openReqAppointmentSteps(ClubId,CoachUserID,elem){
    logStatus("Calling openReqAppointmentSteps", LOG_FUNCTIONS);
    try {
        var coachname = $(elem).find('strong').html();
        //(_CoachList[CoachUserID].userimage)
        var coachimg = $(elem).find('.coachicon_'+CoachUserID).attr('src');
        var _coachname = coachname.split(' ');
        var coach_firstname = _coachname[0];
        //$('#pnlcoachresult').hide();
                
        //$('.coachinfodetail').show();
        $('#coachicon').attr('src',coachimg);
        
        UpdateLastSelectedCoach(_CoachList[CoachUserID]);
        //$('.coach_name').text(coach_firstname);
		$('.coach_name_span').text(coach_firstname);
		
        //$('#appointment_title').hide();
        var $appointmentSlct=$('#appointmentSlct');
        $appointmentSlct.attr('clubid',ClubId);
        $appointmentSlct.attr('coachid',CoachUserID);
        $('#appointmentSlct').trigger('click');
    //_movePageTo('appointment-clientinfo',{ClubId:base64_encode(ClubId),CoachUserID:base64_encode(CoachUserID)});
    } catch(e) {
        ShowExceptionMessage("openReqAppointmentSteps", e);
    }
}
function savePersonalInfo(obj){
    logStatus("savePersonalInfo", LOG_FUNCTIONS);
    try{
        //var client_dob			=	$("#client_dob").val();
        //var client_length		=	$("#client_length").val();
        //var client_weight		=	$("#client_weight").val();
        var client_gender		=   '';//$("#gender").val();
        var ClubId	=	GetUrlArg('ClubId',true,"0");
        var CoachUserID	=	GetUrlArg('CoachUserID',true,"0");
        /****************/
        var phone = $("#phno").val();
        /****************/
        if(validateSteps('.clientinfo')){
            //if(getAgeByDOB($('#client_dob').val())>=13){
            if(validPhone(phone)){
                 AddProcessingLoader(obj);
                 
                _updateClientInformation({
                    userId:app_tempvars('regclientid'),
                    //dob:client_dob,
                    //height:client_length,
                    //weight:client_weight,
                    phone: phone,
                    userGender:client_gender
                },function(){
                    //updateUserInfoLocal({dob:client_dob,height:client_length,weight:client_weight,gender:client_gender});
                    updateUserInfoLocal({phone: phone,gender:client_gender});
                    RemoveProcessingLoader(obj);
                    //HideLoader();
                    _movePageTo('coach-thankyou');
                });      
            }else{
                //ShowToastMessageTrans(MSG_EXCEPTION_PHONE, _TOAST_LONG);
                ShowAlertMessage(GetLanguageText(MSG_EXCEPTION_PHONE));
                //alert("Enter valid 10 digit number");
                $("#phno").focus();
                return;
            }
            
        }
    } catch(e){
        ShowExceptionMessage("savePersonalInfo", e);
    }
}
function LoadCoachInformation(){
    logStatus("LoadCoachInformation", LOG_FUNCTIONS);
    try{
        //var coachdetailcnt = COACH_DETAIL_PARA.split(MARKER_QUERY_PAGEBREAK);
        /*var clientdetail = '<div class="swiper-wrapper">';
        $.each(coachdetailcnt,function(key,value){
            clientdetail+='<div class="welcome_ctn_bx swiper-slide" >'+value+'</div>';
            console.log(value);
        });
        clientdetail+='</div><div class="swiper-pagination"></div>';
        $('.coachdetail').html(clientdetail);
        var clientinfodetailpage = new Swiper ('.coachdetail',{
            preventClicks:true,
            slideToClickedSlide:false,
            mode:'horizontal',
            loop: false,
            autoHeight:false,
            pagination: '.swiper-pagination',
            paginationClickable: true,
            spaceBetween:5
            //swipePagination('.coachdetail');
        });*/
    }catch(e){
        ShowExceptionMessage("LoadCoachInformation", e);
    }
}
function selectClientAppointment(eobj){
	$(".page-title h4 .one").text(GetLanguageTextLocal("7"));
    logStatus("selectClientAppointment", LOG_FUNCTIONS);
    try{
        var $appointmentSlct=  $('#appointmentSlct');
        var ClubId  = $appointmentSlct.attr('clubid');
        var CoachUserID  = $appointmentSlct.attr('coachid');

        AddProcessingLoader(eobj);
        ShowLoader();
        sendAppoinmentRequest(ClubId,CoachUserID,this);
    }catch(e){
        ShowExceptionMessage("selectClientAppointment", e);
    }
}
function proceedClient(){
    logStatus("proceedClient", LOG_FUNCTIONS);
    try{
        _movePageTo('dashboard');
    }catch(e){
        ShowExceptionMessage("proceedClient", e);
    }
}
//function cancelAppointment(eobj){
function cancelAppointment(){
$(".page-title h4 .one").text("SELECT COACH");
    logStatus("cancelAppointment", LOG_FUNCTIONS);
    try{
        //AddProcessingLoader(eobj);
        $('.coachinfodetail').hide();
        $('#pnlcoachresult').show();
        $('#appointment_title').show();
    }catch(e){
        ShowExceptionMessage("cancelAppointment", e);
    }
}
function etidPersonalInfo(){
    logStatus("etidPersonalInfo", LOG_FUNCTIONS);
    try{
        var login_info = getCurrentLoginInfo();
        var user = getCurrentUser();
        if(!$.isEmptyObject(user) && !$.isEmptyObject(login_info)){
            //var dob = login_info.user.dob;
            var gender	=	(user.gender==0 ) ? 'Male':'Female';
            //$('#client_dob').val(dob);
            //$('#client_length').val(login_info.user.height);
            //$('#client_weight').val(login_info.user.weight);
            $('#phno').val($.trim(login_info.user.phone));
            /*if(login_info.user.height){
                 $("#idlength label, #idlength input").addClass("ipfilled");
            }
            if(login_info.user.weight){
                 $("#idweight label, #idweight input").addClass("ipfilled");
            }*/
            var $gender = $('#gender');
            //$gender.val(login_info.gender);
            $gender.val(user.gender);
            $gender.trigger("change");
        }
    }catch(e){
        ShowExceptionMessage("etidPersonalInfo", e);
    }
}
/*function swipeScrollTop(){
    $(".home_welcome_ctn_bx ").scrollTop(0);
}*/
function showtext(){
     $('.info-popup').show();
        UpdateDynamicPageForId('.info-content',APPOINTMENT_CLIENTINFO,function(content,pagetitle){
     });
}

function getDeviceIdOfCoach(coachid,user_id){
	 logStatus("Calling getdevicecoach", LOG_FUNCTIONS);
        try {
        var postdata = {};
         postdata.coach_id  =   coachid;
		 postdata.user_id  =   user_id;
         postdata.is_reporting=  BASE_URL+"/reporting/index.php/voucherlist/getDeviceId";
         getdeviceid(postdata,function(response){

                if(response.status==1){
              console.log(response.msg);
                }
				else{
					 console.log(response.msg);
				}
            
        });
    }
     catch(e) {
        ShowExceptionMessage("getdevicecoach", e);
    }
}
