$(document).ready(function(){
	$(".page-title h4 .one").text(GetLanguageTextLocal("0"));
    TrnasIntro();
    logStatus("Calling Welcome.Document.Event.Ready", LOG_FUNCTIONS);
    var appversion = '';
    $('.versionnums').text(appversion);
    try{
        /*
		UpdateDynamicPageForId('.home_slider_inner .content',INTRO_PAGE_CONTENT_ID,function(content,pagetitle){
            var introcntstr = content.toString();
            var introcnt = introcntstr.split(MARKER_QUERY_PAGEBREAK);
            //XX var intropagecnt = '<div class="swiper-wrapper">';
            var intropagecnt = '';
			var multing = 1;
            $.each(introcnt,function(key,value){
                intropagecnt+='<div class="welcome_ctn_bx home_welcome_ctn_bx swiper-slide" >'
				+'<div class="comm_banner_img_box">'
					+'<div class="transparent-stripe"></div>'
					+'<img src="images/banner/move'+multing+'.jpg" alt="move12" />'
					//+'<h4>A healthy life </h4>'
					//+'<img class="banner_logo" src="images/movesmartlogo.png" alt="movesmartlogo" />'
				+'</div>'
				+'<div class="home_ctn_padding" >'+value+'</div>'
				+'<div class="slider_btn_bx">'
					+'<button onclick="_movePageTo(\'register\')" id="newuser" class="white_btn transnewuser">I&#39;m new</button>'
					+'<button id="intrologin" onclick="goIntroLogin()" class="white_btn translogin" >login</button>'
				+'</div>'
				+'</div>';
				multing++;
            });
            //XX intropagecnt+='</div><div class="swiper-pagination"></div>';
            $('#intro_cnt').html(intropagecnt);
            $(".comm_banner_img_box h4").text(pagetitle);
            //var intropage = new Swiper ('.home_slider_inner',{
            new Swiper ('.home_slider_inner',{
                preventClicks:true,
                slideToClickedSlide:false,
                mode:'horizontal',
                loop: false,
                autoHeight:false,
                pagination: '.swiper-pagination',
                paginationClickable: true,
				onSlideNextEnd: swipeScrollTop,
				onTransitionEnd: swipeScrollTop
            });
        });
		*/
        initPageEvents();
    } catch (e) {
        ShowExceptionMessage("Welcome.Document.Event.Ready", e);
    }
});
function initPageEvents(){
    /*
    var swiperIntro	=	new Swiper('.home_slider_inner',{

    });
    */
    //var loginlabel	=	is_login() ? GetLanguageText(PAGE_TTL_HOME):GetLanguageText(PAGE_TTL_LOGIN_ON);
    var loginlabel	=	is_login() ? "HOME":GetLanguageText(PAGE_TTL_LOGIN_ON);
	//alert(loginlabel);
    $("#intrologin").html(loginlabel);
}
function goIntroLogin(){
    logStatus("Calling goIntroLogin", LOG_FUNCTIONS);
    try{
        if(is_login()){
            _movePageTo(GetUserActiveTemplate());
        } else{
            _movePageTo('login');
        }
    } catch(e){
        ShowExceptionMessage("goIntroLogin", e);
    }
}
function TrnasIntro(){
    logStatus("TrnasIntro", LOG_FUNCTIONS);
    try{
        TranslateText(".translogin", TEXTTRANS, BTN_SUBMIT);
        TranslateText(".transnewuser", TEXTTRANS, BTN_NEW_USER);
        TranslateText(".transfacebooklogin", TEXTTRANS, TXT_LOGIN_FB_BTN);
        TranslateText(".transintrouser", TEXTTRANS, "385"); 
    } catch(e){
        ShowExceptionMessage("TrnasIntro", e);
    }
}

function swipeScrollTop(){
	$(".home_welcome_ctn_bx ").scrollTop(0);
}