$(document).ready(function(){
    logStatus("Calling Questions Info.Document.Event.Ready", LOG_FUNCTIONS);
    try{
        var grpid	=	GetUrlArg('grpid',true,"0");
        var qsid	=	GetUrlArg('qsid',true,"0");
        var questtransobj = getQuestionTransObjForGroupIdAndQuestId(grpid, qsid);
        var quesinfo = '';
        if (questtransobj){
            //$("#idinfotext").html(questtransobj.question_info);
            quesinfo = questtransobj.question_info;
        }
        var quesinfopagecnt = '<div class="swiper-wrapper">';
        //$.each(introcnt,function(key,value){
            quesinfopagecnt+='<div class="welcome_ctn_bx swiper-slide" >'+quesinfo+'</div>';
    //	});
        quesinfopagecnt+='</div><div class="swiper-pagination"></div>';
        $('#idinfotext').html(quesinfopagecnt);
        swipePagination('#idinfotext');
    } catch (e) {
        ShowExceptionMessage("Questions Info.Document.Event.Ready", e);
    }
});