var _HR_DATA = [];
var _CHART_MIN_HR_DATA = [];
var _CHART_MAX_HR_DATA = [];
var _ChartObj;
var _CurrTime=0;
var _ActiveTrainingActivity = false;
var _HrTriggersTimer=0;
var _PreHr	=	0;
var _DataHR	=	[];
var _HRZoneStarted = false;
$(document).ready(function(){
    var User	=	getCurrentUser();
    var userimage = User.userimage;
    var first_name = User.first_name;
    var imgname	= userimage;
    $('.username').text(first_name);
    updateprofileimage(PROFILEPATH + imgname);
    var deviceid = _getCacheValue('lasttestdeviceid');
	$(".page-title h4 .one").text('');
	 var template = GetCurrentPath();
        if (template == 'hrmrate') {
			$(".page-title h4 .one").text("OVERZICHT TRAINING");
		}
    _ActiveTrainingActivity = getActiveTrainingActivityObj();
    if (!$.isEmptyObject(_ActiveTrainingActivity)){
        BLEDeviceConnect(deviceid);
    }else{
        ShowAlertMessage("Invalid Activity. Please try again later");
    }
    SetTrainingDetails();
});

function SetTrainingDetails(){
    logStatus("SetTrainingDetails", LOG_FUNCTIONS);
    try {
        //var traingobj = GetLastTrainingObject();
        /**
         * @param  cweek.f_points This is current week points
         * @param  cweek.f_weeknr This is week label
         * @param  cweek.totalpoints This is week totalpoints
         */
        var cweek	=	getCurrentTrainingWeekObj();
        var fitlevel = getCurrentFitlevel();
        var daytarget = getGoalPerDayBasedOnFitLevelAndWeekGoal(fitlevel, cweek.f_points);
        var daypoint = getCurrentDayPointsFromTrainingWeek();
        var traineddays = getNoOfTraingsCompletedFromTrainingWeek();
        $(".week_label").html("Week " + cweek.f_weeknr);
        //$("#idweekacheivedpoint").html(Math.round(cweek.totalpoints));
        //$("#idweekacheivedpoint").val(Math.round(cweek.totalpoints));
		$("#idweekacheivedpoint").val(cweek.totalpoints);
        $("#idpointstoachive").val(cweek.f_points);
        //$("#idpointstoachive").html(cweek.f_points);
       // $("#credit_daytarget").html(daytarget);
        $("#credit_daytarget").val(daytarget);
        $("#idprevcreditpoint").val(daypoint);
       // $("#credit_points").html(Math.round(daypoint));
       //new added code by jeet on 11 April to set initial value of credit to 0
        //$("#credit_points").val(Math.round(daypoint));
        $("#credit_points").val(0);
        //code end
		var maxdate = cweek.max;
		var mindate = cweek.min;
		var maxdate = maxdate.split("-");
		var mindate = mindate.split("-");

		var diffdate = maxdate[2]-mindate[2];
		if(diffdate>=0)
		{
			var datenew = new Date();
			var curr_date = datenew.getDate();
			var datediff = (curr_date-mindate[2])+1;
		}
		//comment the code 13-june-2017
        //traineddays = (daypoint > 0)?traineddays:traineddays+1;
		traineddays = datediff;
		
       // $("#training_day").html(traineddays);	//Adding current day as well
        $("#training_day").val(traineddays);	//Adding current day as well
    } catch (e) {
        ShowExceptionMessage("SetTrainingDetails", e);
    }
}

function PrepareChart(){
    logStatus("PrepareChart", LOG_FUNCTIONS);
    try {
        var chartheights = $(window).height()-($(".ftr_bx_two").height()+$("header").height()+$(".panel_cnt_header").height()+270);
        if (chartheights > 100){
            $("#idhrchartobj").css("height", chartheights);
        }
    } catch (e) {
        ShowExceptionMessage("PrepareChart", e);
    }
}

function DrawHRChart(){
    logStatus("DrawHRChart", LOG_FUNCTIONS);
    try {
        PrepareChart();
        /**
         * @param   _ActiveTrainingActivity.hrzone This is heart rate zone
         */
        var maxpoint = _ActiveTrainingActivity.hrzone.max+5;
        var minpoint = _ActiveTrainingActivity.hrzone.min-5;
        _ChartObj = new CanvasJS.Chart("idhrchartobj",{
            title :{
                text: ""
            },
            axisX:{
                title: "",
                /*interval : 1,*/
                // minimum: 0,
                lineThickness: 0,
                lineColor: "#fff",
                tickLength:0,
                gridColor: "#fff",
                valueFormatString:" ",
                margin: 0
            },
            axisY:{
                title: "",
                lineThickness: 0,
                lineColor: "#fff",
                gridColor: "#fff",
                tickLength:0,
                minimum: minpoint,
                maximum: maxpoint,
                margin: 0,
                stripLines:[
                {
                    value:_ActiveTrainingActivity.hrzone.min,
                   label: _ActiveTrainingActivity.hrzone.min,
                    color: "#44909A",
                    labelBackgroundColor: ""
                },{
                    value:_ActiveTrainingActivity.hrzone.max,
                    label: _ActiveTrainingActivity.hrzone.max,
                    color: "#44909A",
                    labelBackgroundColor: ""
                }
                ],
                // interval: 1,
                valueFormatString:" "
            },
            data:[
                    /*
                  {
                    color: "#44909A",
                    type: "line",
                    markerSize: 1,
                    showInLegend: false,
                    name: "Min",
                    dataPoints: _CHART_MIN_HR_DATA,
                    label: _ActiveTrainingActivity.hrzone.min
                  },
                    {
                    color: "#44909A",
                    type: "line",
                    markerSize: 1,
                    showInLegend: false,
                    name: "Max",
                    dataPoints: _CHART_MAX_HR_DATA
                  },*/
                    {
                    color: "#E6433E",
                    type: "spline",
                    markerSize: 1,
                    dashStyle: 'shortdot',
                    showInLegend: false,
                    name: "HRData",
                    dataPoints: _HR_DATA
                  }
              ]
        });
         _ChartObj.render();
    } catch (e) {
        ShowExceptionMessage("DrawHRChart", e);
    }

}

function OnHeartRateConnectedCallBack(connecteddevice){
    logStatus("OnHeartRateConnectedCallBack", LOG_FUNCTIONS);
    try {
        StartBLENotificationProcess(connecteddevice.id, OnHeartRateDataSuccessCallBack, OnHeartRateDataErrorCallBack);
        DrawHRChart();
    } catch (e) {
        ShowExceptionMessage("OnHeartRateConnectedCallBack", e);
    }
}

function OnHeartRateDisconnectCallBack(){
    logStatus("OnHeartRateDisconnectCallBack", LOG_FUNCTIONS);
    try {
        ShowToastMessage('Device disconnected');
    } catch (e) {
        ShowExceptionMessage("OnHeartRateDisconnectCallBack", e);
    }
}

function OnHeartRateDataErrorCallBack(reason){
    logStatus("OnHeartRateDataErrorCallBack", LOG_FUNCTIONS);
    try {
        ShowAlertMessage('HR Device Error Reading HR Data:' + reason);
    } catch (e) {
        ShowExceptionMessage("OnHeartRateDataErrorCallBack", e);
    }
}

/*ASYNCH:1*/
function OnHeartRateDataSuccessCallBack(buffer){
    logStatus("OnHeartRateDataSuccessCallBack", LOG_FUNCTIONS);
    try {
    //if( ( (!_STOPPED) || (_CoolDownStarted) ) && isValidTestMonitorPage()){
    var data = new Uint8Array(buffer);
    var hrdata =  data[1];
 //   $("#idhearratedata").html(hrdata);
    $("#idhearratedata").val(hrdata);

    var curdatetime = new Date();
    if (_CurrTime == 0){
        _CurrTime = curdatetime;
    }
    var hrtime = Math.round((curdatetime - _CurrTime)/1000);
    var inmin = hrtime/60;
    if ((_HR_DATA.length == 0) || (_HR_DATA[_HR_DATA.length-1].x != inmin)){
        /*var popindex=	_HR_DATA.length-1;
        var pretime	=	_HR_DATA[popindex].x;
        var prehr	=	_HR_DATA[popindex].y;
        var timediff=	(inmin-pretime);*/
        if(_PreHr!=hrdata) {
            //Says HR changed
            var timediff=	(hrtime-_HrTriggersTimer);
            console.log(hrtime +"-" + _HrTriggersTimer+" = "+timediff);
            _DataHR.push({hr:hrdata,timediff:timediff});
            _HrTriggersTimer	=	hrtime;
            _PreHr = hrdata;
        }

        var graphdata = {
                x: inmin,
                y: hrdata
            };
        //_PreHr	=	;

        _HR_DATA.push(graphdata);
        //console.log(inmin+'-'+_ActiveTrainingActivity.hrzone.max);
        /**
         * @param   _ActiveTrainingActivity.hrzone This is heart rate zone
         */
        _CHART_MIN_HR_DATA.push({ x: inmin, y: _ActiveTrainingActivity.hrzone.min});
        _CHART_MAX_HR_DATA.push({ x: inmin, y: _ActiveTrainingActivity.hrzone.max});
        //XX _CHART_MIN_HR_DATA = [{ x: 0, y: 100}, { x: 10, y: 100}]
        //XX _CHART_MIN_HR_DATA = [{ x: 0, y: parseFloat(_ActiveTrainingActivity.hrzone.min)}, { x: inmin, y: parseFloat(_ActiveTrainingActivity.hrzone.min)}]
        //XX _CHART_MAX_HR_DATA = [{ x: 0, y: _ActiveTrainingActivity.hrzone.max}, { x: inmin, y: _ActiveTrainingActivity.hrzone.max}]
        RefreshHRData();
        if (!_HRZoneStarted){
            if((hrdata>=_ActiveTrainingActivity.hrzone.min) && (hrdata<=_ActiveTrainingActivity.hrzone.max)){
                _HRZoneStarted = true;
            }
        }else{
            var cuurentcredits	=	parseValidFloat(getCreditData(_DataHR,_ActiveTrainingActivity,hrtime));
            var prevcredit = parseValidFloat($("#idprevcreditpoint").val());
            var credits = cuurentcredits+prevcredit;
            //new added code by jeet on 11 April to commented the below code as it was not required according to my logic
            //$("#idcurrcreditpoint").val(cuurentcredits);//This needs to be round off. Currently for testing it is 2 decimals
            //$("#credit_points").html(credits.toFixed(2));//This needs to be round off. Currently for testing it is 2 decimals
            //code end
        }

        //$("#training_time").html(formatHHMMSSFromSec(hrtime));
        $("#training_time").val(formatHHMMSSFromSec(hrtime));
        //new added code by jeet on 11 April to call function to alert and set credits
        checktimeAndalert(hrtime,cuurentcredits);
        //code end
     }
    } catch (e) {
        ShowExceptionMessage("OnHeartRateDataErrorCallBack", e);
    }
}
//new added code by jeet on 11 April to create function to alert time by minutes and setting credits
function checktimeAndalert(time,creds=0)
{ try{
    var newcreds = (creds != '' && creds != 'undefined')? creds:0;
    if(time!=0 && time%60==0){
        var timepassed = time/60;
        $("#idcurrcreditpoint").val(newcreds.toFixed(2));
        $("#credit_points").val(newcreds.toFixed(2));
        ShowToastMessage(timepassed+" minuten gepasseerd");
    }
    }catch(e){
        ShowExceptionMessage("checktimeAndalert", e);
    }

}
//code end
function RefreshHRData(){
    _ChartObj.render();
}

function CallBackFromCardioMachineMatrixdata(machinedata){

}
function getCreditData(hrdata,activityobject,time){
    logStatus("getCreditData", LOG_FUNCTIONS);
    logStatus(time, LOG_DEBUG);
    try {
        //var creditpoint	=	0;
        var creditpointtime	=	0;
        /**
         * @param   activityobject.hrzone This is heart rate zone
         */
        $.each(hrdata,function(idx,ed){
            if(ed.hr>=activityobject.hrzone.min && ed.hr<=activityobject.hrzone.max){
                creditpointtime+=parseInt(ed.timediff);
            }
        });
        /**
         * @param   activityobject.activitypoint This is activity point
         * @param   activityobject.activityperiod This is activity perooid
         */
        var pointspermin = (activityobject.activitypoint/activityobject.activityperiod);
        var validtimepermin = creditpointtime/60;
        return pointspermin*validtimepermin;
    }catch(e){
        ShowExceptionMessage("getCreditData", e);
    }
}
/*
function getDayPointsByTrainingDate(trainingObject,currentDate){
    logStatus("getDayPointsByTrainingDate", LOG_FUNCTIONS);
    try {
        var weekdatebreak	=	trainingObject
    }catch(e){
        ShowExceptionMessage("getDayPointsByTrainingDate", e);
    }
}
function CheckAndGetCredit(credittime){
    //console.log("CheckAndGetCredit:"+credittime);
    // var cweek	=	getCurrentTrainingWeekObj();
}

function getTotalTrainingAchivePoints(trainingobject,ondate){
    logStatus("getTotalTrainingAchivePoints", LOG_FUNCTIONS);
    try {
        var achivepoints	=	[];
        var retobj	=	{};
        retobj.totalpoints	=	0;
        /**
         * @param   trainingobject._achivedpointscount This is achieved point count
         * @param   trainingobject._achivedpoints This is achieved point
         * /
        if(trainingobject._achivedpointscount>0){
            achivepoints	=	(trainingobject._achivedpointscount==1) ? [trainingobject._achivedpoints]:trainingobject._achivedpoints;
        }
        $.each(achivepoints,function(i,row){
            retobj.totalpoints	+=	parseInt(row.points);
        });
        return retobj;
    }catch(e){
        ShowExceptionMessage("getTotalTrainingAchivePoints", e);
    }
}
*/
function SaveCardioTraining(){
    logStatus("SaveCardioTraining", LOG_FUNCTIONS);
    try {
        var traingobj = GetLastTrainingObject();
        var selectedgrpid = getlastActiveGroupId();
        var ActiveFaseId = 2; // change the phase because phase id was save wrong in db.
        /**
         * @param   traingobj.f_cardiotrainingid This is cardio training id
         * @param   traingobj.f_trainingtype This is ctraining type
         */
        var trainingid = traingobj.f_cardiotrainingid;
        var trainingtype = traingobj.f_trainingtype;
        var phaseid = ActiveFaseId;
        var groupid = selectedgrpid;
        //var curstatus = POINT_STATUS_NEW;
		var curstatus = 4; // change the status due to the not show the credit immediately after training.
        var hrdata = JSON.stringify(_DataHR);
        var creditdttm = getDateTimeByFormat('Y-m-d');
        var points = $("#idcurrcreditpoint").val();
        var refid = 0;
        var reftypeid = 0;
        var obj = $("#idsavecardiotraining");
        CheckAndDisconnectBT();
        AddProcessingLoader(obj);
        postpointachieveddata(trainingid, trainingtype, phaseid, groupid, curstatus, hrdata, creditdttm, points, refid, reftypeid, function(response){
            if(response.status=='success')
            {
                UpdateQuestionariesObjectWithGeneralQuestions(response.data);
                ble.stopScan();
                RemoveProcessingLoader(obj);
                CheckDashboardAndMovePageTo('dashboard', false);  
            }
            
        });
    }catch(e){
        ShowExceptionMessage("SaveCardioTraining", e);
    }
}