var _CreditMoveTimer;
var _TransmitTotalDelay	=	2;//seconds
var _IndivitualDelay	=	0.10;//seconds

var _TotalCreditPoints;
var _Redeemed;
var _ProcessCredit;

$(document).ready(function(){
    logStatus("Calling Event.CreditFlow.Load", LOG_FUNCTIONS);
    try {
        var cuserinfo	= getCurrentUser();
        var firstname = cuserinfo.first_name;
        $("#idusername").html(firstname);

        var RedeemCredits	=	GetCredits();
        if(parseInt(RedeemCredits.redeemable)!=0) {
            //Credit transmission
            _TotalCreditPoints	=	RedeemCredits.redeemable;
            ProcessMovingCredits();
        } else {
            DashboardCreditsData(RedeemCredits);
        }
        InitPageEvents();
        if ($('#resultchart').length){
            //var chart	=	getSolidGaugeChart('resultchart');
            getSolidGaugeChart('resultchart');
        }
        window.onresize = function() {
            logStatus("Calling window.onresize", LOG_FUNCTIONS);
            if ($('#resultchart').length){
                getSolidGaugeChart('resultchart');
            }
        }
    } catch(e){
        ShowExceptionMessage("Event.CreditFlow.Load", e);
    }
});
function InitPageEvents(){
    logStatus("Calling InitPageEvents", LOG_FUNCTIONS);
    try {

    } catch(e){
        ShowExceptionMessage("InitPageEvents", e);
    }

}
function loadCreditPoints(){
    logStatus("Calling loadCreditPoints", LOG_FUNCTIONS);
    try {
        if(!_Redeemed){
            _Redeemed	=	0;
        }
        var creditper	=	getCreditPercentage(_ProcessCredit);
        var redeemper	=	getCreditPercentage(_Redeemed);
        var fullheight	=	$(".graph_bg_progress").height();
        var cht		=	GetActualHeightPixel(creditper,fullheight);
        var redht	=	GetActualHeightPixel(redeemper,fullheight);
        $(".move_graph_left .graph_center_bg").css("height",cht);
        $(".move_graph_right .graph_center_bg").css("height",redht);
        $("#credits").val(_ProcessCredit);
        $("#redeem").val(_Redeemed);

    } catch(e){
        ShowExceptionMessage("moveCreditPoints", e);
    }
}
function moveCreditPoints(){
    logStatus("Calling moveCreditPoints", LOG_FUNCTIONS);
    try {
        var delay	=	GetTimeDelayByPoints();
        if(_CreditMoveTimer){
            clearTimeout(_CreditMoveTimer);
        }
        _CreditMoveTimer = setTimeout(function(){
            var incvalue = (_ProcessCredit<1 && _ProcessCredit>0) ? _ProcessCredit:1;
            _ProcessCredit =	_ProcessCredit - incvalue;
            _Redeemed		=	_TotalCreditPoints - _ProcessCredit;
            loadCreditPoints();
            console.log(_ProcessCredit + " : " +_TotalCreditPoints+ ": +" + _Redeemed);
            if((_TotalCreditPoints-_Redeemed)>0){
                moveCreditPoints();
            } else {
                clearTimeout(_CreditMoveTimer);
                UpdateRedeemCredits();
            }
        },delay*1000);
    } catch(e){
        ShowExceptionMessage("moveCreditPoints", e);
    }
}
/**
 * @return {number}
 */
function GetTimeDelayByPoints(){
    logStatus("Calling GetTimeDelayByPoints", LOG_FUNCTIONS);
    try {
        var mintranspoints	=	(_TransmitTotalDelay/_IndivitualDelay);//seconds
        if(_TotalCreditPoints<mintranspoints){
            return (_TransmitTotalDelay/parseInt(_TotalCreditPoints));
        }
        return _IndivitualDelay;
    } catch(e){
        ShowExceptionMessage("GetTimeDelayByPoints", e);
    }
}
function getCreditPercentage(points,total){
    if(!total){
        total	=	_TotalCreditPoints;
    }
    return parseInt(((+points)/total)*100);
}
function UpdateRedeemCredits(){
    logStatus("Calling UpdateRedeemCredits", LOG_FUNCTIONS);
    try {
        updateAchivePoints(null,function(response){
            UpdateQuestionariesObjectWithGeneralQuestions(response.data);
            _movePageTo('credit-dashboard');
        });
    } catch(e){
        ShowExceptionMessage("UpdateRedeemCredits", e);
    }
}
function ProcessMovingCredits(){
    logStatus("Calling ProcessMovingCredits", LOG_FUNCTIONS);
    try {
        _ProcessCredit	=	_TotalCreditPoints;
        loadCreditPoints();
        moveCreditPoints();
    } catch(e){
        ShowExceptionMessage("ProcessMovingCredits", e);
    }
}
function DashboardCreditsData(RedeemCredits){
    logStatus("Calling DashboardCreditsData", LOG_FUNCTIONS);
    try {
        var total	=	parseInt(RedeemCredits.inprocess)  + parseInt(RedeemCredits.redeemed);
        var creditper	=	getCreditPercentage(RedeemCredits.inprocess,total);
        var redeemper	=	getCreditPercentage(RedeemCredits.redeemed,total);
        $("#creditsinprocess").val(Math.round(RedeemCredits.inprocess));
        $("#redeemedcredits").val(Math.round(RedeemCredits.redeemed));
        $(".move_graph_left .graph_center_bg").css("height",creditper);
        $(".move_graph_right .graph_center_bg").css("height",redeemper);
    } catch(e){
        ShowExceptionMessage("DashboardCreditsData", e);
    }
}
/**
 * @return {string}
 */
function GetActualHeightPixel(percent,fulllength){
    return ((percent/100)*fulllength).toFixed(2);
}