$(document).ready(function(){
	var template = GetCurrentPath();
	if(template == 'edit-coachprofile'){
		$(".page-title h4 .one").text('COACH-INFO');
	}
    TranslateEditProfile();
    TranslateGenericText();
	$(".edit_ctn_one").slideUp();
	$(".edit_ctn_two").slideDown();
   // $(".page-title h4").text("Coach Info");
    $(".formfocus_ipentry input,.formfocus_ipentry select").trigger("blur");

		$(".page-title h4 .one").text('');
		 var template = GetCurrentPath();
        if (template == 'edit-coachprofile') {
			$(".page-title h4 .one").text("COACH INFO");
		}
    $(".edit_twoshow").click(function(){
    	$(".edit_ctn_one").slideUp();
    	$(".edit_ctn_two").slideDown();
    });
    
    //searchCoachforuser();
    getCoachInfo();
});

function TranslateEditProfile(){
    logStatus("TranslateEditProfile", LOG_FUNCTIONS);
    try {
     TranslateText(".transgetpicture", TEXTTRANS, TEX_GET_IMAGE);
     }catch(e) {
        ShowExceptionMessage("TranslateEditProfile", e);
    }
}


function getCoachInfo()
{   logStatus("Calling getCoachInfo", LOG_FUNCTIONS);
    try{
        var User = getCurrentUser();
        var login_info = getCurrentLoginInfo();
        
        _data = {};
        
        if(!$.isEmptyObject(User) && !$.isEmptyObject(login_info)){ 
            _data.user_id = User.user_id;
            _getCoachInfo(_data,function(response){
                if(response.status==1)
                {   
                    $('#changecoach').show();
                    var coachInfo = response.data;
                    updateCoachprofileimage(PROFILEPATH + coachInfo.userimage);
                    var $coachfirstname = $('#coachfirstname');
                    var $coachlastname = $('#coachlastname');
                    var $coachemail = $('#coachemail');
                    var $coachphone = $('#coachphone');
                    $('.loginusername').text(coachInfo.first_name);
                    $coachfirstname.val(coachInfo.first_name);
                    
                    $coachlastname.val($.trim(coachInfo.last_name));
                    $coachemail.val(coachInfo.email);
                    $coachphone.val(coachInfo.phone);
                    
                }else{
                    ShowAlertMessage(GetLanguageText(MSG_NOCOACH_SELECT));
                    
                }
                

            });

        }
        

    }catch(e){
        ShowExceptionMessage("getCoachInfo", e);
    }
    
}
function updateCoachprofileimage(givenurl) {
    logStatus("Calling updateCoachprofileimage", LOG_FUNCTIONS); 
    try{
        getValidImageUrl(givenurl, function (url) {
                  
            $('#popupcoachprofile').attr('src', url);
            
        
        });
    }catch(e){
        ShowExceptionMessage("updateCoachprofileimage", e);
    }
    
}