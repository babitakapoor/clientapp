/*
* Controller JS file 
* Page : 'securitypage'*/

/*jQuery Events - Starts*/ 
$(document).ready(function(){
    TranslateGenericText();
    TranslateSecurityPage();
    $('#sec_quessave').click();
    //sequrityQuestionsLoad(function(){});
});	
/*jQuery Events - Ends*/
/*Function - Starts*/
/*PK changed 20160204*/
function sendRequest(eobj){
    logStatus("Calling sendRequest", LOG_FUNCTIONS);
    try{
         if(validateSteps('.questions_list')){
        var email = GetUrlArg('email',true,"");
        if(email!=''){
            AddProcessingLoader(eobj);
            sendPasswordrequest('checkSecurityAnswers',email,function(response){
                ShowToastMessage(response.message, _TOAST_LONG);
                RemoveProcessingLoader(eobj);
                        if(response.status_code == 1){
                    _movePageTo('login');
                }else{
                    ShowToastMessage("E-mail bestaat niet", _TOAST_LONG);
                    _movePageTo('login');
                    //return null;
                }
            },'json');
        }
    }
    }catch (e) {
        ShowExceptionMessage("sendRequest", e);
    }
}

function TranslateSecurityPage(){
    logStatus("Calling TranslateSecurityPage", LOG_FUNCTIONS);
    try{
     TranslateText(".transansqus", TEXTTRANS, TEX_ANSWER_SEQURITY_QUESTION);
     }catch (e) {
        ShowExceptionMessage("TranslateSecurityPage", e);
    }

}
/*Function - Ends*/	