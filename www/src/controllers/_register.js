$(function() {
    TranslateGenericText();
    TranslateRegister();
	$(".page-title h4 .one").text(GetLanguageText(BTN_REGISTER));
    $(".formfocus_ipentry input").trigger("blur");
    $(".formfocus_ipentry select").trigger("change");
    $("#gender").trigger("change");
    UpdateDynamicPageForId('.terms_condition',TERMS_AND_CONDITIONS_ID);
     
    var langHTML = "";
    var $language= $("#language");
    $language.html('<option value="0" selected="selected">'+GetLanguageText(TXT_PLEASE_WAIT)+'</option>');
    var selecttext ='';
	langHTML += '<option value="0" >'+GetLanguageText(TXT_SELECT_LANGUAGE)+'</option>';
    getlanguages(function(response){
        $.each(response,function(key,value){
            // var selected = (DEFAULT_LANG==value.language_id) ? 'selected':'';
            // if(DEFAULT_LANG==value.language_id){
                 // selecttext = value.title
            // }
            langHTML+= '<option value="'+value.language_id+'">'+value.title+'</option>';
        });
        $language.html(langHTML);
       // $("#language_text").html(selecttext);
        $language.trigger("change");
    });
});
function registerMember(obj){
    logStatus("registerMember", LOG_FUNCTIONS);
    try {
        var $rpassword= $('#rpassword');
        var requestPassword		= $rpassword.val();
        var cpwd	=	$("#cpassword").val();
        var isTermsAccepted	=	$("#termscond").find('input').is(":checked");
        var phone = $("#phno").val();
        if(validateSteps(".loginform_bx.step_one"))  {
            //DisableProcessBtn("#cliregister", false);
            if(requestPassword==cpwd){
                //ConfirmPrivacyPolicy(function(){
                if($('#regdob').val()!=''){
                    if(getAgeByDOB($('#regdob').val())>=13){
                        if(validPhone(phone))
                        {
                            isAgreePrivacyPolicy(isTermsAccepted,obj);
                        }
                        else
                        {   ShowToastMessageTrans(MSG_EXCEPTION_PHONE, _TOAST_LONG);
                            //ShowAlertMessage(GetLanguageText(MSG_EXCEPTION_PHONE));
                            //alert("Enter valid 10 digit number");
                            $("#phno").focus();
                            return;
                        }
                        
                    }else{
                        ShowToastMessageTrans(MSG_EXCEPTION_AGE, _TOAST_LONG);
                        return;
                    }
                }else{
                     return;
                }   
               
               /*/ if(isTermsAccepted) {

                    var clientinfo	=	{};
                    clientinfo.first_name	= $('#firstname').val();
                    clientinfo.name 		= $('#name').val();
                    clientinfo.email 		= $('#email').val();
                    clientinfo.phno			= $('#phno').val();
                    clientinfo.gender		= $('#gender').val();
                    clientinfo.imgval		= $('#imgval').val();
                    clientinfo.regdob		= $('#regdob').val();
                    clientinfo.requestPassword		= $rpassword.val();
                    clientinfo.language	=	$("#language").val();
                    //var cpwd	=	$("#cpassword").val();
                    //if(clientinfo.requestPassword==cpwd){
                        AddProcessingLoader(obj);
                        if(validateSteps('.step_one')) {
                             ShowLoader();
                            _registerMember(clientinfo,function(response){
                                RemoveProcessingLoader(obj);
                                if(response.success==1){
                                    //ShowToastMessageTrans(MSG_SUCCESS_CLIENT_ADD,_TOAST_LONG);
                                    var userid	=	(response.signup_user && response.signup_user > 0 ) ? response.signup_user:0;
                                    USER_LANG	=	clientinfo.language;
                                    TranslateGenericText();
                                    TranslateRegister();
                                    sequrityQuestionsLoad(function(){
                                        HideLoader();
                                        app_tempvars('regclientid',userid);
                                        $('.loginform_bx.step_one').hide();
                                        $('.loginform_bx.step_two').show();
                                    });
                                } else {
                                    HideLoader();
                                    ShowToastMessageTrans(MSG_EXCEPTION_EMAIL_EXISTS,_TOAST_LONG);
                                }
                            });
                        }
                } else {
                    //Do nothing..
                    ShowToastMessage("Please confirm Terms & condition",_TOAST_LONG);
                }
                */
            } else {
                ShowToastMessage(GetLanguageText(MSG_MISMATCH_PASSWORD));
            }
        }
    } catch(e){
        ShowExceptionMessage("registerMember", e);
    }
}
function isAgreePrivacyPolicy(isTermsAccepted,obj){
    if(isTermsAccepted) {

        var clientinfo	=	{};
        clientinfo.first_name	= $('#firstname').val();
        clientinfo.name 		= $('#name').val();
        clientinfo.email 		= $('#email').val();
        clientinfo.phno			= $('#phno').val();
        clientinfo.gender		= $('#gender').val();
        clientinfo.imgval		= $('#imgval').val();
        clientinfo.regdob		= $('#regdob').val();
        clientinfo.requestPassword		= $('#rpassword').val();
        clientinfo.language	=	$("#language").val();
        //var cpwd	=	$("#cpassword").val();
        //if(clientinfo.requestPassword==cpwd){
            AddProcessingLoader(obj);
            if(validateSteps('.step_one')) {
                 ShowLoader();
                _registerMember(clientinfo,function(response){
                    RemoveProcessingLoader(obj);
                    if(response.status==1){
                        //ShowToastMessageTrans(MSG_SUCCESS_CLIENT_ADD,_TOAST_LONG);
						
                        var userid	=	(response.signup_user && response.signup_user > 0 ) ? response.signup_user:0;
                        USER_LANG	=	clientinfo.language;
                        TranslateGenericText();
                        TranslateRegister();
                        sequrityQuestionsLoad(function(){
                            HideLoader();
							console.log('REGCLIENT ID ===='+userid);
                            app_tempvars('regclientid',userid);
                            $('.loginform_bx.step_one').hide();
                            $('.loginform_bx.step_two').show();
                        });
                    } else {
                        HideLoader();
						
						// EMAIL ALREADY EXIST ERROR COMES HERE
                        ShowToastMessageTrans(MSG_EXCEPTION_EMAIL_EXISTS,_TOAST_LONG);
                    }
                });
            }
    } else {
        //Do nothing..
        ShowToastMessageTrans(MSG_PLZ_CONFIRM_TERMS,_TOAST_LONG);
    }
}
function saveSequrityAnswers(obj){
    logStatus("saveSequrityAnswers ASHIT", LOG_FUNCTIONS);
    try{
        var clientid	=	app_tempvars('regclientid');
		
        var question	=	{};
        $(".sq_ques").each(function(i,eachitem){
            var idstr	=	$(eachitem).attr("id");
            var answer	=	$(eachitem).val();
            var _qid	=	idstr.split('sqQues_')[1];
            question[_qid]=answer;
        });
        if(clientid){
            if(validateSteps('.questionval')) {
                AddProcessingLoader(obj);
                _saveSequrityAnswers('SecurityQuesAns',clientid,question,function(response){
                    RemoveProcessingLoader(obj);
					console.log('RESPONSE ========== '+response.success);
                    if(response.success==1){
                        $('.register.step_two').hide();
                        $('.register.step_three').show();
                        /*MK Needs to change this code structure - after completed registeration functionalities*/
                        regUserData(clientid,function(responsedata){
                            var logindata = responsedata.user;
                            if(logindata.logintype==LOGINTYPE_SOCIALMEDIA){
                                getLoginByUsernamePassword(logindata.email,logindata.password,function(response){
                                    if(response.status_code==1){
                                        updateAppLogin(response);
                                    }
                                });
                            } else {
                                thanksyoupage();
                                // _movePageTo('login');
                            }
                            /*LOGIN_INFO	=	responsedata;
                            USER		=	responsedata.user;
                            _setCacheArray('user',responsedata.user);
                            _setCacheArray('login_info',responsedata);
                            app_tempvars('compregstep',1);
                            _movePageTo('complete-registeration');*/
                        });
                    }
                });
            }
        }
    }catch (e) {
		console.log("EEE========"+e);
        ShowExceptionMessage("saveSequrityAnswers", e);
    }
}
$(document).delegate(".placeholderlabel","click",function(){
    $(this).hide();
});
/*SN added register language type 20160512*/
$(document).delegate("#language","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#language_text").html(selecttext);
});
$(document).delegate(".ipfocus_ph","blur",function(){
    var $self	=	$(this);
    var checkval=	$self.attr("onempty") ? $self.attr("onempty"):'';
    if($self.val()==checkval){
        $self.parent().find(".placeholderlabel").show();
    } else {
        $self.parent().find(".placeholderlabel").hide();
    }
});
/*
$(document).delegate("#clientprofileupload","change",function(e){
    var formData = new FormData(_('profilepicform'));
    formData.append('imagefile', 'image');
    formData.append('uploadtype',UPLOAD_TYPE_USER_PROFILE);
    formData.append('modeUploadonly',1);
    formData.append('image', $('input[type=file]')[0].files[0]);
    updateProfileImage('updateProfileimage',formData,function(data){
        var img = PROFILEPATH+data.trim();
        $('#imgval').val(data.trim());
        $('#userprofile').remove();
        var htmlcnt = '<img src="'+img+'" id="userprofile" class="set_img">';
        $('.getpic').before(htmlcnt);
    });
});
*/
function TranslateRegister(){
    logStatus("TranslateRegister", LOG_FUNCTIONS);
    try{
        TranslateText(".transfacebook", TEXTTRANS, BTN_FACEBOOK);
        TranslateText(".tanssecurity", TEXTTRANS, TEX_SECURITY);
        TranslateText(".transdob", PHTRANS, TEXT_DOB);
        TranslateText(".transagree", TEXTTRANS, TXT_AGREE);
        TranslateText(".transdisagree", TEXTTRANS, TXT_DISAGREE);
        TranslateText(".transpasword", PHTRANS, PH_PASSWORD);
        TranslateText(".transrepasword", PHTRANS, PH_RE_PASSWORD);
        TranslateText(".transacreepolicy", HTMLTRANS, TXT_AGREE_PRIVACY_POLICY);
    }catch (e) {
        ShowExceptionMessage("TranslateRegister", e);
    }
}
/*
function ConfirmPrivacyPolicy(onAgreeTerms,onDisagreeTerms){	
    $(".resitboxpop_outer").show();
    $(".privacy_bg").show();
    $("#idagreeprivacy").on("click",function(){
        closeTermsPopup();
        onAgreeTerms(this);
    });
    $("#iddisagreeprivacy").on("click",function(){
        closeTermsPopup();
        onDisagreeTerms(this);
    });
}
*/
function closeTermsPopup(){
    $(".resitboxpop_outer").hide();
    $(".privacypopup").hide();
    $(".privacy_bg").hide();
    $("#iddisagreeprivacy, #idagreeprivacy").off("click");
}
function thanksyoupage(){
    _movePageTo('thankspage');
}