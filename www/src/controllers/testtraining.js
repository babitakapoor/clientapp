var userTestId = '';

$(document).ready(function(){
    TranslateGenericText();
    TransActivity();
    logStatus("test training", LOG_FUNCTIONS);
    try{
        var User	=	getCurrentUser();
        var userimage = User.userimage;
        var first_name = User.first_name;
        var imgname	= userimage;
		//$('.one').text('TRAINING');
		$(".page-title h4 .one").text('');
		 var template = GetCurrentPath();
        if (template == 'test-training') {
			$(".page-title h4 .one").text("TRAINING");
		}
		else if(template == 'activity_welcome'){
			$(".page-title h4 .one").text('SELECTEER ACTIVITEIT');
		}
		$('.welcomusetxt').text('');
		//$('.welcomusetxt').text(GetLanguageTextLocal("0")+' '+GetLanguageTextLocal("21")+' '+first_name);
		$('.welcomusetxt').text(GetLanguageTextLocal("31"));
        //$('.username').text(first_name);
        updateprofileimage(PROFILEPATH + imgname);
        var testobj = GetAnalysedTest();
        // getUserFitlevelAndPoints(User.user_id,function(response){
        var cweek	=	getCurrentTrainingWeekObj();
        
        $('#fit_level').html(testobj.fitness_level);
        var fit_level = (testobj.fitness_level && testobj.fitness_level!=0) ?testobj.fitness_level:1;
        $('.fitlevelimg img').attr('src','images/piramide-'+fit_level+'.png');
        //alert(testobj.fitness_level);
        $('#achieve_point').html(cweek.f_points);
        /**
         * @param  testobj.user_test_id This is user test id
         * @param  testobj.iant_p This is inant_p
         * @param  testobj.fitness_level This is  fitness_level
         * @param  value.activityid This is activity id
         * @param  value.activityimage This is activity image
         * @param  value.activityname This is activity name
         */
        userTestId = testobj.user_test_id;
        var speedafteranalysis = testobj.iant_p;//_ClientData_g.SPMed_._DeflPoint_.X;
        
        //console.log(activitydata);
        getpointsper_activity_for_testid_iantw(userTestId, getWattFromSpeed(speedafteranalysis, testobj.weight), function(activitydata){
            UpdateTrainingActivityBasedDetailObj(activitydata);
            var activityimgcnt = '<ul class="quest_list_box">';
            $.each(activitydata,function(index,value){
				if(User.r_language_id==3){
					var activity_name = value.activitynameD;
				}
				else{
					var activity_name = value.activityname;
				}
                var $class = (index<=3) ?'':'activityimgcls';
                activityimgcnt+='<li class="'+$class+'"><span actid ="'+value.activityid+'" hrreq="'+value.hrzonereq+'" class="question_image slctactivity" onclick="" >'+
                                    '<img src="'+ACTIVITYIMGPATH+value.activityimage+'" alt="" />'+
                                    '</span>'+
                                    '<h5>'+activity_name+'</h5>'+
                                    '</li>';
            });
            activityimgcnt+= '</ul>';
            $('#slctactivityimg').html(activityimgcnt);
            $('.activityimgcls').hide();
        });
        

        // });

    } catch (e) {
        ShowExceptionMessage("test training", e);
    }
});
$(document).delegate(".slctactivity",'click',function(){
    UpdateLastSelectedActivityIdSession($(this).attr("actid"));
    UpdateLastSelectedActivityIdHrReqSession($(this).attr("hrreq"));
    var hrreqstatus = GetLastSelectedActivityIdHrReqSession();
    
    if(hrreqstatus==1)
    {
        _movePageTo("hr_monitor");
    }
    else
    {
        _movePageTo("no_hr_monitor");
    }
    
});
function trainingStart(){
    logStatus("Calling trainingStart", LOG_FUNCTIONS);
    try {
        var currenttrainingweekobj = getCurrentTrainingWeekObj();
        
        if (currenttrainingweekobj){
            _movePageTo("activity_welcome");
        }else{
            ShowAlertMessage(EXCEPTION_TRAINING_NOT_ACTIVE);
        }
    } catch(e){
        ShowExceptionMessage("trainingStart", e);
    }
}
/*
function goHrmRate(){
    logStatus("Calling goHrmRate", LOG_FUNCTIONS);
    try {
        _movePageTo("hrmrate");
    } catch(e){
        ShowExceptionMessage("goHrmRate", e);
    }
}
*/
function findOtherActivity(){
    logStatus("Calling findOtherActivity", LOG_FUNCTIONS);
    try {
        $('.activityimgcls').show();
		$('#otheractivity').css('display','none');
		$('#fewactivity').css('display','block');
    } catch(e){
        ShowExceptionMessage("findOtherActivity", e);
    }
}
function findfewActivity(){
    logStatus("Calling findfewActivity", LOG_FUNCTIONS);
    try {
        $('.activityimgcls').hide();
		$('#otheractivity').css('display','block');
		$('#fewactivity').css('display','none');
    } catch(e){
        ShowExceptionMessage("findfewActivity", e);
    }
}
function TransActivity(){
    logStatus("TransActivity", LOG_FUNCTIONS);
    try {
    //TranslateText(".transotheractivity", TEXTTRANS, BTN_FIND_ACTIVITY);
    TranslateText(".transselectactivity", TEXTTRANS, BTN_SELECT_ACTIVITY);
    TranslateText(".transtraining", TEXTTRANS, TXT_TRAINING);
	TranslateTextLocal(".transfitlevel", TEXTTRANS, "18");
	TranslateTextLocal(".transpointtoacheive", TEXTTRANS, "19");
	TranslateTextLocal(".transotheractivity", TEXTTRANS, "20");
	TranslateTextLocal(".transfewactivity", TEXTTRANS, "32");
	
    } catch(e){
        ShowExceptionMessage("TransActivity", e);
    }
}