//var _WeekDays	=	{0:'Sunday',1:'Monday',2:'Tuesday',3:'Wednesday',4:'Thursday',5:'Friday',6:'Saturday'};
var _WeekDays	=	{0:GetLanguageText(TXT_SUNDAY),	1:GetLanguageText(TXT_MONDAY), 2:GetLanguageText(TXT_TUESDAY),3:GetLanguageText(TXT_WEDNESDAY),4:GetLanguageText(TXT_THURSDAY),5:GetLanguageText(TXT_FRIDAY),	6:GetLanguageText(TXT_STATURDAY)};

var _EducationSwiperObj	={};
$(document).ready(function(){
    logStatus("Calling Questions.Document.Event.Ready", LOG_FUNCTIONS);
    try{
        var selectedgrpid = getlastActiveGroupId();
        var ActiveFaseId = getCurrentFaseForGroupId(selectedgrpid);
        BuildFase1QuestionsByFaseIdAndGroupId(ActiveFaseId,selectedgrpid);
        InitPageEvents();//Inherited from questions.js
        $(".question_icon.info_btn").click(function(){
            $(".activityinfo_popup").show();
                var dayidx	=	$(".clsdaytabs.scroll_tabs_active").index();
                dayidx	=	(dayidx>1) ? dayidx-1:dayidx;
                swipePagination(".educationpanel"+dayidx);
                /*_EducationSwiperObj[0]	=	new Swiper($(".educationpanel"+dayidx),{
                        mode:'horizontal',
                        loop: false,
                        autoHeight:false,
                        pagination: '.swiper-pagination',
                        paginationHide: false,
                        paginationClickable: true,
                        nextButton: '.swiper-button-next',
                        prevButton: '.swiper-button-prev'
                });*/
            //});
            //
            /*

            */
        });
		/*26-july-2017*/
		var tot = $('#totalquesvalue').val();
		//$('.swiper-containers').html('<p>'+ 1 + '/' + tot +'</p>');
		//change on 17-05-17 
		var int_startslide = $(".swiper-slide-active").attr("data-slide-index");
		localStorage.setItem('int_startslide',int_startslide);
		if(int_startslide>1){
			var startslide = parseInt(int_startslide) + 1;
			$('.swiper-containers').html('<p>'+ startslide + '/' + tot +'</p>');
		}
		else{$('.swiper-containers').html('<p>'+ 1 + '/' + tot +'</p>');}
        /*
        var slideroptions	=	{
            preventClicks:true,
            slideToClickedSlide:false,
            mode:'horizontal',
            loop: false,
            autoHeight:false,
            virtualTranslate: false,
            pagination: '.swiper-pagination',
            paginationHide: false,
            paginationClickable: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev'
        };
        slideroptions.initialSlide=defaultSlideIndex;*/
        //var educationpageswiper= new Swiper ('.educationswiper',slideroptions);
    } catch (e) {
        ShowExceptionMessage("Questions.Document.Event.Ready", e);
    }
});
function BuildFase1QuestionsByFaseIdAndGroupId(PhaseId,GroupId,onHandleSuccess){
    logStatus("Calling BuildFase1QuestionsByFaseIdAndGroupId", LOG_FUNCTIONS);
    try{
        var HTML	=	'';//Wrapper - Check;
        var progressHtml=	'<div class="perfill_outer"><div class="perfill"></div></div>';
        var $pnlcontent	=	$("#question-forms");
        var $progresspanel	=	$("#progress_group");
        var $tabday		=	$(".scroll_tabs");
        var isActivityType	=	false;
        var educationcontent	=	'';
        var DayMenu	=	'';
        var DefSelDay	=	0;
        var AllowSwipeSave	=	{};
        if(_Questierdatainfo){
            var groupobj	=	_Questierdatainfo['group'+GroupId];
            if(!$.isEmptyObject(groupobj)){
                var phasestartdt	=	groupobj['_phasestartdate'];
                
                var curdate			=	new Date(phasestartdt.split(" ")[0]);
                
                var dateExpiration	=	new Date(phasestartdt.split(" ")[0]);
                var today			=	new Date();
                dateExpiration.setDate(dateExpiration.getDate()+QUESTION_OPEN_DAYS);
                /*if(ComapreDate(dateExpiration,today,'less_than')){
                    ShowAlertMessage(GetLanguageText(EXP_EXPIRED_GO_FOR_TEST));
                    _movePageTo('dashboard');
                    return;
                }*/
                

                var phase	=	(groupobj._phase) ? groupobj._phase:{};
                var activity	=	{};
                var questions	=	[];
                var activityanswers	=	[];
                //var answerbydate	=	{};
                var activitypoint	=	{};
                var weekcredits		=	0;
                var credit_limit	=	10000;
                var steps			=	0;
                var todaysteps			=	0;
                var tsteps			=	0;
                var earned_credit	=	0;
                //var hdnHtml			=	'';
                var eachcredit	=	0;
                //var formateddt	=	getDateTimeByFormat('Y-m-d 00:00:00');
                isActivityType	=	(GroupId==GROUP_MOVESMART && PhaseId==PHASE_phase1);
                //DayMenu	='<span class="slider_left_arrow" onclick=" goToPrevDay()"><</span>';
                DayMenu	+=	'<ul><li class="fl clsdaytabs">&nbsp;</li>';
                var education	=	getEducationContent(GroupId);

                
                for(var day=1;day<=QUESTION_OPEN_DAYS;day++) {
                    var progActiveClass = '';
                    var strday = _WeekDays[curdate.getDay()];
                    var formateddt = getDateTimeByFormat('Y-m-d 00:00:00', curdate);

                    //var showeducontent	=	'display:none';
                    var isAllowSave = false;
                    AllowSwipeSave[day] = false;
                    
                    if (ComapreDate(curdate, today, 'equal')) { //MK Temp Fix 2016-07-26

                        if (ComapreDate(curdate, today, 'less_than_or_equal')) {
                            isAllowSave = true;
                            AllowSwipeSave[day] = true;
                        }
                        if (ComapreDate(curdate, today, 'less_than_or_equal')) {
                            var strclass = 'fl';
                            var strdispalay = 'display:block';//'display:none';
                            //var showeducontent	=	'display:block';//'display:none';
                            var addProgress = false;
                            if (ComapreDate(curdate, today, 'equal')) {
                                DefSelDay = day;
                                strday = GetLanguageText(TXT_TODAY);//'Today';
                                strclass = 'fl scroll_tabs_active';
                                strdispalay = 'display:block';
                                addProgress = true;//change needed to show selected
                            }

                            var parentid = 'idface1questionset' + day;
                            if (ComapreDate(curdate, today, 'equal')) {
                                DayMenu += '<li class="' + strclass + ' clsdaytabs" seldate=' + (formateddt.split(" ")[0]) + ' onclick="selectTab(this,\'' + day + '\',' + GroupId + ')">' + strday + '</li>';
                                DayMenu += '<span class="daycountcls">('+GetLanguageText(TXT_DAY)+' ' + day + ' '+GetLanguageText(TXT_OF)+' ' + QUESTION_OPEN_DAYS + ')</span>';
								//DayMenu += '<span class="daycountcls"></span>';
                            }

                            HTML += '<div class="question-forms_eachday pnlday' + day + '" id="' + parentid + '" style="width:100%;' + strdispalay + '" day="'+day+'">' +
                                '<div class="swiper-wrapper">';
                            if (day == QUESTION_OPEN_DAYS) {
                                HTML += '<div class="day_credit move_graph_left">' +

                                    '<div class="newwel_left creditchart" id="chartphase"></div>' +
                                    '<div class="comm_coin_box">' +
                                    '<h4 class="comm_h4">Credits</h4>' +
                                    '<input type="text" placeholder="000" class="phase_credit" disabled="disabled"/> ' +

                                    '</div>' +
                                    '</div>';
                                HTML += '<div class="day_credit move_graph_right">' +

                                    '<div class="newwel_left creditchart" id="curphase"></div>' +
                                    '<div class="comm_coin_box">' +
                                    '<h4 class="comm_h4">credits</h4>' +
                                    '<input type="text" placeholder="000" class="curcredit" /> ' +

                                    '</div>' +
                                    '</div>';
                            } else {
                                /*31-august-2017 if (isActivityType) {
                                    if (phase._activitiescount > 0) {
                                        activity = (phase._activitiescount == 1) ? phase._activities : phase._activities[0];
                                    } 31-august-2017*/
                                    /**
                                     * @param   activity._pointscount This is point count
                                     * @param   activity._points This is points
                                     * @param   activity.activity_seconds This is activity second
                                     * @param   activity.perweek This is activity per week
                                     * @param   activity.weekcredits This is week credits
                                     * @param   activity.activitysteps This is activity  steps
                                     * @param   activity._answerscount This is answer count
                                     * @param   activity._answers This is answers
                                     * @param   activity._actualvalues This is actual values
                                     * @param   activity._actualvalues.actmv_otherdata This is actual values monitoe variable other data
                                     * @param   activity.activity_id This is activity id
                                     * @param   activity.monitorvariable_id This is monitor variable id
                                     * @param   questions._answercount This is questions answer count
                                     */
                               /* 31-august-2017     if(!$.isEmptyObject(activity)){
                                        activitypoint	=	(activity._pointscount==1) ? activity._points:activity._points[0];
                                        
                                        var total_seconds =	activitypoint.activity_seconds;
                                        var perweek		=	activitypoint.perweek;
                                        weekcredits		=	activitypoint.weekcredits;
                                        eachcredit		=	(weekcredits / perweek);
                                        credit_limit	=	activity.activitysteps ? activity.activitysteps :10000;
                                        if(activity._answerscount>0){
                                            activityanswers	=	(activity._answerscount==1) ? [activity._answers]:activity._answers;
                                            var actanswer	=	GetAnswerByDate(activityanswers,formateddt.split(" ")[0]);
                                            steps =	0;
											todaysteps = 0;
                                            if(actanswer._actualvalues){
                                                steps	=	(actanswer._actualvalues.actmv_otherdata) ? actanswer._actualvalues.actmv_otherdata:0
                                                todaysteps	=	(actanswer._actualvalues.actmv_otherdata) ? actanswer._actualvalues.actmv_otherdata:0
                                            }
                                            $.each(activityanswers,function(i,answer){
                                                earned_credit+=parseInt(answer.value);
                                                if(answer._actualvalues){
                                                    tsteps	+=	parseInt(answer._actualvalues.actmv_otherdata);
                                                }
                                            });
                                        }
                                        var user	=	getCurrentUser();
                                        var lblsteps=	GetLanguageText(TXT_STEPS);
                                        var transstep = GetLanguageText(TXT_ENTER_STEPS_HERE);
                                        if(user.stepgame_type==STEP_GAME_BYMINUTES){
                                            lblsteps	=	"Minutes";
                                            transstep	=	GetLanguageText(TXT_ENTER_MINUTES_HERE);
                                            var tseconds = (total_seconds/credit_limit) * tsteps;
                                            var tseconds2 = (total_seconds/credit_limit) * todaysteps;
											
                                            tsteps = parseFloat(tseconds/60);
                                            tsteps = tsteps.toFixed(2);
                                            tsteps = Math.round(tsteps);
											todaysteps = parseFloat(tseconds2/60);
                                            todaysteps = todaysteps.toFixed(2);
                                            todaysteps = Math.round(todaysteps);
                                        }       

                                        HTML+=	'<div class="comm_panel_bx swiper-slide" >'+ 31-august-2017*/
                                                   /* '<div class="step_left edison">'+
                                                    '<div class="newwel_left creditchart" id="chartphase'+day+'"></div>'+
                                                    '<h4 class="comm_h4">Credits</h4>'+
                                                    '<input type="text" placeholder="000" class="phase_credit"  disabled="disabled"/>'+
                                                '</div>'+ */
                                            /* 31-august-2017    '<div class="step_right">'+
                                                    '<input type="hidden" id="credit'+day+'" value="' + eachcredit + '" />'+
                                                    '<input type="hidden" id="credit_limit'+day+'" value="' + credit_limit + '"/>'+
                                                    '<input type="hidden" id="lastearned_credit'+day+'" value="' + earned_credit + '"/>'+
                                                    '<input type="hidden" id="week_credits'+day+'" value="' + weekcredits + '"/>'+
                                                    '<input type="hidden" id="total_seconds'+day+'" value="' + total_seconds + '"/>'+
                                                    '<div class="meeting-wrap">'+
                                                        '<h3 id="steps'+day+'" class="input_blue1 generaltotalsteps" >'+GetLanguageTextLocal("17")+' : <strong>' + tsteps + ' ' + lblsteps + '</strong></h3>'+'<h3 class="input_blue1 generaltotalsteps" >'+GetLanguageTextLocal("29")+' : <strong>' + todaysteps + ' ' + lblsteps + '</strong></h3>'+
                                                        '<input type="hidden" id="steps'+day+'" class="input_blue" placeholder="general total : ' + lblsteps + '"  value="general total : ' + tsteps + ' ' + lblsteps + '" readonly="readonly" />'+
                                                    '</div>'+
                                                    '<div class="input_box formfocus_ipentry">'+
                                                        '<input type="hidden" name="exipactivityval'+day+'" id="exipactivityval'+day+'" value="' + steps + '"/>'+
                                                        '<input type="number" class="input_blue shoes" id="ipactivityval'+day+'"  placeholder="' + transstep + '" />'+
                                                    '</div>'+
                                                    '<div class="input_box">'+
                                                        (isAllowSave ?
                                                            '<button class="move-save-btn blue_btn center-left" onclick="saveMonitorActivityAnswer(this,\''+ formateddt + '\',\''+day+'\')">'+GetLanguageText(BTN_SAVE)+'</button>':'')+
                                                    '</div>'+
                                                    '<div class="clear"></div>'+
                                                    '<div class="question_hits" style="width:100%;overflow:hidden">'+
                                                    '</div>'+
                                                    '<input type="hidden" id="activityId'+day+'" value="' + activity.activity_id + '"/>'+
                                                    '<input type="hidden" id="mvkey'+day+'" value="' + activity.monitorvariable_id + '"/>'+
                                                '</div></div>';
                                    }
                                } else { 31-august-2017*/
                                    var answers	=	[];
                                    if(phase._questionscount>0) {
                                       var questions	=	(phase._questionscount==1) ? [phase._questions]:phase._questions;										   
                                          if(IsRequestSentForTestAppointment()){
												var ques_type = 'coach';
												var questions = $.grep(questions, function(e){ 
												return e.ques_type != ques_type; 
												});
										} 
										if(questions._answercount>0){
                                            answers	=	(questions._answercount==1)?[questions._answers]:questions._answers;
                                        }
                                    }
                                    //var sanswer		=	GetAnswerByDate(answers,formateddt.split(" ")[0]);
                                    if(questions.length>0){
                                        incSlideIndex	=	0;
                                        var parentid = 'idface1questionset'+day;
                                        $.each(questions,function(quesidx,question){
                                            var nextQuesId	=	(questions[quesidx+1] && questions[quesidx+1].id) ? questions[quesidx+1].id:0;//getNextGroupQuestionId(groupobj.group_id);
                                            var prevQuesId	=	(questions[quesidx-1] && questions[quesidx-1].id) ? questions[quesidx-1].id:0;//getNextGroupQuestionId(groupobj.group_id);
                                            var template	=	getQuestionTemplate(question,nextQuesId,prevQuesId,'#'+parentid, formateddt.split(" ")[0]);
                                            if(template){
                                                HTML+=	'<div class="swiper-slide" ' +
                                                        'data-slide-index="'+incSlideIndex+'" ' +
                                                        'id="questiontemp_' + question.id + '" ' +
                                                        'dsuffix='+day+' questionId="' + question.id + '">'
                                                            + addIPFormAttributes({
                                                                monitorquestion:question.monitorvariable_id,
                                                                nextId:nextQuesId,
                                                                parentid:(
                                                                    '#'+parentid + ' #questiontemp_' + question.id
                                                                ),
                                                                questiontype:question.type,
                                                                selansdate:curdate
                                                            },question.id+'_d_'+day) + ''
                                                            + template
														//  '<span class="question_icon info_btn" onclick="">Edu</span>
														  +'</div>';
                                                          
                                            }
                                            incSlideIndex++;
                                        });

                                        if(addProgress) {
                                            progressHtml+=	'<label id="progtabgroup'+ groupobj.group_id +'" class="' + progActiveClass + '" >&nbsp;</label>';
                                            //By Sankar Since they dont want them now. progressHtml+=	'<label id="progtabgroup'+ groupobj.group_id +'" class="' + progActiveClass + '" >' + groupobj.group_name + '</label>';
                                        }
                                    }
                              //31-august-2017  }
                            }
                            var clsactstepgame = (isActivityType || day == QUESTION_OPEN_DAYS) ? 'clsactstepgame' : '';
                            educationcontent += '<div class="' + clsactstepgame + ' eduction educationpanel' + day + ' educationswiper" style="display:none">' +
                                getEducationSlider(education['days_' + day]) +
                                '</div>';
                            // '<div class="swiper-button-next slider_left_arrow"  ></div>'+
                            // '<div class="swiper-button-prev slider_right_arrow" ></div>';

                            /*<div class="edu-prev"><</div>'+
                             '<div class="edu-next">></div><div class="edu-pagination">></div>';*/
                        }
                        //}

                        HTML += '</div>' +
                            '</div>';
                    }
                curdate.setDate(curdate.getDate()+1);
               }
                /*SN commanded education popup pagination next prev arrow
                educationcontent+='<div class="swiper-button-next slider_left_arrow"  ></div>'+
                                        '<div class="swiper-button-prev slider_right_arrow" ></div>'; */
            }
            DayMenu+='<li class="fl clsdaytabs">&nbsp;</li></ul><!--<span class="slider_right_arrow" onclick="goToNextDay()">></span>--><div class="clear" ></div>';
        } else {
            HTML+='<div class="noquestion_box" >No questions found</div>';
        }
        HTML	+=	'</div>';
        //var text=	getQuickWinText();
        $pnlcontent.html(HTML);
        $progresspanel.html(progressHtml);
        $tabday.html(DayMenu);
        var phasecredit = GetPhaseCredits(GroupId,PHASE_phase1);
        var checkphasecredit = GetPhaseCredits(GroupId,PHASE_Check);
        $(".phase_credit").val(parseFloat(checkphasecredit.inprocess).toFixed(0));
        $(".curcredit").val(parseFloat(phasecredit.inprocess).toFixed(0));
        getCircleChart('#chartphase',(parseInt((checkphasecredit.inprocess/100)*10)),GroupId);
        getCircleChart('#curphase',(parseInt((phasecredit.inprocess/100)*10)),GroupId);
        var $education	=	$('.educationcontent');
        var $questionformseachday	=	$('.question-forms_eachday');
        $education.html(educationcontent);
        $tabday.find('li').css("width",(100/3)+'%');
        if(isActivityType) {
            /*A Static Popup is loaded and need to change later for the dynamic contents*/
            //EatfreshTips();
        }
        //var hintsSwipe	= new Swiper ('.question_hits',{
         new Swiper ('.question_hits',{
            autoplay: 2500,
            loop: true
        });

        $questionformseachday.each(function(idx,elemobj){
            /*MK Performance logStatus(elemobj, LOG_DEBUG);*/
            /*MK Performance logStatus(idx, LOG_DEBUG);*/
            var $elemobj 	=	$(this);
            var dayindx = $(this).attr('day');
            var settings	=	{};
            settings.initialSlide	=	defaultSlideIndex;
           // if(!isActivityType) {
                settings.onSlideNextEnd	=	CheckFaseSldierMove;
				settings.onSlidePrevEnd	=	CheckFaseSldierMove;
                settings.onTransitionEnd=	TrackSaveLaseSlide;
          //  }
            if((AllowSwipeSave[dayindx])){
                settings.allowSwipeToNext	=	true;
            }
            _QuestionSliderObj[$elemobj.index()]	= new Swiper($elemobj,settings);

        });
        $education.html(educationcontent);
        /*$(".eduction").each(function(eduidx,elem){
            var $elemobj 	=	$(this);
            $elemobj.css("overflow","hidden");
            _E elemobj.index()]	=	new Swiper($elemobj,{
                    nested:true,
                    slideToClickedSlide:false,
                    mode:'horizontal',
                    loop: false,
                    autoHeight:false,
                    pagination: '.swiper-pagination',
                    paginationHide: false,
                    paginationClickable: true,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev'
            });/** /
        });*/
        //InitOptionPicker(".question_option");
        UpdateRetainedValuesOnBack();
        $questionformseachday.hide();
        $(".clsdaytabs.scroll_tabs_active").trigger("click");
        if(onHandleSuccess){
            onHandleSuccess();
        }
    } catch (e) {
        ShowExceptionMessage("BuildFase1QuestionsByFaseIdAndGroupId", e);
    }
}
/*
function EatfreshTips(day){
    logStatus("Calling EatfreshTips", LOG_FUNCTIONS);
    try {
        var SWIPER_DATA='<div class="lamp_popup">'+
                        '<div class="lamp_popup_header" >'+
                            '<span class="light_icon"></span>'+
                            '<span class="lamp_popup_close" onclick="" >X</span>'+
                        '</div>'+
                        '<div class="move_cnt_tip">'+
                            '<div class="swiper-wrapper">'+
                                '<div class="swiper-slide">'+
                                    '<p class="comm_para"> Try to drink lots of water today.</p>'+
                                    '<p class="comm_para">1. It helps maintain the Balance of Body Fluids.</p>'+
                                    '<p class="comm_para">2. Water can help control calories.</p>'+
                                '</div>'+
                                '<div class="swiper-slide">'+
                                    '<p class="comm_para"> Try to drink lots of water today.</p>'+
                                    '<p class="comm_para">1. It helps maintain the Balance of Body Fluids.</p>'+
                                    '<p class="comm_para">2. Water can help control calories.</p>'+
                                '</div>'+
                                '<div class="swiper-slide">'+
                                    '<p class="comm_para"> Try to drink lots of water today.</p>'+
                                    '<p class="comm_para">1. It helps maintain the Balance of Body Fluids.</p>'+
                                    '<p class="comm_para">2. Water can help control calories.</p>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<button class="white_btn close_btn"  onclick="">'+GetLanguageText(LBL_CLOSE)+'</button></div>';
            ShowTipsPopup(SWIPER_DATA);
            var earfreshcntpopuppage = new Swiper ('.move_cnt_tip',{
                preventClicks:true,
                slideToClickedSlide:false,
                mode:'horizontal',
                loop: false,
                autoHeight:true,
                autoplay:3000,
                pagination: '.swiper-pagination',
                paginationClickable: true,
                spaceBetween:5
            });
    } catch(e) {
        ShowExceptionMessage("EatfreshTips", e);
    }
}
*/
/**
 * description: Get answer by date key
 */
/*
function getAnswersByDateKey(answers){
    logStatus("Calling getAnswersByDateKey", LOG_FUNCTIONS);
    try {
        var retansobj	=	{};
        /**
         * @param   answer.date This is answer date
         * /
        $.each(answers,function(idx,answer){
            retansobj[answer.date]	=	answer;
        });
        return retansobj;
    } catch(e) {
        ShowExceptionMessage("getAnswersByDateKey", e);
    }
}*/
function goToNextDay(){
    logStatus("Calling goToNextDay", LOG_FUNCTIONS);
    try {
        var curindex = $('.clsdaytabs.scroll_tabs_active').index();
        var $clsdaytabs=$('.clsdaytabs');
        var nextindex = curindex+1;
        if(nextindex < $clsdaytabs.length){
            //$(".clsdaytabs.scroll_tabs_active").trigger("click");
            $clsdaytabs.eq(nextindex).trigger("click");
        }
    }catch(e) {
        ShowExceptionMessage("goToNextDay", e);
    }
}
function goToPrevDay(){
    logStatus("Calling goToPrevDay", LOG_FUNCTIONS);
    try {
        var curindex = $('.clsdaytabs.scroll_tabs_active').index();
        var previndex = curindex-1;
        if(previndex!=-1) {
            $(".clsdaytabs").eq(previndex).trigger("click");
        }
    }catch(e) {
        ShowExceptionMessage("goToPrevDay", e);
    }
}

function selectTab(elemobj,selday,GroupId){
    logStatus("Calling selectTab", LOG_FUNCTIONS);
    try {
        var $clsdaytabs	=	$(".clsdaytabs");
        var pnlday	=	'.pnlday'+selday;
        var pnleduday	=	'.educationpanel'+selday;
        var active	=	'scroll_tabs_active';
        $clsdaytabs.removeClass(active);
        var sliderindex;
        sliderindex = +$(elemobj).index() - 1;
        _QuestionSliderObjIndex	=	sliderindex;
        $(elemobj).addClass(active);
        var selDate	=	$(elemobj).attr('seldate');
        $(".question-forms_eachday").hide();
        $(".eduction").hide();
        $(pnlday + "," + pnleduday).show();
        $clsdaytabs.hide();
        $(elemobj).show();
        var $next	=	$(elemobj).next();
        var $prev	=	$(elemobj).prev();
        var $Larrow	=	$(".scroll_tabs .slider_left_arrow");
        var $Rarrow	=	$(".scroll_tabs .slider_right_arrow");
        if($.trim($next.text())==''){
            $Rarrow.hide();
        } else {
            $Rarrow.show();
        }
        if($.trim($prev.text())==''){
            $Larrow.hide();
        } else {
            $Larrow.show();
        }
        $next.show();
        $prev.show();
        LoadPercentageCompleteForPhaseAndGroup(GroupId,new Date(selDate));
        var isActivityType	=	(GroupId==GROUP_MOVESMART);
        if(isActivityType) {
            _EducationSwiperObj[0]	=	new Swiper(".educationpanel"+selday,{
                    nested:true,
                    slideToClickedSlide:false,
                    mode:'horizontal',
                    loop: false,
                    autoHeight:false,
                    pagination: '.swiper-pagination',
                    paginationHide: false,
                    paginationClickable: true,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev'
            });
        }
        //var phasecredit = GetPhaseCredits(GroupId,PHASE_phase1);
        if(typeof($('#chartphase'+selday).html())!='undefined')  {
            var creditindex= getCurrentGroupwiseIndexPercentage();
            var val	=	creditindex[GroupId].creditindex/100;
            val	=	isNaN(val) ? 0:val;
            //var val = isNaN(phasecredit.inprocess/phasecredit.totalcredits)?0:isNaN(phasecredit.inprocess/phasecredit.totalcredits);

            getCircleChart('#chartphase'+selday,'',(parseFloat(val*100)),GroupId);
        }
        /*
        if(_QuestionSliderObj[_QuestionSliderObjIndex]){
            _QuestionSliderObj[_QuestionSliderObjIndex].slideNext();
        }
        //alert(1);
        /*_QuestionSliderObj= new Swiper(pnlday,{

        })*/
    } catch(e) {
        ShowExceptionMessage("selectTab", e);
    }
}
function getEducationContent(GroupId){
    logStatus("Calling getEducationContent", LOG_FUNCTIONS);
    try {
        var eduobj	=	GetEducationObject();
        return (eduobj['groupid_' + GroupId]) ? eduobj['groupid_' + GroupId]:{}
    } catch(e) {
        ShowExceptionMessage("getEducationContent", e);
    }
}
function getEducationSlider(edudayobj){
    logStatus("Calling getEducationSlider", LOG_FUNCTIONS);
    try {
        var contenthtml	=	'';
        if(!$.isEmptyObject(edudayobj)) {
            var edulangobj	=	edudayobj['lang' + USER_LANG];
            /**
             * @param    edulangobj.languagae_desc This is  edulangobj languagae_desc
             */
            if(edulangobj && edulangobj.languagae_desc) {
                var c	=	edulangobj.languagae_desc;
                var pcobj	=	c.split(MARKER_QUERY_PAGEBREAK);
                contenthtml	=	'<div class="swiper-wrapper">';
                $.each(pcobj,function(i,pc){
                    contenthtml+=	'<div class="swiper-slide edu_inner">' + pc + '</div>';
                });
                contenthtml+=	'</div><div class="swiper-pagination"></div>';
            }
        }
        return contenthtml;
    } catch(e) {
        ShowExceptionMessage("getEducationSlider", e);
    }
}
/*****************
 Mind Tab Activation
*******************/
// $(document).ready(function(){
// mindtabactivation();
// });

function mindtabactivation()
{
 var loginInfos = getCurrentLoginInfo();
    var postdata= {};
    postdata.user_id = loginInfos.user.user_id;
    postdata.is_reporting = BASE_URL+'reporting/index.php/mind_switch/checkIsactive';
     mindTabAct(postdata,function(response){
        if(response.status==1){
           localStorage.setItem('favoriteflavor',response.status);
           $('#frst_sd').css('display','block');
                    var a = response.data.mindlink;
           $("#hidenquesval").val(a);
   
   
        }
        else{
            //return response.status;
            $('#scf_fg').css('display','block');
        }
    });
}
function openquesmind()
{
    var as = $("#hidenquesval").val();
    window.open(as, '_system');
}


