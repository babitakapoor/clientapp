var _HR_DATA = [];
var _CHART_MIN_HR_DATA = [];
var _CHART_MAX_HR_DATA = [];
var _ChartObj;
var _CurrTime=0;
var _ActiveTrainingActivity = false;
var _HrTriggersTimer=0;
var _PreHr  =   0;
var _DataHR =   [];
var _HRZoneStarted = false;
$(document).ready(function(){
    var User    =   getCurrentUser();
    var userimage = User.userimage;
    var first_name = User.first_name;
    var imgname = userimage;
    $('.username').text(first_name);
    updateprofileimage(PROFILEPATH + imgname);
    var deviceid = _getCacheValue('lasttestdeviceid');
    _ActiveTrainingActivity = getActiveTrainingActivityObj();
    SetTrainingDetails();
});

function SetTrainingDetails(){
    logStatus("SetTrainingDetails", LOG_FUNCTIONS);
    try {
        //var traingobj = GetLastTrainingObject();
        /**
         * @param  cweek.f_points This is current week points
         * @param  cweek.f_weeknr This is week label
         * @param  cweek.totalpoints This is week totalpoints
         */
        TranslateText(".transtimemsg", TEXTTRANS, TXT_NOHR_TRAIN);
        var cweek   =   getCurrentTrainingWeekObj();
        

        var fitlevel = getCurrentFitlevel();
        var daytarget = getGoalPerDayBasedOnFitLevelAndWeekGoal(fitlevel, cweek.f_points);
        var daypoint = getCurrentDayPointsFromTrainingWeek();
        var traineddays = getNoOfTraingsCompletedFromTrainingWeek();
        $(".week_labele").html("Week " + cweek.f_weeknr);
             
        
        traineddays = (daypoint > 0)?traineddays:traineddays+1;
       
    } catch (e) {
        ShowExceptionMessage("SetTrainingDetails", e);
    }
}


/*function SaveCardioTraining(){
    logStatus("SaveCardioTraining", LOG_FUNCTIONS);
    try {
        var traingobj = GetLastTrainingObject();
        //var cweek =   getCurrentTrainingWeekObj();
        var selectedgrpid = getlastActiveGroupId();
        var ActiveFaseId = getCurrentFaseForGroupId(selectedgrpid);
        /**
         * @param   traingobj.f_cardiotrainingid This is cardio training id
         * @param   traingobj.f_trainingtype This is ctraining type
         
        var trainingid = traingobj.f_cardiotrainingid;
        var trainingtype = traingobj.f_trainingtype;
        var phaseid = ActiveFaseId;
        var groupid = selectedgrpid;
        var curstatus = POINT_STATUS_NEW;
        var hrdata = JSON.stringify(_DataHR);
        var creditdttm = getDateTimeByFormat('Y-m-d');
        var points = $("#idcurrcreditpoint").val();
        var refid = 0;
        var reftypeid = 0;
        var obj = $("#idsavecardiotraining");
        CheckAndDisconnectBT();
        AddProcessingLoader(obj);
        postpointachieveddata(trainingid, trainingtype, phaseid, groupid, curstatus, hrdata, creditdttm, points, refid, reftypeid, function(){
            RemoveProcessingLoader(obj);
            CheckDashboardAndMovePageTo('dashboard', false);
        });
    }catch(e){
        ShowExceptionMessage("SaveCardioTraining", e);
    }
}*/

function SaveNoHrTest(obj)
{   logStatus("SaveNoHrTest", LOG_FUNCTIONS);
    try{

        /**********************/
        var traingobj = GetLastTrainingObject();
        
        //var cweek =   getCurrentTrainingWeekObj();
        var selectedgrpid = getlastActiveGroupId();
        
        var ActiveFaseId = getCurrentFaseForGroupId(selectedgrpid);
        /**
         * @param   traingobj.f_cardiotrainingid This is cardio training id
         * @param   traingobj.f_trainingtype This is ctraining type
         */
        
        var trainingid = traingobj.f_cardiotrainingid;
        
        var trainingtype = traingobj.f_trainingtype;
        
        var phaseid = ActiveFaseId;
        
        var groupid = selectedgrpid;
        
        var curstatus = POINT_STATUS_NEW;
        
        var hrdata = "NULL";
        var creditdttm = getDateTimeByFormat('Y-m-d');
        
        //var points = $("#idcurrcreditpoint").val();
        var refid = 0;
        var reftypeid = 0;
        //var obj = $("#idsavecardiotraining");
        /***********************/
        AddProcessingLoader(obj);
        var ttime= $("#training_minutes").val();
        
        if(checkTrainTimevalid(ttime))
        {
            
            var traingobj = GetLastTrainingObject();
            var User    =   getCurrentUser();
            
            var userId = User.user_id;
            _ActiveTrainingActivity = getActiveTrainingActivityObj();
            var trainingInfo = {};
            trainingInfo.activityid = _ActiveTrainingActivity.activityid;
            trainingInfo.userId = userId;
            trainingInfo.ttime = ttime;
            trainingInfo.fitness_level = traingobj.fitness_level 
            
            _getNoHrTestResult(trainingInfo,function(response){
                if(response.status==1)
                {  
                    var points = response.point;
                    postpointachieveddata(trainingid, trainingtype, phaseid, groupid, curstatus, hrdata, creditdttm, points, refid, reftypeid, function(response){
                        if(response.status=='success')
                        {
                            UpdateQuestionariesObjectWithGeneralQuestions(response.data);
                            RemoveProcessingLoader(obj);
                            CheckDashboardAndMovePageTo('dashboard', false);  
                        }
                        
                    });
                    

                }
                else
                {   RemoveProcessingLoader(obj);
                    return ;
                }
            });
        }else{
            RemoveProcessingLoader(obj);
            ShowAlertMessage(GetLanguageText(TXT_NOHR_TIME_ERR));
        }    

    }
    catch(e){
        ShowExceptionMessage("SaveNoHrTest", e);
    }

}
function checkTrainTimevalid(ttimevalue)
{   logStatus("checkTrainTimevalid", LOG_FUNCTIONS);
    try{
        var _ttimevalue = ttimevalue;
        if(_ttimevalue && _ttimevalue!='')
        {
            if(_ttimevalue > 0)
            {
                return true;
            }
            else
            {
                return;
            }
        }else
        {
            return;
        }   
    }catch(e){
        ShowExceptionMessage("checkTrainTimevalid", e);
    }     
}