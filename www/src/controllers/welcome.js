$(document).ready(function(){
    logStatus("Calling Welcome.Document.Event.Ready", LOG_FUNCTIONS);
	$(".page-title h4 .one").text("CHECK");
    try{
        var appversion = '';
         $('.versionnums').text(appversion);

        TranslateGenericText();
        TransWelcome();
        UpdateDynamicPageForId('#welcome_cnt',WELCOME_PAGE_CONTENT_ID,function(cnt,pagetitle){
			
            $(".comm_banner_img_box h4").text(pagetitle);
        });
        var User	=	getCurrentUser();
        var userimage = User.userimage;
        var first_name = User.first_name;
        var imgname	= userimage;
        $('.username').text(first_name);
		
        updateprofileimage(PROFILEPATH + imgname);

    } catch (e) {
	
		
        ShowExceptionMessage("Welcome.Document.Event.Ready", e);
    }
});
function startCheck(elem){
    logStatus("Calling Welcome.Document.Event.Ready", LOG_FUNCTIONS);
    try{
        //updateUserStartCheck(GROUP_MOVESMART,elem);
        updateUserStartCheck(elem);
        //_movePageTo('phase-check');
    } catch (e) {
        ShowExceptionMessage("Welcome.Document.Event.Ready", e);
    }
}
function TransWelcome(){
    logStatus("TransWelcome", LOG_FUNCTIONS);
    try{
	
     TranslateText(".transtartcheck", TEXTTRANS, BTN_START_CHECK);
     TranslateText(".transactivity", TEXTTRANS, BTN_SELECT_ACTIVITY);
     TranslateText(".transfindactivity", TEXTTRANS, BTN_FIND_ACTIVITY);
     TranslateText(".transstartcheck", TEXTTRANS,BTN_START_CHECK);
     } catch (e) {
        ShowExceptionMessage("TransWelcome", e);
    }
}