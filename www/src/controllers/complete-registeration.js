/*
* Controller JS file 
* Page : 'register'*/
var registeration_step	=	{};
registeration_step.step	=	0;
/*jQuery Events - Starts*/


$(function() {
  TranslateGenericText();
  TranslateCompleteRegister();
});
/**
 * description: document ready ControllerRegister
 */
$(document).ready(function(){
    logStatus("Document.Ready ControllerRegister", LOG_FUNCTIONS);
    try {
        var urldata	=	getPathHashData();//['args'];
        var incomplete_step	=	(urldata.args.step) ? base64_decode(urldata.args.step) :0;
        if(urldata.path=='complete-registeration'){
            var loginuser	=	getCurrentLoginInfo();
            var loginusrdata = (loginuser.user) ? loginuser.user:loginuser;
            // if(incomplete_step==0){
                // if(loginusrdata.personal_track!=''){
                    // var steptrack	=	JSON.parse(loginusrdata.personal_track);
                    // incomplete_step	=	(steptrack.step) ? steptrack.step:0;
                // }
            // }
            //var imgname = loginusrdata.userimage;
            // SetDateAttr("#entrydate",{
                // maxToday:true,
                // defaultToday:true
            // })
            /*if(loginusrdata.userimage!='undefined' || loginusrdata.userimage!==''){
            $('#regusrimg').attr('src',imgpath);
            }else{
                $('#regusrimg').attr('src','images/no_image.png');
            }*/
            // updateprofileimage(PROFILEPATH + imgname);
            /*
            getValidImageUrl(PROFILEPATH + imgname,function(url){
                $('#regusrimg,#footerprofilepic').attr('src',url);
                $('.footuser_icon.user').css({'background-image':'url("'+url+'")'});
            });
            */
            /*var filename = (imgname && imgname!='') ? imgname.split('.').pop():'';
            if(jQuery.inArray(filename, EXT) !== -1){
                $('#regusrimg,#footerprofilepic').attr('src',PROFILEPATH + imgname);
            }else{
                $('#regusrimg,#footerprofilepic').attr('src','images/no_image.png');
            }*/
            $('.regusername').html(loginusrdata.first_name);
            $('#isclubmedicalinfo').val(loginusrdata.isclubmedicalinfo);
            app_tempvars('regclientid',loginusrdata.user_id);
            var $country	=	$("#client_country,#client_doctor_country");
            $country.html('<option selected="selected"">Loading Please wait..</option>');
            /**
             * @param  response.nationalities this is nationalities string
             * @param  row.nationality_id this is nationality Id
             */
            loadMemberOptionList('getNationalityList',function(response){
                if(response.nationalities && (response.nationalities.length>0)){
                    var naionalityoptions	=	'';
                    $.each(response.nationalities,function(_index,row){
                        var selected =(row.nationality_id==23) ?'selected':'';
                        if(row.nationality_id==23){
                            $("#client_countrytext").html(row.nationality_name);
                            $("#client_doctor_countrytext").html(row.nationality_name);
                        }
                        naionalityoptions+='<option value="'+row.nationality_id+'" '+selected+'>'+row.nationality_name+'</option>';
                    });
                    $country.html(naionalityoptions);
                    $country.removeAttr("placeholder");
                }
            });
            var $profession	=	$("#client_profession");
            $profession	.html('<option selected="selected">Loading Please wait..</option>');
            /**
             *  Description:Load Member Profession List
             * @param  response.professions  This is professions List
             * @param  row.profession_id  This is Profession Id
             * @param  row.profession_name  This is Profession Name
             */
            loadMemberOptionList('getProfessionList',function(response){
                var professionoptions	=	'<option value="0">Select Profession</option>';
                if(response.professions && (response.professions.length>0)){
                    $.each(response.professions,function(_index,row){
                         professionoptions	+=	'<option value="' + row.profession_id + '">' + row.profession_name + '</option>';
                    });
                    $profession.html(professionoptions);
                }
            });
            var $activity	=	$(".client_activity");
            /**
             * Description:Load Member Activity List
             * @param  response.activities This is Activities List
             * @param  row.activity_id This is Activity Id
             * @param  row.activity_name This is Activity Name
             */
            loadMemberOptionList('getActivityList',function(response){
                var activity_options	=	'<option value="0">select activity</option>';
                if(response.activities && (response.activities.length>0)){
                    $.each(response.activities,function(_index,row){
                         activity_options	+=	'<option value="' + row.activity_id + '">' + row.activity_name + '</option>';
                    });
                    $activity.html(activity_options);
                }
            });
            $(window).on("resize",function(){
                /*if($(".signaturebox").html()){
                    var signaturebox = $(".signaturebox").width();
                    mineSignature("myCanvas",signaturebox);
                }*/
            });
            //$(window).trigger("resize");
            //mineSignature("myCanvas");
            //MK mineSignature("myCanvas");
            /*SN changed by 20160204*/
            var step_incomplete = parseInt(incomplete_step);
            if((step_incomplete!=0) && (step_incomplete<12)){
                //updateCompletedStep(parseInt(incomplete_step));
                var stepupdated	=	app_tempvars('stepupdated');
                /*MK Added Before Check*/
                if((loginusrdata.isclubmedicalinfo!=1) && (step_incomplete>=2)){
                     _movePageTo('dashboard');
                }else{
                    saveContinueIncompleteCurrentStep(step_incomplete);
                    app_tempvars('compregstep',(step_incomplete + 1));
                }
            }
            if (typeof(doAnimationToButtons) === "defined"){
                doAnimationToButtons(); //Remove  Animation if it is already there.
            }
        }
    } catch(e){
        ShowExceptionMessage("Document.Ready ControllerRegister", e);
    }
});
$(document).delegate('input[name="training_freq"]',"change",function(){
    var $this	=	$(this);
    if($this.is(":checked"))  {
        var $elemval	=	$("#trainingfreq_" + $this.attr("row"));
        $elemval.val($this.val());
    }
});

$(document).delegate('input[name="haveMedication"]',"change",function(){
    var $this=	$(this);
    if($this.is(":checked")) {
        $("#haveMedication").val($this.val());
        var $medicationDesc= $("#medicationDesc");
        if($this.val()==0){
            $medicationDesc.removeClass("req-nonempty");
            $medicationDesc.attr('disabled',"disabled");
            $medicationDesc.css('background-color',"#DCD4D4");
        } else {
            $medicationDesc.addClass("req-nonempty");
            $medicationDesc.removeAttr("disabled");
            $medicationDesc.css('background-color',"#0296ad");
        }
    }
});
$(document).delegate('input[name="howFeelToday"]',"change",function(){
    var $this=	$(this);
    if($this.is(":checked")) {
        $("#howFeelToday").val($this.val());
    }
});
$(document).delegate('input[name="haveStressTest"]',"change",function(){
    var $this=	$(this);
    if($this.is(":checked")) {
        $("#haveStressTest").val($this.val());
        var $stressTestOn = $("#stressTestOn");
        if($this.val()==1)  {
            $stressTestOn.addClass('req-nonempty');
            $stressTestOn.removeAttr("disabled");
            $stressTestOn.css('background-color',"#0296ad");
        } else {
            $stressTestOn.removeClass('req-nonempty');
            $stressTestOn.attr('disabled',"disabled");
            $stressTestOn.css('background-color',"#DCD4D4");
        }
    }
});
$(document).delegate('input[name="haveSmoke"]',"change",function(){
    var $this=	$(this);
    if($this.is(":checked")) {
        $("#haveSmoke").val($this.val());
        var $smokeAmount = $("#smokeAmount");
        if($this.val()==1)  {
            $smokeAmount.addClass('req-nonempty');
            $smokeAmount.removeAttr("disabled");
            $smokeAmount.css('background-color',"#0296ad");
        } else {
            $smokeAmount.removeClass('req-nonempty');
            $smokeAmount.attr('disabled',"disabled");
            $smokeAmount.css('background-color',"#DCD4D4");
        }
    }
});
$(document).delegate("#client_zipcode, #client_doctor_zipcode","change",function(){
    var $this 	=	$(this);
    if($this.val()!=''){
        var $cityoption	=	($this.attr('id')=='client_zipcode')  ? $("#client_place"):$("#client_doctor_place");
        $cityoption.attr("disabled", "disabled");
        $('#client_doctor_placetext').html('Loading Place, Please wait..');
        $('#client_placetext').html('Loading Place, Please wait..');
        $cityoption.html('<option value="0">Loading, Please wait..</option>');
        /**
         * Description:Get City List By zipcode
         * @param  response.CityByZipcode This is City by zipcode list
         * @param   city.city_id This is City Id
         * @param   city.city_name This is City Name
         */
        getCityListByZipCode({zipCode:$this.val()},function(response){
            $('#client_doctor_placetext').html(GetLanguageText(BTN_SELECT_PLACE));
            $('#client_placetext').html(GetLanguageText(BTN_SELECT_PLACE));
            var optionhtml	=	'<option value="0">'+GetLanguageText(BTN_SELECT_CITY)+'</option>';//$cityoption.html(city.city_name);
            if(response.CityByZipcode) {
                var cities 	=	(response.CityByZipcode.length>0) ? response.CityByZipcode:{};
                $.each(cities,function(_index,city){
                    optionhtml+='<option value="' + city.city_id + '">' + city.city_name + '</option>';
                });
            } else{
                optionhtml	=	'<option value="0">Please enter valid zipcode</option>';
            }
            $cityoption.html(optionhtml);
            $cityoption.removeAttr("disabled");
        })
    }
});
/*$(document).delegate("#clientprofileupload","change",function(e){
    var formData = new FormData(_('profilepicform'));
    formData.append('imagefile', 'image');
    formData.append('uploadtype',UPLOAD_TYPE_USER_PROFILE);
    formData.append('modeUploadonly',1);
    formData.append('image', $('input[type=file]')[0].files[0]);
    updateProfileImage('updateProfileimage',formData,function(data){
        var img = PROFILEPATH+data.trim();
        $('#imgval').val(data.trim());
        $('#userprofile').remove();
        var htmlcnt = '<img src="'+img+'" id="userprofile" class="set_img">';
        $('.getpic').before(htmlcnt);
    });
});*/
/*ED - added select option align center 20160205*/
/*MK - Selected text to be updated on the Placeholder text() is changed to html()*/
/*$(document).delegate("#gender","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#gendertext").html(selecttext);
});*/

$(document).delegate("#client_place","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#client_placetext").html(selecttext);
});
$(document).delegate("#client_country","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#client_countrytext").html(selecttext);
});
$(document).delegate("#client_profession","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#client_professiontext").html(selecttext);
});
$(document).delegate(".client_ac_select","change",function(){
    var select_text = $(this).find("option:selected").text();
    $(this).parent().find(".client_ac_select_text").html(select_text);
});
$(document).delegate("#client_doctor_place","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#client_doctor_placetext").html(selecttext);
});
$(document).delegate("#client_doctor_country","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#client_doctor_countrytext").html(selecttext);
});
/*jQuery Events - Ends*/
/*Function - Starts*/
/*
function registerMember(){
    logStatus("registerMember", LOG_FUNCTIONS);
    try {
        if(validateSteps(".register.step_one"))  {
            //DisableProcessBtn("#cliregister", false);
            AddProcessingLoader('.saveclass');
            var clientinfo	=	{};
            clientinfo.first_name	= $('#firstname').val();
            clientinfo.name 		= $('#name').val();
            clientinfo.email 		= $('#email').val();
            clientinfo.phno			= $('#phno').val();
            clientinfo.gender		= $('#gender').val();
            clientinfo.imgval		= $('#imgval').val();
            /**
             * Description:Get Register User Detail List
             * @param  response.signup_user This is register user detail
             * /
            _registerMember(clientinfo,function(response){
                //DisableProcessBtn("#cliregister", true, GetLanguageText(BTN_SAVE));
                    RemoveProcessingLoader('.saveclass');
                if(response.success==1){
                    ShowToastMessageTrans(MSG_SUCCESS_CLIENT_ADD,_TOAST_LONG);
                    var userid	=	(response.signup_user && response.signup_user > 0 ) ? response.signup_user:0;
                    sequrityQuestionsLoad(function(){
                        app_tempvars('regclientid',userid);
                        $('.register.step_one').hide();
                        $('.register.step_two').show();
                    });
                } else {
                    ShowToastMessageTrans(MSG_EXCEPTION_EMAIL_EXISTS,_TOAST_LONG);
                }
            });
        }
    } catch(e){
        ShowExceptionMessage("registerMember", e);
    }
}
*/
/**
 * Description:Load security Questions

 */
function sequrityQuestionsLoad(handleOnSuccess){
    /**
     * * @param  response.securityQuestions This is security question list
     */
    getsecurityQuestions('getSequirityQuestions',function(reponse){
        var _sqQuestions=	reponse.securityQuestions;
        var quesHTML	=	"";
        /**
         * @param  _ques.secure_question_id This is security question id
         */
        $.each(_sqQuestions,function(_index,_ques){
            quesHTML+='<div class="inpt_bx field_in_box ">'+'<input class="blu_input sq_ques req-nonempty" type="text" id="sqQues_'+_ques.secure_question_id+'" placeholder="'+_ques.question+'" />'+'</div>';
        });
        $(".questions_list").html(quesHTML);
        if(handleOnSuccess){
            handleOnSuccess();
        }/*
        setCaptchaContent(".recapchabox",function(reponse){
            /*On load Captcha Success* /
        }); /*Temp commented MK*/
    });
}
/*
function saveSequrityAnswers(){
    logStatus("saveSequrityAnswers", LOG_FUNCTIONS);
    try{
        var clientid	=	app_tempvars('regclientid');
        var question	=	{};
        $(".sq_ques").each(function(i,eachitem){
            var idstr	=	$(eachitem).attr("id");
            var answer	=	$(eachitem).val();
            var _qid	=	idstr.split('sqQues_')[1];
            question[_qid]=answer;
        });
        if(clientid){
            //DisableProcessBtn("#savesecuritybtn", false);
            AddProcessingLoader('.savebtn');
            if(validateSteps('.register.step_two')) {
                _saveSequrityAnswers('SecurityQuesAns',clientid,question,function(response){
                    //DisableProcessBtn("#savesecuritybtn", true, "Save");
                    RemoveProcessingLoader('.savebtn');
                    if(response.success==1){
                        $('.register.step_two').hide();
                        $('.register.step_three').show();
                        /*MK Needs to change this code structure - after completed registeration functionalities* /
                        //regUserData(clientid,function(responsedata){
                        regUserData(clientid,function(){
                            _movePageTo('login');
                            /*LOGIN_INFO	=	responsedata;
                            USER		=	responsedata.user;
                            _setCacheArray('user',responsedata.user);
                            _setCacheArray('login_info',responsedata);
                            app_tempvars('compregstep',1);
                            _movePageTo('complete-registeration');* /
                        });
                    }
                });
            }
        }
    }catch (e) {
        ShowExceptionMessage("saveSequrityAnswers", e);
    }
}
*/
function saveContinueCurrentStep(){
/*SN changed register personalinfo track 20160201 TASK NO 4244*/
    logStatus("saveContinueCurrentStep", LOG_FUNCTIONS);
    try{
        var $body = $('body');
        $body.scrollTop(0);
        var $clsregisterstep = $('.clsregisterstep');
        var $compreg_step_two= $('.compreg_step_two');
        var $compreg_step_three= $('.compreg_step_three');
        var $compreg_step_four= $('.compreg_step_four');
        var $compreg_step_five= $('.compreg_step_five');
        var $compreg_step_six= $('.compreg_step_six');
        var $compreg_step_seven= $('.compreg_step_seven');
        var $compreg_step_eight= $('.compreg_step_eight');
        var $compreg_step_nine= $('.compreg_step_nine');
        var $compreg_step_ten= $('.compreg_step_ten');
        var $compreg_step_eleven= $('.compreg_step_eleven');
        var $compreg_step_twelve= $('.compreg_step_twelve');
        var step	=	app_tempvars('compregstep');
        var isclubmedicalinfo = $('#isclubmedicalinfo').val();
        if (isclubmedicalinfo == ''){
            isclubmedicalinfo = 1;
        }
        if(typeof(step)=='undefined' || step==''){
            step	=	1;
        }
        step	=	parseInt(step);
        switch(step){
            case 1:
                $clsregisterstep.hide();
                $compreg_step_two.show();
                updateCompletedStep(step);
                break;
            case 2:
                if ($compreg_step_two.is(":visible")){
                    if(validateSteps('.compreg_step_two')) {
                        saveAddressInfo(step,function(){
                            ShowToastMessageTrans(MSG_SUCCESS_ADDRESSINFO,_TOAST_LONG);
                            if (isclubmedicalinfo != 1) {
                                _movePageTo('dashboard');
                                //return;
                            } else {
                                $clsregisterstep.hide();
                                $compreg_step_three.show();
                                updateCompletedStep(step);
                            }
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    $compreg_step_two.show();
                }
                break;
            case 3:
                if ($compreg_step_three.is(":visible")){
                    if(validateSteps('.compreg_step_three')) {
                        savePersonalInfo(step,function(){
                            ShowToastMessageTrans(MSG_SUCCESS_PERSONALINFO,_TOAST_LONG);
                            $clsregisterstep.hide();
                            $compreg_step_four.show();
                            updateCompletedStep(step);
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    $compreg_step_three.show();
                }
                break;
            case 4:
                if ($compreg_step_four.is(":visible")){
                    if(validateSteps('.compreg_step_four')){
                        saveProfessionWorkType(step,function(){
                            ShowToastMessageTrans(MSG_SUCCESS_PROFESSION_WORK_TYPE,_TOAST_LONG);
                            $clsregisterstep.hide();
                            $compreg_step_five.show();
                            updateCompletedStep(step);
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    $compreg_step_four.show();
                }
                    break;
            case 5:
                if ($compreg_step_five.is(":visible")){
                    if(validateSteps('.compreg_step_five')){
                        saveClientActivity(step,function(){
                            ShowToastMessageTrans(MSG_SUCCESS_CLIENTACTIVITY,_TOAST_LONG);
                            updateClientCurrentStep(step,function(){
                                $clsregisterstep.hide();
                                $compreg_step_six.show();
                                updateCompletedStep(step);
                            });
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    $compreg_step_five.show();
                }
                break;
            case 6:
                if ($compreg_step_six.is(":visible")){
                    if(validateSteps('.compreg_step_six')){
                        saveGPInfomormation(step,function(){
                            updateClientCurrentStep(step,function(){
                                ShowToastMessageTrans(MSG_SUCCESS_GPINFOMORMATION,_TOAST_LONG);
                                $clsregisterstep.hide();
                                $compreg_step_seven.show();
                                    updateCompletedStep(step);
                            });
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    $compreg_step_six.show();
                }
                break;
            case 7:
                if ($compreg_step_seven.is(":visible")){
                    if(validateSteps('.compreg_step_seven')){
                        saveMedicalInjury(step,function(){
                            updateClientCurrentStep(step,function(){
                                ShowToastMessageTrans(MSG_SUCCESS_MEDICALINJURY,_TOAST_LONG);
                                $clsregisterstep.hide();
                                $compreg_step_eight.show();
                                updateCompletedStep(step);
                            });
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    //$('body').scrollTop();
                    $compreg_step_seven.show();
                }
                break;
            case 8:
                if ($compreg_step_eight.is(":visible")){
                    if(validateSteps('.compreg_step_eight')){
                        saveMedicationInfo(step,function(){
                            updateClientCurrentStep(step,function(){
                                ShowToastMessageTrans(MSG_SUCCESS_MEDICAL_INFO,_TOAST_LONG);
                                $clsregisterstep.hide();
                                $compreg_step_nine.show();
                                updateCompletedStep(step);
                            });
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    //$('body').scrollTop();
                    $compreg_step_eight.show();
                }
                break;
            case 9:
                if ($compreg_step_nine.is(":visible")){
                    if(validateSteps('.compreg_step_nine')){
                        saveStressTestInfo(step,function(){
                            updateClientCurrentStep(step,function(){
                                ShowToastMessageTrans(MSG_SUCCESS_STRESSTESTINFO,_TOAST_LONG);
                                $clsregisterstep.hide();
                                $compreg_step_ten.show();
                                updateCompletedStep(step);
                            });
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    $compreg_step_nine.show();
                    //$(".compreg-step_nine").
                    $body.scrollTop();
                }
                break;
            case 10:
                if ($compreg_step_ten.is(":visible")){
                    if(validateSteps('.compreg_step_ten')) {
                        saveSmokeInfo(function(){
                            updateClientCurrentStep(step,function(){
                                ShowToastMessageTrans(MSG_SUCCESS_SMOKEINFO,_TOAST_LONG);
                                $clsregisterstep.hide();
                                $compreg_step_eleven.show();
                                updateCompletedStep(step);
                            });
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    $compreg_step_ten.show();
                    //$('body').scrollTop();
                }
                break;
            case 11:
                if ($compreg_step_eleven.is(":visible")){
                    if(validateSteps('.compreg-step_eleven')) {
                        saveClientCurrentFeel(function(){
                            updateClientCurrentStep(step,function(){
                                ShowToastMessageTrans(MSG_SUCCESS_CLIENTCURRENTFEEL,_TOAST_LONG);
                                $clsregisterstep.hide();
                                $compreg_step_twelve.show();
                                updateCompletedStep(step);
                            });
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                     $compreg_step_eleven.show();
                    $body.scrollTop();
                }
                break;
            case 12:
                if ($compreg_step_twelve.is(":visible")){
                    if(validateSteps('.compreg_step_twelve')) {
                        saveClientSignature(step,function(){
                            updateClientCurrentStep(step,function(){
                                ShowToastMessageTrans(MSG_SUCCESS_SIGNATURE_UPDATED,_TOAST_LONG);
                                updateCompletedStep(step);
                                 _movePageTo('dashboard');
                            });
                        });
                    }
                }else{
                    $clsregisterstep.hide();
                    $compreg_step_twelve.show();
                }
                break;
        }
    }catch(e){
        ShowExceptionMessage("saveContinueCurrentStep", e);
    }
}
function updateCompletedStep(step){
    logStatus("updateCompletedStep", LOG_FUNCTIONS);
    try{
        if(step<12)  {
            registeration_step.step	=	step;
            app_tempvars('compregstep',(step + 1));
        }
    }catch(e){
        ShowExceptionMessage("updateCompletedStep", e);
    }
}
function saveContinueIncompleteCurrentStep(step){
    logStatus("saveContinueIncompleteCurrentStep", LOG_FUNCTIONS);
    try{
        //var step	=	parseInt(step);
        var $clsregisterstep = $('.clsregisterstep');
        var $compreg_step_two= $('.compreg_step_two');
        var $compreg_step_three= $('.compreg_step_three');
        var $compreg_step_four= $('.compreg_step_four');
        var $compreg_step_five= $('.compreg_step_five');
        var $compreg_step_six= $('.compreg_step_six');
        var $compreg_step_seven= $('.compreg_step_seven');
        var $compreg_step_eight= $('.compreg_step_eight');
        var $compreg_step_nine= $('.compreg_step_nine');
        var $compreg_step_ten= $('.compreg_step_ten');
        var $compreg_step_eleven= $('.compreg_step_eleven');
        var $compreg_step_twelve= $('.compreg_step_twelve');
        $clsregisterstep.hide();
        var isclubmedicalinfo = $('#isclubmedicalinfo').val();
        switch(step){
            case 1:
                $clsregisterstep.hide();
                $compreg_step_two.show();
                break;
            case 2:
                $compreg_step_two.hide();
                $compreg_step_three.show();
                break;
            case 3:
                $compreg_step_three.hide();
                $compreg_step_four.show();
                break;
            case 4:
                $compreg_step_four.hide();
                $compreg_step_five.show();
                break;
            case 5:
                $compreg_step_five.hide();
                $compreg_step_six.show();
                break;
            case 6:
                $compreg_step_six.hide();
                $compreg_step_seven.show();
                break;
            case 7:
                $compreg_step_seven.hide();
                $compreg_step_eight.show();
                break;
            case 8:
                $compreg_step_eight.hide();
                $compreg_step_nine.show();
                break;
            case 9:
                $compreg_step_nine.hide();
                $compreg_step_ten.show();
                break;
            case 10:
                $compreg_step_ten.hide();
                $compreg_step_eleven.show();
                break;
            case 11:
                $compreg_step_eleven.hide();
                $compreg_step_twelve.show();
                break;
        }
    }catch(e){
        ShowExceptionMessage("saveContinueIncompleteCurrentStep", e);
    }
}
/*
function addActivityRow(){
    var minplaceholder = GetLanguageText(PH_MIN);
    var rowcnt	=	$(".activity_row").length;
    var selectoptions	=	$("#client_activity_0").html();
    var HTML	=	'<div class="activity_row">'+
                        '<div class="input_box">'+
                            '<span id="idselectactivity_' + rowcnt + 'text" class="input_blue selectchrbox  client_ac_select_text ">'+GetLanguageText(LBL_SELECT_ACTIVITY)+'</span>'+
                            '<select err_reporton="idselectactivity_' + rowcnt + 'text" id="client_activity_' + rowcnt + '"  class="input_blue client_ac_select client_activity selectbox select_opcity req-nonzero">'+
                                selectoptions +
                            '</select>' +
                        '</div>'+
                    '<div class="two_radio_box">' +
                        '<div class="single_radio_box">' +
                            '<span class="radiobox">' +
                                '<input class="training_freq'+ rowcnt +'" type="radio" name="training_freq" value="1" row="' + rowcnt + '"/>' +
                            '</span>' +
                            '<label>'+GetLanguageText(LBL_MONTHLY)+'</label>' +
                        '</div>' +
                        '<div class="single_radio_box">' +
                            '<span class="radiobox">' +
                                '<input class="training_freq'+ rowcnt +'" type="radio" name="training_freq" value="2" row="' + rowcnt + '"/>' +
                            '</span>' +
                            '<label>'+GetLanguageText(LBL_WEEKLY)+'</label>' +
                        '</div>' +
                        '<input type="hidden" id="trainingfreq_' + rowcnt + '" value="1" />' +
                    '</div>' +
                    '<div class="input_box avgtime_box">' +
                        '<label class="transavgtime_label" >'+GetLanguageText(LBL_AVERAGE_TIME)+'</label>' +
                        '<input type="text" class="input_blue transmin_input" id="avgtime_' + rowcnt + '" placeholder="'+minplaceholder+'" /> '+
                        '<div class="clear"></div>'+
                    '</div>'+
                '</div>';
    $(".activity_row:last").after(HTML);
}*/
function saveAddressInfo(stepvalue,handleOnSuccess){
    logStatus("saveAddressInfo", LOG_FUNCTIONS);
    try{
        //var step ={};
        var client_street	=	$("#client_street").val();
        var client_number	=	$("#client_number").val();
        var client_bus		=	$("#client_bus").val();
        var client_zipcode	=	$("#client_zipcode").val();
        var client_place	=	$("#client_place").val();
        var client_country	=	$("#client_country").val();
        // var language        =   $("#language").val();
        _updateClientInformation({
            userId:app_tempvars('regclientid'),
            doorNo:client_number,
            address:client_street,
            bus:client_bus,
            zipCode:client_zipcode,
            cityId:client_place,
            nationalityId:client_country,
            clientCurrentStep:stepvalue
        },function(){
            if(handleOnSuccess){
                handleOnSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("saveAddressInfo", e);
    }
}
function savePersonalInfo(stepvalue,handleOnSuccess){
    logStatus("savePersonalInfo", LOG_FUNCTIONS);
    try{
        var client_dob			=	$("#client_dob").val();
        var client_length		=	$("#client_length").val();
        var client_weight		=	$("#client_weight").val();
        var client_profession	=	$("#client_profession").val();
        _updateClientInformation({
            userId:app_tempvars('regclientid'),
            dob:client_dob,
            height:client_length,
            weight:client_weight,
            professionId:client_profession,
            clientCurrentStep:stepvalue
        },function(){
            if(handleOnSuccess){
                handleOnSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("savePersonalInfo", e);
    }
}
/*
function saveMedicalInfo(){
    $('.compreg_step_four').hide();
    $('.compreg_step_five').show();
}*/
function saveGPInfomormation(stepvalue,handleOnSuccess){
    logStatus("Calling saveGPInfomormation", LOG_FUNCTIONS);
    try{
        var step ={};
        var client_doctor_name		=	$("#client_doctor_name").val();
        var client_doctor_street	=	$("#client_doctor_street").val();
        var client_doctor_doorno	=	$("#client_doctor_doorno").val();
        var client_doctor_bus		=	$("#client_doctor_bus").val();
        var client_doctor_zipcode	=	$("#client_doctor_zipcode").val();
        var client_doctor_place		=	$("#client_doctor_place").val();
        var client_doctor_country	=	$("#client_doctor_country").val();
        var client_doctor_phone		=	$("#client_doctor_phone").val();
        var client_doctor_email		=	$("#client_doctor_email").val();
        step['step'] = stepvalue;
        _updateClientInformation({
            userId:app_tempvars('regclientid'),
            doctorName:client_doctor_name,
            doctorDoorno:client_doctor_doorno,
            doctorStreet:client_doctor_street,
            doctorBus:client_doctor_bus,
            doctorZipcode:client_doctor_zipcode,
            doctorCountry:client_doctor_country,
            doctorPlace:client_doctor_place,
            doctorPhone:client_doctor_phone,
            doctorEmail:client_doctor_email,
            clientCurrentStep:JSON.stringify(step)
        },function(){
            if(handleOnSuccess){
                handleOnSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("saveGPInfomormation", e);
    }
}
function saveProfessionWorkType(stepvalue,handleOnSuccess){
    logStatus("Calling saveProfessionWorkType", LOG_FUNCTIONS);
    try{
        var professionType	=	$("#physicalworktype").val();
        _updateClientInformation({
            userId:app_tempvars('regclientid'),
            professionWorkType:professionType,
            clientCurrentStep:stepvalue
        },function(){
            if(handleOnSuccess){
                handleOnSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("saveGPInfomormation", e);
    }
}
function saveClientActivity(stepvalue,handleOnSuccess){
    logStatus("Calling saveClientActivity", LOG_FUNCTIONS);
    logStatus(stepvalue, LOG_DEBUG);
    try {
        //var step = {};
        var data	=	{};
        var upload	=	{};
        $(".activity_row").each(function(rowcnt,elem){
            logStatus(elem, LOG_DEBUG);
            var activityid		=	$("#client_activity_"+rowcnt).val();
            var training_freq	=	$("#trainingfreq_"+rowcnt).val();
            var avgtime			=	$("#avgtime_"+rowcnt).val();
            data[rowcnt]		=	{activity:activityid,freq:training_freq,avgtime:avgtime};
        });
        upload.userId=	app_tempvars('regclientid');
        upload.activity_freq=	JSON.stringify(data);
        _saveClientActivity(upload,function(){
            /*step['step'] = stepvalue;
            _updateClientInformation({
                userId:app_tempvars('regclientid'),
                clientCurrentStep:JSON.stringify(step)
                },function(){
                if(handleOnSuccess){
                    handleOnSuccess();
                }
            });*/
            if(handleOnSuccess) {
                handleOnSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("saveClientActivity", e);
    }
}
function saveMedicalInjury(step,handleOnSuccess){
    logStatus("Calling saveMedicalInjury", LOG_FUNCTIONS);
    logStatus(step, LOG_DEBUG);
    try {
        var injurytext	=	$("#injurytext").val();
        _updateMemberMedicalInformation({
            userId:app_tempvars('regclientid'),
            injuryDesc:injurytext
        },function(response){
            if(handleOnSuccess){
                handleOnSuccess(response);
            }
        });
    } catch(e){
        ShowExceptionMessage("saveMedicalInjury", e);
    }
}
function saveMedicationInfo(step,handleOnSuccess){
    logStatus("Calling saveMedicationInfo", LOG_FUNCTIONS);
    logStatus(step, LOG_DEBUG);
    try {
        var injurytext	=	$("#injurytext").val();
        _updateMemberMedicalInformation({
            userId:app_tempvars('regclientid'),
            injuryDesc:injurytext
        //},function(response){
        },function(){
            if(handleOnSuccess){
                handleOnSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("saveMedicationInfo", e);
    }
}
function saveStressTestInfo(step,handleOnSuccess){
    logStatus("Calling saveStressTestInfo", LOG_FUNCTIONS);
    logStatus(step, LOG_DEBUG);
    try {
        var haveStressTest	=	$("#haveStressTest").val();
        var stressTestOn	=	$("#stressTestOn").val();
        _updateMemberMedicalInformation({
            userId:app_tempvars('regclientid'),
            haveStressTest:haveStressTest,
            stressTestOn:stressTestOn
        //},function(response){
        },function(){
            if(handleOnSuccess){
                handleOnSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("saveStressTestInfo", e);
    }
}
/*PK Added Smoke Information */
function saveSmokeInfo(handleOnSuccess){
    logStatus("Calling saveSmokeInfo", LOG_FUNCTIONS);
    try {
        var haveSmoke	=	$("#haveSmoke").val();
        /*SN changed 20160202*/
        var smokeAmount	=	$("#smokeAmount").val();
        _updateMemberMedicalInformation({
            userId:app_tempvars('regclientid'),
            haveSmoke:haveSmoke,
            smokeAmount:smokeAmount
        //},function(response){
        },function(){
            if(handleOnSuccess){
                handleOnSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("saveSmokeInfo", e);
    }
}
function saveClientCurrentFeel(handleOnSuccess){
    logStatus("Calling saveClientCurrentFeel", LOG_FUNCTIONS);
    try {
        var howFeelToday	=	$("#howFeelToday").val();
        _updateMemberMedicalInformation({
            userId:app_tempvars('regclientid'),
            howFeelToday:howFeelToday
        //},function(response){
        },function(){
            if(handleOnSuccess){
                handleOnSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("saveClientCurrentFeel", e);
    }
}
function saveClientSignature(stepvalue,handleOnSuccess){
    logStatus("Calling saveClientSignature", LOG_FUNCTIONS);
    try {
        var clientSignature	=	"";
        var clientEntryDate	=	$("#entrydate").val();
        var $declaration	=	$("#acceptDeclaration");
        if($declaration.is(":checked")) {
            var signature = document.getElementById("myCanvas");
            var imagedata = signature.toDataURL();// Get the data as an image.
            if(!checkCanvasEmpty(signature)) {
                clientSignature	=	{signature:imagedata};
                var clientSignature_str	=	(!$.isEmptyObject(clientSignature)) ? JSON.stringify(clientSignature):'';
                _updateClientInformation({
                    userId:app_tempvars('regclientid'),
                    clientSignature:clientSignature_str,
                    clientEntryDate:clientEntryDate,
                    clientCurrentStep:stepvalue
                },function(){
                    if(handleOnSuccess){
                        handleOnSuccess();
                    }
                });
            } else {
                ShowToastMessageTrans(MSG_EXCEPTION_SIGNATURE_MISSING,_TOAST_LONG);
            }
        } else {
            ShowToastMessageTrans(MSG_EXCEPTION_ACCEPT_DECLARATION,_TOAST_LONG);
        }
    } catch(e){
        ShowExceptionMessage("saveClientSignature", e);
    }
}
/*
function upadteCurrentStep(onUpdateSuccess){
    if(onUpdateSuccess){
        onUpdateSuccess();
    }
}*/
/*Mk Added Signature capturing by the MyCanvas*/
/*
function mineSignature(signatureElemID,signaturebox) {
    logStatus("Calling mineSignature", LOG_FUNCTIONS);
    try {
        var element		=	document.getElementById(signatureElemID);
        var sigCapture	=	new SignatureCapture(signatureElemID);
        var sigContex = element.getContext('2d');
        if(!signaturebox) {
            signaturebox	=	window.innerWidth-50;
        }
        sigContex.canvas.width	=	signaturebox - 20;
        sigContex.canvas.height	=	100;
    } catch(e) {
        ShowExceptionMessage("mineSignature", e);
    }
}
*/
/*SN added 20160202*/
function updateClientCurrentStep(step,onUpdateSuccess){
    _updateClientInformation({
        userId:app_tempvars('regclientid'),
        clientCurrentStep:step
    },function(response){
        if(response.status=="success"){
            /*MK Update User information - on Local Storage By Key Value*/
            updateUserInfoLocal({
                personal_track:JSON.stringify({step:step})
            });
        }
        if(onUpdateSuccess){
            onUpdateSuccess();
        }
    });
}
function checkCanvasEmpty($canvas){
    logStatus("Calling checkCanvasEmpty", LOG_FUNCTIONS);
    try {
        var $blankCanvas = document.createElement('canvas');
        $blankCanvas.width = $canvas.width;
        $blankCanvas.height = $canvas.height;
        return $canvas.toDataURL() == $blankCanvas.toDataURL();
    } catch(e) {
        ShowExceptionMessage("checkCanvasEmpty", e);
    }
}
/*
function showSignaturePopup(){
    logStatus("Calling showSignaturePopup", LOG_FUNCTIONS);
    try {
        var $signature_pup = $(".signature_pup");
        var signaturebox = $signature_pup.width();
        mineSignature("myCanvas",signaturebox);
        $signature_pup.show();
        $(".pup_bg").show();
    } catch(e) {
        ShowExceptionMessage("showSignaturePopup", e);
    }
}
function updateSignatureImgSrc(){
    logStatus("Calling updateSignatureImgSrc", LOG_FUNCTIONS);
    try {
        $(".signature_pup").hide();
        $(".pup_bg").hide();
        var $signature_src = $("#signature_src");
        var signature = document.getElementById("myCanvas");
        var imagedata = signature.toDataURL();// Get the data as an image.
        $signature_src.attr("src",imagedata);
        $signature_src.show();
    } catch(e) {
        ShowExceptionMessage("updateSignatureImgSrc", e);
    }
}*/
function TranslateCompleteRegister(){
     TranslateText(".transstreet", PHTRANS, PH_STREET);
     TranslateText(".transnumber", PHTRANS, PH_NUMBER);
     TranslateText(".transbus", PHTRANS, PH_BUS);
     TranslateText(".transzipcode", PHTRANS, PH_ZIPCODE);
     TranslateText(".transplace", TEXTTRANS, TEX_PLACE);
     TranslateText(".transnext", TEXTTRANS, BTN_NEXT_ARROW);
     TranslateText(".transtypehere", PHTRANS, PH_TYPE_HERE);
     TranslateText(".transyes", TEXTTRANS, TEX_YES);
     TranslateText(".transno", TEXTTRANS, TEX_NO);
     TranslateText(".transselecttestday", PHTRANS, PH_SELECT_TEST_DAY);
     TranslateText(".transamount", PHTRANS, PH_AMOUNT);
     TranslateText(".transdateentry", PHTRANS, PH_DATE_ENTRY);
     TranslateText(".transsigfullname", TEXTTRANS, LBL_SIGNATURE_FULL_NAME);
     TranslateText(".transok", TEXTTRANS, BTN_OK);
     TranslateText(".transcancel", TEXTTRANS, BTN_CANCEL);
     TranslateText(".transselectactivity", TEXTTRANS,LBL_SELECT_ACTIVITY);
     TranslateText(".transmonthly", TEXTTRANS,LBL_MONTHLY);
     TranslateText(".transweekly", TEXTTRANS,LBL_WEEKLY);
     TranslateText(".averagetime", TEXTTRANS,LBL_AVERAGE_TIME);
     TranslateText(".transmin", PHTRANS, PH_MIN);
     TranslateText(".transnextactivity", TEXTTRANS, BTN_ADD_NEXT_ACTIVITY);
     TranslateText(".transnamegp", PHTRANS, PH_NAME_GP);
     TranslateText(".transregistermanually", TEXTTRANS, LBL_REGISTER_MANUALLY_OR_FACEBOOK);
     TranslateText(".transfacebook", TEXTTRANS, BTN_FACEBOOK);
     TranslateText(".transgetpicture", TEXTTRANS, TEX_GET_IMAGE);

     /*selva*/
     TranslateText(".translength", PHTRANS, PH_LENGTH);
     TranslateText(".transweight", PHTRANS, LBL_WEIGHT);
     TranslateText(".transhvylabor", TEXTTRANS, LBL_HEAVY_PHY_LABOUR);
     TranslateText(".translgtlabor", TEXTTRANS, LBL_LIGHT_PHYSICAL_LABOUR);
     TranslateText(".transjobs", TEXTTRANS, LBL_SEDENTARY_JOBS);
     TranslateText(".transexcellent", TEXTTRANS, LBL_EXCELLENT);
     TranslateText(".transgood", TEXTTRANS, BTN_GOOD);
     TranslateText(".transavg", TEXTTRANS, LBL_AVERAGE);
     TranslateText(".transpoor", TEXTTRANS, LBL_POOR);
     TranslateText(".transbad", TEXTTRANS, LBL_BAD);
     TranslateText(".translgtphylabour", TEXTTRANS, LBL_LIGHT_PHYSICAL_LABOUR);
     TranslateText(".transdeclare", TEXTTRANS, LBL_DECLARE_FOR_REAL_READ);


     TranslateText(".transmedicalinfo", TEXTTRANS, TXT_MEDICAL_INFO);
     TranslateText(".transtests", TEXTTRANS, TXT_TESTS);
     TranslateText(".transfeeltoday", TEXTTRANS, TXT_HOW_DO_FEEL_TODAY);
     TranslateText(".transawarness", TEXTTRANS, TXT_AWARNESS);
     TranslateText(".transphyactivity", TEXTTRANS, TXT_PHYSICAL_ACT);
     TranslateText(".transexecisereg", TEXTTRANS, TXT_HOW_EXERCISE_REG);
     TranslateText(".transavgtime", TEXTTRANS, LBL_AVG_TIME);
     TranslateText(".transinjurypblms", TEXTTRANS, TXT_INJURY_PBLMS);
     TranslateText(".transmedication", TEXTTRANS, TXT_MEDICATION);
     TranslateText(".transgpinfo", TEXTTRANS, TXT_GP_INFO);
         TranslateText(".transsmoke", TEXTTRANS, TXT_SMOKE);
     TranslateText(".transbirthday", TEXTTRANS, PH_BIRTHDAY);
         TranslateText(".transselectpro", TEXTTRANS, TXT_SELECT_PROFESSION);
     TranslateText(".transcityoption", TEXTTRANS, TXT_CITY_ZIPCODE);
     TranslateText(".transwelcome", TEXTTRANS, TXT_WELCOME);

//Aneetha
     TranslateText(".transsecurityqus", TEXTTRANS, TEX_SECURITY_QUESTIONS);
}
/*Function - Ends*/