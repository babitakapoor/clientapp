var vaucher_value = "";
var _data = {};
$(document).ready(function(){
    $('.question_icon').click(function(){
	   $('.info-popup').show();
	});
    $(".page-title h4 .one").text("VOUCHER");
	var loginInfo = getCurrentLoginInfo();
    var userEmail = loginInfo.user.email;
    var userid = loginInfo.user.user_id;
    
    $('#username_sidemenu').html("");
	$('#username_sidemenu').html(loginInfo.user.first_name);
    //TransDashboard();
    TranslateVoucherPageText();
    setvoucherlist();
});  

function setvoucherlist(){
    logStatus("setvoucherlist", LOG_FUNCTIONS);
    try{
        getvoucherlist(_data,function(response){
            var HTML = '';
            if(response.status==1)
            {

                var vlist = response.data;
                
                $.each(vlist,function(key,value){
                    var vid = value.id;
                    var vvalue = value.voucher_list;
                    HTML+="<p><input type='radio' value='"+vvalue+"' id='voucher_"+vid+"' name='voucherList' onclick='setvouvherval(this.value)'>&nbsp;"+vvalue+" credits</p> ";
                    //aaa = vid+" "+vvalue;
                });
            }
            
            $('.vouchrlst').html(HTML);
        });
    }
    catch(e){
        ShowExceptionMessage("setvoucherlist", e);
    }
    
        
}    
function setvouvherval(val){
    $('#hidn_voucher_val').val(val);
}
function CheckVoucherPoints()
{
    logStatus("CheckVoucherPoints", LOG_FUNCTIONS);
    try {
        var vaucher_name= $('#hidn_voucher_val').val();

        var ac	=	GetCredits();
        if(vaucher_name && vaucher_name!="")
        {   
            var total_v_credits = parseInt(ac.redeemed);
                  	
        	if(total_v_credits<vaucher_name)
        	{
        		alert("Not sufficient credits");
                //ShowToastMessageTrans(MSG_INSUF_CREDITS, _TOAST_LONG);
        	}
        	else
        	{
        		
                
                //ShowToastMessageTrans(MSG_SUCCESS_VOUCHER_CREDITS, _TOAST_LONG);
        	    var userCreditsData  =   {};
                var loginInfo = getCurrentLoginInfo();
                
                var selectedgrpid = 1;//getlastActiveGroupId();
                var activeFaseId = getCurrentFaseForGroupId(selectedgrpid);       
                
                userCreditsData.user_id = loginInfo.user.user_id;
                userCreditsData.v_credit = vaucher_name;
                userCreditsData.group_id = selectedgrpid;
                userCreditsData.phase_id = activeFaseId;
                //ac.inprocess;
                SendVoucherMail(userCreditsData,function(response){
                    
                    if(response.status==1)
                    {   
                       alert("A mail has been sent");

                    }
                    else
                    {
                        alert("Failed to send mail");
                    }
                });


            }
        }
        else
        {
        	alert("Select a voucher");
            //ShowToastMessageTrans(MSG_SELECT_VOUCHER, _TOAST_LONG); 
                        
    }} catch(e){
        ShowExceptionMessage("CheckVoucherPoints", e);
    }
       
}

function getvoucherlist(_data,onsucessResponse)
{   
    _data.is_reporting = BASE_URL+"reporting/index.php/Voucherlist/vlist";
    do_service(_data, _data, function (response) {
        if (onsucessResponse) {
            onsucessResponse(response);
        }
    }, 'json');
}